#!/usr/bin/env python
#
# $Id: mkheader.py,v 17918dbbbd5c 2008/09/04 05:17:53 mikek $
#
# Create a C header file from a template and a dictionary.  The template consists of
# lines of the form:
#
#   #undef FOO
#
# If the supplied dictionary contains a key "FOO", the associated value is used
# to create the following macro in the output file.
#
#   #define FOO VALUE
#
# All other lines in the template are passed through unchanged.
#
# Usage:
#
#    env = Environment(tools=['mkheader', ...])
#    env.MakeHeader('config.h.in', HEADER_DEFS={...})
#
import re
from SCons.Script import *

def build_header_file(infile, outfile, features):
    pattern = re.compile(r'^#undef (?P<feature>\w+)')
    for line in infile:
        m = pattern.search(line)
        if m:
            key = m.group('feature')
            try:
                line = '#define %s %s\n' % (key, features[key])
            except KeyError:
                pass
        outfile.write(line)

def bh_action(target, source, env):
    d = env['HEADER_DEFS']
    print 'd=', d
    build_header_file(open(str(source[0]), 'r'),
                      open(str(target[0]), 'w'),
                      d)
    return 0

def bh_string(target, source, env):
    return "building '%s' from '%s'" % (str(target[0]), str(source[0]))

def bh_emitter(target, source, env):
    Depends(target, SCons.Node.Python.Value(env['HEADER_DEFS']))
    return target, source

def generate(env):
    action = env.Action(bh_action, bh_string)
    env['BUILDERS']['MakeHeader'] = env.Builder(action=action,
                                                emitter=bh_emitter,
                                                single_source=True,
                                                src_suffix='.in')

def exists(env):
    return True

