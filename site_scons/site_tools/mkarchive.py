#
# $Id: mkarchive.py,v 17918dbbbd5c 2008/09/04 05:17:53 mikek $
#
# Create a zip archive of MLF2 "executables".
#
import zipfile
import os
from SCons.Errors import UserError

def find_files(top):
    for root, dirs, files in os.walk(top):
        names = (os.path.splitext(f) for f in files)
        for n in names:
            if n[1] in ('.run', '.scm'):
                fname = n[0]+n[1]
                yield fname, os.path.join(root, fname)

def archive_action(target, source, env):
    ar_name = str(target[0])
    z = zipfile.ZipFile(ar_name, 'w', zipfile.ZIP_DEFLATED)
    files = []
    for s in source:
        path = str(s)
        if os.path.isdir(path):
            files.extend(find_files(path))
        else:
            files.append((os.path.basename(path), path))
    if len(files) == 0:
        return 1
    seen = set()
    for base, path in files:
        if base in seen:
            raise UserError, 'Duplicate filename found, %s (%s)' % (base, path)
        seen.add(base)
        z.write(path, base)
    z.close()
    return 0

def archive_string(target, source, env):
    slist = [str(s) for s in source]
    return "building archive '%s' from %s" % (str(target[0]), str(slist))

def generate(env):
    action = env.Action(archive_action, archive_string)
    env['BUILDERS']['MakeArchive'] = env.Builder(action=action)

def exists(env):
    return True


if __name__ == '__main__':
    import sys
    files = []
    files.extend(find_files(sys.argv[1]))
    archive_action(['test.zip'], files, {})
