#!/usr/bin/env python
#
# $Id: combineparams.py,v 17918dbbbd5c 2008/09/04 05:17:53 mikek $
#
# SCons builder to combine a series of parameter-table files into
# a single XML file.
#
try:
    from xml.etree import cElementTree as ElementTree
except ImportError:
    from elementtree import cElementTree as ElementTree

def extract_elems(filename, root):
    infile = open(filename, 'r')
    tree = ElementTree.parse(infile)
    top = tree.getroot()
    for node in top:
        root.append(node)
    infile.close()
    
def cp_action(target, source, env):
    root = ElementTree.Element('ptable')
    for s in source:
        extract_elems(str(s), root)
    ElementTree.ElementTree(element=root).write(str(target[0]), encoding='utf-8')
    return 0

def cp_string(target, source, env):
    return "building parameter file '%s'" % (str(target[0]),)

def generate(env):
    action = env.Action(cp_action, cp_string)
    env['BUILDERS']['CombineParams'] = env.Builder(action=action,
                                                   src_suffix='.pt',
                                                   suffix='.mpt')

def exists(env):
    return True

