#!/usr/bin/env python
#
# $Id: findparams.py,v 17918dbbbd5c 2008/09/04 05:17:53 mikek $
#
# SCons builder to extract parameter information from an MLF2
# source file and create a parameter-table file.
#
import re
import os
try:
    from xml.etree import cElementTree as ElementTree
except ImportError:
    from elementtree import cElementTree as ElementTree

pattern = re.compile(r'add_param\("([^"]*)",\s*([^,]*),\s*&([^\)]*)')

def check(filename):
    s = open(filename, 'r').read()
    return pattern.search(s)

def scan(filename, root):
    """
    Scan a source code file looking for calls to the function
    add_param() and extract the parameter name and access type (read-only
    or read-write) and variable name. The result is a series of 'param'
    elements which are appended to the root element.

    <!ELEMENT param (path, location)>
    <!ATTLIST param name CDATA #REQUIRED
                    access CDATA #REQUIRED>
    <!ELEMENT path (PCDATA)>
    <!ELEMENT location (PCDATA)>
    
    """
    s = open(filename, 'r').read()
    base = os.path.basename(filename)
    matches = pattern.findall(s)
    for m in matches:
        name = m[0].lower()
        access = m[1]
        e = ElementTree.SubElement(root, 'param')
        e.set('name', name)
        if access.find('READ_ONLY') == -1:
            e.set('access', 'rw')
        else:
            e.set('access', 'ro')
        ElementTree.SubElement(e, 'path').text = base
        ElementTree.SubElement(e, 'location').text = m[2]
    return len(matches)

def fp_action(target, source, env):
    root = ElementTree.Element('ptable')
    n = scan(str(source[0]), root)
    ElementTree.ElementTree(element=root).write(str(target[0]), encoding='utf-8')
    return 0

def fp_string(target, source, env):
    return "building '%s' from '%s'" % (str(target[0]), str(source[0]))

def fp_emitter(target, source, env):
    valid_targets = []
    valid_sources = []
    if check(str(source[0])):
        valid_targets = target
        valid_sources = source
    return valid_targets, valid_sources
        
def generate(env):
    action = env.Action(fp_action, fp_string)
    env['BUILDERS']['FindParams'] = env.Builder(action=action,
                                                emitter=fp_emitter,
                                                single_source=True,
                                                suffix='.pt',
                                                src_suffix='.c')

def exists(env):
    return True
