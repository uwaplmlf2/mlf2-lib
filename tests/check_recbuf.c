// Unit tests for recbuf.c using the Check framework
#include <stdlib.h>
#include <check.h>
#include "../libmlf2/recbuf.h"

START_TEST(test_recbuf_init)
{
    RecBuf      rb;
    char        *buf = "1,2,3,4";
    char        *fields[10];

    recbuf_init(&rb, buf, (int)strlen(buf), fields, 10);
    ck_assert_int_eq(rb.nfields, 0);
}
END_TEST

START_TEST(test_nmea_split)
{
    RecBuf      rb;
    char        buf[64];
    char        *fields[10];

    recbuf_init(&rb, buf, (int)sizeof(buf)-1, fields, 10);
    strncpy(buf, "$PRVAT,00.115,M,0010.073,dBar*39", 63);
    recbuf_split(&rb, ',');
    ck_assert_int_eq(rb.nfields, 5);
    ck_assert_str_eq(rb.field[1], "00.115");
    ck_assert_str_eq(rb.field[3], "0010.073");
}
END_TEST

START_TEST(test_field_limit)
{
    RecBuf      rb;
    char        buf[64];
    char        *fields[3];

    recbuf_init(&rb, buf, (int)sizeof(buf)-1, fields, 3);
    strncpy(buf, "$PRVAT,00.115,M,0010.073,dBar*39", 63);
    recbuf_split(&rb, ',');
    ck_assert_int_eq(rb.nfields, 3);
    ck_assert_str_eq(rb.field[1], "00.115");
    ck_assert_str_eq(rb.field[2], "M,0010.073,dBar*39");
}
END_TEST

Suite * recbuf_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("Recbuf");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_recbuf_init);
    tcase_add_test(tc_core, test_nmea_split);
    tcase_add_test(tc_core, test_field_limit);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = recbuf_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
