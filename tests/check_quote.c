// Unit tests for quote.c using the Check framework
#include <stdlib.h>
#include <check.h>
#include "../libmlf2/quote.h"

START_TEST(test_quote1)
{
    char        qbuf[32];
    size_t      n;
    const char  *input = "Line one\nLine two\n";
    const char  *expect = "\"Line one\\nLine two\\n\"";

    n = quote_string(input, qbuf, sizeof(qbuf));
    ck_assert_int_eq(n, 22);
    ck_assert_str_eq(qbuf, expect);
}
END_TEST

START_TEST(test_quote_short)
{
    char        qbuf[20];
    size_t      n;
    const char  *input = "Line one\nLine two\n";
    const char  *expect = "\"Line one\\nLin...\"";

    quote_string(input, qbuf, sizeof(qbuf));
    ck_assert_str_eq(qbuf, expect);
}
END_TEST

Suite * quote_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("Quote");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_quote1);
    tcase_add_test(tc_core, test_quote_short);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = quote_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
