/*
** $Id: tsmlf2.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Interface MLF2 library functions to TinyScheme.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <userio.h>
#include "pr.h"
#include "ctd.h"
#include "alt.h"
#include "flr.h"
#include "adcp.h"
#include "inertial.h"
#include "gps.h"
#include "par.h"
#include "noise.h"
#include "motor.h"
#include "scheme.h"
#include "ts.h"
#include "log.h"

typedef enum {DEV_PR=0, DEV_PRF, DEV_CTD, DEV_ALT, DEV_FLR, DEV_ADCP,
	      DEV_INERTIAL, DEV_GPS, DEV_NOISE, DEV_PAR} devtype_t;

static struct dev {
    const char	*name;
    devtype_t	type;
} devtab[] = {
    { "pressure",	DEV_PR },
    { "fpressure",	DEV_PRF },
    { "ctd",		DEV_CTD },
    { "altimeter",	DEV_ALT },
    { "fluorometer",	DEV_FLR },
    { "adcp",		DEV_ADCP },
    { "inertial",	DEV_INERTIAL },
    { "gps",		DEV_GPS },
    { "noise",		DEV_NOISE },
    { "par",		DEV_PAR },
    { NULL,		-1 }
};

static unsigned long dev_opened = 0;

static __inline__ int lookup_type(const char *name)
{
    struct dev	*devp = &devtab[0];
    while(devp->name != NULL)
    {
	if(!strcmp(name, devp->name))
	    return devp->type;
	devp++;
    }

    return -1;
}

/*
 * dev_open - initialize a device and return the type code.
 * @sc: pointer to interpreter data structure
 * @args: argument list
 *
 * Open a device for further interaction.  Typical device interaction
 * would proceed as follows:
 *
 * (letrec ((d (dev-open "gps"))
 *          (wait (lambda ()
 *                  (if (not (dev-ready? d)) (wait) t))))
 *   (wait)
 */
static pointer
dev_open(scheme *sc, pointer args)
{
    int		type, state;
    char	*name;
    struct dev	*devp;
    
    if(args == sc->NIL || !is_string(ARG1(args)))
	return sc->NIL;
    name = string_value(ARG1(args));

    type = lookup_type(name);
    if((dev_opened & (1U << type)))
	return mk_integer(sc, (long)type);

    state = 0;
    switch(type)
    {
	case DEV_PR:
	case DEV_PRF:
	    state = pr_init();
	    break;	    
	case DEV_CTD:
	    state = ctd_init(CTD_A|CTD_B);
	    break;
	case DEV_ALT:
	    state = alt_init();
	    break;
	case DEV_FLR:
	    state = flr_init();
	    break;
	case DEV_ADCP:
	    state = adcp_init();
	    break;
	case DEV_INERTIAL:
	    state = inertial_init();
	    break;
	case DEV_GPS:
	    state = gps_init();
	    break;
	case DEV_NOISE:
	    noise_init_tables();
	    state = 1;
	    break;
	case DEV_PAR:
	    state = par_init();
	    break;
    }

    if(state)
    {
	dev_opened |= (1U << type);
	return mk_integer(sc, (long)type);
    }
    
    return sc->NIL;
}

static pointer
dev_ready(scheme *sc, pointer args)
{
    int		type, state;
    
    if(args == sc->NIL || !is_integer(ARG1(args)))
	return sc->NIL;
    type = ivalue(ARG1(args));
    if(!(dev_opened & (1U << type)))
	return sc->T;
    
    state = 0;
    switch(type)
    {
	case DEV_PR:
	case DEV_PRF:
	    state = pr_dev_ready();
	    break;
	case DEV_CTD:
	    state = ctd_dev_ready();
	    break;
	case DEV_ALT:
	    state = alt_dev_ready();
	    break;
	case DEV_FLR:
	    state = flr_dev_ready();
	    break;
	case DEV_ADCP:
	    state = adcp_dev_ready();
	    break;
	case DEV_INERTIAL:
	    state = inertial_dev_ready();
	    break;
	case DEV_GPS:
	    state = gps_dev_ready();
	    break;
	case DEV_NOISE:
	    state = 1;
	    break;
	case DEV_PAR:
	    state = par_dev_ready();
	    break;
    }
    
    return state ? sc->T : sc->F;
}

static pointer
dev_read(scheme *sc, pointer args)
{
    int			type, i;
    long		pr;
    pointer		p1, p2, list;
    char		*psd;
    double		min;
    CTDdata		ctd;
    InertialData	id;
    GPSdata		gps;
    AdcpData		adcp;
    
    if(args == sc->NIL || !is_integer(ARG1(args)))
	return sc->NIL;
    type = ivalue(ARG1(args));
    if(!(dev_opened & (1U << type)))
	return sc->NIL;
    
    switch(type)
    {
	case DEV_PR:
	    pr = pr_read_data();
	    if(HAVE_ARG2(sc, args))
		return mk_real(sc, pr_mv_to_psi(pr_counts_to_mv(pr))/1.47);
	    else
		return mk_integer(sc, pr);
	    break;
	case DEV_PRF:
	    pr = pr_read_data_filtered(7);
	    if(HAVE_ARG2(sc, args))
		return mk_real(sc, pr_mv_to_psi(pr_counts_to_mv(pr))/1.47);
	    else
		return mk_integer(sc, pr);
	case DEV_CTD:
	    if(HAVE_ARG2(sc, args))
		pr = (long)rvalue(ARG2(args));
	    else
		pr = 0;
	    ctd_read_data(pr*10, &ctd);
	    p1 = cons(sc, mk_real(sc, ctd.t[0]),
		      cons(sc, mk_real(sc, ctd.s[0]), sc->NIL));
	    p2 = cons(sc, mk_real(sc, ctd.t[1]),
		      cons(sc, mk_real(sc, ctd.s[1]), sc->NIL));
	    return cons(sc, p1,	cons(sc, p2, sc->NIL));
	case DEV_ALT:
	    return mk_integer(sc, (long)alt_read_data());
	case DEV_FLR:
	    return mk_integer(sc, (long)flr_read_data());
	case DEV_ADCP:
	    if(!adcp_set_params(1000L, 1, (char**)0) ||
	       !adcp_sample_start() || !adcp_read_data(&adcp, 1, 1))
		return sc->NIL;
	    list = sc->NIL;
	    for(i = 0;i < ADCP_PINGS;i++)
	    {
		p1 = sc->NIL;
		p2 = cons(sc, mk_integer(sc, adcp.corr[i].x),
			  cons(sc, mk_integer(sc, adcp.corr[i].y),
			       cons(sc, mk_integer(sc, adcp.corr[i].z),
				    sc->NIL)));
		p1 = cons(sc, p2, p1);
		p2 = cons(sc, mk_integer(sc, adcp.amp[i].x),
			  cons(sc, mk_integer(sc, adcp.amp[i].y),
			       cons(sc, mk_integer(sc, adcp.amp[i].z),
				    sc->NIL)));
		p1 = cons(sc, p2, p1);
		p2 = cons(sc, mk_integer(sc, adcp.vel[i].x),
			  cons(sc, mk_integer(sc, adcp.vel[i].y),
			       cons(sc, mk_integer(sc, adcp.vel[i].z),
				    sc->NIL)));
		p1 = cons(sc, p2, p1);
		list = cons(sc, p1, list);
	    }
	    
	    p2 = cons(sc, mk_integer(sc, adcp.mag.x),
		      cons(sc, mk_integer(sc, adcp.mag.y),
			   cons(sc, mk_integer(sc, adcp.mag.z),
				sc->NIL)));
	    list = cons(sc, p2, list);
	    p2 = cons(sc, mk_integer(sc, adcp.pitch),
		      cons(sc, mk_integer(sc, adcp.roll),
			   cons(sc, mk_integer(sc, adcp.heading),
				sc->NIL)));
	    return cons(sc, p2, list);
	case DEV_INERTIAL:
	    if(!inertial_read_data(&id))
		return sc->NIL;
	    list = cons(sc, mk_integer(sc, id.timestamp), sc->NIL);
	    p1 = cons(sc, mk_integer(sc, id.accel.x),
		      cons(sc, mk_integer(sc, id.accel.y),
			   cons(sc, mk_integer(sc, id.accel.z),
				sc->NIL)));
	    list = cons(sc, p1, list);
	    p1 = cons(sc, mk_integer(sc, id.ang_rate.x),
		      cons(sc, mk_integer(sc, id.ang_rate.y),
			   cons(sc, mk_integer(sc, id.ang_rate.z),
				sc->NIL)));
	    return cons(sc, p1, list);
	case DEV_GPS:
	    if(!gps_read_data(&gps))
		return sc->NIL;
	    if(gps.lat.dir == 'S')
		gps.lat.deg = -gps.lat.deg;
	    if(gps.lon.dir == 'W')
		gps.lon.deg = -gps.lon.deg;

	    list = cons(sc, mk_integer(sc, gps.satellites), sc->NIL);

	    min = gps.lon.min + (double)gps.lon.frac*1.0e-4;
	    p1 = cons(sc, mk_integer(sc, gps.lon.deg),
		      cons(sc, mk_real(sc, min), sc->NIL));
	    list = cons(sc, p1, list);

	    min = gps.lat.min + (double)gps.lat.frac*1.0e-4;
	    p1 = cons(sc, mk_integer(sc, gps.lat.deg),
		      cons(sc, mk_real(sc, min), sc->NIL));
	    return cons(sc, p1, list);
	case DEV_NOISE:
	    if((psd = malloc(NR_BINS*2)) == NULL)
		return sc->NIL;
	    noise_sample();
	    noise_get_psd(psd);
	    noise_clear_psd();
	    
	    p1 = sc->NIL;
	    p2 = sc->NIL;
	    for(i = NR_BINS;i > 0;i--)
	    {
		p1 = cons(sc, mk_integer(sc, psd[2*i-2]), p1);
		p2 = cons(sc, mk_integer(sc, psd[2*i-1]), p2);
	    }
	    free(psd);
	    
	    return cons(sc, p1, cons(sc, p2, sc->NIL));
	case DEV_PAR:
	    return mk_integer(sc, (long)par_read_data());
    }
    
    return sc->NIL;
}


static pointer
dev_close(scheme *sc, pointer args)
{
    int		type;
    GPSdata	gps;
    
    if(args == sc->NIL || !is_integer(ARG1(args)))
	return sc->NIL;
    type = ivalue(ARG1(args));
    if(!(dev_opened & (1U << type)))
	return sc->F;
    
    switch(type)
    {
	case DEV_PR:
	case DEV_PRF:
	    pr_shutdown();
	    break;
	case DEV_CTD:
	    ctd_shutdown();
	    break;
	case DEV_ALT:
	    alt_shutdown();
	    break;
	case DEV_FLR:
	    flr_shutdown();
	    break;
	case DEV_ADCP:
	    adcp_shutdown();
	    break;
	case DEV_INERTIAL:
	    inertial_shutdown();
	    break;
	case DEV_GPS:
	    if(gps_read_data(&gps) && gps.satellites > 0)
	    {
		gps_set_clock();
		log_event("Setting clock from GPS\n");
	    }
	    gps_shutdown();
	    break;
	case DEV_NOISE:
	    break;
	case DEV_PAR:
	    par_shutdown();
	    break;
	default:
	    return sc->F;
    }

    dev_opened &= ~(1U << type);
    
    return sc->T;
}

static pointer
piston_home(scheme *sc, pointer args)
{
    long	timeout;
    
    if(args == sc->NIL)
	timeout = 900;
    else
	timeout = ivalue(ARG1(args));
    return mk_integer(sc, motor_home(timeout));
}

static pointer
piston_location(scheme *sc, pointer args)
{
    return mk_integer(sc, motor_pos());
}

static pointer
piston_move(scheme *sc, pointer args)
{
    long	timeout = 0, error = 100, x;
    
    if(args == sc->NIL)
	return sc->NIL;

    x = ivalue(ARG1(args));
    
    if(HAVE_ARG2(sc, args))
    {
	timeout = ivalue(ARG2(args));
	if(HAVE_ARG3(sc, args))
	    error = ivalue(ARG3(args));
    }
   
    return mk_integer(sc, motor_move(motor_pos()+x, error, timeout, 0));
}

static pointer
counts2cm(scheme *sc, pointer args)
{
    if(args == sc->NIL)
	return sc->NIL;
    return mk_real(sc, counts_to_cm(ivalue(ARG1(args))));
}

static pointer
cm2counts(scheme *sc, pointer args)
{
    if(args == sc->NIL)
	return sc->NIL;
    return mk_integer(sc, cm_to_counts(rvalue(ARG1(args))));
}

void
init_tsmlf2(scheme *sc, int safemode)
{
    add_to_ffi(sc, dev_open, "dev-open");
    add_to_ffi(sc, dev_ready, "dev-ready?");
    add_to_ffi(sc, dev_read, "dev-read");
    add_to_ffi(sc, dev_close, "dev-close");

    add_to_ffi(sc, piston_home, "piston-home");
    add_to_ffi(sc, piston_move, "piston-move");
    add_to_ffi(sc, piston_location, "piston-loc");
    add_to_ffi(sc, counts2cm, "to-cm");
    add_to_ffi(sc, cm2counts, "from-cm");
}
