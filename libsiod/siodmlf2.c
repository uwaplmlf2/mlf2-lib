/*
** siod interface to mlf2 library
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#ifdef __TT8__
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <userio.h>
#endif

#include "siod.h"
#include "siodp.h"
#include "atod.h"
#ifdef USE_FFT
#include "ifft.h"
#endif
#include "events.h"
#include "ioports.h"
#include "power.h"
#include "gps.h"
#include "newctd.h"
#include "par.h"
#include "i490.h"
#include "inertial.h"
#include "motor.h"
#include "nvram.h"
#include "adcp.h"
#include "adv.h"
#include "rh.h"
#include "flr.h"
#include "eco.h"
#include "ctdo.h"
#include "gtd.h"
#include "therm.h"
#include "argos.h"
#include "log.h"
#include "serial.h"
#include "internalpr.h"
#include "rafos.h"
#include "xponder.h"
#include "camera.h"
#include "optode.h"
#include "cstar.h"
#include "flntu.h"
#include "isus.h"
#include "anr.h"
#include "sbe41p.h"
#include "tchain.h"
#include "sbe41apl.h"
#include "vpalt.h"
#include "sbe63.h"

unsigned short crc(char *block, int n);

#define DEV_GPS         0L
#define DEV_ADCP        1L
#define DEV_INERTIAL    2L
#define DEV_ALTIMETER   3L
#define DEV_CTD         4L
#define DEV_PR          5L
#define DEV_PAR         6L
#define DEV_RH          7L
#define DEV_PRF         8L
#define DEV_FLR         9L
#define DEV_ECO         10L
#define DEV_CTDO        11L
#define DEV_I490        12L
#define DEV_GTD         13L
#define DEV_ADV         14L
#define DEV_THERM       15L
#define DEV_IPR         16L
#define DEV_OPTODE      17L
#define DEV_CSTAR       18L
#define DEV_FLNTU       19L
#define DEV_ISUS        20L
#define DEV_ANR         21L
#define DEV_SBECTD      22L
#define DEV_TCHAIN      23L
#define DEV_SBE63       24L

static void
init_mlf2_vars(void)
{
    setvar(cintern("*mlf2lib-version*"),
        cintern("$Id: siodmlf2.c,v a810c05338bd 2008/06/02 21:13:00 mikek $"),
        NIL);
    setvar(cintern("/dev/gps"), inumcons(DEV_GPS), NIL);
    setvar(cintern("/dev/adcp"), inumcons(DEV_ADCP), NIL);
    setvar(cintern("/dev/inertial"), inumcons(DEV_INERTIAL), NIL);
    setvar(cintern("/dev/alt"), inumcons(DEV_ALTIMETER), NIL);
    setvar(cintern("/dev/ctd"), inumcons(DEV_CTD), NIL);
    setvar(cintern("/dev/pr"), inumcons(DEV_PR), NIL);
    setvar(cintern("/dev/par"), inumcons(DEV_PAR), NIL);
    setvar(cintern("/dev/rh"), inumcons(DEV_RH), NIL);
    setvar(cintern("/dev/prf"), inumcons(DEV_PRF), NIL);
    setvar(cintern("/dev/flr"), inumcons(DEV_FLR), NIL);
    setvar(cintern("/dev/eco"), inumcons(DEV_ECO), NIL);
    setvar(cintern("CTD-A"), inumcons(0), NIL);
    setvar(cintern("CTD-B"), inumcons(1), NIL);
    setvar(cintern("/dev/ctdo"), inumcons(DEV_CTDO), NIL);
    setvar(cintern("/dev/i490"), inumcons(DEV_I490), NIL);
    setvar(cintern("/dev/gtd"), inumcons(DEV_GTD), NIL);
    setvar(cintern("/dev/adv"), inumcons(DEV_ADV), NIL);
    setvar(cintern("/dev/therm"), inumcons(DEV_THERM), NIL);
    setvar(cintern("/dev/ipr"), inumcons(DEV_IPR), NIL);
    setvar(cintern("res320x240"), inumcons(RES_320x240), NIL);
    setvar(cintern("res640x480"), inumcons(RES_640x480), NIL);
    setvar(cintern("/dev/optode"), inumcons(DEV_OPTODE), NIL);
    setvar(cintern("/dev/cstar"), inumcons(DEV_CSTAR), NIL);
    setvar(cintern("/dev/flntu"), inumcons(DEV_FLNTU), NIL);
    setvar(cintern("/dev/isus"), inumcons(DEV_ISUS), NIL);
    setvar(cintern("/dev/anr"), inumcons(DEV_ANR), NIL);
    setvar(cintern("/dev/sbe41p"), inumcons(DEV_SBECTD), NIL);
    setvar(cintern("/dev/tchain"), inumcons(DEV_TCHAIN), NIL);
    setvar(cintern("/dev/sbe63"), inumcons(DEV_SBE63), NIL);
}

LISP
lad_init(void)
{
    atod_init();
    return a_true_value();
}

LISP
lad_shutdown(void)
{
    atod_shutdown();
    return a_true_value();
}

LISP
lad_read(LISP c)
{
    return inumcons((long)atod_read((int)get_c_long(c)));
}

LISP
lad_read_buf(LISP c, LISP p, LISP buf)
{
    int         chan, period;
    long        n;

    chan = get_c_long(c);
    period = get_c_long(p);
    if(NTYPEP(buf, tc_short_array))
        return(err("Bad arg 3 to adburst", buf));

    n = buf->storage_as.short_array.dim;
    period = atod_read_buf(chan, 0, period, n,
                           buf->storage_as.short_array.data);

    return inumcons((long)period);
}

#ifdef USE_FFT
LISP
ifft_coeff(LISP n, LISP flag)
{
    LISP        w;
    int         dir = get_c_long(flag);

    w = arcons((long)tc_short_array, get_c_long(n), 0L);
    make_coeff(w->storage_as.short_array.data,
               (short)w->storage_as.short_array.dim,
               (dir == 1) ? FFT_FORWARD : FFT_INVERSE);
    return w;
}

LISP
lifft(LISP data, LISP w)
{
    LISP                trans;
    register long       *xt;
    register short      *x;
    register int        i, n;

    if(NTYPEP(data, tc_short_array))
        return(err("Bad arg 1 to ifft", data));
    if(NTYPEP(w, tc_short_array))
        return(err("Bad arg 2 to ifft", w));

    n     = data->storage_as.short_array.dim;
    trans = arcons((long)tc_long_array, n*2L, 0L);
    xt    = trans->storage_as.long_array.data;
    x     = data->storage_as.short_array.data;

    i = n;
    while(i--)
    {
        *xt++ = *x++;
        *xt++ = 0L;
    }

    ifft(trans->storage_as.long_array.data, n, w->storage_as.short_array.data);

    return trans;
}

LISP
unbias(LISP a)
{
    register long       n, ix;
    double              x;
    register int        i;

    switch(TYPE(a))
    {
        case tc_short_array:
            n = a->storage_as.short_array.dim;
            ix = 0L;
            for(i = 0;i < n;i++)
                ix += a->storage_as.short_array.data[i];
            ix /= n;
            for(i = 0;i < n;i++)
                a->storage_as.short_array.data[i] -= ix;
            break;
        case tc_float_array:
            n = a->storage_as.float_array.dim;
            x = 0.;
            for(i = 0;i < n;i++)
                x += a->storage_as.float_array.data[i];
            x /= n;
            for(i = 0;i < n;i++)
                a->storage_as.float_array.data[i] -= x;
            break;
        default:
            return(err("arg must be short or float array", a));
    }
    return a_true_value();
}
#endif


static int
get_port_id(const char *name)
{
    int         id;

    id = (tolower(name[0]) - 'a') + IO_A;
    if(tolower(name[1]) == 'i')
        id |= REAL_INPUT;
    return id;
}

LISP
ioport_read(LISP p)
{
    int         port;

    if(NSYMBOLP(p))
        err("Expected a symbol argument", p);
    port = get_port_id(PNAME(p));
    return inumcons((long)iop_read(port));
}

LISP
ioport_write(LISP p, LISP x)
{
    int         port;

    if(NSYMBOLP(p))
        err("Expected a symbol argument", p);
    port = get_port_id(PNAME(p));
    iop_write(port, (unsigned char)get_c_long(x));
    return a_true_value();
}

LISP
ioport_set(LISP p, LISP x)
{
    int         port;

    if(NSYMBOLP(p))
        err("Expected a symbol argument", p);
    port = get_port_id(PNAME(p));
    iop_set(port, (unsigned char)get_c_long(x));
    return a_true_value();
}

LISP
ioport_clear(LISP p, LISP x)
{
    int         port;

    if(NSYMBOLP(p))
        err("Expected a symbol argument", p);
    port = get_port_id(PNAME(p));
    iop_clear(port, (unsigned char)get_c_long(x));
    return a_true_value();
}


static LISP
display_menu(LISP title, LISP entries, LISP back)
{
    register char       *str;
    int                 n;
    register int        i;
    LISP                e;


    str = get_c_string(title);
    n = strlen(str);
    printf("\n    %s    \n", str);

    n += 8;
    for(i = 0;i < n;i++)
        fputc('-', stdout);
    fputc('\n', stdout);

    /*
    ** Walk through the 'entries' list.  Each element of 'entries' is
    ** itself a list.  The first element of each sublist is either a
    ** string or a function which returns a string -- this becomes
    ** the menu-item label.  The second element is the function which
    ** is evaluated when the menu-item is selected (not used here).
    */
    i = 1;
    while(NNULLP(entries))
    {
        e = CAR(entries);
        if(TYPE(CAR(e)) == tc_string)
            str = get_c_string(CAR(e));
        else
            str = get_c_string(lapply(CAR(e), NIL));

        printf("  %d) %s%s", i, str,
                (CONSP(CAR(CDR(e))) ? " >>\n" : "\n"));
        entries = CDR(entries);
        i++;
    }
    printf("  %d) %s\n", i,
                   (NNULLP(back) ? get_c_string(back) : "QUIT"));
    return a_true_value();
}

static char iline[80];


LISP
get_integer(LISP prompt, LISP def)
{
    long        n;

    if(NNULLP(def))
    {
        n = get_c_long(def);
        printf("\n%s [%ld] ", get_c_string(prompt), n);
        fflush(stdout);
        fgets(iline, (int)sizeof(iline), stdin);
        if(iline[0] != '\n' && sscanf(iline, "%ld", &n) != 1)
            return NIL;
    }
    else
    {
        printf("\n%s ", get_c_string(prompt));
        fflush(stdout);
        fgets(iline, (int)sizeof(iline), stdin);
        if(sscanf(iline, "%ld", &n) != 1)
            return NIL;
    }

    return inumcons(n);
}

LISP
get_float(LISP prompt, LISP def)
{
    double      n;

    if(NNULLP(def))
    {
        n = get_c_double(def);
        printf("\n%s [%g] ", get_c_string(prompt), n);
        fflush(stdout);
        fgets(iline, (int)sizeof(iline), stdin);
        if(iline[0] != '\n' && sscanf(iline, "%lf", &n) != 1)
            return NIL;
    }
    else
    {
        printf("\n%s ", get_c_string(prompt));
        fflush(stdout);
        fgets(iline, (int)sizeof(iline), stdin);
        if(sscanf(iline, "%lf", &n) != 1)
            return NIL;
    }

    return flocons(n);
}

LISP
get_string(LISP prompt, LISP def)
{
    register char       *p, *q;
    int                 n;

    printf("\n%s [%s]", get_c_string(prompt),
           NNULLP(def) ? get_c_string(def) : "");
    fflush(stdout);
    fgets(iline, (int)sizeof(iline), stdin);
    if(iline[0] == '\n')
        return NNULLP(def) ? def : NIL;

    n = strlen(iline);

    /* Trim leading white space */
    for(p = iline;isspace(*p);p++)
        ;

    /* Trim trailing white space */
    for(q = &iline[n-1];q > p && isspace(*q);q--)
        ;

    return strcons((long)(q - p + 1), p);
}

/*************************** SENSOR INTERFACES *************************/
LISP
dev_init(LISP dev, LISP arg)
{
    register long       i = get_c_long(dev);
    int                 r = 0;

    switch(i)
    {
        case DEV_GPS:
            if((r = gps_init()) == 1)
            {
                GPSdata         gd;
                if(gps_read_data(&gd) && gd.satellites > 0)
                    gps_set_clock();
            }
            break;
        case DEV_ALTIMETER:
            r = vpalt_init();
            break;
        case DEV_PR:
        case DEV_PRF:
            r = 1;
            break;
        case DEV_INERTIAL:
            r = inertial_init();
            break;
        case DEV_CTD:
            if(NULLP(arg))
                r = sbe41apl_init(0);
            else
                r = sbe41apl_init(get_c_long(arg));
            break;
        case DEV_SBECTD:
            if(NULLP(arg))
                r = sbe41p_init(0);
            else
                r = sbe41p_init(get_c_long(arg));
            break;
        case DEV_SBE63:
            if(NULLP(arg))
                r = sbe63_init(0);
            else
                r = sbe63_init(get_c_long(arg));
            break;
        case DEV_TCHAIN:
            if(NULLP(arg))
                r = tchain_init(0);
            else
                r = tchain_init(get_c_long(arg));
            break;
        case DEV_PAR:
            r = par_init();
            break;
        case DEV_I490:
            r = i490_init();
            break;
        case DEV_RH:
            r = rh_init();
            break;
        case DEV_ADCP:
            r = adcp_init();
            break;
        case DEV_ADV:
            r = adv_init();
            break;
        case DEV_FLR:
            r = flr_init();
            break;
        case DEV_ECO:
            r = eco_init();
            break;
        case DEV_CTDO:
            r = ctdo_init();
            break;
        case DEV_GTD:
            r = gtd_init();
            break;
        case DEV_THERM:
            r = therm_init();
            break;
        case DEV_IPR:
            r = ipr_init();
            break;
        case DEV_OPTODE:
            r = optode_init();
            break;
        case DEV_CSTAR:
            r = cstar_init();
            break;
        case DEV_FLNTU:
            r = flntu_init();
            break;
        case DEV_ISUS:
            r = isus_init();
            break;
        case DEV_ANR:
            r = anr_init();
            break;
    }

    return (r > 0) ? a_true_value() : NIL;
}

LISP
dev_ready(LISP dev, LISP arg)
{
    register long       i = get_c_long(dev);
    int                 r = 1;
    int                 opt = NNULLP(arg) ? get_c_long(arg) : 0;

    switch(i)
    {
        case DEV_GPS:
            r = gps_dev_ready();
            break;
        case DEV_ALTIMETER:
            r = vpalt_dev_ready();
            break;
        case DEV_PR:
        case DEV_PRF:
            r = 1;
            break;
        case DEV_INERTIAL:
            r = inertial_dev_ready();
            break;
        case DEV_CTD:
            r = sbe41apl_dev_ready(opt);
            break;
        case DEV_SBECTD:
            r = 1;
            break;
        case DEV_SBE63:
            r = sbe63_dev_ready(opt);
            break;
        case DEV_TCHAIN:
            r = tchain_dev_ready(opt);
            break;
        case DEV_PAR:
            r = par_dev_ready();
            break;
        case DEV_I490:
            r = i490_dev_ready();
            break;
        case DEV_RH:
            r = rh_dev_ready();
            break;
        case DEV_ADCP:
            r = adcp_dev_ready();
            break;
        case DEV_ADV:
            r = adv_dev_ready();
            break;
        case DEV_FLR:
            r = flr_dev_ready();
            break;
        case DEV_ECO:
            r = eco_dev_ready();
            break;
        case DEV_CTDO:
            r = ctdo_dev_ready();
            break;
        case DEV_GTD:
            r = gtd_dev_ready();
            break;
        case DEV_THERM:
            r = therm_dev_ready();
            break;
        case DEV_IPR:
            r = ipr_dev_ready();
            break;
        case DEV_OPTODE:
            r = optode_dev_ready();
            break;
        case DEV_CSTAR:
            r = cstar_dev_ready();
            break;
        case DEV_FLNTU:
            r = flntu_dev_ready();
            break;
        case DEV_ISUS:
            r = isus_dev_ready();
            break;
        case DEV_ANR:
            r = anr_dev_ready();
            break;
    }

    return (r > 0) ? a_true_value() : NIL;
}

LISP
dev_close(LISP dev, LISP arg)
{
    GPSdata             gd;
    register long       i = get_c_long(dev);
    int                 opt = NNULLP(arg) ? get_c_long(arg) : 0;

    switch(i)
    {
        case DEV_GPS:
            if(gps_read_data(&gd) && gd.satellites > 0)
                gps_set_clock();
            gps_shutdown();
            break;
        case DEV_ALTIMETER:
            vpalt_shutdown();
            break;
        case DEV_PR:
        case DEV_PRF:
            break;
        case DEV_INERTIAL:
            inertial_shutdown();
            break;
        case DEV_CTD:
            sbe41apl_shutdown(opt);
            break;
        case DEV_SBECTD:
            sbe41p_shutdown(opt);
            break;
        case DEV_SBE63:
            sbe63_shutdown(opt);
            break;
        case DEV_TCHAIN:
            tchain_shutdown(opt);
            break;
        case DEV_PAR:
            par_shutdown();
            break;
        case DEV_I490:
            i490_shutdown();
            break;
        case DEV_RH:
            rh_shutdown();
            break;
        case DEV_ADCP:
            adcp_shutdown();
            break;
        case DEV_ADV:
            adv_shutdown();
            break;
        case DEV_FLR:
            flr_shutdown();
            break;
        case DEV_ECO:
            eco_shutdown();
            break;
        case DEV_CTDO:
            ctdo_shutdown();
            break;
        case DEV_GTD:
            gtd_shutdown();
            break;
        case DEV_THERM:
            therm_shutdown();
            break;
        case DEV_IPR:
            ipr_shutdown();
            break;
        case DEV_OPTODE:
            optode_shutdown();
            break;
        case DEV_CSTAR:
            cstar_shutdown();
            break;
        case DEV_FLNTU:
            flntu_close_shutter();
            flntu_shutdown();
            break;
        case DEV_ISUS:
            isus_shutdown();
            break;
        case DEV_ANR:
            anr_shutdown();
            break;
    }

    return a_true_value();
}

LISP
dev_test(LISP dev, LISP arg)
{
    long        i = get_c_long(dev), r = 1;
    int         opt = NNULLP(arg) ? get_c_long(arg) : 0;

    switch(i)
    {
        case DEV_CTD:
            sbe41apl_test(opt);
            break;
        case DEV_SBECTD:
            sbe41p_test(opt);
            break;
        case DEV_SBE63:
            sbe63_test(opt);
            break;
        case DEV_TCHAIN:
            tchain_test(opt);
            break;
        case DEV_ADCP:
            adcp_test();
            break;
        case DEV_ADV:
            if(!adv_set_clock())
                adv_set_clock();
            adv_test();
            break;
        case DEV_CTDO:
            ctdo_test();
            break;
        case DEV_THERM:
            therm_test();
            break;
        case DEV_OPTODE:
            optode_test();
            break;
        case DEV_CSTAR:
            cstar_test();
            break;
        case DEV_FLNTU:
            flntu_test();
            break;
        case DEV_ISUS:
            isus_test();
            break;
        case DEV_ANR:
            anr_test();
            break;
        case DEV_GTD:
            gtd_test();
            break;
        default:
            r = 0;
            break;
    }

    return (r > 0) ? a_true_value() : NIL;
}

static LISP
build_list(float *x, int n, LISP accum)
{
    if(n == 0)
        return accum;
    else
    {
        accum = cons(flocons(*x), accum);
        x++;
        n--;
        return build_list(x, n, accum);
    }
}

LISP
dev_read(LISP dev, LISP eng, LISP arg)
{
    register long       i = get_c_long(dev);
    long                r = 0, t_start;
    int                 index, nvals;
    unsigned            uval;
    double              pr[2];
    float               ctd_pr;
    LISP                l1, l2;
    GTDdata_t           gtd_data;
    GTDsample_t         gtd_samp = GTD_DEFAULT_SAMPLE;
    union {
        GPSdata             gd;
        InertialData        id;
        NewCTDdata          ctd;
        CTDOdata            ctdo;
        ECOdata             ed;
        FlntuData           fd;
        OptodeData          od;
        TchainData          td;
        VpaltData           alt;
        SBE63Data           oxy;
    } u;

    switch(i)
    {
        case DEV_GPS:
            if(gps_dev_ready() && gps_read_data(&u.gd))
            {
                double  min;

                if(u.gd.lat.dir == 'S')
                    u.gd.lat.deg = -u.gd.lat.deg;

                min = u.gd.lat.min + (double)u.gd.lat.frac*1.0e-4;

                l1 = cons(inumcons((long)u.gd.lat.deg),
                          cons(flocons(min), NIL));

                if(u.gd.lon.dir == 'W')
                    u.gd.lon.deg = -u.gd.lon.deg;

                min = u.gd.lon.min + (double)u.gd.lon.frac*1.0e-4;

                l2 = cons(inumcons((long)u.gd.lon.deg),
                          cons(flocons(min), NIL));
                return cons(l1,
                            cons(l2,
                                 cons(inumcons((long)u.gd.satellites),
                                      cons(inumcons((long)u.gd.status), NIL))));
            }
            else
                return NIL;
            break;
        case DEV_ALTIMETER:
            if(vpalt_dev_ready() && (r = vpalt_read_data(&u.alt, 5000L)) >= 0)
            {
                l1 = symalist("range", flocons(u.alt.alt),
                              "pr", flocons(u.alt.pr), NULL);
                return l1;
            }
            else
                return NIL;
            break;
        case DEV_PAR:
            r = par_read_data();
            return cons(inumcons((r >> 16) & 0xffff),
                        cons(inumcons(r & 0xffff), NIL));
            break;
        case DEV_I490:
            r = i490_read_data();
            return cons(inumcons((r >> 16) & 0xffff),
                        cons(inumcons(r & 0xffff), NIL));
            break;
        case DEV_RH:
            if(rh_dev_ready() && (r = rh_read_data()) >= 0)
            {
                if(NNULLP(eng))
                {
                    double      percent;
                    percent = rh_mv_to_percent((int)r);
                    /* trim excess precision */
                    r = percent * 100;
                    return flocons((double)r/100.);
                }
                else
                    return inumcons(r);
            }
            else
                return NIL;
            break;
        case DEV_IPR:
            if(ipr_dev_ready() && (r = ipr_read_data()) >= 0)
            {
                if(NNULLP(eng))
                {
                    double      psi;
                    psi = ipr_mv_to_psi((int)r);
                    r = psi*100;
                    /* trim excess precision */
                    return flocons((double)r/100.);
                }
                else
                    return inumcons(r);
            }
            else
                return NIL;
            break;
        case DEV_PR:
        case DEV_PRF:
            DelayMilliSecs(100L);
            return flocons(0.);
            break;
        case DEV_INERTIAL:
            if(inertial_dev_ready() && inertial_read_data(&u.id))
            {
                l1 = cons(inumcons((long)u.id.ang_rate.x),
                           cons(inumcons((long)u.id.ang_rate.y),
                                cons(inumcons((long)u.id.ang_rate.z), NIL)));


                l2 = cons(inumcons((long)u.id.accel.x),
                              cons(inumcons((long)u.id.accel.y),
                                   cons(inumcons((long)u.id.accel.z), NIL)));
                return cons(l1,
                            cons(l2,
                                 cons(inumcons((long)u.id.temp),
                                      cons(strcons(1L, &u.id.type),
                                           cons(inumcons(u.id.timestamp),
                                                         NIL)))));
            }
            else
                return NIL;
            break;
        case DEV_GTD:
            t_start = RtcToCtm();

            gtd_samp.pump_mode = NNULLP(arg) ? get_c_long(arg) : 0;
            if(gtd_start_sample(&gtd_samp))
            {
                while(!gtd_data_ready())
                    if((RtcToCtm() - t_start) > 35L)
                    {
                        log_error("gtd", "Timeout waiting for sample\n");
                        return NIL;
                    }

                if(gtd_read_data(&gtd_data))
                    return cons(flocons(gtd_data.pressure),
                                cons(flocons(gtd_data.temperature), NIL));
            }
            return NIL;
            break;
        case DEV_THERM:
            if(therm_start_sample())
            {
                while(!therm_data_ready())
                    ;
                return flocons(therm_read_data());
            }
            return NIL;
            break;
        case DEV_CTD:
            index = NNULLP(arg) ? get_c_long(arg) : 0;
            if(sbe41apl_sample(index, 5000L, 0, &u.ctd))
            {
                l1 = symalist("temp", flocons(u.ctd.t),
                              "sal", flocons(u.ctd.s), NULL);
                return l1;
            }
            else
                return NIL;
            break;
        case DEV_SBECTD:
            index = NNULLP(arg) ? get_c_long(arg) : 0;
            if(sbe41p_sample(index, 5000L, &u.ctd, &ctd_pr))
            {
                l1 = symalist("pr", flocons(ctd_pr),
                              "temp", flocons(u.ctd.t),
                              "sal", flocons(u.ctd.s), NULL);
                return l1;
            }
            else
                return NIL;
            break;
        case DEV_SBE63:
            index = NNULLP(arg) ? get_c_long(arg) : 0;
            if(sbe63_read_data(index, &u.oxy, 5000L))
            {
                l1 = symalist("oxy", flocons(u.oxy.oxy),
                              "temp", flocons(u.oxy.temp),
                              "phase", flocons(u.oxy.phase),
                              "voltage", flocons(u.oxy.voltage), NULL);
                return l1;
            }
            else
                return NIL;
            break;
        case DEV_TCHAIN:
            index = NNULLP(arg) ? get_c_long(arg) : 0;
            if((nvals = tchain_sample(index, &u.td)) > 0)
                return build_list(&u.td.data[0], nvals, NIL);
            else
                return NIL;
            break;
        case DEV_FLR:
            if((r = flr_read_data()) >= 0)
                return inumcons(r);
            else
                return NIL;
            break;
        case DEV_CSTAR:
            if(cstar_read_data(&uval, 3000L))
                return inumcons(uval);
            else
                return NIL;
            break;
        case DEV_ECO:
            if(eco_dev_ready() && eco_read_data(&u.ed))
            {
                l1 = symalist("blue", inumcons(u.ed.blue[ECO_SIG]),
                              "blue-ref", inumcons(u.ed.blue[ECO_REF]),
                              "red", inumcons(u.ed.red[ECO_SIG]),
                              "red-ref", inumcons(u.ed.red[ECO_REF]),
                              "flr", inumcons(u.ed.flr),
                              "therm", inumcons(u.ed.therm), NULL);
                return l1;
            }
            else
                return NIL;
            break;
        case DEV_FLNTU:
            if(flntu_read_data(&u.fd, 3000L))
            {
                l1 = symalist("chl", inumcons(u.fd.chl),
                              "chl-ref", inumcons(u.fd.chl_ref),
                              "ntu", inumcons(u.fd.ntu),
                              "ntu-ref", inumcons(u.fd.ntu_ref), NULL);
                return l1;
            }
            else
                return NIL;
            break;
        case DEV_OPTODE:
            if(optode_dev_ready() && optode_read_data(&u.od, 4000L))
            {
                if(u.od.version == 2)
                {
                    l1 = symalist("Phase",
                                  cons(flocons(u.od.data.v2.c1rph),
                                       cons(flocons(u.od.data.v2.c2rph), NIL)),
                                  "Amp",
                                  cons(flocons(u.od.data.v2.c1amp),
                                       cons(flocons(u.od.data.v2.c2amp), NIL)),
                                  "Temp.", flocons(u.od.data.v2.temp),
                                  "Sat.", flocons(u.od.data.v2.sat), NULL);
                }
                else
                {
                    l1 = symalist("BPhase", flocons(u.od.data.v1.bphase),
                                  "Oxy", flocons(u.od.data.v1.oxy),
                                  "Temp.", flocons(u.od.data.v1.temp),
                                  "Sat.", flocons(u.od.data.v1.sat), NULL);
                }

                return l1;
            }
            else
                return NIL;
            break;
        case DEV_CTDO:
            pr[0] = NNULLP(arg) ? get_c_double(arg)+1.397 : 0;
            ctdo_start_sample(pr[0]);
            while(!ctdo_data_ready())
                ;
            if(ctdo_read_data(&u.ctdo))
            {
                return cons(flocons(u.ctdo.t),
                            cons(flocons(u.ctdo.s),
                                 cons(inumcons(u.ctdo.oxy), NIL)));
            }
            else
                return NIL;
            break;
        case DEV_ISUS:
            return NIL;
    }

    return NIL;
}

LISP
dev_config(LISP dev, LISP arg)
{
    switch(get_c_long(dev))
    {
        case DEV_CTDO:
            if(ctdo_cmd(get_c_string(arg)))
                return a_true_value();
            else
                return NIL;
            break;
        case DEV_ANR:
            if(anr_cmd(get_c_string(arg)))
                return a_true_value();
            else
                return NIL;
            break;
        default:
            break;
    }

    return NIL;
}

/*************************** MOTOR INTERFACE *************************/
LISP
lmotor_move(LISP lx, LISP ltimeout, LISP epsilon)
{
    long        r, x, timeout, err;

    timeout = NNULLP(ltimeout) ? get_c_long(ltimeout) : 0L;
    err = NNULLP(epsilon) ? get_c_long(epsilon) : 1000L;
    x = get_c_long(lx);

    r = motor_move(motor_pos()+x, err, timeout, 0);

    return inumcons(r);
}

// Bump the motor forward for a fixed time interval. The motor need not
// be homed and the motor position will be invalidated.
LISP
lmotor_bump(LISP ms)
{
    long        interval;

    interval = NNULLP(ms) ? get_c_long(ms) : 1000L;
    motor_bump(interval);

    return NIL;
}

LISP
lmotor_home(LISP ltimeout)
{
    long        r, timeout;

    timeout = NNULLP(ltimeout) ? get_c_long(ltimeout) : 900L;
    r = motor_home(timeout);

    return inumcons(r);
}

LISP
lmotor_set_enc(LISP cpt, LISP gr)
{
    motor_set_encoder(get_c_long(cpt), get_c_long(gr));
    return a_true_value();
}

LISP
lmotor_trace(LISP bool)
{
    motor_trace(NNULLP(bool) ? 1: 0);
    return a_true_value();
}

LISP
tocm(LISP x)
{
    return flocons(counts_to_cm(get_c_long(x)));
}

LISP
fromcm(LISP x)
{
    return inumcons(cm_to_counts(get_c_double(x)));
}


LISP
lmotor_pos(void)
{
    return inumcons(motor_pos());
}

LISP
lmotor_speed(void)
{
    return inumcons(motor_speed());
}

LISP
lmotor_overshoot(void)
{
    return inumcons(motor_overshoot());
}

LISP
lmotor_energy(void)
{
    return flocons(motor_energy());
}

/*************************** RAFOS INTERFACE *************************/
static LISP
raf_open(void)
{
    return rafos_init() ? a_true_value() : NIL;
}

static LISP
raf_set_clock(void)
{
    return rafos_set_clock() ? a_true_value() : NIL;
}

static LISP
raf_sync_clock(void)
{
    return rafos_set_tt8_clock() ? a_true_value() : NIL;
}

static LISP
raf_close(void)
{
    rafos_shutdown();
    return a_true_value();
}

static LISP
raf_listen(LISP duration)
{
    unsigned    r;

    r = rafos_start_sample((unsigned)get_c_long(duration));
    return inumcons(r);
}

static LISP
raf_check(void)
{
    return rafos_data_ready() ? a_true_value() : NIL;
}

static LISP
raf_reopen(void)
{
    rafos_reopen();
    return a_true_value();
}

static LISP
raf_read(void)
{
    RafosData   rd;

    if(!rafos_read_data(&rd))
        return NIL;
    return cons(inumcons(rd.t),
                cons(cons(inumcons(rd.index[0]), inumcons(rd.corr[0])),
                     cons(cons(inumcons(rd.index[1]), inumcons(rd.corr[1])),
                          cons(cons(inumcons(rd.index[2]), inumcons(rd.corr[2])),
                               NIL))));
}

static LISP
raf_test(void)
{
    rafos_test();
    return a_true_value();
}

static LISP
raf_sod(void)
{
    return inumcons(rafos_sod());
}

/*************************** CAMERA INTERFACE *************************/
static LISP
camera_open(void)
{
    int         n;

    if(!cam_init())
        return NIL;
    if((n = cam_sync()) == 0)
    {
        cam_shutdown();
        return NIL;
    }

    return inumcons(n);
}

static LISP
camera_close(void)
{
    cam_shutdown();
    return a_true_value();
}

static LISP
camera_filenames(void)
{
    char        lowres[16], highres[16];
    struct tm   *now;
    time_t      t;

    t = time(0);
    now = localtime(&t);

    sprintf(lowres, "L%03d%02d%02d.jpg", now->tm_yday, now->tm_hour,
        now->tm_min);
    sprintf(highres,"H%03d%02d%02d.jpg", now->tm_yday, now->tm_hour,
        now->tm_min);

    return cons(strcons(strlen(lowres), lowres),
                strcons(strlen(highres), highres));
}

static LISP
camera_snapshot(LISP res, LISP fname)
{
    FILE        *fp;
    int         r;

    if((fp = fopen(get_c_string(fname), "w")) == NULL)
        return NIL;
    r = cam_snapshot((cam_res_t)get_c_long(res), fp);
    fclose(fp);

    return (r > 0) ? a_true_value() : NIL;
}

/*************************** TRANSPONDER INTERFACE *************************/
static LISP
xpd_open(void)
{
    return xp_init() ? a_true_value() : NIL;
}

static LISP
xpd_close(void)
{
    xp_shutdown();
    return a_true_value();
}

static LISP
xpd_range(void)
{
    return inumcons(xp_range());
}

static LISP
xpd_ping(LISP freqs, LISP timeo, LISP pw, LISP nowait)
{
    float       f_xmit, f_recv;
    long        t;
    int         pulse_width;

    if(CONSP(freqs))
    {
        f_xmit = get_c_double(CAR(freqs));
        f_recv = get_c_double(CAR(CDR(freqs)));
    }
    else
        f_xmit = f_recv = get_c_double(freqs);

    t = get_c_long(timeo);
    pulse_width = NNULLP(pw) ? get_c_long(pw) : 1;
    if(!xp_ping(f_xmit, f_recv, pulse_width, t))
        return NIL;
    if(NULLP(nowait))
    {
        DelayMilliSecs(t+150);
        return inumcons(xp_range());
    }
    return a_true_value();
}

static LISP
xpd_listen(LISP freqs, LISP pw)
{
    float       f_xmit, f_recv, resp_pw;

    if(CONSP(freqs))
    {
        f_xmit = get_c_double(CAR(freqs));
        f_recv = get_c_double(CAR(CDR(freqs)));
    }
    else
        f_xmit = f_recv = get_c_double(freqs);

    resp_pw = NNULLP(pw) ? get_c_double(pw) : 1.5;

    if(!xp_listen(f_xmit, f_recv, resp_pw))
        return NIL;

    return a_true_value();
}

static LISP
xpd_replies(void)
{
    return inumcons(xp_replies());
}

/*************************** ARGOS INTERFACE *************************/
static LISP
ar_open(void)
{
    return argos_init() ? a_true_value() : NIL;
}

static LISP
ar_ready(void)
{
    return argos_dev_ready() ? a_true_value() : NIL;
}

static LISP
ar_next(void)
{
    return inumcons(argos_next());
}

static LISP
ar_wait(LISP t)
{
    return argos_wait(get_c_long(t)) ? a_true_value() : NIL;
}

static LISP
ar_send(LISP data)
{
    char        *s = get_c_string(data);
    return argos_send_msg(s, (int)strlen(s)) ? a_true_value() : NIL;
}

static LISP
ar_close(void)
{
    argos_shutdown();
    return a_true_value();
}

static int
pack_status_message(GPSdata *gdp, short *v, short rh, int code)
{
    time_t              t;
    int                 n;
    unsigned char       rh_byte;
    void                *dp[5];
    static char         buf[ARGOS_MSG_LEN];

    t = RtcToCtm();
    rh_byte = rh;

    dp[0] = &t;
    dp[1] = &v[0];
    dp[2] = &v[1];
    dp[3] = &rh_byte;
    dp[4] = &code;

    n = argos_build_packet(gdp, "LhhBB", dp, 5, buf);

    log_event("Waiting for ARGOS transmission interval\n");

    if(!argos_wait(ARGOS_MSG_INTERVAL+ARGOS_MSG_TIME))
        return 0;
    log_event("Sending ARGOS message\n");

    return argos_send_msg((unsigned char*)buf, n);
}


static LISP
ar_test(void)
{
    GPSdata     gd;
    short       rh, v[2];

    if(!gps_init())
        return NIL;

    while(!gps_dev_ready())
        ;
    gps_read_data(&gd);
    gps_shutdown();

    atod_init();
    iop_set(IO_D, (unsigned char)0x20);
    DelayMilliSecs(50L);
    v[0] = atod_read(5)*4.322;
    v[1] = atod_read(6)*4.322;
    iop_clear(IO_D, (unsigned char)0x20);
    atod_shutdown();

    rh = 0;
    if(rh_init())
    {
        while(!rh_dev_ready())
            ;
        rh = rh_read_data();
        rh_shutdown();
    }

    return pack_status_message(&gd, v, (short)rh_mv_to_percent(rh), 0x55) ?
      a_true_value() : NIL;
}

/*************************** MODEM INTERFACE *************************/
#ifdef USE_MODEM
LISP
lmodem_open(void)
{
    return modem_init() ? a_true_value() : NIL;
}

LISP
lmodem_ready(void)
{
    return modem_dev_ready() ? a_true_value() : NIL;
}

LISP
lmodem_chat(LISP send, LISP expect, LISP timeout)
{
    return chat(NNULLP(send) ? get_c_string(send) : "",
                NNULLP(expect) ? get_c_string(expect) : "",
                get_c_long(timeout)) ? a_true_value() : NIL;
}

LISP
lmodem_use_stdio(void)
{
    modem_set_interface(MODEM_COM1);
    return a_true_value();
}

LISP
lmodem_use_tpu(void)
{
    modem_set_interface(MODEM_COM2);
    return a_true_value();
}

LISP
lmodem_close(void)
{
    modem_shutdown();
    return a_true_value();
}
#endif /* USE_MODEM */

/*************************** NVRAM INTERFACE *************************/
LISP
lnvram_insert(LISP key, LISP value, LISP sync)
{
    nv_value    nv;
    int         dowrite = 0, r;

    if(NNULLP(sync))
        dowrite = 1;

    switch(TYPE(value))
    {
        case tc_string:
            strncpy(nv.c, get_c_string(value), 7);
            r = nv_insert(get_c_string(key), &nv, NV_CHARS, dowrite);
            break;
        case tc_flonum:
            nv.d = get_c_double(value);
            r = nv_insert(get_c_string(key), &nv, NV_FLOAT, dowrite);
            break;
        case tc_inum:
            nv.l = get_c_long(value);
            r = nv_insert(get_c_string(key), &nv, NV_INT, dowrite);
            break;
        default:
            r = -1;
            break;
    }


    return (r >= 0) ? a_true_value() : NIL;
}

LISP
lnvram_write(void)
{
    return nv_write() ? a_true_value() : NIL;
}

LISP
lnvram_find(LISP key)
{
    nv_value    nv;
    int         type;
    LISP        l;

    type = nv_lookup(get_c_string(key), &nv);
    switch(type)
    {
        case NV_CHARS:
            l = strcons(strlen(nv.c), nv.c);
            break;
        case NV_INT:
            l = inumcons(nv.l);
            break;
        case NV_FLOAT:
            l = flocons(nv.d);
            break;
        default:
            l = NIL;
            break;
    }

    return l;
}

LISP
picodos_df(void)
{
    long        size, free;

    pdcfinfo("A:", &size, &free);

    return inumcons(free);
}


void
init_mlf2_lib(int safemode)
{
    init_mlf2_vars();

    if(!safemode)
    {
        init_subr_2("outb", ioport_write);
        init_subr_2("setb", ioport_set);
        init_subr_2("clearb", ioport_clear);
        init_subr_1("motor-home", lmotor_home);
        init_subr_3("motor-move", lmotor_move);
        init_subr_1("motor-trace", lmotor_trace);
        init_subr_2("motor-set-encoder", lmotor_set_enc);
        init_subr_1("motor-bump", lmotor_bump);
    }

    init_subr_0("ad-init", lad_init);
    init_subr_0("ad-close", lad_shutdown);
    init_subr_1("ad-read", lad_read);
    init_subr_3("ad-read-buf", lad_read_buf);

    init_subr_1("inb", ioport_read);

#ifdef USE_FFT
    init_subr_1("unbias!", unbias);
    init_subr_2("mkcoeff", ifft_coeff);
    init_subr_2("ifft", lifft);
#endif
    init_subr_3("display-menu", display_menu);
    init_subr_2("get-integer", get_integer);
    init_subr_2("get-float", get_float);
    init_subr_2("get-string", get_string);

#ifdef USE_MODEM
    init_subr_0("modem-open", lmodem_open);
    init_subr_0("modem-close", lmodem_close);
    init_subr_0("modem-ready?", lmodem_ready);
    init_subr_3("modem-chat", lmodem_chat);
    init_subr_0("modem-use-stdio", lmodem_use_stdio);
    init_subr_0("modem-use-tpu", lmodem_use_tpu);
#endif

    init_subr_2("dev-init", dev_init);
    init_subr_2("dev-close", dev_close);
    init_subr_3("dev-read", dev_read);
    init_subr_2("dev-ready?", dev_ready);
    init_subr_2("dev-test", dev_test);
    init_subr_2("dev-config", dev_config);

    init_subr_0("motor-speed", lmotor_speed);
    init_subr_0("motor-pos", lmotor_pos);
    init_subr_0("motor-overshoot", lmotor_overshoot);
    init_subr_0("motor-energy", lmotor_energy);
    init_subr_1("to-cm", tocm);
    init_subr_1("from-cm", fromcm);

    init_subr_3("nv-insert", lnvram_insert);
    init_subr_0("nv-write", lnvram_write);
    init_subr_1("nv-find", lnvram_find);

#ifdef USE_NETCDF
    init_subr_3("nc-add-variable", nc_write_var);
    init_subr_2("nc-flush-record", nc_flush_rec);
    init_subr_3("nc-write-header", nc_write_header);
#endif

    init_subr_0("df", picodos_df);

    init_subr_0("argos-open", ar_open);
    init_subr_0("argos-ready?", ar_ready);
    init_subr_0("argos-next", ar_next);
    init_subr_0("argos-close", ar_close);
    init_subr_1("argos-wait", ar_wait);
    init_subr_1("argos-send", ar_send);
    init_subr_0("argos-test", ar_test);

    init_subr_0("rafos-open", raf_open);
    init_subr_0("rafos-close", raf_close);
    init_subr_0("rafos-set-clock", raf_set_clock);
    init_subr_0("rafos-sync-clock", raf_sync_clock);
    init_subr_1("rafos-listen", raf_listen);
    init_subr_0("rafos-check", raf_check);
    init_subr_0("rafos-read", raf_read);
    init_subr_0("rafos-reopen", raf_reopen);
    init_subr_0("rafos-test", raf_test);
    init_subr_0("rafos-sod", raf_sod);

    init_subr_0("xp-open", xpd_open);
    init_subr_0("xp-close", xpd_close);
    init_subr_4("xp-ping", xpd_ping);
    init_subr_2("xp-listen", xpd_listen);
    init_subr_0("xp-replies", xpd_replies);
    init_subr_0("xp-range", xpd_range);

    init_subr_0("camera-open", camera_open);
    init_subr_0("camera-close", camera_close);
    init_subr_2("snapshot", camera_snapshot);
    init_subr_0("camera-filenames", camera_filenames);

}
