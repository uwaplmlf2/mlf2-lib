/*
** $Id: siodtt8.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** SIOD interface to TT8 library functions
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#ifdef __TT8__
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <userio.h>
#endif

#include "siod.h"
#include "siodp.h"
#include "salarm.h"
#include "lpsleep.h"

#define FLASH_END       0x03ffff
#define RAM_END         0x2fffff

static void
init_tt8_version(void)
{
    setvar(cintern("*tt8lib-version*"),
        cintern("$Id: siodtt8.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $"),
        NIL);
}

LISP
tpu_set(LISP n)
{
    if(NUMP(n))
        TPUSetPin((int)IORFNUM(n), 1);
    return a_true_value();
}

LISP
tpu_clear(LISP n)
{
    if(NUMP(n))
        TPUSetPin((int)IORFNUM(n), 0);
    return a_true_value();
}

LISP
tpu_get(LISP n)
{
    return inumcons((long)TPUGetPin((int)get_c_long(n)));
}


LISP
sleep_secs(LISP n)
{
    short       interrupted = 1;
    if(NUMP(n))
        interrupted = isleep((long)IORFNUM(n));

    return interrupted ? a_true_value() : NIL;
}

LISP
wakeup_at(LISP n)
{
    short       interrupted = 1;
    time_tt     when;

    if(NUMP(n))
    {
        when.secs = (long)IORFNUM(n);
        when.ticks = 0L;
        interrupted = isleep_till(when);
    }
    return interrupted ? a_true_value() : NIL;
}

LISP
total_mem(void)
{
    long size;
    GetRamInfo(&size, 0);

    return inumcons(size);
}

extern unsigned long _mtop, _mcur;

LISP
avail_mem(void)
{
    return inumcons(_mtop - _mcur);
}

LISP
lpeek(LISP a)
{
    long                addr;
    unsigned char       value = 0;

    /*
    ** Read access allowed to FLASH, RAM, and module space.
    */
    addr = get_c_long(a);
    if((addr >= 0 && addr <= RAM_END) ||
       (addr >= IMBASE && addr <= IMBASE+0xfff))
        value = *((unsigned char*)addr);
    return inumcons((long)value);
}

LISP
lpoke(LISP a, LISP x)
{
    long                addr;
    unsigned char       value;

    /*
    ** Write access to RAM and module space only.
    */
    addr = get_c_long(a);
    if((addr > FLASH_END && addr <= RAM_END) ||
       (addr >= IMBASE && addr <= IMBASE+0xfff))
    {
        value = get_c_long(x);
        *((unsigned char*)addr) = value;
        return a_true_value();
    }

    return NIL;
}


LISP
unix_time(void)
{
    return inumcons(RtcToCtm());
}

LISP
symalist(char *arg,...)
{
    va_list args;
    LISP result,l,val;
    char *key;

    if (!arg)
        return(NIL);
    va_start(args,arg);
    val = va_arg(args,LISP);
    result = cons(cons(cintern(arg),val),NIL);
    l = result;
    while((key = va_arg(args,char *)))
    {
        val = va_arg(args,LISP);
        CDR(l) = cons(cons(cintern(key),val),NIL);
        l = CDR(l);
    }
    va_end(args);
    return(result);
}


LISP
isotime(LISP value)
{
    time_t      b;
    struct tm   *t;
    char        iso[22];

    if(NNULLP(value))
        b = get_c_long(value);
    else
        time(&b);

    t = localtime(&b);

    sprintf(iso, "%4d-%02d-%02d %02d:%02d:%02d",
            t->tm_year+1900, t->tm_mon+1, t->tm_mday,
            t->tm_hour, t->tm_min, t->tm_sec);
    return strcons((long)strlen(iso), iso);
}

LISP
tm_to_secs(LISP tmlist)
{
    int         i, datetime[6];
    struct tm   tm;

    i = 0;
    while(NNULLP(tmlist))
    {
        if(i < 6)
            datetime[i++] = get_c_long(CAR(tmlist));
        tmlist = CDR(tmlist);
    }

    while(i < 6)
        datetime[i++] = 0;

    tm.tm_year = datetime[0] - 1900;
    tm.tm_mon = datetime[1] - 1;
    tm.tm_mday = datetime[2];
    tm.tm_hour = datetime[3];
    tm.tm_min = datetime[4];
    tm.tm_sec = datetime[5];
    tm.tm_wday = tm.tm_yday = tm.tm_isdst = 0;

    return inumcons(mktime(&tm));
}

LISP
set_unix_time(LISP value)
{
    time_t      t;

    if(NINUMP(value))
        err("Bad argument to set_unix_time", value);

    t = get_c_long(value);
    SetTimeSecs(t, NULL);
    return inumcons(t);
}

LISP
set_datetime(void)
{
    struct tm   *t;
    time_t      secs = RtcToCtm();

    t = localtime(&secs);

    if(QueryDateTime("Set date/time ", TRUE, t))
    {
        SetTimeTM(t, NULL);
        return a_true_value();
    }
    else
        return NIL;
}

static int handle_sigalrm_flag = 0;
static LISP alarm_fun;

static void
lalarm_handler(void)
{
    if(nointerrupt == 1)
    {
        if(handle_sigalrm_flag)
            interrupt_differed = 1;
    }
    else
    {
        if(NNULLP(alarm_fun))
            lapply(alarm_fun, NIL);
        else
            err("alarm signal", NIL);
    }

}


LISP
lalarm(LISP seconds, LISP f)
{
    long iflag;
    int r;

    iflag = no_interrupt(1L);
    if(NNULLP(f))
    {
        handle_sigalrm_flag = 1;
        alarm_fun = NNUMP(f) ? f : NIL;
    }
    else
    {
        handle_sigalrm_flag = 0;
        alarm_fun = NIL;
    }

    r = alarm_set(get_c_long(seconds), ALARM_ONESHOT, lalarm_handler);
    no_interrupt(iflag);
    return (r == 1) ? a_true_value() : NIL;
}

LISP
lalarm_off(void)
{
    alarm_shutdown();
    return a_true_value();
}

LISP
lalarm_reset(void)
{
    alarm_reset();
    return a_true_value();
}

LISP
lkbhit(void)
{
    return SerByteAvail() ? a_true_value() : NIL;
}

LISP
lkbflush(void)
{
    if(SerByteAvail())
    {
        (void)SerGetByte();
        return a_true_value();
    }

    return NIL;
}

LISP
timer_start(void)
{
    StopWatchStart();
    return a_true_value();
}

LISP
timer_read(void)
{
    return inumcons(StopWatchTime());
}

LISP
lreset(LISP arg)
{
    if(NULLP(arg))
        Reset();
    else
        ResetToMon();

    return NIL;
}

LISP
delay_ms(LISP ms)
{
    DelayMilliSecs(get_c_long(ms));
    return a_true_value();
}

LISP
lmillisecs(void)
{
    return inumcons(MilliSecs());
}

LISP
set_cpu_clock(LISP freq)
{
    long        r;
    long        baud;

    baud = SerGetBaud(0L, 0L);
    r = SimSetFSys(get_c_long(freq));
    if(r > 0)
        SerSetBaud(baud, 0L);
    return inumcons(r);
}

LISP
get_cpu_clock(void)
{
    return inumcons(SimGetFSys());
}

LISP
lsystime(void)
{
#ifdef __GNUC__
    time_tt     now = ttm_now();
#else
    time_tt     now = ttmnow();
#endif

    return flocons(now.secs + (double)now.ticks/GetTickRate());
}

LISP
xmodem_send_data(LISP data, LISP timeout)
{
    register void       *buf;
    register long       nbytes;

    switch(data->type)
    {
        case tc_string:
            buf = data->storage_as.string.data;
            nbytes = strlen(buf);
            break;
        case tc_double_array:
            buf = data->storage_as.double_array.data;
            nbytes = data->storage_as.double_array.dim * sizeof(double);
            break;
        case tc_float_array:
            buf = data->storage_as.float_array.data;
            nbytes = data->storage_as.float_array.dim * sizeof(double);
            break;
        case tc_long_array:
            buf = data->storage_as.long_array.data;
            nbytes = data->storage_as.long_array.dim * sizeof(double);
            break;
        case tc_short_array:
            buf = data->storage_as.short_array.data;
            nbytes = data->storage_as.short_array.dim * sizeof(double);
            break;
        case tc_byte_array:
            buf = data->storage_as.string.data;
            nbytes = data->storage_as.string.dim * sizeof(double);
            break;
        default:
            return NIL;
    }

    return (XmodemSendMem(buf, nbytes, (ushort)get_c_long(timeout))==xmdmOk) ?
      a_true_value() : NIL;
}

static LISP
xmodem_upload_file(LISP fname)
{
    FILE        *fp;
    char        *f, *buf;
    long        nbytes;

    if(TYPEP(fname,tc_string) &&
       (fp = fopen(f = get_c_string(fname), "r")) != NULL)
    {
        fseek(fp, 0L, SEEK_END);
        nbytes = ftell(fp);
        fseek(fp, 0L, SEEK_SET);

        if((buf = malloc(nbytes)) != NULL)
        {
            fread(buf, 1L, nbytes, fp);
            fclose(fp);

            if(XmodemSendMem(buf, nbytes, 30) == xmdmOk)
            {
                free(buf);
                return a_true_value();
            }
            free(buf);
        }
    }

    return NIL;
}


void
init_tt8_lib(int safemode)
{
    init_tt8_version();

    if(!safemode)
    {
        init_subr_2("poke", lpoke);
        init_subr_1("tpu-set", tpu_set);
        init_subr_1("tpu-clear", tpu_clear);
        init_subr_1("reset", lreset);
        init_subr_1("sleep", sleep_secs);
        init_subr_1("sleep-till", wakeup_at);

    }

    init_subr_0("getfsys", get_cpu_clock);
    init_subr_1("setfsys", set_cpu_clock);

    init_subr_0("avail-mem", avail_mem);
    init_subr_0("total-mem", total_mem);
    init_subr_1("peek", lpeek);

    init_subr_1("tpu-get", tpu_get);
    init_subr_1("delay-ms", delay_ms);

    init_subr_0("time", unix_time);
    init_subr_1("isotime", isotime);
    init_subr_1("set-unix-time", set_unix_time);
    init_subr_0("set-date/time", set_datetime);
    init_subr_1("mktime", tm_to_secs);

    init_subr_2("alarm", lalarm);
    init_subr_0("alarm-off", lalarm_off);
    init_subr_0("alarm-reset", lalarm_reset);

    init_subr_0("kbhit", lkbhit);
    init_subr_0("kbflush", lkbflush);
    init_subr_0("start-timer", timer_start);
    init_subr_0("read-timer", timer_read);

    init_subr_0("time-ms", lmillisecs);
    init_subr_0("systime", lsystime);

    init_subr_2("xmodem-send", xmodem_send_data);
    init_subr_1("xmodem-upload", xmodem_upload_file);
}
