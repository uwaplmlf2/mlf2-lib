/*
** $Id: tstt8.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Interface TT8 library functions to TinyScheme.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <userio.h>
#include "lpsleep.h"
#include "nvram.h"
#include "scheme.h"
#include "ts.h"

#define FLASH_END	0x03ffff
#define RAM_END		0x2fffff

extern unsigned long _mtop, _mcur;

pointer
tpu_set(scheme *sc, pointer args)
{
    if(args != sc->NIL && is_integer(ARG1(args))) 
    {
	TPUSetPin((int)ivalue(ARG1(args)), 1);
	return sc->T;
    }
    return sc->F;
}

pointer
tpu_clear(scheme *sc, pointer args)
{
    if(args != sc->NIL && is_integer(ARG1(args))) 
    {
	TPUSetPin((int)ivalue(ARG1(args)), 0);
	return sc->T;
    }
    return sc->F;
}

pointer
tpu_get(scheme *sc, pointer args)
{
    if(args != sc->NIL && is_integer(ARG1(args))) 
    {
	long x = TPUGetPin((int)ivalue(ARG1(args)));
	return mk_integer(sc, x);
    }
    return sc->F;
}

pointer
lowpower_sleep(scheme *sc, pointer args)
{
    if(args != sc->NIL && is_integer(ARG1(args)))
    {
	long secs = ivalue(ARG1(args));
	if(isleep(secs))
	    return sc->T;
    }

    return sc->F;
}


pointer
wakeup_at(scheme *sc, pointer args)
{
    time_tt	twakeup;

    if(args != sc->NIL && is_integer(pair_car(args)))
    {
	twakeup.secs = ivalue(pair_car(args));
	twakeup.ticks = 0;
	
	if(isleep_till(twakeup))
	    return sc->T;
    }

    return sc->F;
}

pointer
avail_mem(scheme *sc, pointer args)
{
    return mk_integer(sc, _mtop - _mcur);
}

pointer
peek(scheme *sc, pointer args)
{
    long		addr;
    unsigned char	value = 0;
    
    if(args == sc->NIL)
	return sc->F;
    addr = ivalue(ARG1(args));
    if((addr >= 0 && addr <= RAM_END) ||
       (addr >= IMBASE && addr <= IMBASE+0xfff))
	value = *((unsigned char*)addr);
    return mk_integer(sc, (long)value);
}

pointer
poke(scheme *sc, pointer args)
{
    long		addr;
    unsigned char	value;
    
    if(args == sc->NIL)
	return sc->F;
    addr = ivalue(ARG1(args));
    value = ivalue(ARG2(args));
    
    if((addr > FLASH_END && addr <= RAM_END) ||
       (addr >= IMBASE && addr <= IMBASE+0xfff))
    {
	*((unsigned char*)addr) = value;
	return sc->T;
    }
    
    return sc->F;
}

pointer
clktime(scheme *sc, pointer args)
{
    return mk_integer(sc, RtcToCtm());
}

pointer
isotime(scheme *sc, pointer args)
{
    time_t	t;
    struct tm	*tm;
    char	iso[22];
    
    if(args != sc->NIL)
	t = ivalue(ARG1(args));
    else
	t = RtcToCtm();
    
    tm = localtime(&t);
    sprintf(iso, "%4d-%02d-%02d %02d:%02d:%02d", tm->tm_year+1900,
	    tm->tm_mon+1, tm->tm_mday, tm->tm_hour, tm->tm_min,
	    tm->tm_sec);
    return mk_string(sc, iso);
}

pointer
settime(scheme *sc, pointer args)
{
    if(args != sc->NIL && is_integer(ARG1(args)))
    {
	SetTimeSecs(ivalue(ARG1(args)), NULL);
	return sc->T;
    }
    return sc->F;
}

pointer
keypress(scheme *sc, pointer args)
{
    return SerByteAvail() ? sc->T : sc->F;
}

pointer
keyflush(scheme *sc, pointer args)
{
    if(SerByteAvail())
	return mk_character(sc, SerGetByte());
    return sc->F;
}

pointer
timer_start(scheme *sc, pointer args)
{
    StopWatchStart();
    return sc->T;
}

pointer
timer_read(scheme *sc, pointer args)
{
    return mk_integer(sc, StopWatchTime());
}

pointer
sysreset(scheme *sc, pointer args)
{
    if(args != sc->NIL)
	ResetToMon();
    else
	Reset();
    return sc->T;
}

pointer
delay_ms(scheme *sc, pointer args)
{
    if(args != sc->NIL && is_integer(ARG1(args)))
    {
	DelayMilliSecs(ivalue(ARG1(args)));
	return sc->T;
    }
    return sc->F;
}

static int
set_nvram_value_helper(scheme *sc, pointer name, pointer value, int sync)
{
    int		type;
    nv_value	nv;

    if(is_integer(value))
    {
	type = NV_INT;
	nv.l = ivalue(value);
    }
    else if(is_real(value))
    {
	type = NV_FLOAT;
	nv.d = rvalue(value);
    }
    else if(is_string(value))
    {
	type = NV_CHARS;
	strncpy(nv.c, string_value(value), 7L);
	nv.c[7] = '\0';
    }
    else if(is_symbol(value))
    {
	type = NV_CHARS;
	strncpy(nv.c, symname(value), 7L);
	nv.c[7] = '\0';
    }
    else
	return 0;
    
    return (nv_insert(string_value(name), &nv, type, sync) != -1);
}

pointer
set_nvram_value(scheme *sc, pointer args)
{
    pointer	name, value;
    
    if(args == sc->NIL)
	return sc->F;

    name = ARG1(args);
    value = ARG2(args);

    return set_nvram_value_helper(sc, name, value, HAVE_ARG3(sc, args)) ?
      sc->T : sc->F;
}

pointer
get_nvram_value(scheme *sc, pointer args)
{
    pointer	name, value;
    int		type;
    nv_value	nv;
    
    if(args == sc->NIL)
	return sc->NIL;

    name = ARG1(args);
    type = nv_lookup(string_value(name), &nv);

    switch(type)
    {
	case NV_INT:
	    value = mk_integer(sc, nv.l);
	    break;
	case NV_FLOAT:
	    value = mk_real(sc, nv.d);
	    break;
	case NV_CHARS:
	    value = mk_string(sc, nv.c);
	    break;
	default:
	    value = sc->NIL;
	    break;
    }
    
    return value;
}

static scheme *_this_sc;

static void
add_to_nvram_list(nv_entry *e, void *calldata)
{
    pointer	*list = (pointer*)calldata;
    pointer	next;

    switch(e->type)
    {
	case NV_CHARS:
	    next = cons(_this_sc, mk_string(_this_sc, e->key), 
			mk_string(_this_sc, e->value.c));
	    break;
	case NV_INT:
	    next = cons(_this_sc, mk_string(_this_sc, e->key), 
			mk_integer(_this_sc, e->value.l));
	    break;
	case NV_FLOAT:
	    next = cons(_this_sc, mk_string(_this_sc, e->key), 
			mk_real(_this_sc, e->value.d));
	    break;
	default:
	    next = _this_sc->NIL;
	    break;
    }

    *list = cons(_this_sc, next, *list);
}

pointer
list_nvram(scheme *sc, pointer args)
{
    pointer	list;

    if(args == sc->NIL)
    {
	/*
	** No args.  Return the NVRAM as an alist
	*/

	/* Assigned to static variable for callback */
	_this_sc = sc;
	/* Initialize list */
	list = sc->NIL;
    	nv_foreach(add_to_nvram_list, (void*)&list);
    	return list;
    }
    else
    {
	pointer e;
	
	/*
	** Argument is an alist specifying the values to store
	** in the NVRAM table.
	*/
	list = ARG1(args);
	while((e = pair_car(list)) != sc->NIL)
	{
	    set_nvram_value_helper(sc, pair_car(e), pair_cdr(e), 0);
	    list = pair_cdr(list);
	}
	nv_write();
	
	return sc->T;
    }
    
}

pointer
delete_nvram_entry(scheme *sc, pointer args)
{
    pointer	name;
    
    if(args != sc->NIL && is_string(name = ARG1(args)))
	nv_delete(string_value(name));
    return sc->T;
}

pointer
sync_nvram(scheme *sc, pointer args)
{
    nv_write();
    return sc->T;
}

pointer
initialize_nvram(scheme *sc, pointer args)
{
    init_nvram();
    return sc->T;
}

void
init_tstt8(scheme *sc, int safemode)
{
    if(safemode)
    {
	add_to_ffi(sc, poke, "poke");
	add_to_ffi(sc, tpu_set, "tpu-set");
	add_to_ffi(sc, tpu_clear, "tpu-clear");
	add_to_ffi(sc, tpu_get, "tpu-get");
	add_to_ffi(sc, sysreset, "reset");
	add_to_ffi(sc, lowpower_sleep, "sleep");
	add_to_ffi(sc, wakeup_at, "sleep-till");
	add_to_ffi(sc, delay_ms, "delay-ms");
	add_to_ffi(sc, settime, "settime");
    }
    
    add_to_ffi(sc, avail_mem, "avail-mem");
    add_to_ffi(sc, peek, "peek");
    add_to_ffi(sc, clktime, "time");
    add_to_ffi(sc, isotime, "isotime");
    add_to_ffi(sc, keypress, "kbhit");
    add_to_ffi(sc, keyflush, "kbflush");
    add_to_ffi(sc, timer_start, "start-timer");
    add_to_ffi(sc, timer_read, "read-timer");
    add_to_ffi(sc, set_nvram_value, "nvset");
    add_to_ffi(sc, get_nvram_value, "nvget");
    add_to_ffi(sc, sync_nvram, "nvsync");
    add_to_ffi(sc, initialize_nvram, "nvinit");
    add_to_ffi(sc, list_nvram, "nvlist");
    add_to_ffi(sc, delete_nvram_entry, "nvdel");
    
}
