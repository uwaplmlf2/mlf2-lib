/*
** $Id: ts.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Interface TinyScheme to TT8/MLF2 libraries
**
*/
#ifndef _TS_H_
#define _TS_H_

/* Convenience macros for foreign function interface */
#define ARG1(args)	pair_car(args)
#define ARG2(args)  	pair_car(pair_cdr(args))
#define ARG3(args)	pair_car(pair_cdr(pair_cdr(args)))
#define HAVE_ARG2(sc,args)	(pair_cdr(args) != sc->NIL)
#define HAVE_ARG3(sc,args)	(pair_cdr(pair_cdr(args)) != sc->NIL)
#define add_symbol(sc, p, name)		scheme_define(sc, sc->global_env,\
                                                  mk_symbol(sc, name), (p))
#define add_to_ffi(sc, func, name)	scheme_define(sc, sc->global_env,\
                                                  mk_symbol(sc, name),\
                                                  mk_foreign_func(sc, func))

void init_tstt8(scheme *sc, int safemode);
void init_tsmlf2(scheme *sc, int safemode);


#endif /* _TS_H_ */
