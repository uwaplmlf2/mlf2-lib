/**@file
 * Driver module for the ISUS (InSitu Ultraviolet Spectrophotometer).
 *
 * $Id: isus.c,v c992eb2d937a 2008/01/09 20:28:31 mikek $
 *
 * This driver has fairly limited capabilities. It can power the device on,
 * start the sampling process, and read the data records. No device
 * configuration is done.
 *
 * IMPORTANT:
 * Before using this driver, insure that the ISUS is configured to operate in
 * triggered mode and to output "full binary" data frames to the serial
 * connection

 */
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "lpsleep.h"
#include "ioports.h"
#include "serial.h"
#include "log.h"
#include "isus.h"

#include "iofuncs.h"

/** Start-up time in milliseconds */
#define WARM_UP_MSECS	32000L
/** Sampling time in milliseconds */
#define ISUS_SAMPLE_MS	23000L

static int serial_dev = -1;
static long init_time;
static unsigned long t_sample;
static unsigned long t_start;

static void
isus_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
}

/**
 * Open connection to the device and initialize. Errors are logged.
 *
 *
 * @return 1 if successful, otherwise 0.
 */
int
isus_init(void)
{
    if(serial_dev >= 0)
	goto already_open;
    
    if((serial_dev = serial_open(ISUS_DEVICE, 0, ISUS_BAUD)) < 0)
    {
	log_error("isus", "Cannot open serial device (code = %d)\n",
		  serial_dev);
	return 0;
    }
    
    init_time = MilliSecs();
    t_sample = 0;
    
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode. This will allow the device to remain
    ** open across calls to lpsleep.
    */
    add_after_hook("ISUS-reopen", isus_reopen);

 already_open:
    return 1;
}

/**
 * Shutdown the device interface and power off the device.
 */
void
isus_shutdown(void)
{
    if(serial_dev >= 0)
    {
	serial_close(serial_dev);
	serial_dev = -1;
    }
    
}


/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
isus_dev_ready(void)
{
    return (serial_dev >= 0) && ((MilliSecs() - init_time) > WARM_UP_MSECS);
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
isus_test(void)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

/**
 * Trigger the device to start the sampling process.  Data frames will be
 * written to the serial line in binary.  The TPU-uart buffer is sized large
 * enough to fit the two data frames (light and dark spectra) and the 
 * standard diagnostic output.
 *
 */
int
isus_start(void)
{
    serial_inflush(serial_dev);

    if(serial_chat(serial_dev, "x", "'g'", 2L) != 1)
    {
	log_error("isus", "No response to wakeup\n");
	return 0;
    }

    if(serial_chat(serial_dev, "g", "Charging", 2L) != 1)
    {
	log_error("isus", "No response to trigger\n");
	return 0;
    }
    
    t_sample = MilliSecs();
    t_start = RtcToCtm();
    
    
    /*
    ** All subsequent output from the device will be buffered and
    ** parsed by isus_read_data.
    */

    return 1;
}

/**
 * Test whether sampling process is done.
 *
 * @return true if sampling process is done.
 */
int
isus_data_ready(void)
{
    return (MilliSecs() - t_sample) >= ISUS_SAMPLE_MS;
}

/**
 * Return the sample start time.
 *
 */
unsigned long
isus_start_time(void)
{
    return t_start;
}

/**
 * Read a data sample.
 *
 * @param  idp  pointer to returned data.
 * @param  timeout  read timeout in ms.
 *
 * @return bitmask listing the data frames read (should be 0x03)
 */
int
isus_read_data(IsusData *idp, long timeout)
{
    unsigned long	t;
    int			c, frames, i;
    long		n;
    unsigned char	hdr[ISUS_HDRLEN];
    enum {ST_START=0,
	  ST_HDR_CHK,
	  ST_READ_DARK,
	  ST_READ_LIGHT} state;

    memset(idp, 0, sizeof(IsusData));
    
    memcpy(&idp->dark[0], ISUS_DARK_HEADER, ISUS_HDRLEN);
    memcpy(&idp->light[0], ISUS_LIGHT_HEADER, ISUS_HDRLEN);

    
    frames = 0;
    state = ST_START;
    i = 0;
    t = MilliSecs();
    while(frames != ISUS_BOTH_FRAMES && 
	  (c = sgetc_timed(serial_dev, t, timeout)) >= 0)
    {
	switch(state)
	{
	    case ST_START:
		i = 0;
		memset(hdr, 0, sizeof(hdr));
		state = ST_HDR_CHK;
		hdr[i++] = c;
		break;
	    case ST_HDR_CHK:
		if(i == ISUS_HDRLEN)
		{
		    memmove(&hdr[0], &hdr[1], i-1);
		    hdr[i-1] = c;
		}
		else
		    hdr[i++] = c;
		if(!memcmp(hdr, ISUS_DARK_HEADER, ISUS_HDRLEN))
		    state = ST_READ_DARK;
		else if(!memcmp(hdr, ISUS_LIGHT_HEADER, ISUS_HDRLEN))
		    state = ST_READ_LIGHT;
		break;
	    case ST_READ_DARK:
		log_event("Reading dark data frame\n");
		idp->dark[ISUS_HDRLEN] = c;
		n = serial_read(serial_dev, &idp->dark[ISUS_HDRLEN+1],
				ISUS_BINARY_FULL_SIZE-1) + 1;
		log_event("Read %ld bytes\n", n);
	    
		if(n != ISUS_BINARY_FULL_SIZE)
		    log_error("isus", "Error reading dark data\n");
		frames |= ISUS_DARK_FRAME;
		state = ST_START;
		break;
	    case ST_READ_LIGHT:
		log_event("Reading light data frame\n");
		idp->light[ISUS_HDRLEN] = c;
		n = serial_read(serial_dev, &idp->light[ISUS_HDRLEN+1],
				ISUS_BINARY_FULL_SIZE-1) + 1;
		log_event("Read %ld bytes\n", n);
	    
		if(n != ISUS_BINARY_FULL_SIZE)
		    log_error("isus", "Error reading light data\n");
		frames |= ISUS_LIGHT_FRAME;
		state = ST_START;
		break;
	}
	
    }
    
    
    return frames;
}

