/*****************************************************************************
** $Id: ad7714.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
**  Module: ad7714.c             Programmer: Russ Light       Date: 02/21/96  *
**                                                                            *
**  Description:                                                              *
**    This module contains a collection of functions to setup and operate the *
**  Virtual Mooring AD7716 A/D converter used to measure the pressure         *
**  transducer.   The TT8 interface to the AD7716 is via the QSPI serial      *
**  interface.  In addition, the following signals are also used:             *
**                                                                            *
**  PCS1(output) - chip select (pin 19, CS), active low                       *
**  TPU15(input)  - data ready (pin 20, DRDY), active low                     *
**                                                                            *
**  The DRDY line goes low when a conversion is completed by the AD7714 or    *
**  when it has completed a calibration.                                      *
**                                                                            *
**  Since the pressure and A/D circuit is power cycled during VM operation    *
**  the AD7714 is self-calibrated for each measurement.  The self-calibration *
**  operation is performed in three steps by the AD7714:                      *
**                                                                            *
**  1) zero-scale calibration                                                 *
**  2) full-scale calibration                                                 *
**  3) conversion of analog pressure channel                                  *
**                                                                            *
**  Each of these steps is automatically performed when a self-calibration    *
**  cycle is initiated.                                                       *
**                                                                            *
**  Once the AD7714 circuit is powered up the following steps must be         *
**  completed:                                                                *
**                                                                            *
**  1) Delay 10mS to allow AD7714 oscillator to stabilize                     *
**  2) Configure the QSPI for working with the AD7714                         *
**  3) Write to the AD7714 communication register to setup differential       *
**     channel AIN1-AIN2                                                      *
**  4) Write High Filter Register - set up the following parameters:          *
**     Unipolar Operation                                                     *
**     24-bit mode                                                            *
**     Current Boost On - Must be on for gains 8 -128 and Fclk = 2.4576Mhz    *
**     Upper 4 bits of filter word                                            *
**  5) Write Low Filter Register - set up the following parameters:           *
**     Lower 8 bits of filter word                                            *
**  6) Write to the mode register - set up the following parameters:          *
**     Gain = 64                                                              *
**     Filter Syncronization Off                                              *
**     Initiate Self-Calibration                                              *
**  7) Delay 1mS in case DRDY is low (see note below)                         *
**  8) Poll TPU4 line for DRDY until LOW                                      *
**  9) Read the Data Register                                                 *
** 10) Repeat steps 8 and 9 for more conversion samples if required           *
**                                                                            *
**  NOTE on DRDY:                                                             *
**  The AD7714 documenation says "If DRDY is low before (or goes low during)  *
**  the calibration command write to the Mode Register, it may take up to     *
**  one modulation cycle (MCLK IN/128) before DRDY goes high to indicate      *
**  that the calibration is in progress.  Therefore, DRDY should be ignored   *
**  for up to one modulator cycle after the last bit of the calibration       *
**  command is written to the Mode Register."  MCLK in the VM design is       *
**  2.4576Mhz so the length of one modulator cycle is: 52uS                   *
**                                                                            *
**                                                                            *
**  NOTES ON PRESSURE XDCR/AD7714 INTERFACE:                                  *
**  To interface properly with the pressure circuit and the pressure          *
**  transducer (Bourns ST3100, 3000PSIA), the AD7714 is setup as follows:     *
**                                                                            *
**  Software Issues:                                                          *
**     Unipolar                                                               *
**     Fully Differential Input AIN1-AIN2                                     *
**     Gain = 64                                                              *
**     Digital Filter First Notch Frequency (see note below)                  *
**     24-Bit                                                                 *
**     Current Boost On                                                       *
**  Hardware Issues:                                                          *
**     Buffer [Pin 13] = Low (JM13 not installed)                             *
**     POL    [Pin 4]  = High (JM14 installed)                                *
**                                                                            *
**  NOTE on Digital Filter Setting:                                           *
**  The digital filter cut-off frequency is set by a 12-bit value.  This      *
**  filter performs low pass filtering of the sigma-delta modulator output    *
**  and decimation, therefore also setting the output sample rate.  The       *
**  filter response follows a sinc function to the third power.  The freq.    *
**  of the first notch in this response is given by:                          *
**                                                                            *
**        Filter first notch freq = (Fclk in/128)/code                        *
**                                                                            *
**  where Fclk in = 2.4576Mhz                                                 *
**        code = decimal equivalent of 12-bit value                           *
**               (allowable range 19 - 4000)                                  *
**                                                                            *
**  Rearranging the equation in terms of the code value produces the equation *
**                                                                            *
**  The filter first notch frequency is also equal to the output sample       *
**  rate of the converter.  The above equation yields an output sample        *
**  rate range of: 4.8Hz - 1.01Khz.  By rearranging the equation to find an   *
**  expression for code in terms of output sample rate:                       *
**                                                                            *
**         Code = Fclk in/(128 * output_sample_rate)                          *
**                                                                            *
**  The -3dB frequency of the filter is given by the equation:                *
**                                                                            *
**        -3dB Frequency = 0.262 * filter first notch freq                    *
**                                                                            *
**  This equation yeilds a -3dB frequency range of: 1.26Hz - 264.6Hz          *
**                                                                            *
**                                                                            *
**  Due to the digital filter there is a settling time for the filter when    *
**  a step change in the analog input occurs.  During self-calibration the    *
**  AD7714 internally converters a zero scale input, a full scale input,      *
**  and the selected analog channel.  When conversions are done syncronous    *
**  to reseting of the digital filter the settling time will be a minimum     *
**  value of 3 x Output Rate.  Therefore to perform the self-calibration the  *
**  process takes 9 x Output Rate.  Subsequent conversions on the same        *
**  channel will be performed at the output rate.  If a new channel is        *
**  selected the AD7714 will take 3 x Output rate for the conversion.  If     *
**  an external multiplexor were used and therefore not syncronized to the    *
**  reset of the digital filter 4 x Output rate time should be performed      *
**  before reading a sample (VM design does not use an external mux).         *
**                                                                            *
**  Functions:                                                                *
**    cfg_qspi_ad7714()                                                       *
**    reset_ad7714()                                                          *
**    set_mode_ad7714()                                                       *
**    set_filter_ad7714()                                                     *
**    self_cal_ad7714()                                                       *
**    get_data_ad7714()                                                       *
**    read_cfg_ad7714()                                                       *
**    read_cal_ad7714()                                                       *
**                                                                            *
**  Modification Record:                                                      *
**    9/1998 modified by Mike Kenney for MLF2 board                           *
**                                                                            *
*****************************************************************************/

#include	<tt8.h>	    /* Tattletale Model 8 Definitions */
#include	<tat332.h>  /* 68332 Tattletale (7,8) Hardware Definitions */
#include	<sim332.h>  /* 68332 System Integration Module Definitions */
#include	<qsm332.h>  /* 68332 Queued Serial Module Definitions */
#include	<tpu332.h>  /* 68332 Time Processing Unit Definitions */
#include	<dio332.h>  /* 68332 Digital I/O Port Pin Definitions */
#include	<tt8pic.h>  /* Model 8 PIC Parallel Slave Port Definitions */

#include	<tt8lib.h>  /* definitions and prototypes for Model 8 library */
#include	<math.h>
#include	<stddef.h>
#include	<stdlib.h>

#include  "ad7714.h"     /* definitions for AD7714 */

/*
** Macro to wait for the DRDY line to go low and then go high a specified
** number of times.  This indicates that the data sample has been latched.
*/
#define WAIT_FOR_LATCH(n) { register int __i = n; do {\
  while(TPUGetPin(TPU_AD7714_DRDY) == 0);\
  while(TPUGetPin(TPU_AD7714_DRDY) == 1);\
  } while(--__i); }



   /***********************************************************************
   * Defined QSPI basic and last cmd values                               *
   ***********************************************************************/
static 
ushort basic_cmd = (M_CONT  & SET)    /* CS remains asserted after cmd */
	     | (M_BITSE & SET)	/* # of bits set in BITS field of SPCR0 */
	     | (M_DT    & SET)	/* use SPCR1 DTL delay after xfer */
	     | (M_DSCK  & SET)	/* use SPCR1 DSCKL delay, SCK to PCS */
	     | (M_CS3   & SET)	/* PCS3: TT8 Max186 A/D, SET = not enabled */
	     | (M_CS2   & CLR)	/* PCS2: PICpr, CLR == disabled */
	     | (M_CS1   & CLR)	/* PCS1: Enable AD7714, CLR = enabled */
	     | (M_CS0   & SET);	/* PCS0: 2nd Max186 A/D, SET = disabled */

static
ushort last_cmd  = (M_CONT  & CLR)    /* CS un-asserted after last cmd */
	     | (M_BITSE & SET)	/* # of bits set in BITS field of SPCR0 */
	     | (M_DT    & SET)	/* use SPCR1 DTL delay after xfer */
	     | (M_DSCK  & SET)	/* use SPCR1 DSCKL delay, SCK to PCS */
	     | (M_CS3   & SET)	/* PCS3: TT8 Max186 A/D, SET = not enabled */
	     | (M_CS2   & CLR)	/* PCS2: PICpr, CLR == disabled */
	     | (M_CS1   & CLR)	/* PCS1: Enable AD7714, CLR = enabled */
	     | (M_CS0   & SET);	/* PCS0: 2nd Max186 A/D, SET = disabled */


/**
 * cfg_qspi_ad7714 - initialize QSPI interface.
 *
 *  This function initializes the QSPI for operating with the AD7714 a/d    
 *  converter.  The routine initializes the QSM Pin Control Registers and     
 *  control regsisters. The command queue and transmit RAM need to be set     
 *  up for the particluar operation to be performed.  In addition, the        
 *  folllowing parameters should be set for each operation.
 *                                                                            
 *  %_SPCR0->BITS  - bits per transfer 
 *
 *  %_SPCR2->ENDQP - last cmd queue address
 *
 *  %_SPCR2->NEWQP - first cmd queue address
 *                                                                            
 */
void
cfg_qspi_ad7714(void)
{
    
    /***********************************************************************
     * QPDR = PORTQS: Address = 0xC15 (8 bit reg)                           *
     * Port QS Data Register                                                *
     * Determines acutal input or output value of a QSM port pin, if pin    *
     * is defined in QPAR as general-purpose I/O.                           *
     ***********************************************************************/
    *QPDR |= (M_PCS1 | M_SCK | M_MISO);
   
   
    /***********************************************************************
     * QDDR = DDRQS: Address = 0xC17 (8 bit reg)                            *
     * Port QS Data Direction Register                                      *
     * Determines QSM pins as either inputs or outputs.                     *
     *                                                                      *
     ***********************************************************************/

    *QDDR = M_PCS3 | M_PCS2 | M_PCS1 | M_PCS0 | M_SCK | M_MOSI;
   
   
    /***********************************************************************
     * QPAR = PQSPAR: Address = 0xC16 (8 bit reg)                           *
     * Port QS Pin Assignment Register                                      *
     * Determines whether pins are assigned as general-purpose or to the    *
     * QSPI. SET = QSPI                                                     *
     *                                                                      *
     ***********************************************************************/

    *QPAR |= (M_PCS1 | M_MOSI | M_MISO);
   
    /***********************************************************************
     * SPCR0 = SPCR0: Address = 0xC18 (16 bit reg)                          *
     * QSPI Control Register 0                                              *
     * Configures QSPI for master/slave, bits per transfer, clock           *
     * polarity, clock phase, baud rate, etc.                               *
     * Configure before enabing QSPI.                                       *
     ***********************************************************************/
    _SPCR0->MSTR = SET;		/* master mode */
    _SPCR0->WOMQ = CLR;		/* outputs have normal MOS drivers */
   
    /* This value needs to be changed for specific operation */
    _SPCR0->BITS = 0x08;		/* 8 bits per transfer */

    _SPCR0->CPOL = SET;		/* inactive state of SCK = high
   				   AD7714 POL pin = High
   				   See page 5-52 MC68332 User's Manual for
   				   timing waveforms, Figure 5-14 */
    _SPCR0->CPHA = SET;		/* data read/sent on rising edge of SCK */
   
    _SPCR0->BAUD = 0x08;         /* Baud Rate = System Clock/(2*value)
			   	   For VM, System Clock/2 = 8Mhz 
			   	   value = 8Mhz/Baud Rate  
                                   For value = 16, Baud = 500Khz 
                                   For value =  8, Baud = 1 Mhz */
   
    /***********************************************************************
     * SPCR1 = SPCR1: Address = 0xC1A (16 bit reg)                          *
     * QSPI Control Register 1                                              *
     * Enables the QSPI and specifies timing delays.                        *
     * Configure before enabling QSPI.                                      *
     ***********************************************************************/
    _SPCR1->SPE	 = CLR;		/* QSPI disable - still initializing */
    _SPCR1->DSCKL = 0x05;	/* delay from chip select until leading
   				   edge of SCK (1-127):
   				   delay  = value/system clock
   				          = 5/16Mhz = 312.5nS */
    _SPCR1->DTL	 = 0x05;	/* delay after each serial data transfer
   				   for subsequent QSPI RAM command (1-255):
   				   delay = (32 * value)/system clock
				   = (32 * 5)/16Mhz = 10uS */
   

    /***********************************************************************
     * SPCR2 = SPCR2: Address = 0xC1C (16 bit reg)                          *
     * QSPI Control Register 2                                              *
     * Queue pointers, wraparound mode, interrupt enable.                   *
     *                                                                      *
     ***********************************************************************/
    _SPCR2->SPIFIE = CLR;	/* QSPI interrupts disabled */
    _SPCR2->WREN	  = CLR;	/* wraparound mode disabled */
    _SPCR2->WRTO	  = CLR;	/* wrap to pointer address $0 */
   
    /* These values need to be changed for specific operations */
    _SPCR2->ENDQP  = 0x00;	/* set last queue address - 4 bits */
    _SPCR2->NEWQP  = 0x00;	/* set first queue address  - 4 bits */
   
    /***********************************************************************
     * SPCR3 = SPCR3: Address = 0xC1E (8 bit reg)                           *
     * QSPI Control Register 3                                              *
     * Loop mode enable, halt and mode fault interrupt enable, halt control *
     * bit.                                                                 *
     ***********************************************************************/
    _SPCR3->LOOPQ = CLR;		/* feedback path disabled */
    _SPCR3->HMIE  = CLR;		/* HALTA and MODF interrupts disabled */
    _SPCR3->HALT  = CLR;		/* halt not enabled */

}


/**
 * reset_ad7714 - reset the AD7714 serial interface.
 *
 *  This function resets the AD7714 serial interface.  32 1's are written
 *  to the device which ensures that a interface reset occurs.  The part is
 *  then in a known state and expecting a write to the communications
 *  register.
 *
 */

void
reset_ad7714(void)
{

    /***********************************************************************
   * Set parameters in QSPI control registers for this operation          *
   ***********************************************************************/
    _SPCR0->BITS = 0x10;		/* 16 bits per transfer */   
    _SPCR2->NEWQP  = 0x00;	/* set first queue address */
    _SPCR2->ENDQP  = 0x01;	/* send 2 cmds, each cmd sends 16 one's
   				   to AD7714 to reset interface */

   /***********************************************************************
   * Load first command to QSPI command queue                             *
   ***********************************************************************/
    SPICMD[0] = basic_cmd;
    SPIXMT[0] = 0x0FFFF;      		/* load all one's */

   /***********************************************************************
   * Load second command to QSPI command queue                            *
   ***********************************************************************/
    SPICMD[1] = last_cmd;
    SPIXMT[1] = 0x0FFFF;      		/* load all one's */

   /***********************************************************************
   * Perform AD7714 Interface Reset                                       *
   ***********************************************************************/
    _SPSR->SPIF = CLR;			/* ensure done flag is cleared */
    _SPCR1->SPE = SET;			/* send cmd */
    while (_SPSR->SPIF == CLR);		/* wait for xfer to complete */

}


/**
 * set_mode_ad7714 - set mode register
 * @channel: AD7714 channel
 * @mode: mode register value
 *
 *  This function loads the QSPI command queue and transmit RAM with the
 *  AD7714 commands to set the mode register.  @channel must be set to
 *  one of the %AIN* constants in ad7714.h.  @mode is formed by a 
 *  bitwise OR of %NORMAL_MODE or %SELF_CAL_MODE along with one of the
 *  gain value constants, %GAIN_* from ad7714.h.
 */
void
set_mode_ad7714(unsigned short channel, unsigned short mode)
{

   /***********************************************************************
   * Set parameters in QSPI control registers for this operation          *
   ***********************************************************************/
   _SPCR0->BITS = 0x08;		/* 8 bits per transfer */   
   _SPCR2->NEWQP  = 0x00;	/* set first queue address */
   _SPCR2->ENDQP  = 0x01;	/* send 2 cmds to set AD7714 mode reg */
   
   /***********************************************************************
   * Select Mode Register - Cmd 1                                         *
   ***********************************************************************/
   SPICMD[0] = basic_cmd;
   SPIXMT[0] = MODE_REG | WRITE | channel; /* load write mode register cmd */

   /***********************************************************************
   * Load Mode Register - Cmd 2                                           *
   ***********************************************************************/
   SPICMD[1] = last_cmd;
   SPIXMT[1] = mode;			/* load mode register value */
   
   /***********************************************************************
   * Load AD7714 Mode Register                                            *
   ***********************************************************************/
   _SPSR->SPIF = CLR;			/* ensure done flag is cleared */
   _SPCR1->SPE = SET;			/* send cmd */
   while (_SPSR->SPIF == CLR);		/* wait for xfer to complete */
   
}


/**
 * set_filter_ad7714 - set the AD7714 filter registers
 * @filter: register value
 *
 *  This function sets the AD7714 filter registers.  There are two 8 bit
 *  filter registers on the AD7714; high and low.  12 bits split accross the
 *  registers specify the filter operation and in turn the output sample
 *  rate.  Three bits in the 
 *  high filter register specify biploar/unipolar mode, word length, and 
 *  current boost.
 *
 *  The filter value is passed as an argument to the function and specifies
 *  the first notch of the filter response and output sample rate by the
 *  following formula:
 *
 *        Filter first notch freq = (Fclk in/128)/@filter
 *
 *  where Fclk in = 2.4576Mhz and the allowable range of @filter is 19-4000.
 *
 */
void
set_filter_ad7714(unsigned int filter)
{
    unsigned short low_filter,
      high_filter;

   /***********************************************************************
   * Form two filter bytes from argument                                  *
   ***********************************************************************/
    low_filter = (unsigned short) filter & 0x0FF;
    high_filter = (unsigned short) ((filter & 0x0F00) >> 8);
    high_filter = high_filter | AD7714_OP_MODE;


    /***********************************************************************
   * Set parameters in QSPI control registers for this operation          *
   ***********************************************************************/
    _SPCR0->BITS = 0x08;		/* 8 bits per transfer */   
    _SPCR2->NEWQP  = 0x00;	/* set first queue address */
    _SPCR2->ENDQP  = 0x03;	/* send 4 cmds to setup AD7714 filter regs */
   
    /***********************************************************************
   * Select High Filter Register - Cmd 1                                  *
   ***********************************************************************/
    SPICMD[0] = basic_cmd;
    SPIXMT[0] = FILTER_H_REG | WRITE | AIN1_AIN2; /* load write filter high
   						    byte cmd - channel value
   						    for looks only, only
   						    one filter register for
   						    all channels */

   /***********************************************************************
   * Load High Filter Register - Cmd 2                                    *
   ***********************************************************************/
    SPICMD[1] = basic_cmd;
    SPIXMT[1] = high_filter;		/* load high filter value */

   /***********************************************************************
   * Select Low Filter Register - Cmd 3                                   *
   ***********************************************************************/
    SPICMD[2] = basic_cmd;
    SPIXMT[2] = FILTER_L_REG | WRITE | AIN1_AIN2; /* load write filter low
   						    byte cmd - channel value
   						    for looks only, only
   						    one filter register for
   						    all channels */

   /***********************************************************************
   * Load Low Filter Register - Cmd 4                                     *
   ***********************************************************************/
    SPICMD[3] = last_cmd;
    SPIXMT[3] = low_filter;		/* load low filter value */

   /***********************************************************************
   * Load AD7714 Filter Regsisters                                        *
   ***********************************************************************/
    _SPSR->SPIF = CLR;			/* ensure done flag is cleared */
    _SPCR1->SPE = SET;			/* send cmd */
    while (_SPSR->SPIF == CLR);		/* wait for xfer to complete */

}


/**
 * self_cal_ad7714 - perform AD7714 self-calibration.
 * @channel: channel code
 * @gain: gain code
 *
 *  This function performs a self-calibration operation in the AD7714.
 *  The AD7714 performs the self-calibration autonomously.  Three steps
 *  occur during the operation; zero-scale cal, full scale cal, and a input
 *  sample convertion.  This function uses its own code to read the input
 *  sample instead of get_data_ad7714().  This is done to optimize
 *  getting the first sample which is available at the end of the self-cal
 *  operation.  A self-calibration operation requires 9 x Output Sample Rate
 *  and includes obtaining the first sample.  Subsequent samples are
 *  available at the Output Sample Rate.  Since the status of DRDY is known
 *  when self-cal is invoked only a test for this line going low is
 *  required.  For other reads of sample data which may be asyncronous to
 *  AD7714 operation the get_data_ad7714() function is used.  This
 *  function syncronizes the the AD7714 by waiting for the DRDY to go from
 *  a high to low transition thus ensuring the latest sample and avoid
 *  reading data when an update may occur.
 *
 *  @channel must be set to one of the %AIN* constants in ad7714.h and @gain
 *  must be set to one of the gain value constants, %GAIN_*.  The return value
 *  is the 24-bit sample generated by the self-cal.
 *
 */
unsigned long int
self_cal_ad7714(unsigned short channel, unsigned short gain)
{
   unsigned long int sample;		/* AD7714 first sample */
   
   unsigned char first_byte,		/* sample bytes */
   		second_byte,
   		third_byte;
   unsigned long msb,
   		 lsb1,
   		 lsb2;

   /***********************************************************************
   * Initiate Self-Calibration                                            *
   ***********************************************************************/
   set_mode_ad7714(channel, SELF_CAL_MODE | gain);

   /***********************************************************************
   * Delay                                                                *
   ***********************************************************************/
   LMDelay(2000);			/* TT8 function call to delay
   					   1 mS to ensure DRDY goes HIGH
   					   after initiating self-cal */

  /***********************************************************************
   * Wait for AD7714 to complete self-cal and convert first sample        *
   ***********************************************************************/
    while(TPUGetPin(TPU_AD7714_DRDY) == 1);        /* wait for DRDY to go low */

   /***********************************************************************
   * Set parameters in QSPI control registers for this operation          *
   ***********************************************************************/
   _SPCR0->BITS = 0x08;		/* 8 bits per transfer */   
   _SPCR2->NEWQP  = 0x00;	/* set first queue address */
   _SPCR2->ENDQP  = 0x03;	/* send 4 cmds to read sample from AD7714 */
   
   /***********************************************************************
   * Select Data Register - Cmd 1                                         *
   ***********************************************************************/
   SPICMD[0] = basic_cmd;
   SPIXMT[0] = DATA_REG | READ | channel; /* load read data register cmd */

   /***********************************************************************
   * Read High Sample Byte - Cmd 2                                        *
   ***********************************************************************/
   SPICMD[1] = basic_cmd;
   SPIXMT[1] = 0x0000;     	/* load cmd RAM with nothing just for grins */

   /***********************************************************************
   * Read Middle Sample Byte - Cmd 3                                      *
   ***********************************************************************/
   SPICMD[2] = basic_cmd;
   SPIXMT[2] = 0x0000;     	/* load cmd RAM with nothing just for grins */

   /***********************************************************************
   * Read Low Sample Byte - Cmd 4                                         *
   ***********************************************************************/
   SPICMD[3] = last_cmd;
   SPIXMT[3] = 0x0000;     	/* load cmd RAM with nothing just for grins */
	        
   /***********************************************************************
   * Enable QSPI to read sample from AD7714                               *
   ***********************************************************************/
   _SPSR->SPIF = CLR;			/* ensure done flag is cleared */
   _SPCR1->SPE = SET;			/* read third byte */
   while (_SPSR->SPIF == CLR);		/* wait for xfer to complete */

   /***********************************************************************
   * Read sample from QSPI receive RAM                                    *
   ***********************************************************************/
   first_byte = (unsigned char) *(SPIRCV+1);	/* read bytes for 24-bit */
   second_byte = (unsigned char) *(SPIRCV+2);
   third_byte = (unsigned char) *(SPIRCV+3);

   msb = ((ulong) first_byte) << 16;		/* combine bytes */
   lsb1 = ((ulong) second_byte) << 8;
   lsb2 = (ulong) third_byte;
   
   sample = msb + lsb1 + lsb2;
   
   return(sample);
}


/**
 *  get_data_ad7714 - read a sample from the AD7714
 *  @channel: channel code
 *  @sync: if non-zero, wait for DRDY transition.
 *
 *  This functions reads the AD7714 to obtain a 24-bit converted sample.
 *  The QSPI command queue is setup to read the 24-bit sample from the
 *  AD7714.  @channel is the channel code %AIN_* from ad7714.h.  If @sync
 *  is non-zero, we synchronize to the AD7714 converter by waiting for
 *  the DRDY line to go from a high to low transition.  This ensures the
 *  sample read from the AD7714 was NOT updated during the read operation.
 *  The QSPI is then enabled and the 24-bit sample is read.  The 24-bit
 *  sample is transfered by the QSPI in three 8 bit transfers.  The 3 bytes
 *  are combined into a single long int and returned.
 *
 */
  
unsigned long
get_data_ad7714(unsigned short channel, int sync)

{
   unsigned long int sample;		/* AD7714 sample */
      
   unsigned char first_byte,		/* sample bytes */
   		second_byte,
   		third_byte;
   unsigned long msb,
   		 lsb1,
   		 lsb2;

   /***********************************************************************
   * Synchronize with AD7714 for sample read                               *
   * only need to do it in pre-launch mode, in launched mode, we are
   * synchronized to the falling edge of DRDY
   ***********************************************************************/
    if(sync)
	WAIT_FOR_LATCH(1);

   /***********************************************************************
   * Set parameters in QSPI control registers for this operation          *
   ***********************************************************************/
   _SPCR0->BITS = 0x08;		/* 8 bits per transfer */   
   _SPCR2->NEWQP  = 0x00;	/* set first queue address */
   _SPCR2->ENDQP  = 0x03;	/* send 4 cmds to read sample from AD7714 */
   
   /***********************************************************************
   * Select Data Register - Cmd 1                                         *
   ***********************************************************************/
   SPICMD[0] = basic_cmd;
   SPIXMT[0] = DATA_REG | READ | channel; /* load read data register cmd */

   /***********************************************************************
   * Read Sample - Cmd 2,3,4                                              *
   ***********************************************************************/
   SPICMD[1] = basic_cmd;	/* read upper 8 bits */
   SPIXMT[1] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   SPICMD[2] = basic_cmd;	/* read middle 8 bits */
   SPIXMT[2] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   SPICMD[3] = last_cmd;	/* read lower 8 bits */
   SPIXMT[3] = 0x0000;     	/* load cmd RAM with nothing just for grins */
	        
   /***********************************************************************
   * Enable QSPI and wait for completion                                  *
   ***********************************************************************/
   _SPSR->SPIF = CLR;			/* ensure done flag is cleared */
   _SPCR1->SPE = SET;			/* read third byte */
   while (_SPSR->SPIF == CLR);		/* wait for xfer to complete */
   
   /***********************************************************************
   * Read sample from QSPI receive RAM                                    *
   ***********************************************************************/
   first_byte = (unsigned char) *(SPIRCV+1);
   second_byte = (unsigned char) *(SPIRCV+2);
   third_byte = (unsigned char) *(SPIRCV+3);

   /***********************************************************************
   * Combined bytes into single 24-bit value                              *
   ***********************************************************************/
   msb = ((ulong) first_byte) << 16;
   lsb1 = ((ulong) second_byte) << 8;
   lsb2 = (ulong) third_byte;
   
   sample = msb + lsb1 + lsb2;
   return(sample);
}

/**
 * switch_channel_ad7714 - handle channel switch transients
 * @channel: channel code.
 *
 * This is a convenience function to handle the transient which occurs
 * when the AD7714 begins reading from a new channel.  It sends a READ
 * DATA command to the device and then monitors DRDY to wait three
 * sample times, a sufficient delay for the transient to die out.  This
 * function should be called before the first call to get_data_ad7714()
 * on a new channel.
 */
void
switch_channel_ad7714(unsigned short channel)
{

   /***********************************************************************
   * Synchronize with AD7714 for sample read                               *
   ***********************************************************************/
    WAIT_FOR_LATCH(1);


   /***********************************************************************
   * Set parameters in QSPI control registers for this operation          *
   ***********************************************************************/
   _SPCR0->BITS = 0x08;		/* 8 bits per transfer */   
   _SPCR2->NEWQP  = 0x00;	/* set first queue address */
   _SPCR2->ENDQP  = 0x03;	/* send 4 cmds to read sample from AD7714 */
   
   /***********************************************************************
   * Select Data Register - Cmd 1                                         *
   ***********************************************************************/
   SPICMD[0] = basic_cmd;
   SPIXMT[0] = DATA_REG | READ | channel; /* load read data register cmd */

   /***********************************************************************
   * Read Sample - Cmd 2,3,4                                              *
   ***********************************************************************/
   SPICMD[1] = basic_cmd;	/* read upper 8 bits */
   SPIXMT[1] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   SPICMD[2] = basic_cmd;	/* read middle 8 bits */
   SPIXMT[2] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   SPICMD[3] = last_cmd;	/* read lower 8 bits */
   SPIXMT[3] = 0x0000;     	/* load cmd RAM with nothing just for grins */
	        
   /***********************************************************************
   * Enable QSPI and wait for completion                                  *
   ***********************************************************************/
   _SPSR->SPIF = CLR;			/* ensure done flag is cleared */
   _SPCR1->SPE = SET;			/* read third byte */
   while (_SPSR->SPIF == CLR);		/* wait for xfer to complete */
   
   /* Now wait 3 sample times for channel switch to take effect */
   WAIT_FOR_LATCH(3);
}



/**
 * read_cfg_ad7714 - read configuration registers.
 * @high_filter: pointer to upper byte of filter register
 * @low_filter: pointer to lower byte of filter register
 * @mode_reg: pointer mode register
 *
 *    This function reads the configuration registers of the AD7714.  The
 *  high filter, low filter, and mode registers are read.  The routine
 *  setups the QSPI command queue and executes it.  The AD7714 values are
 *  returned in the passed arguments.
 *
 */

void
read_cfg_ad7714(unsigned char *high_filter, unsigned char *low_filter, 
		unsigned char *mode_reg)

{
   

   /***********************************************************************
   * Set parameters in QSPI control registers for this operation          *
   ***********************************************************************/
   _SPCR0->BITS = 0x08;		/* 8 bits per transfer */   
   _SPCR2->NEWQP  = 0x00;	/* set first queue address */
   _SPCR2->ENDQP  = 0x05;	/* send 6 cmds to read AD7714 cfg registers */
   
   /***********************************************************************
   * Send read high filter cmd - Cmd 1                                    *
   ***********************************************************************/
   SPICMD[0] = basic_cmd;
   SPIXMT[0] = FILTER_H_REG | READ | AIN1_AIN2; /* load read high filter
   						   cmd, channel value
  						   for looks only, only
   						   one filter register for
   						   all channels */

   /***********************************************************************
   * Read High Filter - Cmd 2                                             *
   ***********************************************************************/
   SPICMD[1] = basic_cmd;
   SPIXMT[1] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   /***********************************************************************
   * Send read low filter cmd - Cmd 3                                     *
   ***********************************************************************/
   SPICMD[2] = basic_cmd;
   SPIXMT[2] = FILTER_L_REG | READ | AIN1_AIN2; /* load read low filter
   						   cmd, channel value
  						   for looks only, only
   						   one filter register for
   						   all channels */

   /***********************************************************************
   * Read Low Filter - Cmd 4                                              *
   ***********************************************************************/
   SPICMD[3] = basic_cmd;
   SPIXMT[3] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   /***********************************************************************
   * Send read mode reg cmd - Cmd 5                                       *
   ***********************************************************************/
   SPICMD[4] = basic_cmd;
   SPIXMT[4] = MODE_REG | READ | AIN1_AIN2; 	/* load read mode register
   						   cmd, channel value
  						   for looks only, only
   						   one filter register for
   						   all channels */

   /***********************************************************************
   * Read mode register - Cmd 6                                           *
   ***********************************************************************/
   SPICMD[5] = last_cmd;
   SPIXMT[5] = 0x0000;     	/* load cmd RAM with nothing just for grins */

   /***********************************************************************
   * Enable QSPI and wait for completion                                  *
   ***********************************************************************/
   _SPSR->SPIF = CLR;			/* ensure done flag is cleared */
   _SPCR1->SPE = SET;			/* read third byte */
   while (_SPSR->SPIF == CLR);		/* wait for xfer to complete */

   *(high_filter) = (unsigned char) *(SPIRCV+1);
   *(low_filter) = (unsigned char) *(SPIRCV+3);
   *(mode_reg) = (unsigned char) *(SPIRCV+5);

}


/**
 * read_cal_ad7714 - read calibration registers
 * @channel: channel code.
 * @zero_scale: pointer to zero-scale calibration value
 * @full_scale: pointer to full-scale calibration value
 *
 *    This function reads the calibration registers in the AD7714.  There are
 *  two 24-bit registers: zero-scale and full-scale.  When reading these
 *  registers the AD7714 loses its own access to the registers and can
 *  produce a bad output sample on the next output.  The function preforms
 *  a dummy read of the data register after reading the calibration registers
 *  to prevent other functions from reading a corrupted sample.  The
 *  calibration register values are returned to the calling function via
 *  the function arguments.
 *
 */

void
read_cal_ad7714(unsigned short channel, unsigned long *zero_scale, 
		unsigned long *full_scale)

{
   unsigned char first_byte,		/* calibration bytes */
   		second_byte,
   		third_byte;
   unsigned long msb,
   		 lsb1,
   		 lsb2;
   
   unsigned long int dummy_sample;	/* AD7714 sample */
   
   

   /***********************************************************************
   * Set parameters in QSPI control registers for this operation          *
   ***********************************************************************/
   _SPCR0->BITS = 0x08;		/* 8 bits per transfer */   
   _SPCR2->NEWQP  = 0x00;	/* set first queue address */
   _SPCR2->ENDQP  = 0x07;	/* send 8 cmds to AD7714 cal registers */
   
   /***********************************************************************
   * Send read zero-scale calibration register - Cmd 1                    *
   ***********************************************************************/
   SPICMD[0] = basic_cmd;
   SPIXMT[0] = ZERO_SCALE_REG | READ | channel;	/* load read zero-scale
   						    calibration reg cmd */

   /***********************************************************************
   * Read zero-scale calibration register - Cmd 2,3,4                     *
   ***********************************************************************/
   SPICMD[1] = basic_cmd;	/* read upper 8 bits */
   SPIXMT[1] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   SPICMD[2] = basic_cmd;	/* read middle 8 bits */
   SPIXMT[2] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   SPICMD[3] = basic_cmd;	/* read lower 8 bits */
   SPIXMT[3] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   /***********************************************************************
   * Send read full-scale calibration register - Cmd 5                    *
   ***********************************************************************/
   SPICMD[4] = basic_cmd;
   SPIXMT[4] = FULL_SCALE_REG | READ | channel;	/* load read full-scale
   						    calibration reg cmd */

   /***********************************************************************
   * Read full-scale calibration register - Cmd 6,7,8                     *
   ***********************************************************************/
   SPICMD[5] = basic_cmd;	/* read upper 8 bits */
   SPIXMT[5] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   SPICMD[6] = basic_cmd;	/* read middle 8 bits */
   SPIXMT[6] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   SPICMD[7] = last_cmd;	/* read lower 8 bits */
   SPIXMT[7] = 0x0000;     	/* load cmd RAM with nothing just for grins */
   
   /***********************************************************************
   * Enable QSPI and wait for completion                                  *
   ***********************************************************************/
   _SPSR->SPIF = CLR;			/* ensure done flag is cleared */
   _SPCR1->SPE = SET;			/* read third byte */
   while (_SPSR->SPIF == CLR);		/* wait for xfer to complete */

   /***********************************************************************
   * Read zero-scale calibration values from QSPI receive RAM             *
   ***********************************************************************/
   first_byte = (unsigned char) *(SPIRCV+1);
   second_byte = (unsigned char) *(SPIRCV+2);
   third_byte = (unsigned char) *(SPIRCV+3);
   
   msb = ((ulong) first_byte) << 16;	/* create single 24-bit value */
   lsb1 = ((ulong) second_byte) << 8;
   lsb2 = (ulong) third_byte;
   
   *(zero_scale) = msb + lsb1 + lsb2;

   /***********************************************************************
   * Read full-scale calibration values from QSPI receive RAM             *
   ***********************************************************************/
   first_byte = (unsigned char) *(SPIRCV+5);
   second_byte = (unsigned char) *(SPIRCV+6);
   third_byte = (unsigned char) *(SPIRCV+7);
   
   msb = ((ulong) first_byte) << 16;	/* create single 24-bit value */
   lsb1 = ((ulong) second_byte) << 8;
   lsb2 = (ulong) third_byte;
   
   *(full_scale) = msb + lsb1 + lsb2;
      
   /***********************************************************************
   * Read the data register to clear possible corrupted value             *
   ***********************************************************************/
   dummy_sample = get_data_ad7714(channel, 1);

}

