#ifndef _RECBUF_H_
#define _RECBUF_H_

typedef struct recbuf {
    char        *buf;
    int         bufsize;
    char        **field;
    int         maxfields;
    int         nfields;
} RecBuf;

void recbuf_init(RecBuf *rbp, char *buf, int n, char **fbuf, int nf);
int recbuf_split(RecBuf *rbp, char sep);

#endif
