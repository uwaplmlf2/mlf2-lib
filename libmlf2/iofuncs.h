/**@file
** Serial I/O convenience functions.
**
** $Id: iofuncs.h,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
**
**
** @sa serial.c
*/
#ifndef _IOFUNCS_H_
#define _IOFUNCS_H_

/**
 * Read the next character from a serial port.
 *
 * @param  desc  serial port descriptor.
 * @return next character.
 */
static __inline__ int sgetc(int desc)
{
    unsigned char   c;
    return (serial_read(desc, (char*)&c, (size_t)1) == 1) ? (int)c : -1;
}

/**
 * Read the next character from a serial port.
 * Variant of sgetc that allows for time-outs. The serial port must
 * be set to non-blocking mode before calling this function.
 *
 * @param  desc  serial port descriptor.
 * @param  start  timer start time in milliseconds (output of MilliSecs())
 * @param  timeout  timer duration in milliseconds.
 * @return next character or -1 on time-out.
 */
static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char   c;

    while(serial_read(desc, (char*)&c, 1L) != 1L)
    {
        if((MilliSecs() - start) >= timeout)
            return -1;
    }
    return ((int) c)&0xff;
}

/**
 * Write a character to a serial port.
 *
 * @param  desc  serial port descriptor.
 * @param  out  output character.
 */
static __inline__ void sputc(int desc, int out)
{
    char    c = out;
    serial_write(desc, &c, 1L);
}

/**
 * Write a character array (string) to a serial port.
 *
 * @param  desc  serial port descriptor.
 * @param  str  character array.
 * @param  n  array length.
 */
static __inline__ void sputs(int desc, unsigned char *str, int n)
{
    while(n--)
    {
        serial_write(desc, (char*)str, 1L);
        str++;
    }
}

/**
 * Write a null (zero) terminated (string) to a serial port.
 *
 * @param  desc  serial port descriptor.
 * @param  str  null-terminated string.
 */
static __inline__ void sputz(int desc, const char *str)
{
    while(*str)
    {
        serial_write(desc, (char*)str, 1L);
        str++;
    }
}


#endif /* _IOFUNCS_H_ */
