/*
** arch-tag: ambient noise module.
**
** Ambient noise sampling functions.  Produce a Power Spectra Density estimate
** of the ambient sound.
**
** The sampling procedure is as follows:
**
**  1. Zero the averaging buffer.
**  2. Take 1024 samples on both channels using a sampling frequency of
**     100khz.
**  3. Fourier transform the samples and add the transform to the
**     averaging buffer (delta F = 97.65625 hz)
**  4. Repeat steps 2 and 3 until the desired number of transform
**     samples have been averaged.
**  5. Bin the spectra using a bin size of 10 (new delta F = 976.5625 hz)
**     and scale to produce PSD estimate.
**  6. Convert to dB, add receiver sensitivity, and subtract amplifier gain.
**
** Notes:
**   -  the center frequency for bin i is:
**         (i + 0.5)*976.5625 where i = (0, 50)
**         49902.34 where i = 51 (the last bin has only 3 samples)
**
**
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <tt8lib.h>
#include "ioports.h"
#include "power.h"
#include "ifft.h"
#include "atod.h"
#ifdef NOISE_NETCDF
#include "netcdf.h"
#endif
#include "noise.h"
#include "log.h"

#define CHAN0_GAIN		20
#define CHAN1_GAIN		40

#define USECS_PER_SAMPLE	10
#define DELTA_T			(USECS_PER_SAMPLE*1.0e-6)
#define EXTRA_DATA		32

static long 		itrans[2*FFTLEN];
static short 		W[FFTLEN], data[FFTLEN+EXTRA_DATA];
static unsigned short 	H[FFTLEN];
static float 		spec[2][FFTLEN+2];
static float 		psd[NR_BINS];
static char 		psd_dB[2*NR_BINS];
static int		channels[] = {LOW_GAIN_CHAN, HIGH_GAIN_CHAN};
static int		Rgain[2] = {20, 40};

static long		nr_fft, start_time, last_time, fft_time;
static long		t_interface_stable;
static int		tables_ready = 0;
#ifdef NOISE_NETCDF
static netCDFdesc	*nd;
#endif
static double		hamming_scale;

/*
 * Calculate the Power Spectral Density from the averaged FFTs.
 */
static int
calc_psd(double dt, double w)
{
    register int	i, j, k, n;
    int			x;
    float		psd_scale;
    
    if(nr_fft == 0)
	return 0;
    
    /*
    ** Scale factor to convert from a (one sided) mV^2 amplitude spectrum to
    ** V^2 power spectral density estimate.  'w' is the weighting factor to
    ** compensate for the Hamming window. 'nr_fft*nr_fft' compensates for the
    ** number of FFTs being averaged.  
    */
    psd_scale = 2.*dt*1.0e-6/(w*nr_fft*nr_fft);
    
    for(k = 0;k < 2;k++)
    {
	n = 0;
	for(i = 0;i <= FFTLEN+1;)
	{
	    /*
	    ** Begin calculation of Power Spectral density by averaging the
	    ** power spectrum into bins.
	    */
	    psd[n] = 0.;
	    for(j = 0;j < BIN_SIZE;j++)
	    {
		if(i > FFTLEN+1)
		    break;
	    
		psd[n] += (spec[k][i]*spec[k][i] + spec[k][i+1]*spec[k][i+1]);
		i += 2;
	    }
	    psd[n] /= (float)j;

	    /*
	    ** Scale the spectrum.
	    */
	    psd[n] *= psd_scale;
	
	    n++;
	}

	/*
	** Convert to dB and add nominal receiver sensitivity.
	*/
	for(i = 0;i < n;i++)
	{
	    x = (psd[i] == 0.) ? -128
	      : (int)(10.*log10(psd[i])) + RECEIVER_SENS - Rgain[k];
	    psd_dB[i*2+k] = x;
	}
	
    }

    return 1;
}

/**
 * noise_init_tables - initialize FFT lookup tables.
 *
 * Initialize the lookup tables for the FFT and Hamming Window
 * coefficients.  This function need only be called once.
 */
void
noise_init_tables(void)
{
    unsigned long	t;

    if(tables_ready)
	return;

    log_event("Creating FFT coefficient tables\n");
    
    /*
    ** Build lookup tables and time the process.
    */
    StopWatchStart();
    hamming_scale = hamming_window(H, (short)FFTLEN);
    t = StopWatchTime();
    log_event("Hamming window coeff. table time: %lu usecs\n", t);

    StopWatchStart();
    make_coeff(W, (short)FFTLEN, FFT_FORWARD);
    t = StopWatchTime();
    log_event("Fixed point coeff. table time: %lu usecs\n", t);
    tables_ready = 1;
}

/**
 * noise_init - initialize the ambient noise interface.
 * @t_warmup: warm-up time (seconds)
 */
void
noise_init(long t_warmup)
{
    if(!tables_ready)
	noise_init_tables();

    /*
    ** Initialize the A/D interface and power-up the receiver board.
    */
    atod_init();
    power_on(ALGDEV_POWER1);
    t_interface_stable = RtcToCtm() + t_warmup;
}

int
noise_dev_ready(void)
{
    return (t_interface_stable && (RtcToCtm() > t_interface_stable));
}

void
noise_shutdown(void)
{
    /*
    ** Shutdown the A/D interface.
    */
    atod_shutdown();
    power_off(ALGDEV_POWER1);
    t_interface_stable = 0;
}

/**
 * noise_sample - take an ambient noise spectrum sample.
 *
 * Take an ambient noise sample.  %FFTLEN points are read from each
 * channel at a sampling frequency of 100khz.  A fixed-point FFT
 * is performed on each dataset and the result is added into an
 * averaging buffer.  Returns 1 if successful, 0 if an error occurs.
 */
int
noise_sample(void)
{
    short		*datap;
    register long	i, j, n, mean;

    last_time = RtcToCtm();
    if(start_time == 0L)
	start_time = last_time;

    for(n = 0;n < 2;n++)
    {
	if(atod_read_buf(channels[n], 0, USECS_PER_SAMPLE, 
			 FFTLEN+EXTRA_DATA, data) == 0)
	{
	    log_error("noise", "Error reading A/D\n");
	    power_off(ALGDEV_POWER1);
	    atod_shutdown();
	    return 0;
	}

	datap = &data[EXTRA_DATA];
	StopWatchStart();
	mean = 0L;
	for(i = 0;i < FFTLEN;i++)
	{
	    mean += datap[i];
	}
	
	mean /= FFTLEN;

	/*
	** Remove the mean and apply a Hamming Window to the data before
	** writing it to the transform array.
	*/
	for(i = 0,j = 0;i < FFTLEN;i++)
	{
	    datap[i] -= mean;
	    itrans[j++] = (H[i]*(long)datap[i])/65536;
	    itrans[j++] = 0L;
	}

	/* Fixed-point FFT */
	ifft(itrans, FFTLEN_EX, W);

	/*
	** Add the positive half of the FFT to the averaging buffer
	*/
	for(i = 0;i <= FFTLEN+1;i++)
	    spec[n][i] += (float)itrans[i];
	fft_time = StopWatchTime();
    }
    
    nr_fft++;

    return 1;
}

/**
 * noise_raw_sample - return a raw time series.
 * @chan: channel code, %LOW_GAIN_CHAN or %HIGH_GAIN_CHAN.
 * @buf: array for returned data.
 * @np: number of data points to store in @buf.
 *
 * Read @np A/D samples from the specified channel.  Returns 1 if successful
 * or 0 if an error occurs.
 */
int
noise_raw_sample(int chan, short *buf, int np)
{
    int		r = 1;
    
    if(chan != LOW_GAIN_CHAN && chan != HIGH_GAIN_CHAN)
    {
	log_error("noise", "Unknown channel, %d\n", chan);
	return 0;
    }
    
    /*
    ** Initialize the A/D interface and power-up the receiver board.
    */
    atod_init();
    power_on(ALGDEV_POWER1);
    DelayMilliSecs(500L);

    if(atod_read_buf(chan, 0, USECS_PER_SAMPLE, np, buf) == 0)
    {
	log_error("noise", "Error reading A/D\n");
	r = 0;
    }

    power_off(ALGDEV_POWER1);
    atod_shutdown();

    return r;
}

#ifdef NOISE_NETCDF
/**
 * noise_open_file - open ambient noise data file.
 * @fname: filename.
 *
 * Open the netCDF data file for the noise samples.  Returns FILE pointer or 
 * NULL if the file cannot be opened.
 */
FILE*
noise_open_file(const char *fname)
{
    FILE	*fp;
    
    if((fp = fopen(fname, "wb")) == NULL)
	return NULL;
    if(nd)
	free(nd);
	
    nd = write_noise_header(fp, "Ambient noise spectra", NR_BINS);
    
    return fp;
}

/**
 * noise_close_file - close ambient noise data file.
 * @fp: file pointer.
 *
 * Close a file opened by noise_open_file().
 */
void
noise_close_file(FILE *fp)
{
    sync_file(nd, fp);
    fclose(fp);
}

/**
 * noise_write_psd - write a power spectrum record to the data file.
 * @ofp: file pointer.
 *
 * Calculate the Power Spectral Density estimate for each channel from
 * the current contents of the averaging buffer.  The power spectrum is
 * averaged into %NR_BINS frequency bins.  The result is written as a
 * single record to the netCDF data file and the averaging buffers are
 * flushed.
 *
 * Returns 1 if successful, otherwise 0.
 */
int
noise_write_psd(FILE *ofp)
{
    return noise_write_psd2(ofp, nd);
}

int
noise_write_psd2(FILE *ofp, netCDFdesc *nd)
{
    long		t;

    if(!calc_psd(DELTA_T, hamming_scale))
	return 0;
    
    log_event("Writing noise spectra\n");
    
    write_noise_variable(nd, "time", &last_time, 4);
    write_noise_variable(nd, "psd", psd_dB, (int)sizeof(psd_dB));
    flush_record(nd, ofp);
    
    start_time = 0L;
    nr_fft = 0;
    memset(spec, 0, sizeof(spec));
    
    return 1;
}
#endif /* NOISE_NETCDF */

/**
 * noise_get_psd - return the average power spectrum.
 * @buf: buffer to hold the returned spectrum
 *
 * Calculate the power spectrum using the averaged FFT.  @buf must be
 * large enough to hold %NR_BINS values for both channels (2 * %NR_BINS).
 * The values are in dB and are signed single-byte integers.  They are
 * stored interleaved with the low gain channel at the even array
 * indicies and the high gain at the odd.
 */
int
noise_get_psd(char *buf)
{
    if(!calc_psd(DELTA_T, hamming_scale))
	return 0;
    memcpy(buf, psd_dB, sizeof(psd_dB));
    return 1;
}


/**
 * noise_clear_psd - clear the power spectrum buffer.
 *
 * Clear the power spectrum averaging buffers to prepare for a
 * new dataset.
 */
void
noise_clear_psd(void)
{
    start_time = 0L;
    nr_fft = 0;
    memset(spec, 0, sizeof(spec));
}

long
noise_get_fft_time(void)
{
    return fft_time;
}
