/**@file
 *
 * Acoustic Rain Gauge implementation for MLF2.
 *
 * The ARG hydrophone is connected to two A/D channels, each with a different
 * gain level. An ARG sample is a 100khz 1024-point FFT, the Power Spectral
 * Density estimate is calculated using a bin size of 10. The center frequency
 * of bin[i] is::
 *
 *     (i + 0.5) * 976.5625     where i = (0, 50)
 *     49902.34                 where i = 51
 *
 * Note that the last bin has only 3 samples.
 *
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <tt8lib.h>
#include "ioports.h"
#include "power.h"
#include "ifft.h"
#include "atod.h"
#include "arg.h"
#include "log.h"
#include "util.h"

#define HYDRO_CAL_FILE  "argcal.txt"
#define LOW_GAIN_CHAN   10
#define HIGH_GAIN_CHAN  11

#define CHAN0_GAIN      20
#define CHAN1_GAIN      40

#define USECS_PER_SAMPLE    10
#define DELTA_T         (USECS_PER_SAMPLE*1.0e-6)
#define EXTRA_DATA      32

static long             itrans[2*FFTLEN];
static short            W[FFTLEN], data[FFTLEN+EXTRA_DATA];
static unsigned short   H[FFTLEN];
static float            avgspec[2][FFTLEN+2];
static float            spec[2][FFTLEN+2];
static float            psd[NR_BINS];
static char             dB_correction[NR_BINS];
static long             t_interface_stable;
static int              tables_ready = 0, n_spec = 0;
static int              channels[] = {LOW_GAIN_CHAN, HIGH_GAIN_CHAN};
static int              rgain[] = {CHAN0_GAIN, CHAN1_GAIN};
static double           hamming_scale;


/*
 * Calculate the Power Spectral Density from the averaged FFTs. The result
 * is written to the psd_dB array.
 *
 * @param S cummulative spectrum buffer (%FFTLEN+2)/2 complex values
 * @param navg  number of spectrums accumulated
 * @param dt  sampling interval in seconds
 * @param w  weighting factor (Hamming window compensation)
 * @param gain  nominal gain (dB) to add to each value
 * @param psd_dB  output PSD buffer (%NR_BINS values)
 */
static int calc_psd(float *S, int navg, double dt, double w, int gain, char *psd_dB)
{
    int     i, j, n;
    float   psd_scale;
    
    if(navg <= 0)
        return 0;
    
    /*
    ** Scale factor to convert from a (one sided) mV^2 amplitude spectrum to
    ** V^2 power spectral density estimate.  'w' is the weighting factor to
    ** compensate for the Hamming window. 'navg*navg' compensates for the
    ** number of FFTs being averaged.  
    */
    psd_scale = 2.*dt*1.0e-6/(w*navg*navg);
    
    n = 0;
    for(i = 0;i <= FFTLEN+1;)
    {
        /*
        ** Begin calculation of Power Spectral density by averaging the
        ** power spectrum into bins.
        */
        psd[n] = 0.;
        for(j = 0;j < BIN_SIZE;j++)
        {
            if(i > FFTLEN+1)
                break;
    
            psd[n] += (S[i]*S[i] + S[i+1]*S[i+1]);
            i += 2;
        }
        psd[n] /= (float)j;

        /*
        ** Scale the spectrum and convert to dB.
        */
        psd[n] *= psd_scale;
        psd_dB[n] = (psd[n] == 0.) ? -128 : (int)(10.*log10(psd[n])) + gain;

        n++;
    }

    return 1;
}

/**
 * arg_init_tables - initialize FFT lookup tables.
 *
 * Initialize the lookup tables for the FFT and Hamming Window
 * coefficients.  This function need only be called once.
 */
void arg_init_tables(void)
{
    if(tables_ready)
        return;
    hamming_scale = hamming_window(H, (short)FFTLEN);
    make_coeff(W, (short)FFTLEN, FFT_FORWARD);
    if(fileexists(HYDRO_CAL_FILE))
    {
        /* Load the hydrophone calibration corrections */
        FILE    *ifp;
        int     i;
        char    linebuf[80];

        if((ifp = fopen(HYDRO_CAL_FILE, "r")) != NULL)
        {
            i = 0;
            while(fgets(linebuf, sizeof(linebuf)-1, ifp) != NULL)
            {
                if(linebuf[0] != '#')
                {
                    dB_correction[i++] = atoi(linebuf);
                }
            }
            fclose(ifp);
        }
    }
    tables_ready = 1;
}

/**
 * arg_init - initialize the ambient noise interface.
 * @t_warmup: warm-up time (seconds)
 */
void arg_init(long t_warmup)
{
    if(!tables_ready)
        arg_init_tables();

    /*
    ** Initialize the A/D interface and power-up the receiver board.
    */
    atod_init();
    power_on(ALGDEV_POWER1);
    t_interface_stable = RtcToCtm() + t_warmup;
}

int arg_dev_ready(void)
{
    return (t_interface_stable && (RtcToCtm() > t_interface_stable));
}

void arg_shutdown(void)
{
    /*
    ** Shutdown the A/D interface.
    */
    atod_shutdown();
    power_off(ALGDEV_POWER1);
    t_interface_stable = 0;
}

/**
 * arg_sample - take an Acoustic Rain Gauge sample.
 *
 * Read %FFTLEN points from the specified A/D channel at a sampling
 * frequency of 100khz and transform via a fixed-point FFT. Returns
 * a pointer to a buffer containing (%FFTLEN+2)/2 complex values
 * representing the positive half of the transform.
 * 
 * @param  chan  ARG channel to sample
 * @return pointer to transform buffer
 */
float* arg_sample(argchan_t chan)
{
    short       *datap;
    long        i, j, mean;

    if(atod_read_buf(channels[chan], 0, USECS_PER_SAMPLE, 
             FFTLEN+EXTRA_DATA, data) == 0)
    {
        log_error("arg", "Error reading A/D\n");
        power_off(ALGDEV_POWER1);
        atod_shutdown();
        return NULL;
    }

    datap = &data[EXTRA_DATA];
    mean = 0L;
    for(i = 0;i < FFTLEN;i++)
    {
        mean += datap[i];
    }
    
    mean /= FFTLEN;

    /*
    ** Remove the mean and apply a Hamming Window to the data before
    ** writing it to the transform array.
    */
    for(i = 0,j = 0;i < FFTLEN;i++)
    {
        datap[i] -= mean;
        itrans[j++] = (H[i]*(long)datap[i])/65536;
        itrans[j++] = 0L;
    }

    /* Fixed-point FFT */
    ifft(itrans, FFTLEN_EX, W);

    /*
    ** Copy the positive half of the FFT into the spectrum buffer
    */
    for(i = 0;i <= FFTLEN+1;i++)
        spec[chan][i] = (float)itrans[i];

    return &spec[chan][0];
}

/**
 * arg_store_sample - store the latest sample spectrum.
 *
 * Add the most recent sample spectrum to the averaging buffer.
 * 
 * @param chan  ARG channel
 * @return number of samples in the buffer
 */
int arg_store_sample(argchan_t chan)
{
    int     i;

    for(i = 0;i <= FFTLEN+1;i++)
        avgspec[chan][i] += spec[chan][i];
    n_spec++;
    return n_spec;
}

/**
 * arg_calc_psd - calculate the Power Spectral Density estimate.
 * 
 * @param  chan     ARG channel
 * @param  use_avg  if true, use the average spectrum
 * @param  psdbuf   buffer to hold %NR_BINS PSD values
 * @return  number of spectra averaged
 */
int arg_calc_psd(argchan_t chan, int use_avg, char *psdbuf)
{
    int rval = 1;

    if(use_avg)
    {
        calc_psd(&avgspec[chan][0], n_spec, DELTA_T, hamming_scale,
            ARG_RECEIVER_SENS - rgain[chan],
            psdbuf);
        rval = n_spec;
        n_spec = 0;
        memset((void*)&avgspec[chan][0], 0, (FFTLEN+2)*sizeof(float));
    }
    else
    {
        calc_psd(&spec[chan][0], n_spec, DELTA_T, hamming_scale,
            ARG_RECEIVER_SENS - rgain[chan],
            psdbuf);
    }

    return rval;
}