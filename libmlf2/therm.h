/*
** arch-tag: header file for thermometer module
** Time-stamp: <2004-11-22 14:42:09 mike>
*/
#ifndef _THERM_H_
#define _THERM_H_

#define THERM_DEVICE	5
#define THERM_MINOR	0
#define THERM_BAUD	9600L
#define THERM_TIMEOUT	3L

int therm_init(void);
void therm_shutdown(void);
int therm_dev_ready(void);
int therm_start_sample(void);
int therm_data_ready(void);
float therm_read_data(void);
void therm_get_stats(unsigned long *errs, unsigned long *reads);
void therm_test(void);
int therm_cmd(const char *cmd);

#endif
