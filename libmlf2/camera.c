/**@file
** Driver for COMedia C328 rs232 camera.
**
** arch-tag: 5c89bbc7-9055-4195-9b77-fdaf7ceae97e SERIAL
**
**
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "camera.h"

#ifndef __GNUC__
#define __inline__
#endif

#define WARM_UP_MSECS	500

#define MAX_SYNCS	60

/** Command start character */
#define START_CHAR	0xaa
/** Delay in milliseconds between successive packet requests */
#define PACKET_DELAY	5L

#define	SNAPSHOT_COMPRESSED	0
#define SNAPSHOT_UNCOMPRESSED	1
#define PIC_SNAPSHOT		1
#define PIC_PREVIEW		2
#define PIC_JPEG_PREVIEW	5

#define PACKET_SIZE		512L
#define BLOCK_SIZE		(PACKET_SIZE - 6L)
#define DATA_OFFSET		4

typedef struct {
    unsigned char	cmd;
    unsigned char	p[4];
} COMMAND_t;

enum {CMD_INITIAL=0x01, CMD_GETPIC=0x04, CMD_SNAPSHOT,
      CMD_SETPACK, CMD_SETBAUD, CMD_RESET, CMD_PWROFF,
      CMD_DATA, CMD_SYNC=0x0d, CMD_ACK, CMD_NAK,
      CMD_LIGHTFREQ=0x13};


static int 		ref = 0;
static int 		serial_dev;
static unsigned char	cam_packet[PACKET_SIZE];


static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char	c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
	if((MilliSecs() - start) >= timeout)
	    return -1;
    }
    return ((int) c)&0xff;
}

static __inline__ void sputc(int desc, int out)
{
    char	c = out;
    
    serial_write(desc, &c, 1);
}

static int
read_packet(unsigned char *packet, long timeo_ms, int pnum)
{
    long	t0;
    int		i, c, check_pnum, psize;
    unsigned char	*p = packet;
    
    t0 = MilliSecs();
    for(i = 0;i < 4;i++)
    {
	if((c = sgetc_timed(serial_dev, t0, timeo_ms)) == -1)
	    return 0;
	*p++ = c;
    }

    t0 = MilliSecs();
    psize = packet[2] | ((int)packet[3] << 8);
    if(psize <= 0 || psize > BLOCK_SIZE)
    {
	log_error("camera", "Bad packet size 0x%04x\n", psize);
	serial_inflush(serial_dev);
	return 0;
    }

    /* add the two checksum bytes */
    i = psize + 2;
    while(i && (c = sgetc_timed(serial_dev, t0, timeo_ms)) != -1)
    {
	*p++ = c;
	i--;
    }
    
    check_pnum = packet[0] | ((int)packet[1] << 8);
    if(check_pnum != pnum)
	return 0;
    
    return (i == 0) ? psize : 0;
}

    
/*
 * Send a command to the camera and wait for an ACK (if timeout
 * is greater than 0).
 *
 * @param  cmdp  pointer to command data structure.
 * @param  timeo_ms  timeout in milliseconds
 * @return 1 if successful (ACK received), 0 on error.
 */
static int
send_command(COMMAND_t *cmdp, long timeo_ms)
{
    int			i, c;
    long		t0;
    unsigned char	*rp, reply[6];
    
    sputc(serial_dev, START_CHAR);
    sputc(serial_dev, cmdp->cmd);
    for(i = 0;i < 4;i++)
	sputc(serial_dev, cmdp->p[i]);

    if(timeo_ms > 0)
    {
	rp = reply;
	t0 = MilliSecs();
	i = sizeof(reply);
	while(i && (c = sgetc_timed(serial_dev, t0, timeo_ms)) != -1)
	{
	    *rp++ = c;
	    i--;
	}

	if(i > 0 || reply[1] != CMD_ACK)
	{
	    if(reply[1] == CMD_NAK)
		log_error("camera", "NAK error code = %02x\n", reply[4]);
	    
	    return 0;
	}
	
    }
    
    return 1;
}

/**
 * Initialize the device interface.
 *
 * @return 1 if successful, 0 on error.
 */
int
cam_init(void)
{
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(CAM_DEVICE, 0, 
        CAM_BAUD)) < 0)
    {
	log_error("cam", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    DelayMilliSecs(WARM_UP_MSECS);
    
initdone:
    ref++;
    
    serial_inflush(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    
    return 1;
}

/**
 * Shutdown the device interface.
 */
void
cam_shutdown(void)
{
    if(ref == 0)
	return;
    ref = 0;
    serial_close(serial_dev);
}

/**
 * Check device status.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
cam_dev_ready(void)
{
    return ref;
}

/**
 * Wakeup (sync) the camera.
 *
 * @return SYNC count if successful, otherwise 0.
 */
int
cam_sync(void)
{
    COMMAND_t		cmd;
    int			i, j, c;
    long		t0;
    unsigned char	*rp, reply[6];

    cmd.cmd = CMD_SYNC;
    cmd.p[0] = 0x0;
    cmd.p[1] = 0x0;
    cmd.p[2] = 0x0;
    cmd.p[3] = 0x0;
    
    for(i = 0;i < MAX_SYNCS;i++)
    {
	if(send_command(&cmd, 100L))
	{
	    rp = reply;
	    t0 = MilliSecs();
	    j = sizeof(reply);
	    while(j && (c = sgetc_timed(serial_dev, t0, 3000L)) != -1)
	    {
		*rp++ = c;
		j--;
	    }

	    if(reply[1] != CMD_SYNC)
	    {
		log_error("camera", "No SYNC received from camera\n");
		return 0;
	    }

	    cmd.cmd = CMD_ACK;
	    cmd.p[0] = CMD_SYNC;
	    cmd.p[1] = 0x0;
	    cmd.p[2] = 0x0;
	    cmd.p[3] = 0x0;
	    send_command(&cmd, 0L);
	    return i+1;
	}
	
    }
    
    return 0;
}


/**
 * Take a snapshot and save to a file.
 *
 * @param  resolution  resolution constant from camera.h
 * @param  outfp  output file pointer.
 * @return 1 if successful, 0 on error. Errors are logged.
 */
int
cam_snapshot(cam_res_t resolution, FILE *outfp)
{
    COMMAND_t		cmd;
    int			psize = PACKET_SIZE, i, c, npackets, n;
    long		dsize, t0;
    unsigned char	*rp, reply[6];
    
    serial_inflush(serial_dev);
    
    cmd.cmd = CMD_INITIAL;
    cmd.p[0] = 0x0;
    cmd.p[1] = 0x07;	/* JPEG image */
    cmd.p[2] = 0x0;
    cmd.p[3] = resolution;
    if(!send_command(&cmd, 1000L) && !send_command(&cmd, 2000L))
    {
	log_error("camera", "Error sending INITIAL command\n");
	return 0;
    }

    cmd.cmd = CMD_SETPACK;
    cmd.p[0] = 0x08;
    cmd.p[1] = psize & 0xff;
    cmd.p[2] = (psize >> 8) & 0xff;
    cmd.p[3] = 0x0;
    if(!send_command(&cmd, 2000L))
    {
	log_error("camera", "Error sending SET PACKAGE command\n");
	return 0;
    }

    cmd.cmd = CMD_SNAPSHOT;
    cmd.p[0] = SNAPSHOT_COMPRESSED;
    cmd.p[1] = 0x01;		/* skip a frame */
    cmd.p[2] = 0x0;
    cmd.p[3] = 0x0;
    if(!send_command(&cmd, 2000L))
    {
	log_error("camera", "Error sending SNAPSHOT command\n");
	return 0;
    }
    
    DelayMilliSecs(100L);

    cmd.cmd = CMD_GETPIC;
    cmd.p[0] = PIC_SNAPSHOT;
    cmd.p[1] = 0x0;
    cmd.p[2] = 0x0;
    cmd.p[3] = 0x0;
    if(!send_command(&cmd, 2000L))
    {
	log_error("camera", "Error sending GET PICTURE command\n");
	return 0;
    }
    
    rp = reply;
    t0 = MilliSecs();
    i = sizeof(reply);
    while(i && (c = sgetc_timed(serial_dev, t0, 3000L)) != -1)
    {
	*rp++ = c;
	i--;
    }

    if(i > 0 || reply[1] != CMD_DATA)
    {
	log_error("camera", "DATA command not received\n");
	return 0;
    }
    
    dsize = reply[3];
    dsize |= ((long)reply[4] << 8);
    dsize |= ((long)reply[5] << 16);
    log_event("Snapshot size %ld bytes\n", dsize);

    cmd.cmd = CMD_ACK;
    cmd.p[0] = 0x0;
    cmd.p[1] = 0x0;
    cmd.p[2] = 0x0;
    cmd.p[3] = 0x0;
    npackets = (dsize/BLOCK_SIZE) + 1;
    for(i = 0;i < npackets;i++)
    {
	/* request the next image packet */
	cmd.p[2] = i & 0xff;
	cmd.p[3] = (i >> 8) & 0xff;
	send_command(&cmd, 0);
	
	if((n = read_packet(cam_packet, 1000L, i)) == 0)
	{
	    send_command(&cmd, 0);
	    if((n = read_packet(cam_packet, 2000L, i)) == 0)
	    {
		log_error("camera", "Error reading packet %d (%02x %02x %02x %02x)\n", i,
			  cam_packet[0], cam_packet[1], cam_packet[2], cam_packet[3]);
		return 0;
	    }
	    
	}
	fwrite(&cam_packet[DATA_OFFSET], 1L, (size_t)n, outfp);
	DelayMilliSecs(PACKET_DELAY);
    }

    /* Send image done ACK */
    cmd.p[2] = 0xf0;
    cmd.p[3] = 0xf0;
    send_command(&cmd, 0);

    return 1;
}
