/**@file
 ** Parameter table management functions.
 **
 ** $Id: ptable.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
 **
 ** The table associates a name (character string) with a "parameter handle" --
 ** a type code and memory location.  Its purpose is to allow runtime
 ** modification of various control variables by messages sent during the
 ** mission.
 **
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <tt8.h>
#include <tt8lib.h>
#include "log.h"
#include "ptable.h"

static HashTable *ptable = NULL;




/**
 * Initialize the MLF2 parameter table.
 * The table associates a name (character string) with a "parameter handle" --
 * a type code and memory location.  Its purpose is to allow runtime
 * modification of various control variables by messages sent during the
 * mission without having to resort to a large number of global variables.
 *
 * Note that the parameter names are treated as case-insensitive.
 *
 * @return 1 if successful or 0 if an error occurs.
 */

int
init_param_table(void)
{
    if(ptable != NULL)
        return 1;

    if((ptable = ht_create(NR_CELLS, HT_NOCASE)) == NULL)
        return 0;

    return 1;
}


/**
 * Add a new parameter to the table.  Only the address of the parameter
 * is stored, the value is not copied therefore the parameter should
 * be static, global, or stored on the heap (malloc'ed).  The parameter
 * type code must be one of the following constants.
 *
 *  - PTYPE_SHORT - addr points to a short integer.
 *  - PTYPE_LONG - addr points to a long integer.
 *  - PTYPE_DOUBLE - addr points to a double precision float.
 *  - PTYPE_STRING - addr is a char string (upper 8 bits are string length)
 *
 * The type codes may be ORed with PTYPE_READ_ONLY which will disallow
 * modification of the parameter.
 *
 * @param  name  key to associate with the value.
 * @param  type  parameter type code.
 * @param  addr  pointer to parameter value.
 *
 */
void
add_param(const char *name, int type, void *addr)
{
    struct param    p;

    if(ptable == NULL)
        init_param_table();

    /*
    ** All new parameters are marked as "dirty" so they will be printed
    ** on the first call to dump_params().
    */
    p.type = type | PTYPE_DIRTY;
    p.loc = addr;

    /*
    ** insert_elem will copy the contents of 'p' to malloc'ed memory.
    */
    if(ptable)
        ht_insert_elem(ptable, name, &p, (unsigned short)sizeof(struct param));
}

/**
 * Parses the string to obtain the new value for the named parameter.
 *
 * @param  name  parameter name.
 * @param  str  ascii string representation of the new value
 * @return 1 if successful, otherwise 0.
 */
int
set_param_str(const char *name, const char *str)
{
    struct param    *p;
    unsigned        type, n;
    double          val;

    if(!ptable || !ht_find_elem(ptable, name, (void**)&p, 0))
        return 0;

    type = p->type & ~PTYPE_SIZEMASK;

    switch(type & ~PTYPE_DIRTY)
    {
        case PTYPE_DOUBLE:
            *((double*)p->loc) = atof(str);
            log_event("Set %s to %f\n", name, *((double*)p->loc));
            break;
        case PTYPE_LONG:
            val = atof(str);
            *((long*)p->loc) = (long)val;
            log_event("Set %s to %ld\n", name, *((long*)p->loc));
            break;
        case PTYPE_SHORT:
            val = atof(str);
            *((short*)p->loc) = (short)val;
            log_event("Set %s to %d\n", name, *((short*)p->loc));
            break;
        case PTYPE_STRING:
            n = PTYPE_GETSIZE(p->type);
            strncpy((char*)p->loc, str, n-1);
            ((char*)p->loc)[n-1] = '\0';
            log_event("Set %s to '%s'\n", name, (char*)p->loc);
            break;
        default:
            return 0;
    }

    p->type |= PTYPE_DIRTY;

    return 1;
}

/**
 * Sets the named parameter to value.
 *
 * @param  name  parameter name.
 * @param  value   new parameter value.
 * @return 1 if successful, otherwise 0.
 */
int
set_param_int(const char *name, long value)
{
    struct param    *p;
    unsigned        type;

    if(!ptable || !ht_find_elem(ptable, name, (void**)&p, 0))
        return 0;

    type = p->type & ~PTYPE_SIZEMASK;

    switch(type & ~PTYPE_DIRTY)
    {
        case PTYPE_DOUBLE:
            *((double*)p->loc) = (double)value;
            log_event("Set %s to %f\n", name, *((double*)p->loc));
            break;
        case PTYPE_LONG:
            *((long*)p->loc) = value;
            log_event("Set %s to %ld\n", name, *((long*)p->loc));
            break;
        case PTYPE_SHORT:
            *((short*)p->loc) = (short)value;
            log_event("Set %s to %d\n", name, *((short*)p->loc));
            break;
        default:
            return 0;
    }

    p->type |= PTYPE_DIRTY;

    return 1;
}

/**
 * Sets the named parameter to value.
 *
 * @param  name  parameter name.
 * @param  value   new parameter value.
 * @return 1 if successful, otherwise 0.
 */
int
set_param_double(const char *name, double value)
{
    struct param    *p;
    unsigned        type;

    if(!ptable || !ht_find_elem(ptable, name, (void**)&p, 0))
        return 0;

    type = p->type & ~PTYPE_SIZEMASK;

    switch(type & ~PTYPE_DIRTY)
    {
        case PTYPE_DOUBLE:
            *((double*)p->loc) = value;
            log_event("Set %s to %f\n", name, *((double*)p->loc));
            break;
        case PTYPE_LONG:
            *((long*)p->loc) = (long)value;
            log_event("Set %s to %ld\n", name, *((long*)p->loc));
            break;
        case PTYPE_SHORT:
            *((short*)p->loc) = (short)value;
            log_event("Set %s to %d\n", name, *((short*)p->loc));
            break;
        default:
            return 0;
    }

    p->type |= PTYPE_DIRTY;

    return 1;
}

/**
 * Looks up the information (type and address) for the named parameter.
 *
 * @param  name  parameter name
 * @param  param  pointer to returned parameter information.
 * @return 1 if sucessful, otherwise 0.
 */
int
get_param(const char *name, struct param *param)
{
    struct param    *p;

    if(ptable && ht_find_elem(ptable, name, (void**)&p, 0))
    {
        param->type = p->type;
        param->loc = p->loc;
        return 1;
    }

    return 0;
}

/**
 * Return a parameter value as an integer.
 *
 * @param  name  parameter name
 * @return current value.
 */
long
get_param_as_int(const char *name)
{
    struct param    *p;
    long        r = 0L;
    unsigned        type;

    if(ptable && ht_find_elem(ptable, name, (void**)&p, 0))
    {
        type = p->type & ~PTYPE_SIZEMASK;
        switch(type & PTYPE_TYPEMASK)
        {
            case PTYPE_DOUBLE:
                r = (long)(*((double*)p->loc));
                break;
            case PTYPE_LONG:
                r = *((long*)p->loc);
                break;
            case PTYPE_SHORT:
                r = (long)(*((short*)p->loc));
                break;
        }
    }

    return r;
}


/**
 * Return a parameter value as a double.
 *
 * @param  name  parameter name
 * @return current value.
 */
double
get_param_as_double(const char *name)
{
    struct param    *p;
    double      r = 0.;
    unsigned        type;

    if(ptable && ht_find_elem(ptable, name, (void**)&p, 0))
    {
        type = p->type & ~PTYPE_SIZEMASK;
        switch(type & PTYPE_TYPEMASK)
        {
            case PTYPE_DOUBLE:
                r = *((double*)p->loc);
                break;
            case PTYPE_LONG:
                r = (double)(*((long*)p->loc));
                break;
            case PTYPE_SHORT:
                r = (double)(*((short*)p->loc));
                break;
        }
    }

    return r;
}

static void
print_param(struct elem *e, void *calldata)
{
    FILE        *fp = (FILE*)calldata;
    struct param    *p = (struct param*)e->data;
    unsigned        type;

    /* Only print parameters which have changed */
    if(!(p->type & PTYPE_DIRTY))
        return;

    p->type &= ~PTYPE_DIRTY;

    type = p->type & ~PTYPE_SIZEMASK;

    fprintf(fp, "<param name='%s' ", e->name);
    switch(type & PTYPE_TYPEMASK)
    {
        case PTYPE_DOUBLE:
            fprintf(fp, "type='double'>%.6g</param>\n", *((double*)p->loc));
            break;
        case PTYPE_LONG:
            fprintf(fp, "type='long'>%ld</param>\n", *((long*)p->loc));
            break;
        case PTYPE_SHORT:
            fprintf(fp, "type='short'>%hd</param>\n", *((short*)p->loc));
            break;
        case PTYPE_STRING:
            fprintf(fp, "type='string'>%s</param>\n", (char*)p->loc);
            break;
    }
}

/**
 * Dump all updated parameters to a file in XML format.
 *
 * @param  fp  pointer to output FILE
 */
void
dump_params(FILE *fp)
{
    fprintf(fp, "<ptable time='%ld'>\n", RtcToCtm());
    ht_foreach_elem(ptable, print_param, (void*)fp);
    fputs("</ptable>\n", fp);
}
