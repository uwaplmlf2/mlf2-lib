/**@file
** Driver for the Gas Tension Device.
**
** This devices consists of a PIC micro-controller, a pump, and a Paros
** high-precision pressure sensor.
**
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "lpsleep.h"
#include "serial.h"
#include "log.h"
#include "ioports.h"
#include "gtd2.h"

#include "iofuncs.h"

#define WARM_UP_MSECS           2000L
#define SAMPLE_TIMEO_MSECS      4000L

#define GTD2_PROMPT      "GTD>\r\n"

/* start-of-data character */
#define SOD_CHAR        ','

/* end-of-data character */
#define EOD_CHAR        '\n'

/* command template */
#define CV_TMPL "CONTROLVARS=[%d\r\n%d\r\n%d\r\n%d\r\n%d\r\n%d\r\n%d\r\n%d\r\n%d\r\n%d\r\n%d\r\n%d\r\n]"

/* command to read the last sample */
#define CMD_READ_DATA   "f"

/* size of buffer for the start-sample command */
#define CMD_BUF_SIZE    80

#define PURGE_INPUT(d, t) while(sgetc_timed(d, MilliSecs(), (t)) != -1)

static int serial_dev = -1;
static int sampling = 0;
static unsigned long init_time, sample_start, sample_time;

static char command_buf[CMD_BUF_SIZE];

/**
 * Convert the sample-description data structure to ASCII in the
 * format expected by the GTD2 controller.
 *
 * @param  gs  pointer to data structure.
 * @return pointer to GTD2 command string.
 * @warning not re-entrant, command is stored in a static buffer.
 */
static char *
_serialize(GTD2sample_t *gs)
{
    char        *buf = command_buf;

    sprintf(buf, CV_TMPL,
            gs->pres_res/100,
            gs->temp_res/100,
            gs->pump_mode,
            gs->pump_on/1000,
            gs->pump_off/1000,
            gs->pump_cycle,
            gs->pump_delay/1000,
            gs->bb_delay/1000,
            gs->bb_warmup/1000,
            gs->bb_interrupt,
            gs->bb_sample,
            gs->do_flush);

    return buf;
}

/**
 * Callback to reopen the device interface upon return from low-power sleep.
 */
static void
gtd2_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
    log_event("Reopened GTD2 serial interface\n");
}

/**
 * Initialize the device interface.
 *
 * @return 1 if successful, 0 on error.
 */
int
gtd2_init(void)
{
    if(serial_dev >= 0)
        goto already_open;

    if((serial_dev = serial_open(GTD2_DEVICE, 0, GTD2_BAUD)) < 0)
    {
        log_error("gtd2", "Cannot open serial device (code = %d)\n",
                serial_dev);
        return 0;
    }

    init_time = MilliSecs();
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to gtd2_start_sample and gtd2_read_data.
    */
    add_after_hook("GTD2-reopen", gtd2_reopen);
    sampling = 0;
    if(!serial_chat(serial_dev, "", GTD2_PROMPT, 2L))
    {
        log_error("gtd2", "No response from device\n");
        gtd2_shutdown();
        return 0;
    }

 already_open:
    return 1;
}

/**
 * Shutdown the device interface.
 * Shutdown the device interface.  The device is powered off.
 */
void
gtd2_shutdown(void)
{
    if(serial_dev >= 0)
    {
        remove_after_hook("GTD2-reopen");
        serial_close(serial_dev);
        serial_dev = -1;
        sampling = 0;
    }
}


/**
 * Check if device is ready.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
gtd2_dev_ready(void)
{
    return (serial_dev >= 0) && ((MilliSecs() - init_time) > WARM_UP_MSECS);
}


/**
 * Start the pressure sample process.
 *
 * @param  gs  pointer to sample-description data structure.
 * @return 1 if successful, 0 on error.
 */
int
gtd2_start_sample(GTD2sample_t *gs)
{
    char        *cmd;


    PURGE_INPUT(serial_dev, 200L);
    cmd = _serialize(gs);
    sputz(serial_dev, cmd);
    sample_start = MilliSecs();
    sample_time = GTD2_CYCLE_TIME(gs);
    sampling = 1;

    return 1;
}

/**
 * Test whether sampling process is done.
 *
 * @return true if sampling process is done.
 */
int
gtd2_data_ready(void)
{
    return (MilliSecs() - sample_start) > sample_time;
}


/**
 * Read pressure and temperature values. The results are stored in
 * the corresponding fields of the GTD2data_t structure.
 *
 * @param  gd  pointer to return data structure
 * @return 1 if successful or 0 if an error occurs.  Errors are logged.
 */
int
gtd2_read_data(GTDdata_t *gd)
{
    int         i, c, bufsize;
    long        start;
    char        buf[32];

    sampling = 0;

    PURGE_INPUT(serial_dev, 200L);
    memset(buf, 0, sizeof(buf));

    sputz(serial_dev, CMD_READ_DATA);

    i = 0;
    bufsize = sizeof(buf) - 1;
    start = MilliSecs();
    while((c = sgetc_timed(serial_dev, start, SAMPLE_TIMEO_MSECS)) != -1 && c != SOD_CHAR)
        ;

    if(c >= 0)
    {
        while((c = sgetc_timed(serial_dev, start, SAMPLE_TIMEO_MSECS)) != -1 &&
              c != EOD_CHAR && i < bufsize)
        {
            buf[i++] = c;
        }
        buf[i] = '\0';
    }

    PURGE_INPUT(serial_dev, 200L);

    return sscanf(buf, "%f %f", &gd->pressure, &gd->temperature) == 2;
}


/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 */
void
gtd2_test(void)
{
    printf("Pass-through mode: CTRL-c to exit\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

/**
 * Return TRUE if the GTD2 is sampling.
 */
int
gtd2_sampling(void)
{
    return sampling;
}
