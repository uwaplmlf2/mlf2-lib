/**@file
** arch-tag: GTD header file.
*/
#ifndef _GTD2_H_
#define _GTD2_H_

#include "gtd.h"

#define GTD2_DEVICE     6
#define GTD2_MINOR      0
#define GTD2_BAUD       9600L

#define FLUSH_TIME_SEC          10L

/** pump_mode constants */
enum {GTD2_PUMP_PULSED=1, GTD2_PUMP_CONTINUOUS=0};

/** GTD sampling parameters */
typedef struct {
    short       pres_res;       /**< b0 pressure integration time (counts) */
    short       temp_res;       /**< b1 temperature int. time (counts) */
    short       pump_mode;      /**< b2 pump mode */
    short       pump_on;        /**< b3 pump on time (ms) */
    short       pump_off;       /**< b4 pump off time (ms) */
    short       pump_cycle;     /**< b5 number of pumping cycles */
    short       pump_delay;     /**< b6 pump delay (ms) */
    short       bb_delay;       /**< b7 "button board" delay (ms) */
    short       bb_warmup;      /**< b8 button board warm-up (ms) */
    short       bb_interrupt;   /**< b9 button board interrupts */
    short       bb_sample;      /**< b10 number of button board samples */
    short       do_flush;       /**< b11 flush control */
} GTD2sample_t;

/**
 * Calculate the GTD sample cycle time.
 * @param s pointer to sample-parameter data structure.
 * @return sample time in milliseconds.
 */
static __inline__ unsigned long GTD2_CYCLE_TIME(GTD2sample_t *s)
{
    long        T;

    /* pressure sample time (ms) */
    T = (s->pres_res*1000L/P_COUNTS_PER_SEC);
    /* temperature sample time (ms) */
    T += (s->temp_res*1000L/T_COUNTS_PER_SEC);

    if(s->pump_mode == GTD2_PUMP_PULSED)
    {
        /* pulsed mode */
        T += s->pump_cycle*(s->pump_on + s->pump_off);
    }
    else
    {
        /* continuous mode */
        T += (s->pump_delay + s->bb_delay + s->bb_warmup);
    }

    if(s->do_flush)
        T += (FLUSH_TIME_SEC*1000);

    return T;
}


/** Default sample parameters */
#define GTD2_DEFAULT_SAMPLE {\
      pres_res: 300,\
      temp_res: 600,\
      pump_mode: GTD2_PUMP_PULSED,\
      pump_on: 1000,\
      pump_off: 4000,\
      pump_cycle: 5,\
      pump_delay: 1000,\
      bb_delay: 8000,\
      bb_warmup: 7000,\
      bb_interrupt: 3,\
      bb_sample: 1,\
      do_flush: 0\
    }


int gtd2_init(void);
void gtd2_shutdown(void);
int gtd2_dev_ready(void);
int gtd2_start_sample(GTD2sample_t *gs);
int gtd2_data_ready(void);
int gtd2_read_data(GTDdata_t *gd);
void gtd2_test(void);
int gtd2_sampling(void);

#endif
