/*
** arch-tag: netcdf string constants and attribute table.
*/
#ifndef _NCATTR_H_
#define _NCATTR_H_

#define _UNITS		"units"
#define _SCALE_FACTOR	"scale_factor"
#define _TIME		"time"
#define _PRESSURE	"pressure"
#define _TEMP		"temp"
#define _DIRECTION	"direction"
#define _SAL		"sal"
#define _IPAR		"ipar"
#define _ROTATION	"rotation"
#define _ACCEL		"accel"
#define _ANGLE		"angle"
#define _PISTON		"piston"
#define _AMP		"amp"
#define _CORR		"corr"
#define _PING		"ping"
#define _VELOCITY	"velocity"
#define _SAMPLE		"sample"
#define _ALTITUDE	"altitude"
#define _INTERVAL	"interval"
#define _CTD		"ctd"
#define _FREQ		"freq"
#define _HZ		"hz"
#define _PSD		"psd"
#define _DB		"dB"
#define _CHANNEL	"channel"
#define _LAT		"lat"
#define _LON		"lon"
#define _NSATS		"nsats"
#define _MAG		"mag"
#define _FLUOROMETER	"fluorometer"
#define _ECO		"eco"
#define _ECOTHERM	"ecotherm"
#define _RED		"red"
#define _REDREF		"redref"
#define _BLUE		"blue"
#define _BLUEREF	"blueref"
#define _OXY		"oxy"
#define _GAS		"gas"
#define _I490		"i490"
#define _THERM		"therm"
#define _OFFSET		"offset"


/*
** Variable attributes table.  All attributes for a specific variable
** MUST be grouped consecutively.
*/
enum {TIME_UNITS=0, PRESSURE_UNITS, TEMP_UNITS, SAL_UNITS, IPAR_UNITS,
      ROTATION_UNITS, ROTATION_SCALE_FACTOR, ACCEL_UNITS, ACCEL_SCALE_FACTOR,
      LAT_UNITS, LON_UNITS, VELOCITY_UNITS, ANGLE_UNITS, ANGLE_SCALE_FACTOR,
      PISTON_UNITS, AMP_UNITS, CORR_UNITS, ALTITUDE_UNITS,  INTERVAL_UNITS,
      FREQ_UNITS, PSD_UNITS, ECO_UNITS, GAS_UNITS};

#define THERM_UNITS		TEMP_UNITS
#define I490_UNITS		IPAR_UNITS
#define OXY_UNITS		FREQ_UNITS
#define FLUOROMETER_UNITS	ECO_UNITS

static struct attr attr_table[] = {
[TIME_UNITS]            = { _UNITS, NC_CHAR, "seconds since 1970-01-01 UTC" },
[PRESSURE_UNITS]        = { _UNITS, NC_CHAR, "decibars" },
[TEMP_UNITS]            = { _UNITS, NC_CHAR, "degreesC" },
[SAL_UNITS]             = { _UNITS, NC_CHAR, "ppt" },
[IPAR_UNITS]            = { _UNITS, NC_CHAR, "millivolts" },
[ROTATION_UNITS]        = { _UNITS, NC_CHAR, "degrees/sec" },
[ROTATION_SCALE_FACTOR] = { _SCALE_FACTOR, NC_FLOAT, "2.2889e-3" },
[ACCEL_UNITS]           = { _UNITS, NC_CHAR, "G" },
[ACCEL_SCALE_FACTOR]    = { _SCALE_FACTOR, NC_FLOAT, "9.1553e-5f" },
[LAT_UNITS]             = { _UNITS, NC_CHAR, "degrees_north" },
[LON_UNITS]             = { _UNITS, NC_CHAR, "degrees_east" },
[VELOCITY_UNITS]        = { _UNITS, NC_CHAR, "mm/sec" },
[ANGLE_UNITS]           = { _UNITS, NC_CHAR, "degrees" },
[ANGLE_SCALE_FACTOR]    = { _SCALE_FACTOR, NC_FLOAT, "0.1f" },
[PISTON_UNITS]          = { _UNITS, NC_CHAR, "cm" },
[AMP_UNITS]             = { _UNITS, NC_CHAR, "counts"},
[CORR_UNITS]            = { _UNITS, NC_CHAR, "percent"},
[ALTITUDE_UNITS]        = { _UNITS, NC_CHAR, "cm"},
[INTERVAL_UNITS]        = { _UNITS, NC_CHAR, "ms"},
[FREQ_UNITS]            = { _UNITS, NC_CHAR, "hz"},
[PSD_UNITS]             = { _UNITS, NC_CHAR, "dB"},
[ECO_UNITS]             = { _UNITS, NC_CHAR, "counts"},
[GAS_UNITS]             = { _UNITS, NC_CHAR, "mbar"},
  };

#endif /* _NCATTR_H_ */
