/**@file
** Common driver components for Biospherical serial PAR sensors.
**
** This driver expects the sensor to be configured as follows:
**
**    - Quiet startup
**    - Streaming data
**    - No data preamble
**    - Include temperature in data record
**    - Include voltage in data record
**
** 
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "serialpar.h"
#include "iofuncs.h"

#define STRINGIFY(x)        #x

#define RECORD_SIZE     80
#define REC_START       0x0a
#define REC_END         0x0d

static char     recbuf[RECORD_SIZE];

/**
 * spar_init - initialize the device interface.
 *
 */
int
spar_init(const char *name, int devnum, long baud, long warmup_ms)
{
    int     sd;
    
    if((sd = serial_open(devnum, 0, baud)) < 0)
    {
        log_error(name, "Cannot open serial device (code = %d)\n",
            sd);
        return -1;
    }

    DelayMilliSecs(warmup_ms);    
    serial_inmode(sd, SERIAL_NONBLOCKING);    
    
    return sd;
}

/**
 * spar_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
spar_shutdown(int sd)
{
    if(sd >= 0)
        serial_close(sd);
}

/**
 * spar_dev_ready - check if device is ready.
 *
 * Returns 1 if device is ready to accept a command, otherwise 0.
 */
int
spar_dev_ready(int dev)
{
    return (dev >= 0);
}


/**
 * spar_read_data - read a data value.
 *
 */
int
spar_read_data(int dev, ParData *pd, long timeout)
{
    char    *p;
    long    t;
    int     c, i;

    memset(pd, 0, sizeof(ParData));

    t = MilliSecs();
    while((c = sgetc_timed(dev, t, timeout)) != REC_START)
    {
        if(c < 0)
        {
            log_error("spar", "Timeout waiting for record start\n");
            return 0;
        }
    }

    p = recbuf;
    i = 0;
    while(i < RECORD_SIZE && 
         (c = sgetc_timed(dev, t, timeout)) != REC_END)
    {
        if(c < 0)
        {
            log_error("spar", "Timeout waiting for record start\n");
            return 0;
        }
        *p = c;
        p++;
        i++;
    }
    *p = '\0';

    if(sscanf(recbuf, "%f, %f, %f", &pd->light, &pd->temp, &pd->volts) != 3)
    {
        log_error("spar", "Error reading data record\n");
        return 0;
    }

    return 1;
}


/**
 * spar_test - pass through mode.
 *
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
spar_test(int dev)
{
    printf("Pass-through mode: CTRL-c to exit\n");
    serial_passthru(dev, 0x03, 0x02);
}
