/**@file
** Driver for TriOS hyperspectral sensors.
**
** arch-tag: e95f03c2-a1b4-4343-9eac-6cc952a81d0e
** $Id: trios.c,v c992eb2d937a 2008/01/09 20:28:31 mikek $
**
** This driver supports both the ACC and ARC sensors. The ACC device must be
** attached to the first serial port.
**
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "ioports.h"
#include "serial.h"
#include "log.h"
#include "trios.h"

#define WARM_UP_MSECS		1500L

#define START_CHAR	'#'
#define ESC_CHAR	'@'

#define FRAME_MASK	0xe0
#define FRAME_SHIFT	5

/** I2C addresses for ACC subunits */
#define SAM_MODULE	0x18
#define IP_MODULE	0x10
#define MASTER_MODULE	0x40

static int frame_size[] = {2, 4, 8, 16, 32, 64, -1, -1};


typedef struct {
    unsigned char	code;
    unsigned char	params[2];
} COMMAND_t;

enum {CMD_REBOOT=0, CMD_QUERY=0xb0, CMD_SLEEP=0xa0, CMD_ADRINIT=0x98,
      CMD_SETADR=0x60, CMD_SETBAUD=0x50, CMD_START=0xa8, CMD_SETCFG=0x78};


/** Maintain the state of each device */
typedef struct devstate {
    int			sd; /**< serial device descriptor */
    int			ref; /**< reference count */
    int			addr; /** TriOS module address */
    unsigned		wakeup_line; /** IO port E wakeup line */
    unsigned long	t_sample;
    unsigned long	t_start;
    unsigned long	wait_ms;
    unsigned long	init_time;
} devstate_t;

/* ACC address == 0x80, ARC address == 0x0 */
static devstate_t dstab[2] = { {-1, 0, MASTER_MODULE, 0x10}, 
			       {-1, 0, 0x0, 0x20}};


static const char * escape_char(int c)
{
    switch(c)
    {
	case '#':
	    return "@e";
	case '@':
	    return "@d";
	case 0x11:
	    return "@f";
	case 0x13:
	    return "@g";
    }
    return NULL;
}

static int unescape_char(int c)
{
    switch(c)
    {
	case 'e':
	    c = '#';
	    break;
	case 'd':
	    c = '@';
	    break;
	case 'f':
	    c = 0x11;
	    break;
	case 'g':
	    c = 0x13;
	    break;
    }

    return c;
}

	    
static __inline__ int sgetc(int desc)
{
    unsigned char	c;
    return (serial_read(desc, &c, (size_t)1) == 1) ? (int)c : -1;
}

static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char	c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
	if((MilliSecs() - start) >= timeout)
	    return -1;
    }
    return ((int) c)&0xff;
}

static __inline__ void sputc(int desc, int out)
{
    char	c = out;
    const char	*p;
    
    if((p = escape_char(out)) != NULL)
	serial_write(desc, p, 2L);
    else
	serial_write(desc, &c, 1L);
}

static void sputs(int desc, unsigned char *str, int n)
{
    const char	*p;
    
    while(n--)
    {
	if((p = escape_char(*str)) != NULL)
	{
	    serial_write(desc, p, 2L);
	}
	else
	{
	    serial_write(desc, str, 1L);
	}
	
	str++;
    }
}

static __inline__ void xon(int desc)
{
    char	c = 0x11;
    serial_write(desc, &c, 1L);
}

static __inline__ void xoff(int desc)
{
    char	c = 0x13;
    serial_write(desc, &c, 1L);
}


static void send_command(int desc, int addr, COMMAND_t *cmdp)
{
    char		c;
    unsigned char packet[7];

    packet[0] = 0;
    packet[1] = 0;
    packet[2] = addr << 1;
    packet[3] = cmdp->code;
    packet[4] = cmdp->params[0];
    packet[5] = cmdp->params[1];
    packet[6] = 0x01;
    c = START_CHAR;
    serial_write(desc, &c, 1L);
    sputs(desc, packet, sizeof(packet));
}

static int read_frame(int desc, DATAFRAME_t *dfp, long timeout)
{
    int			c, i, n;
    unsigned long	t;
    unsigned char	header[6];

    /* Wait for the start byte */
    t = MilliSecs();
    do
    {
	if((c = sgetc_timed(desc, t, timeout)) < 0)
	    return 0;
    } while(c != START_CHAR);
    

    /* Read the header and determine the frame size */
    n = sizeof(header);
    i = 0;
    t = MilliSecs();
    while(n--)
    {
	if((c = sgetc_timed(desc, t, timeout)) < 0)
	    return 0;
	if(c == ESC_CHAR)
	{
	    /* Read next character and unescape */
	    if((c = sgetc_timed(desc, t, timeout)) < 0)
		return 0;
	    c = unescape_char(c);
	}
	header[i++] = c;
    }
    dfp->size = frame_size[(header[0] & FRAME_MASK) >> FRAME_SHIFT];
    if(dfp->size < 0)
    {
	log_error("trios", "Bad frame size (0x%02x)\n", header[0]);
	return 0;
    }
    

    /* Read the data frame */
    n = dfp->size;
    
    i = 0;
    t = MilliSecs();
    while(n--)
    {
	if((c = sgetc_timed(desc, t, timeout)) < 0)
	    return 0;
	if(c == ESC_CHAR)
	{
	    /* Read next character and unescape */
	    if((c = sgetc_timed(desc, t, timeout)) < 0)
		return 0;
	    c = unescape_char(c);
	}
	
	dfp->data[i++] = c;
    }
    
    /* Discard the check byte */
    c = sgetc_timed(desc, t, timeout);
    dfp->module = header[2] >> 1;
    dfp->index = header[3];
    
    return 1;
}



/**
 * Initialize TriOS interface.
 * Open connection to the device and initialize. Errors are logged.
 *
 * @param  which  device index, 0 or 1.
 *
 * @return 1 if successful, otherwise 0.
 */
int
trios_init(int which)
{
    devstate_t	*dsp;

    dsp = &dstab[which];
    if(dsp->ref > 0)
	goto initdone;

    dsp->ref = 1;
    iop_set(IO_E, dsp->wakeup_line);
    
    if((dsp->sd = serial_open(TRIOS_DEVICE(which), 0, TRIOS_BAUD)) < 0)
    {
	log_error("trios", "Cannot open serial device (code = %d)\n",
		  dsp->sd);
	return 0;
    }

    dsp->wait_ms = 8192;
    dsp->t_sample = 0;
    dsp->init_time = MilliSecs();
    
 initdone:
    
    serial_inmode(dsp->sd, SERIAL_NONBLOCKING);
    
    return 1;
}


/**
 * Shutdown the TriOS interface.
 * Shutdown the TriOS interface and power off the device.
 */
void
trios_shutdown(int which)
{
    if(dstab[which].ref == 0)
	return;
    dstab[which].ref = 0;
    iop_clear(IO_E, dstab[which].wakeup_line);
    
    serial_close(dstab[which].sd);
}


/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
trios_dev_ready(int which)
{
    devstate_t	*dsp;

    dsp = &dstab[which];
    return dsp->ref && ((MilliSecs() - dsp->init_time) > WARM_UP_MSECS);
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
trios_test(int which)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(dstab[which].sd, 0x03, 0x02);
}

/**
 * Send an arbitrary command to the device. An XOFF character is sent after
 * the command so the response can be read at a later time with
 * trios_get_reply().
 *
 * @param  which  device index, 0 or 1
 * @param  code   command code
 * @param  p0     first command parameter
 * @param  p1     second command parameter
 * @param  module  module address, if < 0 use default
 *
 * @returns 1 if successful, 0 on error.
 */
int
trios_command(int which, int code, int p0, int p1, int module)
{
    int		sd = dstab[which].sd;
    int		addr = (module < 0) ? dstab[which].addr : module;
    COMMAND_t	cmd;

    cmd.code = code;
    cmd.params[0] = p0;
    cmd.params[1] = p1;
    send_command(sd, addr, &cmd);
    xoff(sd);

    return 1;
}

/**
 * Set the data integration time.
 *
 * @param  which  device index, 0 or 1
 * @param  tcode  integration time code (see TriOS Manual)
 */
void
trios_set_inttime(int which, inttime_t tcode)
{
    dstab[which].wait_ms = (tcode == T_AUTO) ? 8192 : T_CODE_TO_MS(tcode);
    trios_command(which, CMD_SETCFG, 0x05, tcode,
		  which == 0 ? SAM_MODULE : -1);
}

/**
 * Start the data collection process.
 *
 * @return 1 if successful, 0 on error.
 */
int
trios_start(int which)
{
    log_event("Sending start command to device %d\n", which);
    dstab[which].t_sample = MilliSecs();
    dstab[which].t_start = RtcToCtm();
    return trios_command(which, CMD_START, 0, 0x81, -1);
}

/**
 * Return the start time of the most recent sample.
 *
 * @param  which  device index, 0 or 1.
 * @return start time in seconds.
 */
unsigned long
trios_start_time(int which)
{
    return dstab[which].t_start;
}

/**
 * Test whether sampling process is done.
 *
 * @param  which  device index, 0 or 1.
 * @return true if sampling process is done.
 */
int
trios_data_ready(int which)
{
    devstate_t	*dsp;

    dsp = &dstab[which];
    return (MilliSecs() - dsp->t_sample) >= dsp->wait_ms;
}

/**
 * Read response frames from a TriOS device. Reads frames from the device and
 * passes each in turn to the handler callback.
 *
 * @param  which  device index, 0 or 1
 * @param  handler  callback function to receive data frames.
 * @param  max_frames  maximum number of frames to read
 * @param  timeout  frame timeout in milliseconds
 * @param  cbdata  opaque data pointer passed to handler.
 * @return number of frames read.
 */
int
trios_get_reply(int which, int max_frames, long timeout, 
		trios_datasink handler, void * cbdata)
{
    int		sd = dstab[which].sd, frame;
    DATAFRAME_t	df;
    
    serial_inmode(sd, SERIAL_NONBLOCKING);
    frame = 0;
    xon(sd);
    while(frame < max_frames)
    {
	if(!read_frame(sd, &df, timeout))
	{
	    log_error("trios", "Cannot read frame %d\n", frame);
	    break;
	}
	if(handler)
	    (*handler)(&df, cbdata);
	frame++;
    }
    xoff(sd);
    
    return frame;
}

/**
 * Return the device serial number.
 *
 * @param  which   device index, 0 or 1
 * @return serial number or 0 on failure.
 */
unsigned
trios_get_sn(int which)
{
    int		sd = dstab[which].sd, r;
    DATAFRAME_t	df;

    trios_command(which, CMD_QUERY, 0, 0, -1);
    serial_inmode(sd, SERIAL_NONBLOCKING);
    xon(sd);
    r = read_frame(sd, &df, 2000L);
    xoff(sd);

    if(!r)
	return 0;
    
    return ((unsigned)df.data[1] << 8) | df.data[0];
}
