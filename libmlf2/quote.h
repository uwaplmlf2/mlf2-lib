#ifndef _QUOTE_H_
#define _QUOTE_H_

size_t quote_string(const char *str, char *quoted, size_t n);
void quote_to_file(const char *str, FILE *fp);

#endif
