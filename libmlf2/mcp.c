/**@file
** Driver for Biospherical MCP series PAR sensors.
**
** This driver expects the sensor to be configured as follows:
**
**    - Quiet startup
**    - Streaming data
**    - No data preamble
**    - Include temperature in data record
**    - Include voltage in data record
**
** 
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "mcp.h"

#define WARM_UP_MSECS   500

static int      serial_dev= -1;

/**
 * mcp_init - initialize the device interface.
 *
 */
int
mcp_init(void)
{
    if(serial_dev >= 0)
        goto initdone;

    if((serial_dev = spar_init("mcp", MCP_DEVICE, MCP_BAUD, WARM_UP_MSECS)) < 0)
        return 0;    

initdone:    
    serial_inflush(serial_dev);
    
    return 1;
}

/**
 * mcp_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
mcp_shutdown(void)
{
    spar_shutdown(serial_dev);
    serial_dev = -1;
}

int
mcp_dev_ready(void)
{
    return spar_dev_ready(serial_dev);
}

int
mcp_read_data(ParData *pd, long timeout)
{
    return spar_read_data(serial_dev, pd, timeout);
}

void
mcp_test(void)
{
    spar_test(serial_dev);
}