/*
** $Id: eco.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _ECO_H_
#define _ECO_H_

#define ECO_DEVICE	4
#define ECO_MINOR	0
#define ECO_BAUD	19200L
#define ECO_TIMEOUT_MS	2500L

#define ECO_REF		0
#define ECO_SIG		1
#define ECO_FIELDS	6

typedef struct {
    short	blue[2];
    short	red[2];
    short	flr;
    short	therm;
} ECOdata;

int eco_init(void);
void eco_shutdown(void);
int eco_dev_ready(void);
int eco_set_params(int navg);
int eco_read_data(ECOdata *edp);
void eco_get_stats(unsigned long *errs, unsigned long *reads);

#endif /* _ECO_H_ */
