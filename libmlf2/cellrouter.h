#ifndef CELLROUTER_H
#define CELLROUTER_H

#define CELLROUTER_BAUD     9600L
#define CELLROUTER_DEVICE   11

int cellrouter_init(void);
int cellrouter_dev_ready(void);
void cellrouter_shutdown(void);
int cellrouter_serialdev(void);

#endif /* ROUTER_H */