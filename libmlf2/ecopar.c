/*
** Interface to Wetlabs ECO-PAR sensor.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <setjmp.h>
#include <ctype.h>
#include "serial.h"
#include "log.h"
#include "ecopar.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define START_CHAR  '\n'
#define END_CHAR    '\r'

#define WARM_UP_MSECS   2000L

static int ref = 0;
static int serial_dev = -1;
static long init_time;


/**
 * Initialize the ECO-PAR interface.
 * Open a connection to the ECO-PAR and initialize the device.
 *
 * @return 1 if successful, otherwise 0.
 */
int
ecopar_init(void)
{
    if(ref > 0)
        goto initdone;

    if((serial_dev = serial_open(ECOPAR_DEVICE, 0, ECOPAR_BAUD)) < 0)
    {
        log_error("ecopar", "Cannot open serial device (code = %d)\n",
            serial_dev);
        return 0;
    }

    init_time = MilliSecs();
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
initdone:
    ref++;

    return 1;
}

/**
 * Shutdown the ECO-PAR interface and power off the device.
 */
void
ecopar_shutdown(void)
{
    if(ref == 0 || --ref > 0)
        return;
    serial_close(serial_dev);
}


/**
 * Check if device is ready.
 *
 * @return true if the device is ready to accept commands.
 */
int
ecopar_dev_ready(void)
{
    return ref && ((MilliSecs() - init_time) > WARM_UP_MSECS);
}


/**
 * Close the anti-fouling shutter.  The shutter will reopen automatically
 * when the device is powered back on.
 */
int
ecopar_close_shutter(void)
{
    int     i;

    if(ref <= 0)
        return 0;
    log_event("Closing ecopar shutter\n");

    /*
    ** The shutter is closed when a "stop" command is received. The
    ** stop command is a sequence of five '!' characters.  The device
    ** seems to have some buffering issues so we send a sequence of
    ** ten '!' and add a small pause in-between.
    */
    for(i = 0;i < 10;i++)
    {
        sputc(serial_dev, '!');
        DelayMilliSecs(150L);
    }
    serial_inflush(serial_dev);

    return 1;
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 */
void
ecopar_test(void)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

/**
 * Reads the next sample value from the device.
 *
 * @param  timeout  read timeout in milliseconds
 * @return sample value (> 0) if successful, 0 if an error occurs.
 */
int
ecopar_read_data(long timeout)
{
    int     c, par_sample;
    ulong   t;
    short   num;
    enum {ST_1=1,
      ST_2,
      ST_3,
      ST_DONE} state;

    if(ref <= 0)
    return 0;

    /*
    ** State machine to try and locate the start of the data. We wait
    ** for a linefeed (START_CHAR) to indicate the start of a line and
    ** then for two TABs to skip the first two fields (date and time).
    */
    t = MilliSecs();
    state = ST_1;
    while(state != ST_DONE)
    {
        c = sgetc_timed(serial_dev, t, timeout);

        if(c < 0)
        {
            log_error("ecopar", "Timeout waiting for record start\n");
            return 0;
        }

        switch(state)
        {
            case ST_1:
            if(c == START_CHAR)
                state = ST_2;
            break;
            case ST_2:
            if(c == '\t')
                state = ST_3;
            break;
            case ST_3:
            if(c == START_CHAR)
                state = ST_2;
            else if(c == '\t')
                state = ST_DONE;
            break;
            case ST_DONE:
            break;
        }
    }

    par_sample = 0;
    while((c = sgetc_timed(serial_dev, t, timeout)) != END_CHAR)
    {
        if(c < 0)
        {
            log_error("ecopar", "Error reading sample\n");
            return 0;
        }

        if(isdigit(c))
            par_sample = par_sample*10 + (c - '0');
    }

    return par_sample;
}


