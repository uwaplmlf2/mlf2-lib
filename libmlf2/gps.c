/**@file
** Driver for the GARMIN GPS board.
**
** $Id: gps.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
**
** @sa nmea.c
*/
#include <stdio.h>
#include <stdlib.h>
#include <tt8.h>
#include <tt8lib.h>
#include <time.h>
#include <string.h>
#include "serial.h"
#include "nmea.h"
#include "log.h"
#include "gps.h"

#define GPS_DEVICE      0
#define GPS_BAUD        4800L

#define NMEA_OUT(str)   nmea_write_str(str, (fsink_t)serial_putc,\
                          (void*)&serial_dev, 1)

static int ref = 0;
static int serial_dev;

/*
** NMEA sentence buffer.  Allow for a maximum of 4 sentences; 1 GPGGA and
** 3 GPGSV.
*/
static NMEAsentence nsbuf[4];

/*
** I/O callback functions for the NMEA routines.
*/

static int serial_getc(void *handle)
{
    int         desc = *((int*)handle);
    char        c;
    return (serial_read(desc, &c, (size_t)1) == 1) ? (int)c : -1;
}

static int serial_putc(int c, void *handle)
{
    int         desc = *((int*)handle);
    char        buf = c;
    return serial_write(desc, &buf, (size_t)1);
}

/**
 * Initialize the GPS interface.
 * Open connection to GPS device and initialize.  The device is configured
 * to emit the following standard NMEA sentences.
 * - GPGGA - provides the fix along with some fix quality information and
 * a count of the satellites used.
 * - GPRMC - standard format GPS message which provides the fix and the
 * current UTC time and date.  This message is used to set the TT8 clock.
 *
 * @return 1 if successful, otherwise 0.
 */
int
gps_init(void)
{
    if(ref > 0)
        goto initdone;

    if((serial_dev = serial_open(GPS_DEVICE, 0, GPS_BAUD)) < 0)
    {
        log_error("gps", "Cannot open serial device (code = %d)\n",
                serial_dev);
        return 0;
    }

    /*
    ** Disable all sentences except GPGGA and GPRMC.
    */
    DelayMilliSecs(100L);
    NMEA_OUT("PGRMO,,2");
    DelayMilliSecs(100L);
    NMEA_OUT("PGRMO,GPGGA,1");
    DelayMilliSecs(100L);
    NMEA_OUT("PGRMO,GPRMC,1");
#ifdef EXPERIMENTAL
    DelayMilliSecs(100L);
    NMEA_OUT("PGRMO,GPGSV,1");
#endif

initdone:
    ref++;

    return 1;
}

/**
 * Shutdown the GPS interface.
 * Shutdown the GPS interface.  The device is powered off.
 */
void
gps_shutdown(void)
{
    if(ref == 0 || --ref > 0)
        return;
    serial_close(serial_dev);
}


/**
 * Check if device is ready.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
gps_dev_ready(void)
{
    return (ref > 0) ? 1 : 0;
}

/**
 * Read a GPS data record.
 * Read the next GPS data record containing current position and
 * satellite count.
 *
 * @param  gpd  pointer to output data record
 * @return 1 if successful, otherwise 0.
 */
int
gps_read_data(GPSdata *gpd)
{
    int                 r;
    NMEAsentence        *ns;

#ifdef EXPERIMENTAL
    int                 i, n;
    char                *p, matchstr[10];
#endif

    if(ref <= 0)
        return 0;

#ifdef EXPERIMENTAL
    /*
    ** Initialize match string for GPGSV sentences
    */
    strcpy(matchstr, "GPGSV,");
    p = &matchstr[strlen(matchstr)];
#endif

    if((r = nmea_read_match(&nsbuf[0], "GPGGA", (fsource_t)serial_getc,
                            (void*)&serial_dev, 5, 0)) <= 0)
    {
        log_error("gps", "Cannot read GPGGA message (code = %d)\n", r);
        return 0;
    }

#ifdef EXPERIMENTAL
    if((r = nmea_read_match(&nsbuf[1], "GPGSV", (fsource_t)serial_getc,
                            (void*)&serial_dev, 5, 0)) <= 0)
    {
        log_error("gps", "Cannot read GPGSV message (code = %d)\n", r);
        return 0;
    }

    /*
    ** First field gives the number of GSV sentences.  Append this character
    ** to the match buffer and convert to an integer.
    */
    *p++ = nsbuf[1].fields[0][0];
    n = nsbuf[1].fields[0][0] - '0';

    *p++ = ',';

    /* Read the rest of the GSV sentences */
    for(i = 1;i < n;i++)
    {
        if((r = nmea_read_match(&nsbuf[i+1], "GPGSV", (fsource_t)serial_getc,
                                (void*)&serial_dev, 5, 0)) <= 0)
        {
            log_error("gps", "Cannot read GPGSV message (code = %d)\n", r);
            return 0;
        }
    }
#endif /* EXPERIMENTAL */

    memset(gpd, 0, sizeof(GPSdata));
    ns = &nsbuf[0];

    /*
    ** Fill data structure.
    **
    ** ns->fields[1] = latitude
    ** ns->fields[2] = N or S
    ** ns->fields[3] = longitude
    ** ns->fields[4] = E or W
    ** ns->fields[5] = fix status
    ** ns->fields[6] = # of satellites in use
    */
    if(r > 2 && ns->fields[1][0])
    {
        sscanf(ns->fields[1], "%2hu%2hu.%4hu", &gpd->lat.deg, &gpd->lat.min,
               &gpd->lat.frac);
        gpd->lat.dir = ns->fields[2] ? ns->fields[2][0] : 'X';
    }

    if(r > 4 && ns->fields[3][0])
    {
        sscanf(ns->fields[3], "%3hu%2hu.%4hu", &gpd->lon.deg, &gpd->lon.min,
               &gpd->lon.frac);
        gpd->lon.dir = ns->fields[4] ? ns->fields[4][0] : 'X';
    }

    gpd->status = (r > 5 && ns->fields[5][0]) ? atoi(ns->fields[5]) : 0;
    gpd->satellites = (r > 6 && ns->fields[6][0]) ? atoi(ns->fields[6]) : 0;

    return 1;
}

/**
 * Set system clock from GPS
 *
 * @returns 1 if successful, otherwise 0.
 */
int
gps_set_clock(void)
{
    int                 r;
    struct tm           new_time;
    NMEAsentence        ns;

    if(ref <= 0)
        return 0;

    if((r = nmea_read_match(&ns, "GPRMC", (fsource_t)serial_getc,
                            (void*)&serial_dev, 5,
                            (ftstamp_t)RtcToCtm)) < 0)
    {
        log_error("gps", "Cannot read GPRMC message (code = %d)\n", r);
        return 0;
    }

    if(ns.fields[0][0] == '\0' || ns.fields[8][0] == '\0')
    {
        log_error("gps", "Empty GPRMC message, cannot set time\n");
        return 0;
    }

    /*
    ** Fill data structure.
    **
    ** ns.fields[8] = UTC date in ddmmyy format
    ** ns.fields[0] = UTC time in hhmmss format
    **
    */
    sscanf(ns.fields[8], "%2d%2d%2d", &new_time.tm_mday,
           &new_time.tm_mon, &new_time.tm_year);

    /* struct tm month field is zero based */
    new_time.tm_mon--;

    /* struct tm year field is relative to 1900 */
    if(new_time.tm_year < 80)   /* this will break in 2080 ;-) */
        new_time.tm_year += 100;

    sscanf(ns.fields[0], "%2d%2d%2d", &new_time.tm_hour,
           &new_time.tm_min, &new_time.tm_sec);

    /* Try to account for the time lag since we read the sentence */
    SetTimeSecs(mktime(&new_time) + (RtcToCtm() - ns.timestamp), NULL);
    log_event("Clock adjustment\n");

    return 1;
}
