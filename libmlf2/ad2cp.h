#ifndef AD2CP_H
#define AD2CP_H

#define AD2CP_DEVICE(n) ((n) == 0 ? 6 : 10)
#define AD2CP_BAUD      9600L

/**< Command file, one command per line */
#define AD2CP_CMD_FILE  "adcpcmds.txt"
/**< JSON format log file */
#define AD2CP_LOG_FILE  "adcplog.jsn"

int ad2cp_init(int which);
void ad2cp_shutdown(int which);
int ad2cp_cmd(int which, const char *cmd, char *resp, int rlen);
int ad2cp_cmd_log(int which, const char *cmd, FILE *ofp, int verbose);
long ad2cp_download(int which, FILE *ofp, unsigned long offset, unsigned long nbytes);
int ad2cp_sync_clock(int which);
void ad2cp_load_commands(int which, FILE *ifp, FILE *ofp, int verbose);
int ad2cp_deploy(int which, int keep, int verbose);
int ad2cp_halt(int which, FILE *fp, unsigned long maxsize, unsigned long blocksize);
#endif /* AD2CP_H */
