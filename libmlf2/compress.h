/*
** arch-tag: 13b38a72-fbc8-4b37-8304-bc31a5621e5d
** Time-stamp: <2005-01-21 10:22:00 mike>
*/
#ifndef _COMPRESS_H_
#define _COMPRESS_H_

int compress_file(const char *infile, const char *outfile);


#endif /* _COMPRESS_H_ */
