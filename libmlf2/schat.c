/**@file
 ** Automate an interaction over a serial interface.
 **
 ** The functions in this module can be used to automate a conversational
 ** exchange with a serial device.  This is especially useful when dealing
 ** with modems.
 */
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <string.h>
#include <ctype.h>
#include "serial.h"

#ifndef __GNUC__
#define __inline__
#endif

#define MAX_MATCHES     8
#define RESP_BUFLEN     1024
#define LOG_BUFLEN      10
#define BUFLEN          (1L << LOG_BUFLEN)

static int      debug = 0;
static char     ringbuf[BUFLEN];
static int      bufhead = 0, buflen = 0;

static void
rb_init(void)
{
    bufhead = 0;
    buflen = 0;
}

static void
rb_add_char(char c)
{
    int     i = (bufhead + buflen) & (BUFLEN - 1);
    ringbuf[i] = c;
    buflen++;
    if(buflen > BUFLEN)
    {
        buflen = BUFLEN;
        bufhead = (bufhead + 1) & (BUFLEN - 1);
    }
}

static void
rb_copy(char *buf, int n, int add_null)
{
    int     i, j;

    /* Set n to min(n, buflen) */
    if(n > buflen)
        n = buflen;
    for(j = 0;j < n;j++)
    {
        i = (bufhead + j) & (BUFLEN - 1);
        buf[j] = ringbuf[i];
    }

    if(add_null)
        buf[j] = '\0';
}

static __inline__ int serial_getc(int desc)
{
    unsigned char   c;
    return (serial_read(desc, &c, (size_t)1) == 1) ? ((int)c)&0xff : -1;
}

/**
 * Automate an interaction through a serial interface.
 * This function automates a "conversational" interaction through a serial
 * device (see serial.c).  The send string is sent and the expect
 * string is waited for.  Alternate expect strings can be specified by
 * delimiting them with the '|' character, e.g.:
 *
 *  "login: |NO CARRIER"
 *
 * The above string will match either "login: " or "NO CARRIER".  A maximum
 * of 8 alternates may be specified.
 *
 * @param  desc  serial device descriptor
 * @param  send  string to send
 * @param  expect  response string to wait for.
 * @param  timeout  maximum number of seconds to wait for a response.
 * @return the index (starting from 1) of the matched expect string or 0 on timeout.
 */
int
serial_chat(int desc, const char *send, const char *expect, long timeout)
{
    const char  *p, *q;
    int     i, length[MAX_MATCHES];
    const char  *e[MAX_MATCHES+1];

    /* Multiple expect strings are separated by the '|' character */
    p = expect;
    i = 0;
    while((q = strchr(p, '|')))
    {
        e[i] = p;
        length[i] = (int)(q - p);
        i++;
        p = q + 1;
    }

    e[i] = p;
    length[i] = q ? (int)(q - p) : strlen(p);
    e[i+1] = NULL;

    return serial_chat_bin_multi(desc, send, send ? (int)strlen(send) : 0,
                                 e, length, timeout);
}

/**
 * Automate an interaction through a serial interface.
 * This function automates a "conversational" interaction through a serial
 * device (see serial.c).  The send string is sent and the expect
 * string is waited for.
 *
 * @param  desc  serial device descriptor.
 * @param  send  binary string to send.
 * @param  ns  length of send.
 * @param  expect  binary response string to wait for.
 * @param  nr  length of expect.
 * @param  timeout  maximum number of seconds to wait for a response.
 * @return 1 if successful, 0 on timeout.
 */
int
serial_chat_bin(int desc, const char *send, int ns, const char *expect,
                int nr, long timeout)
{
    int     lengths[2];
    const char  *e[2];

    lengths[0] = nr;
    e[0] = expect;
    e[1] = NULL;
    return serial_chat_bin_multi(desc, send, ns, e, lengths, timeout);
}

/**
 * Turn debug tracing on or off.
 *
 * @param  state  tracing state, 1 (on) or 0 (off).
 */
void
serial_chat_debug(int state)
{
    debug = state;
}

static void
esc_chars(const char *s, int n, FILE *ofp)
{
    char    c, hex[5];
    static char *hexchars = "0123456789abcdef";

    fputc('\"', ofp);
    while(n--)
    {
        c = *s++;
        if(isprint(c))
            fputc(c, ofp);
        else
        {
            hex[0] = '\\';
            hex[1] = 'x';
            hex[2] = hexchars[(c >> 4)&0xf];
            hex[3] = hexchars[c&0xf];
            hex[4] = '\0';
            fputs(hex, ofp);
        }
    }
    fputc('\"', ofp);

}

typedef struct {
    const char  *str;
    int     len;
    int     state;
} match_state_t;

typedef enum {MATCH_RESET=0,
              MATCH_PARTIAL,
              MATCH_COMPLETE } match_t;

static match_t
check_match(int c, match_state_t *mp)
{
    if(c == mp->str[mp->state])
    {
        mp->state++;
        if(mp->state == mp->len)
            return MATCH_COMPLETE;
        else
            return MATCH_PARTIAL;
    }
    else if(c == mp->str[0])
    {
        mp->state = 1;
        return MATCH_PARTIAL;
    }

    mp->state = 0;
    return MATCH_RESET;
}

/**
 * Automate an interaction through a serial interface.
 * This function automates a "conversational" interaction through a serial
 * device (see serial.c).  The send string is sent and one of the expect
 * strings is waited for.  Note that the final element (string) in the
 * expect array must be NULL.
 *
 * @param  desc  serial device descriptor.
 * @param  send  binary string to send.
 * @param  ns  length of send.
 * @param  expect  array of binary response strings to wait for.
 * @param  nr  array containing the length of each entry in expect.
 * @param  timeout  maximum number of seconds to wait for a response.
 * @return the index (starting from 1) of the matched expect string or 0 on timeout.
 */
int
serial_chat_bin_multi(int desc, const char *send, int ns, const char **expect,
                      int *nr, long timeout)
{
    int         c, r, nm, i, mode = 0;
    long        tstart;
    match_state_t   mstate[MAX_MATCHES];

    nm = 0;
    if(nr && expect)
    {
        while(*expect && nm < MAX_MATCHES)
        {
            mstate[nm].str = *expect++;
            mstate[nm].len = *nr++;
            mstate[nm].state = 0;
            nm++;
        }

    }

    if(send && send[0])
    {
        serial_inflush(desc);
        for(i = 0;i < ns;i++)
        {
            serial_write(desc, send+i, 1L);
            DelayMilliSecs(50L);
        }

    }

    if(nm == 0)
        return 1;

    /* Set the interface to not block on input */
    mode = serial_inmode(desc, SERIAL_NONBLOCKING);

    timeout *= 1000L;
    tstart = MilliSecs();

    r = 0;
    rb_init();
    while((MilliSecs() - tstart) < timeout)
    {
        if((c = serial_getc(desc)) == -1)
            continue;
        rb_add_char((char)c);
        for(i = 0;i < nm;i++)
        {
            switch(check_match(c, &mstate[i]))
            {
                case MATCH_COMPLETE:
                    r = i+1;
                    goto done;
                case MATCH_PARTIAL:
                    if(debug)
                    {
                        esc_chars(mstate[i].str, mstate[i].state, stdout);
                        fputc('\n', stdout);
                    }
                    break;
                case MATCH_RESET:
                    break;
            }
        }

    }

done:
    serial_inmode(desc, mode);
    return r;
}

/**
 * Return device response from most recent chat.
 *
 * @param sbuf buffer for returned response string.
 * @param n maximum number of characters to copy.
 */
void
serial_chat_response(char *sbuf, size_t n)
{
    rb_copy(sbuf, (int)n, 1);
}