/*
** $Id: par.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** PAR sensor interface for MLF2.
**
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include "power.h"
#include "adc.h"
#include "atod.h"
#include "par.h"

static int ref = 0;

/**
 * par_init - initialize the PAR sensor interface.
 *
 * Power-on and initialize the PAR sensor interface.  Returns 1 if successful,
 * otherwise 0.
 */
int
par_init(void)
{
    /* Are we already initialized? */
    if(ref > 0)
	goto initdone;

    atod_init();
    power_on(ALGDEV_POWER1);
    
initdone:
    ref++;
    
    return 1;
}

/**
 * par_shutdown - shutdown the PAR interface.
 *
 * Power down the PAR sensor interface.
 */
void
par_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    power_off(ALGDEV_POWER1);
    atod_shutdown();
}

/**
 * par_dev_ready - check if device is ready.
 *
 * Returns true if device is ready to be sampled.
 */
int
par_dev_ready(void)
{
    return ref;
}

/**
 * par_read_data - read a sample from the PAR sensor.
 *
 * Returns next sample.  The upper 16-bits contains the low gain sample
 * and the lower 16-bits contain the high gain sample.
 */
long
par_read_data(void)
{
    long	x;
    
    x = atod_read(PAR_CHANNEL0);
#ifdef PAR_CHANNEL1
    atod_read(PAR_CHANNEL1);	/* First value is garbage */
    x |= ((unsigned long)atod_read(PAR_CHANNEL1) << 16);
#endif
    
    return x;
}
