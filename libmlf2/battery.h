/**@file
** $Id: battery.h,v 4b8aa7bc83d8 2008/08/06 19:01:36 mikek $
*/
#ifndef _BATTERY_H_
#define _BATTERY_H_

typedef struct {
    float	v;
    float	vmax;
    float	vmin;
} battery_state_t;

typedef enum {BATTERY_12v=0, BATTERY_15v=1} battery_pack_t;

/** Convert A/D counts to battery voltage */
#define COUNTS_TO_VOLTS(c)	((float)(c)*4.322e-3)

/** Convert battery-pack index to A/D channel */
#define PACK_TO_CHAN(n)		((n) + 5)

void read_battery(battery_pack_t which, unsigned int n_samples, battery_state_t *result);

#endif /* _BATTERY_H_ */
