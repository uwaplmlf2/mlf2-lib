/*
** 
*/
#ifndef _MCP_H_
#define _MCP_H_

#include "serialpar.h"

#define MCP_DEVICE         6
#define MCP_BAUD           9600

int mcp_init(void);
void mcp_shutdown(void);
int mcp_dev_ready(void);
int mcp_read_data(ParData *pd, long timeout);
void mcp_test(void);

#endif