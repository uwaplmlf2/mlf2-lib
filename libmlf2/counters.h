/**@file
 * Resetable counters.
 *
 */
#ifndef _COUNTERS_H_
#define _COUNTERS_H_

/**
 * Maintain the state of a Counter.
 *
 * A Counter counts down to zero at which point it *fires* and
 * is reset.
 */
typedef struct counter {
    unsigned long       value; /**< current value */
    unsigned long       reset; /**< reset value */
} Counter_t;

/**
 * Maintain the state of a CycleCounter.
 *
 * A CycleCounter has an active/inactive duty-cycle. The duty cycle
 * is set with CCOUNTER_INIT() and the initial state is set with
 * CCOUNTER_ON() or CCOUNTER_OFF(). Thereafter, CCOUNTER_CHECK() is
 * used to return the current state and advance the counter.
 */
 typedef struct ccounter {
     unsigned long   on;  /**< current state of the ON counter */
     unsigned long   off;  /**< current state of the OFF counter */
     unsigned long   on_reset; /**< reset value for ON counter */
     unsigned long   off_reset; /**< reset value for OFF counter */
} CycleCounter_t;

#define CCOUNTER_INIT(t_on, t_off)  {.on_reset = (t_on), .off_reset = (t_off)}
#define COUNTER_INIT(v, r)          {.value = (v), .reset = (r)}

enum {CC_OFF=0, CC_ON=1, CC_DISABLED=-1};

/**
 * Set the CycleCounter state to ON.
 */
static __inline__ void CCOUNTER_ON(CycleCounter_t *cp)
{
    cp->on = cp->on_reset;
    cp->off = 0;
}

/**
 * Set the CycleCounter state to OFF.
 */
static __inline__ void CCOUNTER_OFF(CycleCounter_t *cp)
{
    cp->off = cp->off_reset;
    cp->on = 0;
}

/**
 * Disable a CycleCounter.
 */
static __inline__ void CCOUNTER_DISABLE(CycleCounter_t *cp)
{
    cp->on = cp->off = 0;
}

/**
 * Return the current state of a CycleCounter and advance the
 * internal counters.
 *
 * @returns current state; 1 (ON) or 0 (OFF).
 */
static __inline__ int CCOUNTER_CHECK(CycleCounter_t *cp)
{
    int r = CC_DISABLED;

    if(cp->on != cp->off)
    {
        if(cp->on > 0)
        {
            r = CC_ON;
            cp->on--;
            if(cp->on == 0)
            {
                r = CC_OFF;
                cp->off = cp->off_reset;
            }
        }
        else
        {
            r = CC_OFF;
            cp->off--;
            if(cp->off == 0)
            {
                r = CC_ON;
                cp->on = cp->on_reset;
            }
        }
    }
    return r;
}

/**
 * Test and decrement a Counter.
 *
 * If the Counter is at zero, it *fires* and the return value
 * will be 1, also, the Counter will be set to its *reset*
 * value. The Counter is then decremented.
 *
 * @returns the state (1 or 0) before the counter was decremented.
 */
static __inline__ int COUNTER_DEC_TEST(Counter_t *cp)
{
    int         r = 0;

    if(cp->value == 0)
    {
        cp->value = cp->reset;
        r = 1;
    }

    cp->value--;

    return r;
}

/**
 * Set a Counter to its /reset/ value.
 */
static __inline__ void COUNTER_RESET(Counter_t *cp)
{
    cp->value = cp->reset;
}

/**
 * Set a Counter to zero.
 */
static __inline__ void COUNTER_SET(Counter_t *cp)
{
    cp->value = 0;
}

/**
 * Returns /true/ if the Counter /fired/ on the previous call
 * of COUNTER_DEC_TEST().
 */
static __inline__ int COUNTER_TEST(Counter_t *cp)
{
    return cp->value == (cp->reset - 1);
}

#endif
