/**@file
** Interface to the SonTek Argonaut Acoustic Doppler Current Meter.
**
** $Id: adcp.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
**
**
*/
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include "salarm.h"
#include "serial.h"
#include "iostream.h"
#include "log.h"
#include "lpsleep.h"
#include "adcp.h"

#define COMMAND_PROMPT	">"
#define ADCP_INIT_TIMEOUT	10L
#define WARM_UP_MSECS		1900L

#define SONTEK_CSUM_OFFSET	0xa596

#define SAMPLE_PACKET 		(sizeof(FloatHdrType)+\
                                 (sizeof(FloatDataType)*10)+\
                                 10L)

#define PKT_OK		0
#define E_PKTCSUM	-1
#define E_PKTTIMEO	-2
#define E_PKTLEN	-3

/* Time allowed to receive a packet (ms) */
#define PKT_TIME	3000L

#define PURGE_INPUT(d) while(sgetc_timed(d, MilliSecs(), 5L) != -1)

static unsigned char spktbuf[SAMPLE_PACKET];

static int ref = 0;
static int serial_dev = -1;
static jmp_buf	recover;
static long init_time;
static long sample_start_time;
static long data_acq_time;
static unsigned int data_nr_samples;
static int have_magnetometer_data;
static unsigned long nr_reads, nr_errors;

static __inline__ int sgetc(int desc)
{
    unsigned char	c;
    return (serial_read(desc, &c, (size_t)1) == 1) ? (int)c : -1;
}

static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char	c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
	if((MilliSecs() - start) >= timeout)
	    return -1;
    }
    return ((int) c)&0xff;
}

static void
dump_packet(unsigned char *buf, int n)
{
    printf("PKT:");
    while(n--)
	printf("%02x ", *buf++);
    putchar('\n');
}

  
static int
read_packet(unsigned char *buf, int n, int csum_start_hack)
{
    unsigned short	len, csum, sum;
    int			cbuf[2], c, i;
    long		start;
    
    start = MilliSecs();
    if((cbuf[0] = sgetc_timed(serial_dev, start, PKT_TIME+1000L)) == -1 ||
       (cbuf[1] = sgetc_timed(serial_dev, start, PKT_TIME)) == -1)
	return E_PKTTIMEO;


    len = (cbuf[0] & 0xff) | (cbuf[1] << 8);
    len -= 4;
    if(len <= 0 || len > n)
	return E_PKTLEN;
    csum = SONTEK_CSUM_OFFSET;
    for(i = 0;i < len;i++)
    {
	if((c = sgetc_timed(serial_dev, start, PKT_TIME)) == -1)
	    return i;
	buf[i] = c;
	if(i == csum_start_hack)
	    csum = SONTEK_CSUM_OFFSET;
	csum += c;
    }
    
    if((cbuf[0] = sgetc_timed(serial_dev, start, PKT_TIME)) == -1 ||
       (cbuf[1] = sgetc_timed(serial_dev, start, PKT_TIME)) == -1)
	return E_PKTTIMEO;
    sum = (cbuf[0] & 0xff) | (cbuf[1] << 8);
    
    return (sum == csum) ? len : E_PKTCSUM;
}

static char*
serial_gets(char *buf, int n, int desc)
{
    char	*p = buf;
    int		c;
    
    while(--n)
    {
	if((c = sgetc(desc)) == '\n' || c == -1)
	    break;
	*p++ = c;
    }
    *p = '\0';
    
    return buf;
}

static unsigned int
sontek_checksum(unsigned char *buf, int n)
{
    int		i;
    unsigned int	csum = SONTEK_CSUM_OFFSET;
    
    for(i = 0;i < n;i++)
	csum += buf[i];
    return csum;
}

static int
command_mode(void)
{
    if(serial_chat(serial_dev, "\r\n", COMMAND_PROMPT, 2L))
	return 1;
    serial_inflush(serial_dev);
    
    /*
    ** Send a BREAK to force the device into Command Mode.
    */
    serial_break(serial_dev);
    DelayMilliSecs(2000L);
    
    /*
    ** Now send a CR and wait for the command prompt.
    */
    if(!serial_chat(serial_dev, "\r\n", COMMAND_PROMPT, 4L))
    {
	log_error("adcp", "Cannot get command prompt\n");
	return 0;
    }

    return 1;
}

#if 0
static void
change_speed(long baud)
{
    int		code = 0;
    char	cmd[40];
    
    sprintf(cmd, "userdefaultbaudrate set %ld\r", baud);
    if(serial_chat(serial_dev, cmd, COMMAND_PROMPT, 2L))
	serial_speed(serial_dev, baud);
    
}

static void
check_adcp_speed(void)
{
    /*
    ** Send a BREAK to force the device into Command Mode.
    */
    serial_break(serial_dev);
    DelayMilliSecs(2000L);
    
    if(!serial_chat(serial_dev, "\r\n", COMMAND_PROMPT, 4L))
    {
	serial_speed(serial_dev, 9600L);
	
    }

}
#endif

void
adcp_reopen(void)
{
    serial_reopen(serial_dev);
}

/**
 * Initialize Sontek ADCP interface.
 * Open connection to ADCP device and initialize.  Errors are logged.
 *
 * @return 1 if sucessful, 0 on error
 */
int
adcp_init(void)
{
    
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(ADCP_DEVICE, 0, ADCP_BAUD)) < 0)
    {
	log_error("adcp", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    init_time = MilliSecs();

    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to adcp_sample_start and adcp_read_data.
    */
    add_after_hook("ADCP-reopen", adcp_reopen);

initdone:
    ref++;
    
    return 1;
}

/**
 * Shutdown the ADCP interface.
 * Shutdown the ADCP interface and power off the device.
 */
void
adcp_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    
    remove_after_hook("ADCP-reopen");

    serial_close(serial_dev);
}

/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
adcp_dev_ready(void)
{
    return ((MilliSecs() - init_time) >= WARM_UP_MSECS);
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
adcp_test(void)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

static void
catch_alarm(void)
{
    longjmp(recover, 1L);
}

/**
 * Set pulse parameters.
 *
 * @param  pname  parameter name
 * @param  vals  integer array of values.
 * @param  n  length of vals (number of pulses)
 * @return 1 if sucessful or 0 on error.
 */
int
adcp_set_pulse(const char *pname, int vals[], int n)
{
    int		i;
    char	*p;
    char 	cmd[64];

    if(!command_mode())
	return 0;

    if(n > 10)
	n = 10;
    p = cmd;
    p += sprintf(p, "%s", pname);
    for(i = 0;i < n;i++)
    {
	if(vals[i] > 999)	/* Allow a maximum of 3 digits */
	    return 0;
	p += sprintf(p, " %d", vals[i]);
    }
    p += sprintf(p, "\r\n");

    return serial_chat(serial_dev, cmd, "OK", 4L);
}

/**
 * Set standard ADCP parameters.
 * Set the ADCP sampling parameters.  This function also enables magnetometer
 * output and sets the coordinate system to "BEAM".  extra_cmds allows for
 * sending other commands to the device for additional configuration.  Each
 * element of the array must be a valid Sontek command string including the
 * terminating CR-LF. Errors are logged.
 *
 * @param  extra_cmds  an array of strings containing extra commands.
 * @return 1 if successful, otherwise 0.
 */
int
adcp_set_params(char **extra_cmds)
{
    char	buf[32];

    if(!command_mode())
	return 0;
    
    /*
    ** Turn off the internal recording feature.  Note that the first
    ** command sent after powering-on the Sontek sometimes fails (??) so 
    ** we try twice.
    */
    if(!serial_chat(serial_dev, "recorder off\r\n", "OK", 3L) &&
	!serial_chat(serial_dev, "recorder off\r\n", "OK", 5L))
    {
	log_error("adcp", "Timeout sending command: recorder off\n");
	return 0;
    }

	
    /*
    ** Request magnetometer data
    */
    if(!serial_chat(serial_dev, "usecompassflux set yes\r\n", "OK", 3L))
    {
	log_error("adcp", "Magnetometer data not present\n");
	have_magnetometer_data = 0;
    }
    else
	have_magnetometer_data = 1;
    
    if(!serial_chat(serial_dev, "compassinstalled set yes\r\n", "OK", 3L))
	log_error("adcp", "Compass not present\n");

    /*
    ** Set velocity coordinate system
    */
    if(!serial_chat(serial_dev, "cy BEAM\r\n", "OK", 3L))
    {
	log_error("adcp", "Timeout sending command: cy BEAM\n");
	return 0;
    }

    /*
    ** Set output format.
    */
    if(!serial_chat(serial_dev, "of BINARY\r\n", "OK", 3L))
    {
	log_error("adcp", "Timeout sending command: of BINARY\n");
	return 0;
    }
    
    /*
    ** Send the extra commands (if any).
    */
    if(extra_cmds)
    {
	while(*extra_cmds)
	{
	    if(!serial_chat(serial_dev, *extra_cmds, COMMAND_PROMPT, 4L))
		log_error("adcp", "Command failed: %s\n", *extra_cmds);
	    extra_cmds++;
	}
    }

    return 1;
}

/**
 * Set sonar sampling parameters.
 *
 * @param  si_ms  sampling interval (ms)
 * @param  nr_samples  number of samples
 * @return 1 if successful or 0 if an error occurs.
 */
int
adcp_set_sampling(long si_ms, int nr_samples)
{
    char	buf[32];

    if(!command_mode())
	return 0;

    /*
    ** Set the sampling interval (seconds)
    */
    sprintf(buf, "si %.1f\r\n", (double)si_ms/1000.);
    if(!serial_chat(serial_dev, buf, "OK", 5L))
    {
	log_error("adcp", "Timeout sending command, %s", buf);
	return 0;
    }
    
    /*
    ** Specify the number of samples to take.
    */
    sprintf(buf, "sb %d\r\n", nr_samples);
    if(!serial_chat(serial_dev, buf, "OK", 5L))
    {
	log_error("adcp", "Timeout sending command, %s", buf);
	return 0;
    }

    return 1;
}

/**
 * Save new sonar parameters.
 *
 * @return 1 if successful or 0 if an error occurs.
 */
int
adcp_save_setup(void)
{
    if(!serial_chat(serial_dev, "setup\r\n", COMMAND_PROMPT, 5L))
    {
	log_error("adcp", "Timeout sending SETUP command\n");
	return 0;
    }

    if(!serial_chat(serial_dev, "save setup\r\n", "OK\r\n>", 6L))
    {
	log_error("adcp", "Timeout sending SAVE SETUP command\n");
	return 0;
    }

    return 1;
}

/**
 * Enter sample mode.
 *
 * @return 1 if successful or 0 if an error occurs.
 */
int
adcp_sample_mode(void)
{
    if(!command_mode())
	return 0;

    /*
    ** Switch to sample mode.
    */
    if(!serial_chat(serial_dev, "sample\r\n", "OK\r\n", 5L))
    {
	log_error("adcp", "Timeout sending SAMPLE command\n");
	return 0;
    }

    serial_inflush(serial_dev);
    
    return 1;
}


/**
 * Start ADCP sampling
 * Send the "sample start" command to the ADCP. The device must be in
 * sample mode before calling this function.
 *
 * @return 1 if successful, otherwise 0.
 * @sa adcp_sample_mode()
 */
int
adcp_sample_start(void)
{
    int			mode;
    long		start;
    unsigned char	buf[6];

    mode = serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    PURGE_INPUT(serial_dev);
    
    /*
    ** Begin sampling.
    */
    serial_write(serial_dev, "G", 1L);

    if(read_packet(buf, 6, 0) != 6 && read_packet(buf, 6, 0) != 6)
    {
	log_error("adcp", "No response from G command\n");
	serial_inmode(serial_dev, mode);
	return 0;
    }

    start = MilliSecs();    
    /* Device returns sample count and sample time */
    data_nr_samples = ((unsigned)buf[1] << 8) | buf[0];
    data_acq_time = ((long)buf[5] << 24) | ((long)buf[4] << 16) |
      ((long)buf[3] << 8) | buf[2];
    data_acq_time *= 1000L;	/* Convert to milliseconds */
    sample_start_time = start;
    serial_inmode(serial_dev, mode);
    
    return 1;
}


/**
 * Test whether data is available.
 *
 * @return true if the latest sampling run is complete.
 */
int
adcp_data_ready(void)
{
    return ((MilliSecs() - sample_start_time) > data_acq_time);
}

/**
 * Fetch read error statistics.
 *
 * @param  errs  returned error count
 * @param  reads  returned read count.
 */
void
adcp_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}


/**
 * Read data from ADCP and write to a file.
 * Read a series of samples from the device and write them to a file in
 * a format that mirrors that used on the device itself. Essentially
 * a raw dump of the data.
 *
 * @param  ofp  output FILE pointer.
 * @param  ns  number of samples to read.
 * @param  ts  data timestamp.
 * @param  dt  sample interval (milliseconds)
 * @return number of samples read or 0 if an error occurs.
 */
int
adcp_store_data(FILE *ofp, int ns, long ts, long dt)
{
    int			mode;
    int			i, n, tries;
    AdcpSampleHdr	hdr;
    char		cmd[16];

    nr_reads++;
    
    /* Write the header fields in LSB order to match the data */
    hdr.nr_samples = ((long)(ns & 0xff) << 24) | ((long)(ns & 0xff00) << 8);
    hdr.timestamp = ((ts & 0xff) << 24) |
      ((ts & 0xff00) << 8) |
      ((ts & 0xff0000) >> 8) |
      ((ts & 0xff000000) >> 24);
    hdr.interval = ((dt & 0xff) << 24) |
      ((dt & 0xff00) << 8) |
      ((dt & 0xff0000) >> 8) |
      ((dt & 0xff000000) >> 24);
    fwrite(&hdr, sizeof(hdr), 1L, ofp);


    mode = serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    if(read_packet(cmd, 2, 0) == 2 || read_packet(cmd, 2, 0) == 2)
    {
	n = (int)(cmd[1]) << 8 | cmd[0];
	log_event("%d ADCP samples\n", n);
    }
    else
    {
	PURGE_INPUT(serial_dev);
    }
    
    serial_inflush(serial_dev);
    tries = 3;
    for(i = 1;i <= ns;i++)
    {
	n = sprintf(cmd, "X%dY1\r", i);
	serial_write(serial_dev, cmd, n);
	/* Read packet */
	n = read_packet(spktbuf, SAMPLE_PACKET, 6);
	if(n < 0)
	{
	    log_error("adcp", "Error on sample %d (%d)\n", i, n);
	    PURGE_INPUT(serial_dev);
	    if(tries-- > 0)
		i--;	/* Retry this sample */
	    else
	    {
		log_error("adcp", "Short record, ts=%ld ns=%d\n", ts, i-1);
		break;
	    }
	    
	}
	else
	{
	    fwrite(spktbuf, 1L, (size_t)n, ofp);
	    tries = 3;
	}
	
    }

    serial_inmode(serial_dev, mode);
    i--;
    if(i < ns)
	nr_errors++;
    
    return i;
}
