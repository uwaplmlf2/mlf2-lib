/*
** $Id: adc.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _ADC_H_
#define _ADC_H_

int adburst(int chan, int bipolar, int period, long n, short *databuf);

#endif
