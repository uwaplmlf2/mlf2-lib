/**@file
** Interface to Iridium modem.
**
** @sa ircomm.c
** @sa ircomm2.c
** @defgroup Comm
** @ingroup Comm
**
** $Id: iridium.c,v 836c7c31f63f 2008/01/07 21:07:49 mikek $
**
**
*/
#include <stdio.h>
#ifdef __GNUC__
#include <unistd.h>
#else
#include <fcntl.h>
#endif
#include <tt8.h>
#include <tt8lib.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "serial.h"
#include "ioports.h"
#include "log.h"
#include "iridium.h"

#define WARM_UP_MSECS	4000L
#define BOOT_TIME_MSECS 8000L

static int		serial_dev = -1;
static long		init_time;
static int		ref = 0;
static snumber_t	sn_cache;

/*
** Initialization commands and desired responses
*/
static char *cmd_resp[] = {
    "AT\r",		"OK",
    "AT&K0\r",		"OK",
    "ATS12=255\r",	"OK",
    NULL,	NULL
};

static __inline__ int serial_getc(int desc)
{
    unsigned char	c;
    return (serial_read(desc, &c, (size_t)1) == 1) ? ((int)c)&0xff : -1;
}


static void
change_speed(long baud)
{
    int		code = 0;
    char	cmd[16];
    
    switch(baud)
    {
	case 600:
	    code = 1;
	    break;
	case 1200:
	    code = 2;
	    break;
	case 2400:
	    code = 3;
	    break;
	case 4800:
	    code = 4;
	    break;
	case 9600:
	    code = 5;
	    break;
	case 19200:
	    code = 6;
	    break;
	case 38400:
	    code = 7;
	    break;
	default:
	    return;
    }
    
    sprintf(cmd, "AT+IPR=%d\r", code);
    if(serial_chat(serial_dev, cmd, "OK", 2L))
	serial_speed(serial_dev, baud);
    else
	log_error("iridium", "Cannot change baud rate to %ld\n", baud);
}

/**
 * Initialize Iridium serial interface.
 * Note that the argument baud is deprecated and will be removed in
 * a later version.
 *
 * @param  baud  serial port speed.
 * @return 1 if successful, 0 if an error occurs.
 */
int
iridium_init(long baud)
{
    char	**p;
    snumber_t	sn;
    
    if(ref > 0)
	goto initdone;

    /* Clear the cached serial number */
    sn_cache.data[0] = '\0';
    
    init_time = MilliSecs();    
    if((serial_dev = serial_open(IRIDIUM_DEVICE, 0, IRIDIUM_BAUD)) < 0)
    {
	log_error("iridium", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }
    
    /* Activate the serial comm interface chip */
    iop_set(IO_E, (unsigned char)0x08);
    DelayMilliSecs(250L);

    /* Shutdown serial interface chip */
    iop_set(IO_E, (unsigned char)0x02);
    
    /* Assert DTR */
    iop_clear(IO_E, (unsigned char)0x01);

    /*
    ** Allow a warm-up period then send the initialization commands.
    */
    DelayMilliSecs(BOOT_TIME_MSECS);
    
    while((MilliSecs() - init_time) < WARM_UP_MSECS)
    {
	if(iridium_dev_ready())
	{
	    log_event("Modem is ready\n");
	    break;
	}
    }

#if 0
    /*
    ** The modem powers up with a default baud rate of 19200, if the caller
    ** wants a different baud rate, change it now.
    */
    if(baud != IRIDIUM_BAUD)
	change_speed(baud);
#endif

initdone:
    ref++;

    p = &cmd_resp[0];
    while(p[0] && p[1])
    {
	if(!serial_chat(serial_dev, p[0], p[1], 2L))
	{
	    log_error("iridium", "Error sending command, %s\n", p[0]);
	    iridium_shutdown();
	    return 0;
	}
	p += 2;
	LMDelay(8000);
    }

    
    iridium_get_sn(&sn);
    
    return 1;
}


/**
 * Check if device is ready.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
iridium_dev_ready(void)
{
    return serial_chat(serial_dev, "AT\r", "OK", 2L);
}

/**
 * Get signal quality code
 *
 * @return the received signal strength as an integer between 0 and 5.
 */
int
iridium_get_sq(void)
{
    int		c, rsq, mode, start;
    char	*cmd;
    long	timeout = 6000, tstart;

    rsq = 0;
    
    serial_inflush(serial_dev);
    cmd = "AT+CSQ\r";
    serial_write(serial_dev, cmd, strlen(cmd));

    /* Set the interface to not block on input */
    mode = serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    tstart = MilliSecs();
    
    /*
    ** Response is "CSQ:N" where N is 0-5.
    */
    start = 0;
    while((MilliSecs() - tstart) < timeout)
    {
	if((c = serial_getc(serial_dev)) == -1)
	    continue;

	if(c == ':')
	{
	    start = 1;
	    continue;
	}

	/* short-circuit the loop until we see ':' */
	if(!start)
	    continue;
	
	if(isdigit(c))
	    rsq = c - '0';
	if(c == '\r' || c == '\n')
	    break;
    }
    
    serial_inmode(serial_dev, mode);
    return rsq;
}

/**
 * Get registration status
 * See Iridium AT command reference (command +CREG?) for details.
 *
 * @return registration status code, 0-5.
 */
int
iridium_get_reg(void)
{
    int		c, reg, mode, start;
    char	*cmd;
    long	timeout = 6000, tstart;

    reg = 0;
    
    serial_inflush(serial_dev);
    cmd = "AT+CREG?\r";
    serial_write(serial_dev, cmd, strlen(cmd));

    /* Set the interface to not block on input */
    mode = serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    tstart = MilliSecs();
    
    /*
    ** Response is "+CREG:MMM,NNN" where NNN is 000-005.
    */
    start = 0;
    while((MilliSecs() - tstart) < timeout)
    {
	if((c = serial_getc(serial_dev)) == -1)
	    continue;

	if(c == ',')
	{
	    start = 1;
	    continue;
	}

	/* short-circuit the loop until we see ',' */
	if(!start)
	    continue;
	
	if(isdigit(c))
	    reg = c - '0';
	if(c == '\r' || c == '\n')
	    break;
    }
    
    serial_inmode(serial_dev, mode);
    return reg;
}

/**
 * Get the modem serial number.
 * Store the modem serial number in the supplied data structure.
 *
 * @param  sn  pointer to return serial number
 * @return 1 if successful, otherwise 0.
 */
int
iridium_get_sn(snumber_t *sn)
{
    int		mode, i, r, c, end;
    char	*cmd;
    long	timeout = 1500, tstart;

    if(sn_cache.data[0])
    {
	/* Copy the cached serial number */
	strcpy(sn->data, sn_cache.data);
	return 1;
    }
    
    serial_inflush(serial_dev);
    cmd = "AT+CGSN\r";
    serial_write(serial_dev, cmd, strlen(cmd));

    /* Set the interface to not block on input */
    mode = serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    tstart = MilliSecs();
    i = 0;
    r = 0;
    end = sizeof(sn->data) - 1;
    
    /*
    ** We need to look for a string of digits followed by a \r or \n.
    */
    while(i < end && (MilliSecs() - tstart) < timeout)
    {
	if((c = serial_getc(serial_dev)) == -1)
	    continue;
	if(isdigit(c))
	    sn->data[i++] = c;
	if(i > 0 && (c == '\r' || c == '\n'))
	{
	    r = 1;
	    break;
	}
    }
    
    serial_inmode(serial_dev, mode);
    sn->data[i] = '\0';
    strcpy(sn_cache.data, sn->data);
    
    return r;
}

/**
 * Shutdown the Iridium interface.
 * Shutdown the Iridium device interface and power-off.
 */
void
iridium_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;

    /* Disconnect COM2 */
    iop_clear(IO_E, (unsigned char)0x08);
    
    /* Shutdown serial interface chip */
    iop_clear(IO_E, (unsigned char)0x02);

    /* Close the device and power-off the modem */
    serial_close(serial_dev);
    serial_dev = -1;
}

/**
 * Get serial interface descriptor.
 *
 * @returns the serial device descriptor or -1 if the interface is closed.
 */
int
iridium_serialdev(void)
{
    return serial_dev;
}

/**
 * Send a string and wait for a reply.
 * This function automates a "conversational" interaction through the
 * Iridium modem.

 * @param  send  string to send.
 * @param  expect  response string to wait for.
 * @param  timeout  maximum time to wait (seconds)
 * @return 1 if successful, otherwise 0.
 * @sa schat.c
 */
int
iridium_chat(const char *send, const char *expect, long timeout)
{
    return serial_chat(serial_dev, send, expect, timeout);
}

/**
 * Write characters to the modem.
 *
 * @param  data  array of characters to write.
 * @param  n  number of characters to write.
 * @return the number of bytes written.
 */
size_t
iridium_write(const char *data, size_t n)
{
    return serial_write(serial_dev, data, n);
}

/**
 * Read characters from the modem.
 *
 * @param  data  array of input data.
 * @param  n  maximum number of bytes to read.
 * @return the number of bytes read.
 */
size_t
iridium_read(char *data, size_t n)
{
    return serial_read(serial_dev, data, n);
}


/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
iridium_passthru(void)
{
    printf("*** Pass through mode: CTRL-C to exit\n");
    serial_passthru(serial_dev, 0x03, 0);
}


/**
 * Try to dial a phone number.
 * This function attempts to dial the specified number within the
 * time specified by timeout.
 *
 * @param  pn  phone number string.
 * @param  timeout  timeout in seconds.
 * @return 1 if successful, otherwise 0.
 */
int
iridium_try_dial(const char *pn, long timeout)
{
    int		r;
    char	*dialcmd;
    const char	*e[4];
    int		length[3];
    long	t, tstart, dt;
    
    if((dialcmd = malloc(strlen(pn)+8)) == NULL)
	return 0;
    sprintf(dialcmd, "atdt%s\r", pn);

    /* Expect strings */
    e[0] = "CONNECT";
    e[1] = "NO CARRIER";
    e[2] = "BUSY";
    e[3] = NULL;
    length[0] = strlen(e[0]);
    length[1] = strlen(e[1]);
    length[2] = strlen(e[2]);
    
    r = 0;
    
    tstart = RtcToCtm();
    t = timeout;
    dt = RtcToCtm() - tstart;
    
    while(dt < timeout)
    {
	if(serial_chat_bin_multi(serial_dev, dialcmd, (int)strlen(dialcmd),
				 e, length, t) == 1)
	{
	    r = 1;
	    break;
	}
	dt = RtcToCtm() - tstart;
	t = timeout - dt;
    }
    
    free(dialcmd);
    return r;
}

void
iridium_set_com1(void)
{
    iop_clear(IO_E, (unsigned char)0x08);
    DelayMilliSecs(10L);
    iop_set(IO_E, (unsigned char)0x04);
    DelayMilliSecs(50L);
}

void
iridium_set_com2(void)
{
    iop_clear(IO_E, (unsigned char)0x04);
    DelayMilliSecs(10L);
    iop_set(IO_E, (unsigned char)0x08);
    DelayMilliSecs(50L);
}
