/*
** $Id: orbcomm.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _ORBCOMM_H_
#define _ORBCOMM_H_

#define ORB_MAX_MSG_BODY	2048
#define ORB_MAX_SUBJ		80
#define ORB_MAX_ADDR		128
#define ORB_MAX_PKTLEN		(ORB_MAX_MSG_BODY+ORB_MAX_SUBJ+ORB_MAX_ADDR+20)
#define ORB_PKT_OVERHEAD	7
#define ORB_PKT_HEADERLEN	4

#define ORB_MAX_GGRAM_DATA_OUT	229
#define ORB_MAX_GGRAM_DATA_IN	182

#define ORB_ACK_TIMEOUT		6L
#define ORB_MAX_RETRIES		5

#ifdef __TT8__
#define ORBCOMM_DEVICE	6
#else
#define ORBCOMM_DEVICE	0
#endif
#define ORBCOMM_BAUD	4800L

/*
** Maximum number of bytes to send in a message.
*/
#define MAX_MESSAGE_LEN		ORB_MAX_MSG_BODY

typedef unsigned char byte;

/*
** The following data structures describe the various command packets
** sent between the SC and the Host (DTE) as described in the Orbcomm
** Serial Interface Specification.
*/

/* Generic packet ("Base Class") */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	payload[1];
} ORB_packet_t;

/* Link-level ACK */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	status;
    byte	checksum[2];
} ORB_ack_packet_t;

/* Configuration command */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	pin[4];
    byte	gateway_id;
    byte	def_polled;
    byte	def_ack_level;
    byte	def_rep_recipient;
    byte	def_msg_recipient;
    byte	def_priority;
    byte	def_msg_type;
    byte	def_serv_type;
    byte	gw_search_mode;
    byte	checksum[2];
} ORB_config_packet_t;

/* System command */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	type_code;
    byte	value[4];
    byte	gateway_id;
    byte	checksum[2];
} ORB_comm_packet_t;

/* System announcement */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	announce_code;
    byte	gateway_id;
    byte	destination;
    byte	checksum[2];
} ORB_sysann_packet_t;

/* SC Status */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	state;
    byte	st_diag_code;
    byte	active_mha_msg_ref;
    byte	sat_in_view;
    byte	nr_gateways;
    byte	payload[1];
} ORB_status_packet_t;

/* SC-originated message */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	gateway_id;
    byte	polled;
    byte	ack_level;
    byte	priority;
    byte	msg_type;
    byte	mha_ref_num;
    byte	nr_recipients;
    byte	subject;
    byte	payload[1];
} ORB_outmsg_packet_t;

/* SC-originated GlobalGram */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	gateway_id;
    byte	mha_ref_num;
    byte	recipient;
    byte	payload[1];
} ORB_outggram_packet_t;

/* SC-terminated message */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	gateway_id;
    byte	subject;
    byte	msg_type;
    byte	nr_recipients;
    byte	payload[1];
} ORB_inmsg_packet_t;

/* SC-terminated GlobalGram */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	gateway_id;
    byte	dgram_ref_num;
    byte	recipient;
    byte	payload[1];
} ORB_inggram_packet_t;

/* System response */
typedef struct {
    byte	dir;
    byte	type;
    byte	lsb_length;
    byte	msb_length;
    byte	retry_count;
    byte	origin;
    byte	origin_id;
    byte	status;
    byte	diag_code;
    byte	mha_ref_num;
    byte	gwy_ref_lsb;
    byte	gwy_ref_msb;
    byte	ack_mask;
    byte	checksum[2];
} ORB_sysresp_packet_t;

typedef int (*f_packet_handler)(ORB_packet_t*, unsigned long);
typedef void* (*f_datacpy)(void*, void*, size_t);

#define PACKET_LEN(p)	(((unsigned)((p)->msb_length) << 8) | (p)->lsb_length)

#define NR_PACKET_TYPES	17

/* Packet Type Codes */
typedef enum {ACK=1, CONFIG_CMD, COMM_CMD,
	      SYS_ANNOUNCE, STATUS, SC_ORIG_MSG, SC_ORIG_DEF_MSG, 
	      SC_ORIG_REP, SC_ORIG_DEF_REP, SC_ORIG_GGRAM, SYS_RESPONSE,
	      SC_TERM_MSG, SC_TERM_CMD, SC_TERM_GGRAM, POS_CMD, POS_STATUS,
	      SC_ORIG_POS} Ptype;

/* Status State Codes */
typedef enum {IDLE=0, SENDING_MSG, SENDING_RPT, SENDING_GG,
	      RECIEVING_MSG, RECEIVING_CMD, RECEIVING_GG,
	      SELF_TEST, LOCAL_LOOPBACK, GWY_LOOPBACK} StatusState;

/* Message body types */
typedef enum {MSG_TEXT=0, MSG_BINARY=14} MsgBodyType;

/* Acknowledgement levels */
typedef enum {NO_ACK=0, ACK_GWY_NON_DELIVERY, ACK_GWY_DELIVERY,
	      ACK_RECIP_NON_DELIVERY, ACK_RECIP_DELIVERY} AckLevel;

/* Priority levels */
typedef enum {PRI_LOW=0, PRI_NORMAL, PRI_URGENT, PRI_SPECIAL} PriLevel;

/* Communication Command type codes */
typedef enum {CC_GET_MSG=0, CC_GET_GGRAM=2, CC_STATUS_REQ=16, CC_CLEAR_ACTIVE, 
	      CC_CLEAR_OUTMSG, CC_CLEAR_ALL_OUTMSG, CC_CLEAR_ALL_INMSG, 
	      CC_SELF_TEST, CC_LOCAL_LOOPBACK, CC_GWY_LOOPBACK} CommCmdType;

int orbcomm_init(void);
void orbcomm_shutdown(void);
int orbcomm_dev_ready(void);
ORB_packet_t* orbcomm_read_packet(long timeout, int type, ORB_packet_t *rpkt);
int orbcomm_read_data(char *buf, int n, long timeout);
int orbcomm_send_msg(const char *to, const char *subj, MsgBodyType mtype,
		 void *src, int msglen, f_datacpy fcpy);
int orbcomm_send_text(const char *to, const char *subj, char *msg);
int orbcomm_send_binary(const char *to, const char *subj, char *msg,
		    int msglen);
int orbcomm_send_binary_file(const char *to, const char *subj, FILE *fp,
			     int msglen);
int orbcomm_send_text_file(const char *to, const char *subj, FILE *fp,
			     int msglen);
int orbcomm_get_status(ORB_status_packet_t *rpkt);
void orbcomm_add_handler(Ptype type, f_packet_handler f, 
			 unsigned long user_data);
void orbcomm_del_handler(Ptype type);
int orbcomm_dispatch(long timeout);
int orbcomm_send_comm(CommCmdType cctype);
const char* orbcomm_status_str(int code);

int kxorb_init(void);
void kxorb_shutdown(void);
int kxorb_satellite_visible(void);
int kxorb_read_message(char *buf, register int n, long timeout);
int kxorb_send_text_message(const char *msg, int to);
int kxorb_send_binary_message(const char *msg, int n, int to);
int kxorb_wait_for_ack(long timeout);
int kxorb_read_to_file(FILE *ofp, long timeout);
int kxorb_send_binary_file(FILE *ifp, int n, int to);
int kxorb_send_text_file(FILE *ifp, int n, int to);
int kxorb_wait_for_satellite(long timeout);


#endif /* _ORBCOMM_H_ */
