/*
** Interface to a Valeport Altimeter
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <setjmp.h>
#include <ctype.h>
#include "serial.h"
#include "log.h"
#include "vpalt.h"
#include "valeport.h"
#include "quote.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define START_CHAR      '$'
#define END_CHAR        '\n'

#define WARM_UP_MSECS   3000L

static int ref = 0;
static int serial_dev = -1;
static long init_time;


/**
 * Initialize the altimeter interface.
 * Open a connection to the altimeter and initialize the device.
 *
 * @return 1 if successful, otherwise 0.
 */
int
vpalt_init(void)
{
    if(ref > 0)
        goto initdone;

    if((serial_dev = valeport_init(VPALT_DEVICE, VPALT_BAUD)) < 0)
        return 0;

    init_time = MilliSecs();
initdone:
    ref++;

    return 1;
}

/**
 * Shutdown the fluorometer interface and power off the device.
 */
void
vpalt_shutdown(void)
{
    if(ref == 0 || --ref > 0)
        return;
    valeport_shutdown(serial_dev);
}


/**
 * Check if device is ready.
 *
 * @return true if the device is ready to accept commands.
 */
int
vpalt_dev_ready(void)
{
    return ref && ((MilliSecs() - init_time) > WARM_UP_MSECS);
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 */
void
vpalt_test(void)
{
    valeport_test(serial_dev);
}

/**
 * Reads the next sample value from the device.
 *
 * @param  fdp  pointer to returned data.
 * @param  timeout  read timeout in milliseconds
 * @return 1 if successful, 0 if an error occurs.
 */
int
vpalt_read_data(VpaltData *fdp, long timeout)
{
    int         n;
    VpRec       rec;
    char        qbuf[16];

    if(ref <= 0)
        return 0;

    n = valeport_read_data(serial_dev, &rec, timeout);
    if(strcmp(rec.field[0], "PRVAT") != 0)
    {
        quote_string(rec.field[0], qbuf, sizeof(qbuf)-1);
        log_error("vpalt", "Invalid data record: %s\n", qbuf);
        return 0;
    }

    if(n < 5)
    {
        log_error("vpalt", "Short data record; %d fields\n", n);
        return 0;
    }

    /*
     * Record format: $PRVAT, xx.xxx,M, xxxx.xxx, dBar*hh
     */
    fdp->alt = (float)atof(rec.field[1]);
    fdp->pr = (float)atof(rec.field[3]);

    return 1;
}
