/**@file
** Iridium communication functions.
**
** @ingroup Comm
** @sa ircomm2.c
** @sa iridium.c
**
** $Id: ircomm.c,v ee1ccc9723e7 2008/05/27 18:48:36 mikek $
**
**
*/
#include <stdio.h>
#ifdef __GNUC__
#include <unistd.h>
#else
#include <fcntl.h>
#endif
#include <tt8.h>
#include <tt8lib.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "serial.h"
#include "log.h"
#include "xmodem.h"
#include "iridium.h"

#define MIN(a, b) ((a) < (b)) ? (a) : (b)

struct bufdesc_t {
    char	*data;
    char	*current;
    char	*end;
};

static int last_index = -1;

static int
download_file(char *buf, int size, int index, void *arg)
{
    FILE	*fp = (FILE*)arg;
    
    if(index != last_index)
    {
	SerPutByte('.');
	if(fwrite(buf, (size_t)1, (size_t)size, fp) != (size_t)size)
	{
	    log_error("download", "File write error\n");
	    return 0;
	}

	last_index = index;
    }
    
    return 1;
}

static int
download_buf(char *buf, int size, int index, void *arg)
{
    struct bufdesc_t	*bp = (struct bufdesc_t*)arg;
    int			left;
    
    left = bp->end - bp->current;
    if(left < size)
	return 0;

    if(index != last_index)
    {
	SerPutByte('.');
	memcpy(bp->current, buf, size);
	bp->current += size;
	last_index = index;
    }
    
    return 1;
}

static int
upload_file(char *buf, int size, void *arg)
{
    FILE	*fp = (FILE*)arg;
    
    if(feof(fp))
	return 0;

    SerPutByte('.');
    return (int)fread(buf, 1L, (size_t)size, fp);
}

static int
upload_buf(char *buf, int size, void *arg)
{
    struct bufdesc_t 	*bp = (struct bufdesc_t*)arg;
    int			left, nr_bytes;
    
    left = bp->end - bp->current;
    if(left <= 0)
	return 0;
    nr_bytes = MIN(left, size);
    memcpy(buf, bp->current, nr_bytes);
    bp->current += nr_bytes;
    SerPutByte('.');
    return nr_bytes;
}

/**
 * Download file from remote system using Xmodem
 * This function uses the Xmodem protocol to download a file from the
 * remote system.  The Xmodem send process must have already been started
 * by the calling function.  
 *
 * @param  filename  local name for file.
 * @return 1 if successful, 0 if an error occurs.
 */
int
iridium_get_file(const char *filename)
{
    FILE	*fp;
    int		r, sd;
    
    if((sd = iridium_serialdev()) == -1)
    {
	log_error("iridium", "File transfer attempted on closed device\n");
	return 0;
    }
    
    if((fp = fopen(filename, "wb")) == NULL)
    {
	log_error("iridium", "Cannot open file %s\n", filename);
	return 0;
    }

    printf("Downloading %s ", filename);
    fflush(stdout);
    last_index = -1;
    r = xmodem_recv(sd, download_file, (void*)fp);
    fclose(fp);
    fputs(" done\n", stdout);
    
    return r;
}

/**
 * Download buffer from remote system using Xmodem
 * This function uses the Xmodem protocol to download a file from the
 * remote system. The contents of the file are written to an in-memory
 * buffer. It is the caller's responsibility to insure that the buffer
 * is large enough to hold the file contents.  The Xmodem send process 
 * must have already been started by the calling function.  
 *
 * @param  buf  pointer to memory buffer.
 * @param  size  buffer size.
 * @return number of bytes stored in buffer.
 */
size_t
iridium_get_buffer(char *buf, size_t size)
{
    struct bufdesc_t	bd;
    int			r, sd;

    if((sd = iridium_serialdev()) == -1)
    {
	log_error("iridium", "File transfer attempted on closed device\n");
	return 0;
    }
    
    bd.data = buf;
    bd.current = buf;
    bd.end = buf + size;

    printf("Downloading data ");
    fflush(stdout);
    last_index = -1;
    r = xmodem_recv(sd, download_buf, (void*)&bd);
    fputs(" done\n", stdout);

    return (r == 0) ? 0 : (size_t)(bd.current - bd.data);
}

/**
 * Upload file to remote system using Xmodem
 * This function uses the Xmodem protocol to upload a file to the
 * remote system.  The Xmodem receive process must have already been started
 * by the calling function. Uses 1024-byte packets if the file size is
 * greater than 512 bytes, otherwise it uses 128-byte packets.
 *
 * @param  filename  local name for file.
 * @return number of bytes sent if successful, 0 if an error occurs.
 */
long
iridium_put_file(const char *filename)
{
    FILE	*fp;
    int		r, sd, psize = 1024;
    long	bytes;
    
    if((sd = iridium_serialdev()) == -1)
    {
	log_error("iridium", "File transfer attempted on closed device\n");
	return 0;
    }
    
    if((fp = fopen(filename, "rb")) == NULL)
    {
	log_error("iridium", "Cannot open file %s\n", filename);
	return 0;
    }

	
    /*
    ** Use the file size to determine an appropriate packet size.
    */
    if(fseek(fp, 0L, SEEK_END) == 0 || fseek(fp, 0L, SEEK_END) == 0)
    {
	bytes = lseek(fileno(fp), 0L, SEEK_CUR);
	if(fseek(fp, 0L, SEEK_SET) < 0)
	    fseek(fp, 0L, SEEK_SET);
	
	psize = (bytes > 512L) ? 1024 : 128;
    }
    else
	log_error("iridium", "fseek failed\n");

    
    printf("Uploading %s (packet size = %d) ", filename, psize);
    fflush(stdout);
    
    r = xmodem_send(sd, upload_file, (void*)fp, psize);
    fclose(fp);
    fputs(" done\n", stdout);
    
    return r > 0 ? bytes : (long)r;
}

/**
 * Upload data to remote system using Xmodem.
 * This function uses the Xmodem protocol to upload a block of data to the
 * remote system.  The Xmodem receive process must have already been started
 * by the calling function.  
 *
 * @param  data  pointer to data buffer.
 * @param  size size of data in bytes.
 * @return 1 if successful, 0 if an error occurs.
 */
int
iridium_put_data(char *data, int size)
{
    int		        r, sd;
    struct bufdesc_t	bd;
    
    if((sd = iridium_serialdev()) == -1)
    {
	log_error("iridium", "File transfer attempted on closed device\n");
	return 0;
    }
    
    printf("Uploading from 0x%08lx ", (unsigned long)data);
    fflush(stdout);
    
    bd.data = data;
    bd.current = data;
    bd.end = data + size;
    
    r = xmodem_send(sd, upload_buf, (void*)&bd, 128);
    fputs(" done\n", stdout);
    
    return r;
}

/**
 * Upload a file in chunks.
 * This function uses Xmodem to upload a file in chunks to the remote
 * system.  ctmpl and rtmpl must be a printf-style format string containing
 * a '%d' which will be replaced by the chunk number.  Typical values for
 * ctmpl and rtmpl are shown below:
 *
 *  ctmpl = "lrx -c -q cnk%05d.dat"
 *  rtmpl = "receive cnk%05d.dat|NO CARRIER"
 *
 * It is assumed that we are logged-in to the remote system and the system is
 * ready to accept the Xmodem receive command.

 * @param  filename  name of file to upload.
 * @param  chunksize  size of each chunk.
 * @param  start  starting chunk.
 * @param  ctmpl  Xmodem receive command template for remote system.
 * @param  rtmpl  expected response template for the remote system.
 * @return index of last uploaded chunk or -1 on error.
 */
int
iridium_put_file_chunks(const char *filename, unsigned chunksize, long start,
			const char *ctmpl, const char *rtmpl)
{
    char	*cbuf, *cmd, *response;
    int		i, n, sd, cmdsize, respsize;
    FILE	*fp;

    /* Calculate maximum size of command and response strings */
    cmdsize = strlen(ctmpl) + 10;
    respsize = strlen(rtmpl) + 10;
    
    if((cbuf = malloc(chunksize+cmdsize+respsize)) == NULL)
    {
	log_error("iridium", "Cannot allocate %u bytes\n", chunksize+cmdsize);
	return 0;
    }
    
    cmd = cbuf + chunksize;
    response = cmd + cmdsize;
    
    if((sd = iridium_serialdev()) == -1)
    {
	log_error("iridium", "File transfer attempted on closed device\n");
	free(cbuf);
	return -1;
    }
    
    if((fp = fopen(filename, "rb")) == NULL)
    {
	log_error("iridium", "Cannot open file %s\n", filename);
	free(cbuf);
	return -1;
    }
    
    fseek(fp, start*chunksize, SEEK_SET);
    i = start;
    while(!feof(fp))
    {
	sprintf(cmd, ctmpl, i);
	sprintf(response, rtmpl, i);
	n = fread(cbuf, (size_t)1, (size_t)chunksize, fp);
	if(iridium_chat(cmd, response, 30L) != 1 ||
	   iridium_put_data(cbuf, chunksize) == 0)
	{
	    log_error("iridium", "Error uploading %s+%d\n", filename, i);
	    break;
	}
	
	i++;
    }

    fclose(fp);
    free(cbuf);
    return i;
}
