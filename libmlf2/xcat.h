/*
** arch-tag: 5011531a-c146-4998-9280-747851da7ad7
** Time-stamp: <2007-03-03 13:15:02 mike>
*/
#ifndef _XCAT_H_
#define _XCAT_H_

#define XCAT_DEVICE	1
#define XCAT_BAUD	9600

int xcat_setup(const unsigned char *packet, size_t n);

#endif
