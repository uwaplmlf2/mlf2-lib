/**@file
** New interface to the Seabird CTD modules.
**
** $Id: newctd.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
**
** 
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "newctd.h"

#define MAX_CTDS	2

#ifndef __GNUC__
#define __inline__
#endif

#define SAMPLE_TIMEO_MS		3000L
#define READ_INTERVAL_MS	1000L
#define WARM_UP_MS		1000L
#define SAMPLE_TIME_MS		3000L

enum { T_START=0,
       T_PUMPON,
       T_SAMPLE,
       T_READ,
       T_PROFILE,
       NR_TIMES };

/** Maintain the state of each device */
typedef struct devstate {
    int			sd; /**< serial device descriptor */
    int			ref; /**< reference count */
    int			profile_active; /**< in profile mode */
    unsigned long	nr_reads, nr_errors; /**< count read errors */
    long		timestamps[NR_TIMES]; /**< event timestamps */
} devstate_t;

static devstate_t dstab[MAX_CTDS];

#include "iofuncs.h"

#define PURGE_INPUT(d) while(sgetc_timed(d, MilliSecs(), 100L) != -1)

/**
 * General purpose data reader. Send a commmand and return
 * the result.
 *
 * @param  dsp  pointer to device state
 * @param  cmd  command string
 * @param  linebuf  buffer for returned data
 * @param  n  size of linebuf
 * @return 1 if successful or 0 if an error occurs.
 */
static int
read_data(devstate_t *dsp, const char *cmd, char *linebuf, size_t n)
{
    int		c, i, mode;
    ulong	start;
    char	cmdbuf[24];

    serial_inflush(dsp->sd);
    snprintf(cmdbuf, sizeof(cmdbuf), "%s\r\n", cmd);
    
    /* Get a prompt */
    serial_chat(dsp->sd, "\r\n", "S>", 2L);

    /* Send command and read back echo (two attempts) */
    if(!serial_chat(dsp->sd, cmdbuf, cmdbuf, 2L) &&
       !serial_chat(dsp->sd, cmdbuf, cmdbuf, 2L))
    {
	log_error("ctd", "Error sending '%s'\n", cmd);
	return 0;
    }

    /* Set the interface to not block on input */
    mode = serial_inmode(dsp->sd, SERIAL_NONBLOCKING);

    /*
     * Read a single line of data
     */
    i = 0;
    start = MilliSecs();
    while((c = sgetc_timed(dsp->sd, start, SAMPLE_TIMEO_MS)) != -1 && 
	  !isdigit(c))
	;
    if(c == -1)
    {
	log_error("ctd", "Timeout waiting for response to '%s'\n", cmd);
	serial_inmode(dsp->sd, mode);
	return 1;
    }
    else
	linebuf[i++] = c;
    while(i < n && 
	  (c = sgetc_timed(dsp->sd, start, SAMPLE_TIMEO_MS)) != -1 && 
	  c != '\r')
	linebuf[i++] = c;

    linebuf[i] = '\0';
    serial_inmode(dsp->sd, mode);
    dsp->timestamps[T_READ] = MilliSecs();
    serial_inflush(dsp->sd);

    return 1;
}

/**
 * Initialize the device interface.
 * Open connection one of the two CTD interfaces and initialize
 * the device.
 *
 * @param  which  device index, 0 or 1.
 * @return 1 if successful, 0 on error.
 */
int
newctd_init(int which)
{
    int		sd;
    devstate_t	*dsp;

    dsp = &dstab[which];
    if(dsp->ref > 0)
	return 1;

    dsp->ref = 1;
    
    if((sd = serial_open(CTD_DEVICE+which, 0, CTD_BAUD)) < 0)
    {
	log_error("ctd", "Cannot open serial device (code = %d)\n",
		sd);
	return sd;
    }

    DelayMilliSecs(WARM_UP_MS);
    
    if(!serial_chat(sd, "\r\n", "S>", 4L))
    {
	log_error("ctd", "Cannot initialize device\n");
	newctd_shutdown(which);
	return 0;
    }
    
    dsp->sd = sd;
    memset(dsp->timestamps, 0, sizeof(long)*NR_TIMES);
    dsp->timestamps[T_START] = MilliSecs();
    dsp->timestamps[T_READ] = MilliSecs();
    serial_inflush(dsp->sd);

    return 1;
}

/**
 * Shutdown the CTD interface and power off.
 *
 * @param  which  device index, 0 or 1.
 */
void
newctd_shutdown(int which)
{
    /*
     * Skip shutdown if we are in the middle of a profile
     * or if the device is already shutdown.
     */
    if(dstab[which].profile_active == 1 ||
       dstab[which].ref == 0)
	return;
    dstab[which].ref = 0;
    
    serial_close(dstab[which].sd);
}

/**
 * Check if device is ready.
 *
 * @param  which  device index, 0 or 1.
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
newctd_dev_ready(int which)
{
    return 1;
}



/**
 * Fetch read error statistics.
 *
 * @param  which  device index, 0 or 1.
 * @param  errs  returned error count
 * @param  reads  returned read count.
 */
void
newctd_get_stats(int which, unsigned long *errs, unsigned long *reads)
{
    *errs = dstab[which].nr_errors;
    *reads = dstab[which].nr_reads;
}


static void
msdelay(long ms)
{
    if(ms <= 0L)
	return;
    DelayMilliSecs(ms);
}

/**
 * Start the sampling process.
 *
 * @param  which  device index, 0 or 1.
 * @param  p  pressure value in decibars
 * @return 1 if successful or 0 if an error occurs.
 */
int
newctd_start_sample(int which, double p)
{
    devstate_t	*dsp = &dstab[which];
    char cbuf[12];
    
    if(p < 0)
	p = 0.;

    /*
    ** Delay at least READ_INTERVAL_MS milliseconds since the last
    ** sample read.
    */
    msdelay(READ_INTERVAL_MS - (MilliSecs() - dsp->timestamps[T_READ]));

    serial_inflush(dsp->sd);
    if(!serial_chat(dsp->sd, "\r\n", "S>", 2L))
    {
	if(!serial_chat(dsp->sd, "\r\n", "S>", 2L))
	{
	    log_error("ctd-start", "Device not responding\n");
	    return 0;
	}
    }

    sprintf(cbuf, "%05dTS\r\n", (int)(p*10));

    if(!serial_chat(dsp->sd, cbuf, cbuf, 2L))
    {
	log_error("ctd", "Error sending TS command\n");
	return 0;
    }
    
    dsp->timestamps[T_SAMPLE] = MilliSecs();
    serial_inflush(dsp->sd);

    return 1;
}

/**
 * Test whether sampling process is done.
 *
 * @param  which  device index, 0 or 1.
 * @return true if sampling process is done.
 */
int
newctd_data_ready(int which)
{
    devstate_t	*dsp = &dstab[which];
    return (MilliSecs() - dsp->timestamps[T_SAMPLE]) >= SAMPLE_TIME_MS;
}


/**
 * Read the most recent data sample.
 *
 * @param  which  device index, 0 or 1.
 * @param  ctd  pointer to output data structure.
 * @return 1 if successful or 0 if an error occurs.
 */
int
newctd_read_data(int which, NewCTDdata *ctd)
{
    int		rval;
    devstate_t	*dsp = &dstab[which];
    char	linebuf[64];

    memset(ctd, 0, sizeof(NewCTDdata));    
    dsp->nr_reads++;

    if((rval = read_data(dsp, "SS", linebuf, sizeof(linebuf))) > 0)
    {    
	if(sscanf(linebuf, "%f, %f", &ctd->t, &ctd->s) != 2)
	{
	    log_error("ctd", "Error reading sample (%s)\n", linebuf);
	    dsp->nr_errors++;
	    return 0;
	}
    }
    
    return rval;
}


/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 *
 * @param  which  device index, 0 or 1.
 */
void
newctd_test(int which)
{
    printf("Pass-through mode: CTRL-c to exit\n");
    serial_passthru(dstab[which].sd, 0x03, 0x02);
}

/**
 * Send a command to the device.
 * This is convenience function that is intended for use by the 
 * systest program. It sends the command string to the device
 * and checks for a successful response.
 *
 * @param  cmd  command string
 * @return 1 if successful, otherwise 0.
 */
int
newctd_cmd(int which, const char *cmd)
{
    return (serial_chat(dstab[which].sd, cmd, "?cmd S>|S>", 3L) == 2);
}

/**
 * Start a surface salinity profile.
 * The temperature and conductivity samples are stored internally and can
 * be read later with newctd_upload().
 *
 * @return timestamp in seconds or 0 if an error occurs.
 */
long
newctd_startprofile(int which, long timeout)
{
    devstate_t	*dsp = &dstab[which];
    char	cmdbuf[32];
    
    //PURGE_INPUT(dsp->sd);
    
    if(!serial_chat(dsp->sd, "outputss=n\r\n", "S>", 3L))
    {
	log_error("ctd", "Error sending 'outputss=n' command\n");
	return 0;
    }

    if(timeout > 0)
    {
	snprintf(cmdbuf, sizeof(cmdbuf),
		 "surface_timeout=%ld\r\n", timeout);
	if(!serial_chat(dsp->sd, cmdbuf, "S>", 3L))
	{
	    log_error("ctd", "Error sending 'surface_timeout' command\n");
	    return 0;
	}
	
    }
    
    if(!serial_chat(dsp->sd, "startprofile\r\n", "S>", 3L))
    {
	log_error("ctd", "Cannot start profile\n");
	return 0;
    }

    dsp->profile_active = 1;
    
    return (long)RtcToCtm();
}

int
newctd_stopprofile(int which)
{
    dstab[which].profile_active = 0;
    return newctd_cmd(which, "stopprofile\r\n");
}

/**
 * Upload stored profile data from the device.
 * The stored data is temperature and conductivity, not salinity. The returned
 * data values are formatted as follows:
 *
 * S>dd
 *  tt.tttt, cc.cccc
 *  tt.tttt. cc.cccc
 *  .
 *  .
 *  .
 *  tt.tttt, cc.cccc
 * upload complete
 * S>
 *
 * Each line is terminated by CR-LF.
 */
int 
newctd_upload(int which, int start, int end,
	      void (*callback)(void *obj, float t, float c),
	      void *cb_data)
{
    devstate_t	*dsp = &dstab[which];
    int		mode, i, c, n, lines, n_samples;
    ulong	t_start;
    float	temperature, conductivity;
    char	linebuf[64], cmdbuf[20];
    
    //PURGE_INPUT(dsp->sd);
    serial_chat(dsp->sd, "\r\n", "S>", 2L);

    /* Set the interface to not block on input */
    mode = serial_inmode(dsp->sd, SERIAL_NONBLOCKING);
    if(end > 0)
    {
	snprintf(cmdbuf, sizeof(cmdbuf), "dd%d,%d\r\n", start, end);
	sputz(dsp->sd, cmdbuf);
    }
    else
	sputz(dsp->sd, "dd\r\n");

    t_start = MilliSecs();
    i = 0;
    lines = 0;
    n = sizeof(linebuf) - 1;
    n_samples = 0;
    
    while((c = sgetc_timed(dsp->sd, t_start, SAMPLE_TIMEO_MS)) != -1)
    {
	if(c == '>')
	    break;
	if(c == '\n')
	{
	    /* end-of-line */
	    if(i > 0)
	    {
		/* 
		 * Parse the data values and pass them to the callback
		 */
		linebuf[i] = '\0';
		if(!isalpha(linebuf[0]) && 
		   sscanf(linebuf, "%f, %f", &temperature, &conductivity) == 2)
		{
		    n_samples++;
		    callback(cb_data, temperature, conductivity);
		}
		
	    }
	    i = 0;
	    lines++;
	    t_start = MilliSecs();
	}

	/* Skip the first line */
	if(lines > 0 && i < n)
	    linebuf[i++] = c;
    }

    serial_inmode(dsp->sd, mode);

    return n_samples;
}


/**
 * Take an overlap sample (primary cell and SSAL cell)
 *
 * @param  which  device index, 0 or 1.
 * @param  ctd  pointer to output data structure.
 * @return 1 if successful or 0 if an error occurs.
 */
int
newctd_overlap(int which, CTDOverlapData *ctd)
{
    int		rval;
    devstate_t	*dsp = &dstab[which];
    char	linebuf[64];

    memset(ctd, 0, sizeof(CTDOverlapData));
    
    if((rval = read_data(dsp, "TS", linebuf, sizeof(linebuf))) > 0)
    {
	if(sscanf(linebuf, "%f, %f, %f, %f", &ctd->t, &ctd->c,
		  &ctd->t_ssal, &ctd->c_ssal) != 4)
	{
	    log_error("ctd", "Error parsing TS data (%s)\n", linebuf);
	    return 0;
	}
    }
    
    return rval;
}
