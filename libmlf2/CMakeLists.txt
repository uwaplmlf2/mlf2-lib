cmake_minimum_required(VERSION 3.13)

set(SRC_FILES adcp.c ctd.c gps.c alt.c inertial.c
  rh.c noise.c atod.c par.c flr.c fileq.c
  ioschat.c iostream.c serial.c nmea.c motor.c power.c
  ioports.c ptable.c drogue.c argos.c
  schat.c iridium.c xmodem.c ircomm.c ircomm2.c eco.c
  i490.c ctdo.c newctd.c gtd.c gtd2.c ncbase.c newpr.c adv.c
  therm.c internalpr.c abort.c compress.c mctl.c rafos.c
  xponder.c nortekadv.c camera.c xcat.c analogout.c
  trios.c flntu.c optode.c cstar.c isus.c anr.c battery.c
  picadc.c mc.c arg.c xmotor.c serialpar.c qcp.c mcp.c ecopar.c
  suna.c wetlabs.c bb2f.c aux.c mag.c palargo.c oldpr.c ad2cp.c
  cellrouter.c sbe.c sbe41p.c tchain.c sbe63.c cfsbc.c sbe41apl.c
  recbuf.c valeport.c vpalt.c quote.c)

set(HEADER_FILES
  abort.h
  ad2cp.h
  ad7714.h
  adc.h
  adcp.h
  adv.h
  alt.h
  analogout.h
  anr.h
  arg.h
  argos.h
  atod.h
  aux.h
  battery.h
  bb2f.h
  camera.h
  cellrouter.h
  cfsbc.h
  compress.h
  counters.h
  cpuclock.h
  cstar.h
  ctd.h
  ctdo.h
  drogue.h
  eco.h
  ecopar.h
  fileq.h
  flntu.h
  flr.h
  gps.h
  gtd.h
  gtd2.h
  i490.h
  inertial.h
  internalpr.h
  iofuncs.h
  ioports.h
  iostream.h
  iridium.h
  isus.h
  mag.h
  mc.h
  mcp.h
  mctl.h
  motor.h
  ncattr.h
  netcdf.h
  newctd.h
  newpr.h
  nmea.h
  noise.h
  nortekadv.h
  oldpr.h
  optode.h
  orbcomm.h
  palargo.h
  par.h
  picadc.h
  power.h
  ptable.h
  qcp.h
  quote.h
  rafos.h
  recbuf.h
  rh.h
  sbe.h
  sbe41apl.h
  sbe41p.h
  sbe63.h
  serial.h
  serialpar.h
  suna.h
  tchain.h
  therm.h
  trios.h
  valeport.h
  vpalt.h
  wetlabs.h
  xcat.h
  xmodem.h
  xmotor.h
  xponder.h)

include(CheckGit)

set(TGT "mlf2v2_1")

find_package(libtt8 REQUIRED)
add_library(${TGT} STATIC ${SRC_FILES})
CheckGitSetup(${TGT})

set_target_properties(${TGT} PROPERTIES PUBLIC_HEADER "${HEADER_FILES}")
target_include_directories(${TGT} PUBLIC
  ${CMAKE_BINARY_DIR}/generated
  ${LIBTT8_INCLUDE_DIR})

install(TARGETS ${TGT}
  ARCHIVE DESTINATION lib
  PUBLIC_HEADER DESTINATION include)
file(GLOB GEN_HDRS "${CMAKE_BINARY_DIR}/generated/*.h")
install(FILES "${GEN_HDRS}" DESTINATION include)
