/**@file
** Driver for the Gas Tension Device.
**
** $Id: gtd.c,v 4b8aa7bc83d8 2008/08/06 19:01:36 mikek $
**
** This devices consists of a PIC micro-controller, a pump, and a Paros
** high-precision pressure sensor.
**
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "lpsleep.h"
#include "serial.h"
#include "log.h"
#include "ioports.h"
#include "gtd.h"

#include "iofuncs.h"

#define WARM_UP_MSECS		1000L
#define SAMPLE_TIMEO_MSECS	4000L

#define GTD_PROMPT	"GTD>\r\n"
/* end-of-data character */
#define EOD_CHAR	'G'

/* command to read the last sample */
#define CMD_READ_DATA	"%cf"

/* size of buffer for the start-sample command */
#define CMD_BUF_SIZE	80

#define PURGE_INPUT(d, t) while(sgetc_timed(d, MilliSecs(), (t)) != -1)

static int serial_dev = -1;
static int sampling = 0;
static unsigned long init_time, sample_start;

static char command_buf[CMD_BUF_SIZE];

/**
 * Convert the sample-description data structure to ASCII in the
 * format expected by the GTD controller.
 *
 * @param  gs  pointer to data structure.
 * @return pointer to GTD command string.
 * @warning not re-entrant, command is stored in a static buffer.
 */
static char *
_serialize(GTDsample_t *gs)
{
    char	*buf = command_buf;

    sprintf(buf, "CONTROLVARS=%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,",
	    gs->pres_res/100,
	    gs->temp_res/100,
	    gs->pump_mode,
	    gs->pump_on/1000,
	    gs->pump_off/1000,
	    gs->pump_cycle,
	    gs->pump_delay/1000,
	    gs->bb_delay/1000,
	    gs->bb_warmup/1000,
	    gs->bb_interrupt,
	    gs->bb_sample);

    return buf;
}

/**
 * Callback to reopen the device interface upon return from low-power sleep.
 */
static void
gtd_reopen(void)
{
    if(serial_dev >= 0)
    {
        serial_reopen(serial_dev);
        serial_inmode(serial_dev, SERIAL_NONBLOCKING);
        serial_inflush(serial_dev);
        log_event("Reopened GTD serial interface\n");
    }
}

/**
 * Initialize the device interface.
 *
 * @return 1 if successful, 0 on error.
 */
int
gtd_init(void)
{
    if(serial_dev >= 0)
	goto already_open;

    if((serial_dev = serial_open(GTD_DEVICE, 0, GTD_BAUD)) < 0)
    {
	log_error("gtd", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    init_time = MilliSecs();
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to gtd_start_sample and gtd_read_data.
    */
    add_after_hook("GTD-reopen", gtd_reopen);
    sampling = 0;
    if(!serial_chat(serial_dev, "", GTD_PROMPT, 2L))
    {
	log_error("gtd", "No response from device\n");
	gtd_shutdown();
	return 0;
    }

 already_open:
    return 1;
}

/**
 * Shutdown the device interface.
 * Shutdown the device interface.  The device is powered off.
 */
void
gtd_shutdown(void)
{
    if(serial_dev >= 0)
    {
	remove_after_hook("GTD-reopen");
	serial_close(serial_dev);
	serial_dev = -1;
	sampling = 0;
    }
}


/**
 * Check if device is ready.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
gtd_dev_ready(void)
{
    return (serial_dev >= 0) && ((MilliSecs() - init_time) > WARM_UP_MSECS);
}


/**
 * Start the pressure sample process.
 *
 * @param  gs  pointer to sample-description data structure.
 * @return 1 if successful, 0 on error.
 */
int
gtd_start_sample(GTDsample_t *gs)
{
    char	*cmd;

    if(serial_dev < 0)
        return 0;
    PURGE_INPUT(serial_dev, 200L);
    cmd = _serialize(gs);
    sputz(serial_dev, cmd);
    sample_start = MilliSecs();
    sampling = 1;

    return 1;
}

/**
 * Test whether sampling process is done.
 *
 * @return true if sampling process is done.
 */
int
gtd_data_ready(void)
{
    if(serial_dev < 0)
        return 1;
    return serial_chat(serial_dev, "", GTD_PROMPT, 2L);
}


/**
 * Read pressure and temperature values. The results are stored in
 * the corresponding fields of the GTDdata_t structure.
 *
 * @param  gd  pointer to return data structure
 * @return 1 if successful or 0 if an error occurs.  Errors are logged.
 */
int
gtd_read_data(GTDdata_t *gd)
{
    int		i, c, bufsize;
    long	start;
    char	buf[32];

    if(serial_dev < 0)
        return 0;
    sampling = 0;

    PURGE_INPUT(serial_dev, 200L);
    sputz(serial_dev, CMD_READ_DATA);

    i = 0;
    bufsize = sizeof(buf) - 1;
    start = MilliSecs();
    while((c = sgetc_timed(serial_dev, start, SAMPLE_TIMEO_MSECS)) != -1 &&
	  c != EOD_CHAR && i < bufsize)
    {
	buf[i++] = c;
    }
    buf[i] = '\0';

    PURGE_INPUT(serial_dev, 200L);

    return sscanf(buf, "%f %f", &gd->pressure, &gd->temperature) == 2;
}


/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 */
void
gtd_test(void)
{
    if(serial_dev >= 0)
    {
        printf("Pass-through mode: CTRL-c to exit\n");
        serial_passthru(serial_dev, 0x03, 0x02);
    }
}

/**
 * Return TRUE if the GTD is sampling.
 */
int
gtd_sampling(void)
{
    return sampling;
}
