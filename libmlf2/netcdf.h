/*
** arch-tag: netcdf header file
*/
#ifndef _NETCDF_H_
#define _NETCDF_H_

#define NC_BYTE		1L
#define NC_CHAR		2L
#define NC_SHORT	3L
#define NC_INT		4L
#define NC_FLOAT	5L
#define NC_DOUBLE	6L
#define NC_DIMENSION	10L
#define NC_VARIABLE	11L
#define NC_ATTRIBUTE	12L

#define NC_MAGIC	0x43444601L	/* 'C' 'D' 'F' '\001' */

typedef long		int32;
typedef unsigned long	uint32;
typedef short		int16;
typedef unsigned short	uint16;

struct dim {
    const char	*name;
    int32	length;
};

struct attr {
    const char	*name;
    int32	type;
    const char	*value;
};

struct var {
    const char	*name;
    int32	ndims;
    int32	*dim_id;
    int		nattrs;
    struct attr	*attr;
    int32	type;
    int32	vsize;
    int32	begin;
    int32	offset;
    int		isrec;
};

typedef struct {
    int32		size;
    int32		hsize;
    uint32		count;
#ifdef COMPRESSED_DATA
    gzFile		gz;
#else
    int			gz;
#endif
    struct var		*vars;
    unsigned char	*data;
} netCDFdesc;

/*
** Macro to allocate and initialize a netCDFdesc data structure for each
** file "type".
*/
#define ALLOC_STRUCTURE(type)\
    total_size = nbytes + sizeof(netCDFdesc) + sizeof(type ## _vars);\
    if((nd = (netCDFdesc*)malloc(total_size)) == NULL)\
	return (netCDFdesc*)0;\
    memset((char*)nd, 0, total_size);\
    nd->vars = (struct var*)((char*)nd + sizeof(netCDFdesc));\
    nd->data = (unsigned char*)((char*)nd->vars + sizeof(type ## _vars));\
    nd->count = 0;\
    nd->gz = 0;\
    nd->size = nbytes;\
    nd->hsize = ftell(ofp);\
    memcpy((char*)nd->vars, (char*)&type ## _vars[0], sizeof(type ## _vars))

typedef int (*nc_write_v)(netCDFdesc*, const char*, void*, int);

long write_nc_header(FILE *ofp, struct dim *dims, int32 ndims, 
		     struct var *vars, int32 nvars, const char *desc);
int add_to_record(void *data, int size, unsigned char *rec, struct var *vars, 
		  int32 n, const char *name);
int flush_record(netCDFdesc *nd, FILE *ofp);
void sync_file(netCDFdesc *nd, FILE *ofp);
int write_env_variable(netCDFdesc *nd, const char *name, void *data, int size);
netCDFdesc *write_env_header(FILE *ofp, const char *desc, int nr_ctd);

int write_wave_variable(netCDFdesc *nd, const char *name, void *data, 
			int size);
netCDFdesc *write_wave_header(FILE *ofp, const char *desc, long ns);
int write_adcp_variable(netCDFdesc *nd, const char *name, void *data, 
			int size);
netCDFdesc *write_adcp_header(FILE *ofp, const char *desc, long ns, 
			      char *zfile);

int write_telem_variable(netCDFdesc *nd, const char *name, void *data, 
			 int size);
netCDFdesc *write_telem_header(FILE *ofp, const char *desc, int nr_ctd);

int write_noise_variable(netCDFdesc *nd, const char *name, void *data, 
			 int size);
netCDFdesc *write_noise_header(FILE *ofp, const char *desc, long ns);

int write_fast_variable(netCDFdesc *nd, const char *name, void *data, 
			int size);
netCDFdesc *write_fast_header(FILE *ofp, const char *desc, int nr_ctd);

int write_gps_variable(netCDFdesc *nd, const char *name, void *data, int size);
netCDFdesc *write_gps_header(FILE *ofp, const char *desc);
int write_fpr_variable(netCDFdesc *nd, const char *name, void *data, int size);
netCDFdesc* write_fpr_header(FILE *ofp, const char *desc);

#endif
