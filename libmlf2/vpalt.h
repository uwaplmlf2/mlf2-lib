/*
*/
#ifndef _VPALT_H_
#define _VPALT_H_

#define VPALT_DEVICE    3
#define VPALT_BAUD      38400L

typedef struct vpalt {
    float       alt;        /**< Altitude in meters */
    float       pr;         /**< Absolute pressure in decibars */
} __attribute__((packed)) VpaltData;

/* Constants to describe the s-expression output format */
#define VPALT_DATA_FMT          ">2f"
#define VPALT_DATA_DESC         "alt,pr"


int vpalt_init(void);
void vpalt_shutdown(void);
int vpalt_dev_ready(void);
int vpalt_read_data(VpaltData *dp, long timeout);
void vpalt_test(void);

#endif /* _VPALT_H_ */
