/**@file
** Interface to Valeport Altimeter.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <setjmp.h>
#include <ctype.h>
#include "serial.h"
#include "log.h"
#include "valeport.h"
#include "recbuf.h"
#include "quote.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define START_CHAR      '$'
#define END_CHAR        '\n'
#define SEP_CHAR        ','

/**
 * Initialize the device interface.
 *
 * @param devnum  serial device number.
 * @param baud    serial baud rate.
 * @return serial device ID or -1 on error
 */
int
valeport_init(int devnum, long baud)
{
    int         sdev;

    if((sdev = serial_open(devnum, 0, baud)) < 0)
    {
        log_error("valeport", "Cannot open serial device (code = %d)\n",
                  sdev);
        return -1;
    }

    serial_inmode(sdev, SERIAL_NONBLOCKING);
    return sdev;
}

/**
 * Shutdown the serial interface
 *
 * @param sdev  serial device number.
 */
void
valeport_shutdown(int sdev)
{
    serial_close(sdev);
}

/**
 * Pass through mode.
 *
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 *
 * @param sdev  serial device number.
 */
void
valeport_test(int sdev)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(sdev, 0x03, 0x02);
}

/**
 * Reads the next data record from the device.
 *
 * @param  sdev  serial device ID
 * @param  rec   contains the returned data record
 * @param  timeout  read timeout in milliseconds
 * @return number of record fields read
 */
int
valeport_read_data(int sdev, VpRec *rec, long timeout)
{
    int         c, i;
    ulong       t;
    RecBuf      rb;
    enum {ST_1=1,
          ST_2,
          ST_DONE} state;

    recbuf_init(&rb, rec->buf, VP_MAX_CHARS, rec->field, VP_MAX_FIELDS);

    serial_inflush(sdev);
    t = MilliSecs();
    state = ST_1;
    i = 0;
    while(state != ST_DONE)
    {
        c = sgetc_timed(sdev, t, timeout);

        if(c < 0)
        {
            log_error("valeport", "Timeout waiting for character\n");
            state = ST_DONE;
        }

        switch(state)
        {
            case ST_1:
                if(c == START_CHAR)
                    state = ST_2;
                break;
            case ST_2:
                if(c == END_CHAR || i >= VP_MAX_CHARS)
                    state = ST_DONE;
                else
                {
                    rec->buf[i++] = c;
                }
                break;
            case ST_DONE:
                break;
        }
    }
    rec->buf[i] = '\0';

    return recbuf_split(&rb, ',');
}
