/**@file
 * Driver module for Nortek AD2CP.
 */
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <unistd.h>
#include <tt8.h>
#include <tt8lib.h>
#include "lpsleep.h"
#include "ioports.h"
#include "serial.h"
#include "iofuncs.h"
#include "log.h"
#include "ad2cp.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define MAX_DEVS    2

// Maintain the state of each device
typedef struct devstate {
    int             sd; /**< serial device descriptor */
    int             ref; /**< reference count */
    long            t0; /** < start time in ms */
} devstate_t;
static devstate_t dstab[MAX_DEVS];
static int devs_active = 0;
static char     adcp_cmd[256], adcp_resp[512];

static __inline__ void PURGE_INPUT(int d, unsigned long t)
{
    int count = 10;
    while(count-- > 0 && (sgetc_timed(d, MilliSecs(), (t)) != -1))
        ;
}

static void
write_esc_str(FILE *fp, const char *s)
{
    int     c;

    if(!s)
        return;
    fputc('"', fp);
    while(*s)
    {
        c = (int)*s;
        if(isprint(c))
        {
            if(c == '"')
                fputs("\\\"", fp);
            else
                fputc(c, fp);
        }
        else if(c == '\n')
            fputs("\\n", fp);
        else if(c == '\r')
            fputs("\\r", fp);
        else
            fprintf(fp, "\\u%04x", c);
        s++;
    }
    fputc('"', fp);
}

static int
read_next_cmd(FILE *ifp, char *buf, size_t n)
{
    int     c;
    char    *p;

    p = buf;
    while(n-- > 0)
    {
        if((c = fgetc(ifp)) == EOF || c == '\n' || c == 0x1a)
            break;
        *p++ = c;
    }

    /* Terminate each command with a carriage-return */
    if((int)(p - buf) > 0 && p[-1] != '\r')
        *p++ = '\r';

    *p = '\0';
    return (int)(p - buf);
}

static void
ad2cp_reopen(void)
{
    int  i;
    for(i = 0;i < MAX_DEVS;i++)
    {
        if(dstab[i].ref > 0)
        {
            serial_reopen(dstab[i].sd);
            serial_inmode(dstab[i].sd, SERIAL_NONBLOCKING);
            serial_inflush(dstab[i].sd);
        }
    }
}

static int
wakeup(int which)
{
    if(!serial_chat(dstab[which].sd, "MC\r", "OK\r\n", 3L))
    {
        sputz(dstab[which].sd, "@@@@@@");
        DelayMilliSecs(100L);
        sputz(dstab[which].sd, "K1W%!Q");
        DelayMilliSecs(350L);
        sputz(dstab[which].sd, "K1W%!Q");
        switch(serial_chat(dstab[which].sd, "", "CONFIRM\r\n|OK\r\n", 3L))
        {
            case 1:
                return serial_chat(dstab[which].sd, "MC\r", "OK\r\n", 3L);
                break;
            case 2:
                return 1;
                break;
            default:
                return 0;
        }
    }
    return 1;
}

/**
 * ad2cp_init - initialize the interface.
 */
int
ad2cp_init(int which)
{
    devstate_t  *dsp;

    dsp = &dstab[which];
    if(dsp->ref > 0)
        return 1;

    if((dsp->sd = serial_open(AD2CP_DEVICE(which), 0, AD2CP_BAUD)) < 0)
    {
        log_error("ad2cp", "Cannot open serial device (code = %d)\n",
                  dsp->sd);
        return 0;
    }

    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to nor_sample_start and nor_read_data.
    */
    devs_active++;
    if(devs_active == 1)
        add_after_hook("ADCP-reopen", ad2cp_reopen);

    serial_inmode(dsp->sd, SERIAL_NONBLOCKING);
    dsp->ref = 1;

    return 1;
}

/**
 * ad2cp_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
ad2cp_shutdown(int which)
{
    if(dstab[which].ref == 0)
        return;
    dstab[which].ref = 0;
    serial_close(dstab[which].sd);
    devs_active--;
    if(devs_active == 0)
    {
        remove_after_hook("ADCP-reopen");
    }
}


/**
 * ad2cp_cmd - send a command to the device.
 *
 * @param cmd  command string (including \r)
 * @param resp  if non-NULL buffer for the response.
 * @param rlen  maximum response length.
 * @return 1 if successful, 0 on error.
 */
int
ad2cp_cmd(int which, const char *cmd, char *resp, int rlen)
{
    int rval = 1;

    PURGE_INPUT(dstab[which].sd, 200L);
    if(!wakeup(which) && !wakeup(which))
    {
        log_error("ad2cp", "No response from device\n");
        return 0;
    }

    if(serial_chat(dstab[which].sd, cmd, "OK\r\n|ERROR\r\n", 5L) != 1)
    {
        log_error("ad2cp", "Bad command response\n");
        rval = 0;
    }

    if(rlen > 0 && resp != NULL)
        serial_chat_response(resp, (size_t)rlen);
    return rval;
}

/**
 * ad2cp_sync_clock - set device clock to TT8 clock.
 *
 * @return 1 if successful, 0 on error.
 */
int
ad2cp_sync_clock(int which)
{
    time_t      now;
    struct tm   *t;
    char        cmd[48];

    time(&now);
    t = localtime(&now);
    strftime(cmd, sizeof(cmd)-1,
             "SETCLOCKSTR,TIME=\"%Y-%m-%d %H:%M:%S\"\r", t);
    return ad2cp_cmd(which, cmd, NULL, 0);
}

/**
 * ad2cp_download - download telemetry data.
 *
 * @param ofp  output FILE pointer.
 * @param offset  start address of first byte to be downloaded
 * @param nbytes  maximum number of bytes to download
 * @return number of bytes downloaded.
 */
long
ad2cp_download(int which, FILE *ofp, unsigned long offset, unsigned long nbytes)
{
    long        t, bytes, wd_counter, n;
    int         c;
    char        cmd[48];
    enum {ST_1=1,
          ST_2,
          ST_3,
          ST_DONE} state;
    devstate_t  *dsp;

    dsp = &dstab[which];

    PURGE_INPUT(dsp->sd, 200L);

    if(!wakeup(which) && !wakeup(which))
    {
        log_error("ad2cp", "No response from device\n");
        return 0;
    }

    snprintf(cmd, sizeof(cmd)-1,
             "DOWNLOADTM,%lu,%lu,CRC=0,CKS=1\r",
             offset, nbytes);
    sputz(dsp->sd, cmd);
    t = MilliSecs();
    state = ST_1;
    n = bytes = 0;
    /*
     * Countdown to stroke the watchdog (approx 30s at 9600 baud).
     * Servicing the watchdog results in a 1ms delay (at 16Mhz) which
     * is too much time to expend on every iteration.
     */
    wd_counter = 30000;
    while(state != ST_DONE)
    {
        c = sgetc_timed(dsp->sd, t, 3000L);

        if(c < 0)
        {
            log_error("ad2cp", "Timeout: state=%d remaining=%ld\n",
                      state, n);
            return (bytes - n);
        }

        if(--wd_counter == 0)
        {
            PET_WATCHDOG();
            wd_counter = 30000;
        }

        switch(state)
        {
            case ST_1:
                if(isdigit(c))
                    bytes = bytes*10 + (long)(c - '0');
                else
                    state = ST_2;
                break;
            case ST_2:
                if(c == '\n')
                {
                    log_event("Storing %ld bytes from ADCP\n", bytes);
                    n = bytes;
                    state = (bytes > 0) ? ST_3 : ST_DONE;
                }
                break;
            case ST_3:
                t = MilliSecs();
                fputc(c, ofp);
                if(--n <= 0)
                    state = ST_DONE;
                break;
            case ST_DONE:
                break;
        }
    }

    serial_chat(dsp->sd, "", "OK\r\n", 4L);
    return (bytes - n);
}

/**
 * ad2cp_cmd_log - send a command to the device and log the transaction.
 *
 * @param cmd  command string (including \r)
 * @param ofp  output log file.
 * @param verbose  if non-zero print commands and responses to stdout
 * @return 1 if successful, 0 on error.
 */
int
ad2cp_cmd_log(int which, const char *cmd, FILE *ofp, int verbose)
{
    int rval = 1;

    /*
     * The ADCP command log is a JSON format list. Each
     * list element is an with the following attributes:
     *
     *   t: time-stamp in seconds since 1/1/1970 UTC
     *   cmd: command string
     *   resp: response string
     */
    if(ofp != NULL)
    {
        fprintf(ofp, "{\"t\": %ld, \"cmd\": ", RtcToCtm());
        write_esc_str(ofp, cmd);
    }

    if(verbose)
    {
        fputs("< ", stdout);
        write_esc_str(stdout, cmd);
        fputc('\n', stdout);
    }

    rval = ad2cp_cmd(which, cmd, adcp_resp, sizeof(adcp_resp)-1);

    if(ofp != NULL)
    {
        fputs(", \"resp\": ", ofp);
        write_esc_str(ofp, adcp_resp);
        fputs("},\n", ofp);
    }

    if(verbose)
    {
        fputs("> ", stdout);
        write_esc_str(stdout, adcp_resp);
        fputc('\n', stdout);
    }

    return rval;
}

/**
 * ad2cp_load_commands - load a series of commands from a file.
 *
 * @param ifp  input command file, one command per line.
 * @param ofp  output log file.
 * @param verbose  if non-zero print commands and responses to stdout
 */
void
ad2cp_load_commands(int which, FILE *ifp, FILE *ofp, int verbose)
{
    while(read_next_cmd(ifp, adcp_cmd, sizeof(adcp_cmd)-1))
    {
        if(ad2cp_cmd_log(which, adcp_cmd, ofp, verbose) == 0)
            ad2cp_cmd_log(which, "GETERROR\r", ofp, verbose);
    }
}

/**
 * ad2cp_deploy - start a deployment
 *
 * Read a series of commands from AD2CP_CMD_FILE and pass each command
 * in-turn to the device. Each command and response is logged to a
 * JSON-format file, AD2CP_LOG_FILE.
 *
 * @param keep  if non-zero, do not remove the command file
 * @param verbose  if non-zero print commands and responses to stdout
 * @return 1 if successful, 0 if the START command fails.
 */
int
ad2cp_deploy(int which, int keep, int verbose)
{
    FILE        *ifp, *ofp;
    int         rval = 1;

    ofp = fopen(AD2CP_LOG_FILE, "wb");
    if(ofp == NULL)
        log_error("ad2cp", "Cannot open %s\n", AD2CP_LOG_FILE);
    else
        fputc('[', ofp);

    if(ad2cp_sync_clock(which))
    {
        if((ifp = fopen(AD2CP_CMD_FILE, "rb")) != NULL)
        {
            ad2cp_load_commands(which, ifp, ofp, verbose);
            fclose(ifp);
            if(!keep)
                unlink(AD2CP_CMD_FILE);
        }
        else
            log_error("ad2cp", "Command file not found\n");
    }
    else
    {
        if(ad2cp_cmd(which, "GETERROR\r", adcp_resp, sizeof(adcp_resp)-1))
        {
            if(ofp != NULL)
            {
                fprintf(ofp, "[{\"t\": %ld, \"cmd\": \"GETERROR\", \"resp\": ", RtcToCtm());
                write_esc_str(ofp, adcp_resp);
                fputs("}]", ofp);
            }

            if(verbose)
            {
                fputs("< GETERROR\n", stdout);
                fputs("> ", stdout);
                write_esc_str(stdout, adcp_resp);
                fputc('\n', stdout);
            }
        }
        else
            log_error("ad2cp", "Cannot get error info\n");
    }

    log_event("Sending START to ADCP %d\n", which);
    if((rval = ad2cp_cmd_log(which, "start\r", ofp, verbose)) == 0)
        ad2cp_cmd_log(which, "GETERROR\r", ofp, verbose);

    if(ofp != NULL)
    {
        fputc(']', ofp);
        fclose(ofp);
    }

    return rval;
}

/**
 * ad2cp_halt - stop a deployment and (optionally) store telemetry data
 *
 * @param fp  if non-null, points to telemetry data file.
 * @param maxsize  maximum size of telemetry data.
 * @param blocksize  number of bytes per download request.
 * @return 1 if successful, 0 on error.
 */
int
ad2cp_halt(int which, FILE *fp, unsigned long maxsize, unsigned long blocksize)
{
    int             status;
    long            total_bytes, n;
    unsigned long   offset;
    devstate_t  *dsp;

    dsp = &dstab[which];

    PURGE_INPUT(dsp->sd, 200L);
    if(!wakeup(which) && !wakeup(which))
    {
        log_error("ad2cp", "Cannot interrupt sampling\n");
        return 0;
    }

    status = 1;
    if(fp)
    {
        total_bytes = 0;
        offset = 0;
        while(total_bytes < maxsize &&
              (n = ad2cp_download(which, fp, offset, blocksize)) > 0)
        {
            offset += blocksize;
            total_bytes += n;
            n = maxsize - total_bytes;
            if(blocksize > n)
                blocksize = n;
        }
        log_event("Downloaded %ld bytes of telemtry data\n", total_bytes);
    }

    if(!ad2cp_cmd(which, "powerdown\r", NULL, 0))
    {
        log_error("ad2cp", "Device not sleeping\n");
        status = 0;
    }

    return status;
}
