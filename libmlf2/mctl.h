/*
** arch-tag: ad1bea4c-49f1-4b20-866c-b02362067c54
*/
#ifndef _MCTL_H_
#define _MCTL_H_

void mission_setup(int (*pvalidate)(const char*));
long mission_read(const char *file);
int params_read(const char *file);

#endif /* _MCTL_H_ */
