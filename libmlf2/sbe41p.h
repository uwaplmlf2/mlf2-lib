#ifndef SBE41P_H
#define SBE41P_H

#include "newctd.h"

#define SBE41P_DEVICE(n)    (CTD_DEVICE+n)
#define SBE41P_BAUD         CTD_BAUD

int sbe41p_init(int which);
void sbe41p_shutdown(int which);
void sbe41p_test(int which);
int sbe41p_isprofiling(int which);
int sbe41p_sample(int which, long timeout, NewCTDdata *ctd, float *p);
int sbe41p_slpt(int which, long timeout, NewCTDdata *ctd, float *p);
int sbe41p_start_profile(int which, long timeout);
int sbe41p_stop_profile(int which, long timeout);
int sbe41p_upload(int which, int start, int end,
                  void (*callback)(void *obj, char *linebuf),
                  void *cbdata);
int sbe41p_dev_ready(int which);
int sbe41p_start_pump(int which, long timeout);
int sbe41p_stop_pump(int which, long timeout);
#endif /* SBE41P_H */
