/**@file
 **
 ** $Id: battery.c,v d2d4d6db57fb 2008/07/07 21:13:38 mikek $
 **
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stddef.h>
#include <tt8.h>
#include <tt8lib.h>
#include "ioports.h"
#include "atod.h"
#include "adc.h"
#include "battery.h"

#define WARMUP_MSECS	50L

#define MAX(a, b)	((a) > (b) ? (a) : (b))
#define MIN(a, b)	((a) < (b) ? (a) : (b))

/**
 * Sample the battery voltage.
 *
 * @param  which  battery pack index (BATTERY_12v, BATTERY_15v)
 * @param  n_samples  number of samples to average.
 * @param  result  returned battery data.
 */
void
read_battery(battery_pack_t which, unsigned int n_samples, battery_state_t *result)
{
    short		counts, chan;
    unsigned int	i;
    double		v, sum_v;

    result->v = 0.;
    result->vmax = 0.;
    result->vmin = 100.;
    sum_v = 0.;
    chan = PACK_TO_CHAN(which);
    
    atod_init();
    iop_set(IO_D, (unsigned char)0x20);
    DelayMilliSecs(WARMUP_MSECS);
    for(i = 0;i < n_samples;i++)
    {
	counts = atod_read(chan);
	v = COUNTS_TO_VOLTS(counts);
	sum_v += v;
	result->vmax = MAX(result->vmax, v);
	result->vmin = MIN(result->vmin, v);
    }

    result->v = (sum_v - result->vmax - result->vmin)/(double)(n_samples - 2.);
    
    iop_clear(IO_D, (unsigned char)0x20);
    atod_shutdown();
}
