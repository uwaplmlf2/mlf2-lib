/*
** arch-tag: 3041b81d-e827-4372-ae8e-9e9c177967b4
** Time-stamp: <2016-07-04 20:23:17 mike>
**
** Manage the queue of files to be transmitted to shore.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <tt8lib.h>
#include <tt8.h>
#include <picodcf8.h>
#include "log.h"
#include "util.h"
#include "queue.h"
#include "compress.h"
#include "fileq.h"
#include "cpuclock.h"

#ifndef FILE_Q_NODES
#define FILE_Q_NODES    120
#endif
#define FNAMELEN        13      /* maximum file name length (8+3+.+'\0') */

static QNode qnodes[FILE_Q_NODES];
static Queue filequeue;
static Queue freelist;
static char qbuf[FNAMELEN*FILE_Q_NODES];
static int _initialized = 0;

/* Map filename extensions to compressed filename extensions */
static char *ext_map[] = {
    ".nc",      "ncz",
    ".sx",      "sxz",
    ".ntk",     "ntz",
    ".jsn",     "jsz",
    NULL,       NULL
};

/*
 * add_file - add a filename to the datafile queue.
 * @filename: name of file to add.
 *
 * Returns the file queue length.
 */
static int
add_file(const char *filename, int add_to_front)
{
    QNode       *np;

    if((np = queue_get(&freelist)) == NULL)
    {
        /* freelist is empty, remove the oldest file */
        np = queue_get(&filequeue);
        if(fileexists((char*)np->data))
           unlink((char*)np->data);
    }


    strncpy((char*)np->data, filename, (size_t)FNAMELEN);
    ((char*)np->data)[FNAMELEN-1] = '\0';
    if(add_to_front)
        queue_put_tail(&filequeue, np);
    else
        queue_put(&filequeue, np);

    return queue_size(&filequeue);
}

static void
make_compressed_filename(const char *file, char *zfile)
{
    char        *dst, **p;
    const char  *src;
    int         n;

    src = file;
    dst = zfile;
    n = FNAMELEN - 4;

    /* copy the basename */
    while(*src != '\0' && *src != '.' && n-- > 0)
        *dst++ = *src++;
    *dst++ = '.';

    p = &ext_map[0];
    while(p[0] && p[1])
    {
        if(!strcasecmp(src, p[0]))
        {
            strcpy(dst, p[1]);
            break;
        }
        p += 2;
    }

    if(p[0] == NULL)
        strcpy(dst, "z");
}

static int
add_compressed_file(const char *file, int add_to_front)
{
    int                 n = 0;
    char                zfile[FNAMELEN];

    if(!fileexists(file))
    {
        log_error("fileq", "Trying to add nonexistent file! (%s)\n", file);
        return 0;
    }

    CPU_SET_SPEED(16000000L);
    make_compressed_filename(file, zfile);
    if(!fileexists(zfile))
    {
        if(compress_file(file, zfile))
            n = add_file(zfile, add_to_front);
        else
            n = add_file(file, add_to_front);
    }
    else
        n = queue_size(&filequeue);

    CPU_RESET_SPEED();

    return n;
}

static int
already_compressed(char *ext)
{
    char        **check;
    static char *compressed_ext[] = {".z", ".ncz", ".jpg", ".sxz",
                                     NULL};

    check = &compressed_ext[0];
    while(*check)
    {
        if(!strcasecmp(ext, *check))
            return 1;
        check++;
    }

    return 0;
}

/**
 * fq_init - initialize the file queue data structures.
 */
void
fq_init(void)
{
    char        *p;
    int         i;

    queue_init(&filequeue);
    queue_init(&freelist);

    /* All nodes start out on the free-list */
    p = qbuf;
    for(i = 0;i < FILE_Q_NODES;i++,p+=FNAMELEN)
    {
        qnodes[i].data = p;
        queue_put(&freelist, &qnodes[i]);
    }

    _initialized = 1;
}

/**
 * fq_add - compress a file and add to the queue.
 * @file: file name
 *
 * Compress a file and add the name of the compressed version to the
 * file queue.  Returns the new queue length on success or zero on
 * failure.
 */
int
fq_add(const char *file)
{
    char        *p_ext;

    if(!_initialized)
        fq_init();

    if((p_ext = strrchr(file, '.')) != NULL)
    {
        if(already_compressed(p_ext))
            return add_file(file, 0);
        else
            return add_compressed_file(file, 0);
    }

    return add_compressed_file(file, 0);
}

/**
 * fq_add_next - compress a file and add to the front of the queue.
 * @file: file name
 *
 * Compress a file and add the name of the compressed version to the front of
 * the file queue.  Returns the new queue length on success or zero on
 * failure.
 */
int
fq_add_next(const char *file)
{
    char        *p_ext;

    if(!_initialized)
        fq_init();

    if((p_ext = strrchr(file, '.')) != NULL)
    {
        if(already_compressed(p_ext))
            return add_file(file, 1);
        else
            return add_compressed_file(file, 1);
    }

    return add_compressed_file(file, 1);
}

int
fq_add_uncompressed(const char *file)
{
    if(!_initialized)
        fq_init();

    return add_file(file, 0);
}

/**
 * fq_send - send the next file.
 * @fsend: file transmission function.
 * @keep: if non-zero, do not remove file from disk.
 *
 * Attempts to send the next file by passing its name to @fsend. If the
 * transmission is successful, the file is removed from the queue and
 * from the filesystem (unless @keep is non-zero).
 *
 * Returns 1 on success and 0 on failure.
 *
 */
int
fq_send(int (*fsend)(const char*), int keep)
{
    QNode       *np;

    if(!_initialized)
        return 1;

    if((np = queue_get(&filequeue)) == NULL)
        return 1;       /* queue is empty */

    if(!fileexists((char*)np->data))
    {
        /* File has been removed before it could be sent */
        log_error("fileq", "Queued file does not exist (%s)\n", (char*)np->data);
        queue_put(&freelist, np);
        /* Don't report this as an error */
        return 1;
    }

    if(fsend((char*)np->data))
    {
        /* success, remove file from filesystem */
        if(!keep)
            unlink((char*)np->data);
        queue_put(&freelist, np);
        return 1;
    }

    /* failure, file moves to the end of the queue */
    queue_put(&filequeue, np);
    log_error("fileq", "File send failed (%s)\n", (char*)np->data);

    return 0;
}

/**
 * fq_len - return file queue length
 *
 */
int
fq_len(void)
{
    return queue_size(&filequeue);
}

/**
 * fq_pop - remove and return the next filename from the queue.
 *
 * @next: will contain the filename on return
 * @namelen: maximum length of the returned filename
 *
 * Returns 1 on success and 0 on failure.
 */
int
fq_pop(char *next, size_t namelen)
{
    QNode       *np;

    if(!_initialized)
        return 0;

    if((np = queue_get(&filequeue)) == NULL)
        return 0;       /* queue is empty */

    strncpy(next, (char*)np->data, namelen);

    return 1;
}

/**
 * fq_dump - dump the file-queue to a text file.
 *
 * This is a destructive operation. All entries will be removed
 * from the queue. To restore the contents, call fq_load().
 *
 * Returns queue length on success, -1 on failure.
 */
int
fq_dump(const char *filename)
{
    FILE        *ofp;
    QNode       *np;
    int         n;

    if(!_initialized)
        return -1;


    if((ofp = fopen(filename, "w")) == NULL)
    {
        log_error("fileq", "Cannot open dump file\n");
        return -1;
    }

    n = 0;
    while((np = queue_get(&filequeue)) != NULL)
    {
        if(fileexists((char*)np->data))
        {
            n++;
            fprintf(ofp, "%s\n", (char*)np->data);
            queue_put(&freelist, np);
        }
    }

    fclose(ofp);
    return n;
}

/**
 * fq_load - load the file-queue from a text file.
 *
 * The file-queue is initialized and all contents are overwritten
 * by the contents of the named file.
 *
 * Returns the queue length on success, -1 on failure
 */
int
fq_load(const char *filename)
{
    FILE        *ifp;
    size_t      n;
    int         qlen;
    char        linebuf[32];

    if((ifp = fopen(filename, "r")) == NULL)
    {
        log_error("fileq", "Cannot load queue contents\n");
        return -1;
    }

    qlen = 0;
    while(fgets(linebuf, sizeof(linebuf), ifp) != NULL)
    {
        n = strlen(linebuf);
        if(linebuf[n-1] == '\n')
            linebuf[n-1] = '\0';
        if(fileexists(linebuf))
            qlen = fq_add_uncompressed(linebuf);
        else
            log_error("fileq", "Non-existent file %s\n", linebuf);
    }

    fclose(ifp);
    return qlen;
}

static void
show_node(QNode *np, void *datap)
{
    FILE *fp = (FILE*)datap;

    fprintf(fp, "{\"data\": \"%s\", \"next\": 0x%08lx,  \"prev\": 0x%08lx}\n",
            (char*)np->data,
            (unsigned long)np->next,
            (unsigned long)np->prev);
}

void
fq_show(FILE *fp)
{
    Queue *q = &filequeue;
    fprintf(fp, "{\"qhead\": 0x%08lx, \"qtail\": 0x%08lx}\n",
            (unsigned long)q->head, (unsigned long)q->tail);
    queue_walk(q, show_node, fp);
}
