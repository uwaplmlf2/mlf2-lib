/**@file
** Interface to zlib (gzip) compression functions.
**
** arch-tag: 0abbd90e-082e-4979-9953-bd21c4fe76b5
**
**
*/
#include <tt8lib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <zlib.h>
#include "log.h"
#include "util.h"
#include "ioports.h"

/**
 * Gzip compress a file.
 *
 * @param  infile  input file name
 * @param  outfile  output compressed file name
 * @return 1 if successful, 0 on failure.
 */
int
compress_file(const char *infile, const char *outfile)
{
    char	*buf;
    int		r;
    size_t	n;
    FILE	*ifp;
    gzFile	gz;

    if(!fileexists(infile))
	return 0;
    
    if((buf = malloc((size_t)1024)) == NULL)
    {
	log_error("compress", "Cannot allocate buffer\n");
	return 0;
    }
    
    if((ifp = fopen(infile, "rb")) == NULL)
    {
	log_error("compress", "Cannot open input file %s\n", infile);
	free(buf);
	return 0;
    }
    
    /*
     * Use "level 6" compression as this seems to provide a good balance
     * between the compression amount and memory usage.
     */
    if((gz = gzopen(outfile, "wb6")) == NULL)
    {
	log_error("compress", "Cannot open output file %s\n", outfile);
	free(buf);
	return 0;
    }
    
    while((n = fread(buf, 1L, sizeof(buf), ifp)) > 0)
    {
	PET_WATCHDOG();
	gzwrite(gz, buf, (unsigned)n);
    }
    fclose(ifp);
    
    if((r = gzclose(gz)) != Z_OK)
    {
	log_error("compress", "Compression error %d\n", r);
	free(buf);
	return 0;
    }

    free(buf);
    return 1;
}
