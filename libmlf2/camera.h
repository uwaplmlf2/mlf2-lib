/**@file
** arch-tag: 53fdc17b-1f53-43a4-a5d5-64a16e555618 SERIAL
**
*/
#ifndef _CAM_H_
#define _CAM_H_

#define CAM_DEVICE	2
#define CAM_BAUD		38400

/** Camera resolution constants */
typedef enum {RES_80x64=1, RES_160x120=3,
	      RES_320x240=5, RES_640x480=7} cam_res_t;

int cam_init(void);
void cam_shutdown(void);
int cam_dev_ready(void);
int cam_sync(void);
int cam_snapshot(cam_res_t resolution, FILE *outfp);

#endif
