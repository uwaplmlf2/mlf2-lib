/**@file
** $Id: isus.h,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
*/
#ifndef _ISUS_H_
#define _ISUS_H_

#define ISUS_DEVICE	2
#define ISUS_BAUD	38400L

/**Header for Dark data frame */
#define ISUS_DARK_HEADER	"SATNDB"
/**Header for Light data frame */
#define ISUS_LIGHT_HEADER	"SATNLB"

/**Header length */
#define ISUS_HDRLEN		6L
/**Data length for full-data frame */
#define ISUS_BINARY_FULL_SIZE	597L
/**Data length for concentration-only frame */
#define ISUS_BINARY_CONC_SIZE	36L

#define ISUS_FULL_FRAME		(ISUS_HDRLEN+ISUS_BINARY_FULL_SIZE)
#define ISUS_CONC_FRAME		(ISUS_HDRLEN+ISUS_BINARY_CONC_SIZE)

#define ISUS_DARK_FRAME		0x01
#define ISUS_LIGHT_FRAME	0x02
#define ISUS_BOTH_FRAMES	(ISUS_DARK_FRAME|ISUS_LIGHT_FRAME)

/**Output data structure */
typedef struct isus_data {
    unsigned char	dark[ISUS_FULL_FRAME];
    unsigned char	light[ISUS_FULL_FRAME];
} IsusData;

int isus_init(void);
void isus_shutdown(void);
int isus_dev_ready(void);
void isus_test(void);
int isus_start(void);
int isus_data_ready(void);
int isus_read_data(IsusData *idp, long timeout);
unsigned long isus_start_time(void);


#endif /* _ISUS_H_ */
