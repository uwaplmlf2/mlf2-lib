/*
** $Id: argos.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Interface to second Seimac ARGOS PTT.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <setjmp.h>
#include "serial.h"
#include "iostream.h"
#include "log.h"
#include "gps.h"
#include "argos.h"

unsigned short crc(char *block, int n);

#define WARM_UP_MSECS	1000L

/* Prompt the device sends when it's ready for data */
#define DATA_PROMPT	"Now:"

/* Data acknowledgement */
#define ACK_STR		"\06"

static unsigned char msg_packet[ARGOS_MSG_LEN+4];
static int 	ref = 0;
static int 	serial_dev = -1;
static long 	init_time;
static long	last_transmit;


/**
 * argos_init - initialize the ARGOS interface.
 *
 * Open a connection to the ARGOS PTT and initialize the device.  Returns
 * 1 if successful, otherwise 0.
 */
int
argos_init(void)
{
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(ARGOS_DEVICE, ARGOS_MINOR, ARGOS_BAUD)) < 0)
    {
	log_error("argos", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }
    
    init_time = MilliSecs();
    last_transmit = RtcToCtm();
    
initdone:
    ref++;


    return 1;
}

/**
 * argos_shutdown - shutdown the ARGOS interface.
 *
 * Shutdown the ARGOS interface and power off the device.
 */
void
argos_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    serial_close(serial_dev);
}


/**
 * argos_dev_ready - check if device is ready.
 *
 * Return true if the device has completed it's initialization.
 */
int
argos_dev_ready(void)
{
    return (ref && (MilliSecs() - init_time) < WARM_UP_MSECS);
}


/**
 * argos_wait - wait until device is ready to receive message data.
 * @timeout: maximum number of seconds to wait.
 *
 * Waits for the device to send the string "Send Data Now:" to indicate it
 * is ready to receive message data.  Note that the caller may have to wait
 * as long as %ARGOS_MSG_INTERVAL seconds before the device is ready.  Returns
 * 1 if the device is ready, otherwise 0.
 */
int
argos_wait(long timeout)
{
    if(serial_chat(serial_dev, NULL, DATA_PROMPT, timeout))
    {
	last_transmit = RtcToCtm();
	return 1;
    }
    
    return 0;
}

/**
 * argos_next - returns time of next transmission.
 *
 * Returns an estimate of the next ARGOS transmission time.  Note that this
 * estimate is not very accurate until the first transmission occurs.
 */
long
argos_next(void)
{
    return last_transmit + ARGOS_MSG_INTERVAL;
}

/**
 * argos_send_msg - send message data to the device.
 * @msg: message data.
 * @n: number of bytes to send.
 *
 * Sends the first @n bytes of data from @msg to the device.  The device will
 * include this data in its next transmission.  Returns 1 if message data was
 * sent sucessfully to the device or 0 if an error occured.
 */
int
argos_send_msg(const unsigned char *msg, int n)
{
    int			i;
    unsigned char	*p, csum;
    
    if(n > ARGOS_MSG_LEN)
	n = ARGOS_MSG_LEN;
    
    p = msg_packet;
    *p++ = '*';
    *p++ = (n & 0xff00) >> 8;
    *p++ = n & 0xff;
    csum = 0;
    i = n;
    
    while(i--)
    {
	*p = *msg++;
	csum += *p;
	p++;
    }
    *p = csum;
    
    return serial_chat_bin(serial_dev, msg_packet, n+4, ACK_STR, 1, 
			   ARGOS_MSG_TIME);
}

/**
 * argos_build_packet - create an ARGOS data packet.
 * @gdp: pointer to GPS data.
 * @fmt: format string describing the rest of the packet.
 * @dp: array of pointers to packet data.
 * @n: length of @dp.
 * @pkt: buffer for returned packet.
 *
 * This function uses the format string @fmt to packet the data values pointed
 * to by @dp into a "standard" ARGOS packet.  All of the packets begin with 8
 * bytes of GPS position data and end with a two byte CRC.  This leaves 21
 * bytes for other status information.  The format string is a series of
 * characters describing the layout of the data as follows:
 *
 *	b - signed character (1 byte)
 *	B - unsigned character (1 byte)
 *	h - signed short (2 bytes)
 *	H - unsigned short (2 bytes)
 *	l - signed long (4 bytes)
 *	L - unsigned long (4 bytes)
 *
 * For example, the string "LLBBh", describes a layout of two unsigned long
 * integers followed by two unsigned bytes and one signed short integer.
 *
 * The return value is the length of the packet in bytes.  It is the caller's
 * responsibility to insure that @pkt is large enough to hold the data.
 */
int
argos_build_packet(GPSdata *gdp, const char *fmt, void **dp, int n, 
		   unsigned char *pkt)
{
    unsigned char	*p;
    unsigned long	l;
    unsigned short	h, csum;
    short		x;
    int			secs, frac, i;

    p = pkt;
    
    /* Convert fractional minutes to seconds and tenths of seconds */
    secs = (long)gdp->lat.frac*60L/10000L;
    frac = (long)gdp->lat.frac*600L/10000L - (secs*10L);

    if(gdp->lat.dir == 'S')
	gdp->lat.deg *= -1;

    /* Pack degrees into the upper 9-bits and minutes into low 7 bits */
    x = ((gdp->lat.deg & 0x1ff) << 7) | (gdp->lat.min & 0x7f);
    *p++ = (x >> 8) & 0xff;
    *p++ = x & 0xff;
    *p++ = secs;
    *p++ = ((frac & 0xf) << 4) | (gdp->satellites & 0xf);

    /* Repeat for longitude */
    secs = (long)gdp->lon.frac*60L/10000L;
    frac = (long)gdp->lon.frac*600L/10000L - (secs*10L);

    if(gdp->lon.dir == 'W')
	gdp->lon.deg *= -1;

    x = ((gdp->lon.deg & 0x1ff) << 7) | (gdp->lon.min & 0x7f);
    *p++ = (x >> 8) & 0xff;
    *p++ = x & 0xff;
    *p++ = secs;
    *p++ = ((frac & 0xf) << 4) | (gdp->satellites & 0xf);
    
    i = 0;
    while(i < 21 && n--)
    {
	if(*fmt == '\0')
	    break;
	
	switch(*fmt)
	{
	    case 'b':
	    case 'B':
		*p++ = *((unsigned char*)*dp++);
		i++;
		break;
	    case 'h':
	    case 'H':
		h = *((unsigned short*)*dp++);
		*p++ = h >> 8;
		*p++ = h & 0xff;
		i += 2;
		break;
	    case 'l':
	    case 'L':
		l = *((unsigned long*)*dp++);
		*p++ = l >> 24;
		*p++ = l >> 16;
		*p++ = l >> 8;
		*p++ = l & 0xff;
		i += 4;
		break;
	    default:
		break;
	}
	fmt++;
    }

    csum = crc(pkt, (int)(p - pkt));
    *p++ = csum >> 8;
    *p++ = csum;

    return (int)(p - pkt);
}
