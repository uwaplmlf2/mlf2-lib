/*
*/
#ifndef _ARG_H_
#define _ARG_H_

#define FFTLEN_EX   10
#define FFTLEN      (1L << FFTLEN_EX)
#define BIN_SIZE    10L
#define NR_BINS     (FFTLEN/(2*BIN_SIZE) + 1)

/*
** Nominal receiver sensitivity (dB)
*/
#define ARG_RECEIVER_SENS   160

#define ARG_HIGH_GAIN_DB     40
#define ARG_LOW_GAIN_DB     20

#define ARG_FCENTER(i)  ((i < 51) ? (i+0.5)*976.5625 : 49902.34)

typedef enum {
    ARG_LOW_GAIN=0,
    ARG_HIGH_GAIN=1
} argchan_t;

void arg_init_tables(void);
void arg_init(long t_warmup);
int arg_dev_ready(void);
void arg_shutdown(void);
float* arg_sample(argchan_t chan);
int arg_store_sample(argchan_t chan);
int arg_calc_psd(argchan_t chan, int use_avg, char *psdbuf);
#endif