/*
** Driver module for Magnetometer
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "iofuncs.h"
#include "log.h"
#include "lpsleep.h"
#include "mag.h"

#ifndef __GNUC__
#define __inline__
#endif

#define PURGE_INPUT(d, t) while(sgetc_timed(d, MilliSecs(), (t)) != -1)

/* Menu prompt */
#define MAG_PROMPT      "CMD> "
/* Number of data lines to skip */
#define NSKIP           5

static int              ref = 0;
static int              serial_dev;

/**
 * Callback to reopen the device interface upon return from low-power sleep.
 */
static void
mag_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
    PURGE_INPUT(serial_dev, 100);
    log_event("Reopened Magnetometer serial interface\n");
}

/**
 * mag_init - initialize the interface.
 */
int
mag_init(void)
{
    if(ref > 0)
        goto initdone;

    if((serial_dev = serial_open(MAG_DEVICE, 0, MAG_BAUD)) < 0)
    {
        log_error("mag", "Cannot open serial device (code = %d)\n",
                  serial_dev);
        return 0;
    }

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to the device.
    */
    add_after_hook("MAG-reopen", mag_reopen);

initdone:
    ref++;
    return 1;
}

/**
 * mag_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
mag_shutdown(void)
{
    if(ref == 0)
        return;
    ref = 0;
    remove_after_hook("MAG-reopen");
    serial_close(serial_dev);
}

/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
mag_dev_ready(void)
{
    return (ref > 0) && serial_chat(serial_dev, "?", MAG_PROMPT, 3L);
}

/**
 * Reads the next sample value from the device.
 *
 * @param  mdp  pointer to returned data.
 * @param  timeout  read timeout in milliseconds
 * @return 1 if successful, 0 if an error occurs.
 */
int
mag_read_data(MagData *mdp, long timeout)
{
    int         lines, c, n;
    long        t;
    char        *p;
    static char linebuf[80];

    memset(mdp, 0, sizeof(MagData));

    /* Menu command to read magnetometer */
    sputc(serial_dev, 'e');

    lines = NSKIP;
    t = MilliSecs();
    do
    {
        p = linebuf;
        n = sizeof(linebuf) - 1;
        while((c = sgetc_timed(serial_dev, t, timeout)) != '\n')
        {
            if(c < 0)
            {
                log_error("mag", "Timeout waiting for data\n");
                return 0;
            }
            if(n > 0)
                *p++ = c;
            n--;
        }
        *p = '\0';
    } while(lines-- > 0);

    /* Send a series of <CR> characters to stop the data */
    sputz(serial_dev, "\r\r\r\r");

    /* Parse the data record */
    if(sscanf(linebuf, "X: %hd Y: %hd Z: %hd M: %hd", &mdp->x, &mdp->y,
              &mdp->z, &mdp->m) != 4)
        return 0;

    return 1;
}
