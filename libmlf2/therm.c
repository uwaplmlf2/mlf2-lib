/**@file
** Interface to the Seabird SBE-38 thermometer.
**
** $Id: therm.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
**
** 
*/
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include "serial.h"
#include "log.h"
#include "therm.h"


#ifndef __GNUC__
#define __inline__
#endif

/** Length of averaging window (samples) */
#define NAVG			4
#define STRINGIFY(x)		#x

/** Sample time in milliseconds */
#define THERM_SAMPLE_TIME	(133L*NAVG) + 339L
#define SAMPLE_TIMEO_MS		THERM_SAMPLE_TIME + 100L

enum { T_START=0,
       T_PUMPON,
       T_OXYON,
       T_SAMPLE,
       NR_TIMES };
static long	timestamps[NR_TIMES];

static int 		ref = 0;
static int 		serial_dev;
static unsigned long	nr_reads, nr_errors;

/**Initialization commands and desired responses */
static char *cmd_resp[] = {
    "FORMAT=C\r\n",			"S>",
    "DIGITS=4\r\n",			"S>",
    "NAVG=" STRINGIFY(NAVG) "\r\n",	"S>",
    "AUTORUN=N\r\n",			"S>",
    NULL,	NULL
};

static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char	c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
	if((MilliSecs() - start) >= timeout)
	    return -1;
    }
    return ((int) c)&0xff;
}

/**
 * Initialize the device interface.
 *
 * @return 1 if successful, 0 on error.
 */
int
therm_init(void)
{
    char	**p;

    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(THERM_DEVICE, THERM_MINOR, THERM_BAUD)) < 0)
    {
	log_error("therm", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    DelayMilliSecs(3000L);
    
    memset(timestamps, 0, sizeof(timestamps));
    timestamps[T_START] = MilliSecs();
initdone:
    ref++;
    
    serial_inflush(serial_dev);
    p = &cmd_resp[0];
    while(p[0] && p[1])
    {
	if(!serial_chat(serial_dev, p[0], p[1], 4L))
	{
	    log_error("therm", "Error sending command, %s\n", p[0]);
	    therm_shutdown();
	    return 0;
	}
	p += 2;
	LMDelay(16000);
    }

    serial_inflush(serial_dev);
    
    return 1;
}

/**
 * Shutdown the device interface.
 */
void
therm_shutdown(void)
{
    if(ref == 0)
	return;
    ref = 0;
    serial_close(serial_dev);
}

/**
 * Check if device is ready.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
therm_dev_ready(void)
{
    return ref;
}



/**
 * Fetch read error statistics
 * @param  errs  returned error count
 * @param  reads  returned read count.
 */
void
therm_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}


/**
 * Start the sample process.
 *
 * @return 1 if successful, 0 on error.
 */
int
therm_start_sample(void)
{
    if(!serial_chat(serial_dev, "TH\r\n", "TH\r\n", 4L))
	return 0;
    timestamps[T_SAMPLE] = MilliSecs();
    return 1;
}

/**
 * Check if data sample is ready.
 *
 * @return 1 if sample is ready, otherwise 0.
 */
int
therm_data_ready(void)
{
    if(timestamps[T_SAMPLE] == 0)
	return 1;
    return (MilliSecs() - timestamps[T_SAMPLE]) >= THERM_SAMPLE_TIME;
}


/**
 * Read the most recent sample.
 *
 * @return temperature in degrees C.
 */
float
therm_read_data(void)
{
    int		c, i, mode;
    ulong	start;
    float	T;
    char	linebuf[64];
    
    nr_reads++;
    
    if(!serial_chat(serial_dev, "\r\n", "S>", 3L))
    {
	log_error("therm", "Device not responding\n");
	nr_errors++;
	return 0;
    }

    serial_chat(serial_dev, "SH\r\n", "SH\r\n", 2L);
    
    /* Set the interface to not block on input */
    mode = serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    i = 0;
    start = MilliSecs();
    while((c = sgetc_timed(serial_dev, start, SAMPLE_TIMEO_MS)) != -1 && 
	  c != '\r')
	linebuf[i++] = c;
    linebuf[i] = '\0';
    serial_inmode(serial_dev, mode);
    
    if(sscanf(linebuf, "%f", &T) != 1)
    {
	log_error("therm", "Error reading sample (%s)\n", linebuf);
	nr_errors++;
	return 0;
    }

    return T;
}


/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
therm_test(void)
{
    printf("Pass-through mode: CTRL-c to exit\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

/**
 * Send an arbitrary command to the device.
 *
 * @param  cmd  command string (must end with CRLF).
 * @return 1 if successful, 0 on error.
 */
int
therm_cmd(const char *cmd)
{
    return (serial_chat(serial_dev, cmd, "?cmd S>|S>", 3L) == 2);
}
