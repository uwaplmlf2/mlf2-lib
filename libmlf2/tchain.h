#ifndef TCHAIN_H
#define TCHAIN_H

#define TCHAIN_DEVICE(n)    (4L+n)
#define TCHAIN_BAUD         19200L
#define TCHAIN_SENSORS      24

typedef struct {
    long    ts;
    float   data[TCHAIN_SENSORS];
} TchainData;

int tchain_init(int which);
void tchain_shutdown(int which);
int tchain_dev_ready(int which);
void tchain_test(int which);
int tchain_cmd(int which, const char *cmd, char *reply, size_t n, long timeout);
int tchain_fast_cmd(int which, const char *cmd, char *reply, size_t n, long timeout);
int tchain_sample(int which, TchainData *td);
int tchain_sync_clock(int which);
int tchain_download(int which, int dset, const char *filename);
int tchain_enable(int which, long interval_ms);
int tchain_disable(int which);
#endif /* TCHAIN_H */