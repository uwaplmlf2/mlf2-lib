/*
** $Id: newctd.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _NEWCTD_H_
#define _NEWCTD_H_

#define CTD_DEVICE	7
#define CTD_MINOR	0
#define CTD_BAUD	9600L
#define CTD_TIMEOUT	5L

#define CTD_PUMP_SLOW	0
#define CTD_PUMP_FAST	1

/** CTD data record */
typedef struct nctd {
    float		t;		/**< temperature (degrees C) */
    float		s;		/**< salinity (psu) */
} NewCTDdata;

/*
 * The CTD with a surface salinity sensor (SSAL) implements a command
 * which takes an "overlap" sample (temperature and conductivity from
 * both cells).
 */
//* CTD/SSAL overlap sample record */
typedef struct {
    float	t;		/**< primary cell temperature (degrees C) */
    float	c;		/**< primary cell conductivity (S/m) */
    float	t_ssal;		/**< SSAL cell temperature (degrees C) */
    float	c_ssal;		/**< SSAL cell conductivity (S/m) */
} CTDOverlapData;


int newctd_init(int which);
void newctd_shutdown(int which);
int newctd_dev_ready(int which);
void newctd_get_stats(int which, unsigned long *errs, unsigned long *reads);
int newctd_start_sample(int which, double p);
int newctd_data_ready(int which);
int newctd_read_data(int which, NewCTDdata *ctd);
void newctd_test(int which);
int newctd_cmd(int which, const char *cmd);
long newctd_startprofile(int which, long timeout);
int newctd_stopprofile(int which);
int newctd_upload(int which, int start, int end,
		  void (*callback)(void *obj, float t, float c),
		  void *cb_data);
int newctd_overlap(int which, CTDOverlapData *ctd);

#endif
