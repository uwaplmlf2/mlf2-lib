/*
** $Id: i490.h,v 177125ba8b95 2007/11/26 19:25:30 mikek $
*/
#ifndef _I490_H_
#define _I490_H_

#define I490_HIGH_GAIN	12	/* A/D channel */
#define I490_LOW_GAIN	13

int i490_init(void);
void i490_shutdown(void);
int i490_dev_ready(void);
long i490_read_data(void);

#endif
