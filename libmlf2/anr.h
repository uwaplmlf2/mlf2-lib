/*
** $Id: anr.h,v b6d86840376a 2009/01/14 00:08:08 mikek $
*/
#ifndef _ANR_H_
#define _ANR_H_

#define ANR_DEVICE	2L
#define ANR_BAUD	9600L

int anr_init(void);
void anr_shutdown(void);
int anr_dev_ready(void);
void anr_test(void);
int anr_start(long interval, int no_gtd);
int anr_stop(void);
int anr_cmd(const char *cmd);

#endif /* _ANR_H_ */
