/**@file
** Functions to process mission control files.
**
** arch-tag: e3c829bb-4024-46ab-98ab-3fda7ecf5f6c
**
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <tt8lib.h>
#include <tt8.h>
#include <errno.h>
#include <tpu332.h>
#include <qsm332.h>
#include <sim332.h>
#include <picodcf8.h>
#include <string.h>
#include "util.h"
#include "hash.h"
#include "ptable.h"
#include "log.h"
#include "xmlparse.h"
#include "mctl.h"

#define STRMATCH(s0, s1)	(!strcmp(s0, s1))

struct mission_t {
    long	duration;
    HashTable	*params;
};

/* Files transferred by Xmodem might padded with CTRL-Z */
#define XMODEM_PAD	26

/* ANSI terminal escape sequences */
#define RED_TEXT	"\033[1;31m"
#define NC_TEXT		"\033[0m"

/* Parser states */
#define P_START		0
#define P_TOP	1
#define P_DURATION	2
#define P_PARAM		3

#define SS_SIZE		8
static int state_stack[SS_SIZE];
static int ss_top;

#define SS_PUSH(x)	state_stack[ss_top++] = (x)
#define SS_FULL()	(ss_top == SS_SIZE)
#define SS_EMPTY()	(ss_top == 0)
#define SS_POP()	state_stack[--ss_top]
#define SS_TOP()	state_stack[ss_top-1]

static char line[128];
static struct mission_t mission;
static XML_Parser	parser;


static long
unit_mult(const char *unit)
{
    if(STRMATCH(unit, "days"))
	return 86400;
    else if(STRMATCH(unit, "hours"))
	return 3600;
    else if(STRMATCH(unit, "minutes"))
	return 60;
    return 1;
}

/*
** XML parser callback functions.  The functions are used to parse
** files with either of the following DTDs.
**
** <!ELEMENT mission (duration, param*)>
** <!ELEMENT duration EMPTY>
** <!ELEMENT param EMPTY>
** <!ATTLIST duration units (days|hours|minutes|seconds) #REQUIRED
**                    value CDATA #REQUIRED>
** <!ATTLIST param name CDATA #REQUIRED
**                 value CDATA #REQUIRED>
**
** <!ELEMENT params (param*)>
** <!ELEMENT param EMPTY>
** <!ATTLIST param name CDATA #REQUIRED
**                 value CDATA #REQUIRED>
*/

static void
start_element(void *udata, const char *name, const char **atts)
{
    double	value;
    long	t_units = 1;
    const char	*pname, *pval;
    
    if(STRMATCH(name, "mission") || STRMATCH(name, "params"))
    {
	SS_PUSH(P_TOP);
    }
    else if(STRMATCH(name, "duration"))
    {
	if(SS_TOP() != P_TOP)
	{
	    log_error("mission", "File format error at line %d\n",
		      XML_GetCurrentLineNumber(parser));
	    return;
	}
	
	SS_PUSH(P_DURATION);
	value = 0;
	
	while(**atts)
	{
	    if(STRMATCH(atts[0], "units"))
		t_units = unit_mult(atts[1]);
	    else if(STRMATCH(atts[0], "value"))
		value = atof(atts[1]);
	    atts += 2;
	}
	
	mission.duration = value * t_units;
	
	if(mission.duration <= 0)
	    log_error("mission", "<duration> format error at line %d\n",
		      XML_GetCurrentLineNumber(parser));
    } 
    else if(STRMATCH(name, "param"))
    {
	if(SS_TOP() != P_TOP)
	{
	    log_error("mission", "File format error at line %d\n",
		      XML_GetCurrentLineNumber(parser));
	    return;
	}

	SS_PUSH(P_PARAM);

	pname = pval = 0;
	
	while(**atts)
	{
	    if(STRMATCH(atts[0], "name"))
		pname = atts[1];
	    else if(STRMATCH(atts[0], "value"))
		pval = atts[1];
	    atts += 2;
	}

	if(pname && pval)
	{
	    ht_insert_elem(mission.params, pname, pval,
			   (unsigned short)strlen(pval)+1);
	}
	else
	    log_error("mission", "<param> error at line %d\n",
		      XML_GetCurrentLineNumber(parser));
	
    }
}

static void
end_element(void *udata, const char *name)
{
    int	state;

    if(SS_EMPTY())
	return;
    state = SS_POP();

    switch(state)
    {
	case P_PARAM:
	    if(!STRMATCH(name, "param"))
		log_error("mission", "File format error at line %d\n",
			  XML_GetCurrentLineNumber(parser));
	    break;
	case P_DURATION:
	    if(!STRMATCH(name, "duration"))
		log_error("mission", "File format error at line %d\n",
			  XML_GetCurrentLineNumber(parser));	    
	    break;
    }
}

static void
char_data(void *udata, const char *s, int len)
{
    return;
}

/*
**---------------------------------------------------------------------------
** ht_foreach_elem callbacks.
**---------------------------------------------------------------------------
*/

static void
write_element(struct elem *e, void *call_data)
{
    FILE	*fp = (FILE*)call_data;

    fprintf(fp, "<param name='%s' value='%s' />\n", e->name, (char*)e->data);
}

static void
show_parameters(struct elem *e, void *calldata)
{
    printf("%s = %s\n", e->name, (char*)e->data);
}

static void
update_parameters(struct elem *e, void *calldata)
{
    if(set_param_str(e->name, (char*)e->data) == 0)
	log_error("mission", "Unknown parameter: %s\n", e->name);
}

/*
 * Write mission data structure to a file.
 *
 * @param  fp  file pointer
 * @return 1 if successful, otherwise 0.
 */
static int
write_mission_file(FILE *fp)
{
    fprintf(fp, "<mission>\n<duration units='seconds' value='%ld' />\n",
	    mission.duration);
    ht_foreach_elem(mission.params, write_element, (void*)fp);
    fputs("</mission>", fp);
    
    return 1;
}

/*
 * Read mission data structure from a file.
 *
 * @param  fp  file pointer
 * @return 1 if successful, otherwise 0.
 */
static int
read_mission_file(FILE *fp)
{
    int		done;
    char	*p;
    static char buf[512];
    
    if(parser)
	XML_ParserFree(parser);
    
    parser = XML_ParserCreate(NULL);
    XML_SetElementHandler(parser, start_element, end_element);
    XML_SetCharacterDataHandler(parser, char_data);

    mission.duration = 0;
    if(mission.params)
	ht_destroy(mission.params);
    
    mission.params = ht_create(17, HT_NOCASE);
    if(!mission.params)
    {
	log_error("mission", "Out of memory\n");
	XML_ParserFree(parser);
	parser = 0;
	return 0;
    }
    
    
    do 
    {
	size_t len = fread(buf, (size_t)1, sizeof(buf), fp);
	p = strchr(buf, XMODEM_PAD);
	if(p != NULL)
	    len = p - buf;
	done = len < sizeof(buf);
	if (!XML_Parse(parser, buf, (int)len, done)) {
	    log_error("mission",
		      "%s at line %d\n",
		      XML_ErrorString(XML_GetErrorCode(parser)),
		      XML_GetCurrentLineNumber(parser));
	    XML_ParserFree(parser);
	    parser = 0;
	    return 0;
	}
    } while (!done);

    XML_ParserFree(parser);
    parser = 0;
    
    return 1;
}

static int
check_param_name(const char *name)
{
    return (name != NULL);
}

/*
 * Parse a formatted time string.
 * Parse a time string of the form DAYS:HOURS:MINUTES:SECONDS and return
 * the time value in seconds.
 *
 * @param  tstr  string
 * @return time value in seconds.
 */
static long	t_mult[4] = {86400, 3600, 60, 1};

static long
parse_time(const char *tstr)
{
    long	seconds, t;
    int		i;
    const char	*p;


    seconds = 0;
    p = tstr;
    t = 0;
    i = 0;
    while(*p)
    {
	if(*p == ':')
	{
	    seconds += (t * t_mult[i]);
	    i++;
	    t = 0;
	}
	else if(isdigit(*p))
	    t = (t*10) + (*p - '0');
	p++;
    }

    return seconds + t*t_mult[i];
}

static void
show_time(long seconds)
{
    int		i = 0;
    long	t;
    
    for(i = 0;i < 4;i++)
    {
	t = seconds/t_mult[i];
	printf("%ld%c", t, i < 3 ? ':' : ' ');
	seconds -= (t * t_mult[i]);
	if(seconds <= 0)
	    break;
    }

    fflush(stdout);
    
}

static void
show_mission(void)
{
    fputs(RED_TEXT, stdout);
    fputs("\n---Mission Settings---\n", stdout);
    ht_foreach_elem(mission.params, show_parameters, (void*)0);
    fputs("Mission duration: ", stdout);
    show_time(mission.duration);
    fputs(NC_TEXT, stdout);
    fputs(NC_TEXT, stdout);
    fputs("\n----------------------\n", stdout);

}

/**
 * Start an interactive session to setup mission.
 * Run an interactive session at the console to set mission parameters and
 * mission duration.  pvalidate is an optional function which will be
 * used to validate the parameter names.  If supplied, the function must
 * accept a parameter name as input and return non-zero if valid or zero
 * if invalid.
 *
 * @param  pvalidate  function to validate a parameter name.
 */
void
mission_setup(int (*pvalidate)(const char*))
{
    FILE	*ofp;
    
    if(!pvalidate)
	pvalidate = check_param_name;
    
    if(yes_or_no("\nSet mission parameters", 0))
    {
	char	*p;

	if(fileexists("mission.xml") &&
	   !yes_or_no("\nOverwrite current mission settings", 0))
	{
	    /* read existing mission file */
	    ofp = fopen("mission.xml", "r");
	    read_mission_file(ofp);
	    fclose(ofp);
	    show_mission();
	}
	else
	{
	    /* initialize mission data structure */
	    mission.duration = 0;
	    if(!mission.params)
		mission.params = ht_create(17, HT_NOCASE);
	}
	
	fputs("ENTER name=value or blank line to quit: ", stdout);
	fflush(stdout);
	while(InputLine(line, (short)sizeof(line)-1) && line[0])
	{
	    if((p = strchr(line, ',')) != NULL ||
		(p = strchr(line, '=')) != NULL)
	    {
		*p++ = '\0';
		if((*pvalidate)(line))
		    ht_insert_elem(mission.params, line, p, 
				   (unsigned short)strlen(p)+1);
		else
		    printf("Bad parameter name: %s\n", line);
	    }

	    fputs("ENTER name=value or blank line to quit: ", stdout);
	    fflush(stdout);	    
	}

	if(yes_or_no("\nSet mission duration", 0))
	{
	    printf("Specify duration as DAYS:HOURS:MINUTES:SECONDS:  ");
	    fflush(stdout);
	    if(InputLine(line, (short)sizeof(line)-1))
		mission.duration = parse_time(line);
	}

	show_mission();
	
	if(yes_or_no("\nSave the above settings", 0))
	{
	    if((ofp = fopen("mission.xml", "w")) != NULL)
	    {
		write_mission_file(ofp);
		fclose(ofp);
	    }
	    else
	    {
		log_error("setup", "Could not write parameter file!\n");
		return;
	    }
	    
	}
    }

}

/**
 * Read mission parameters from a file.
 * Read the mission parameters from file and set the corresponding
 * values in the parameter table.  .
 *
 * @param  file  filename.
 * @return the mission duration in seconds or -1 if an error occurs.
 */
long
mission_read(const char *file)
{
    FILE	*fp;
    
    if((fp = fopen(file, "r")) == NULL)
	return -1;
    if(!read_mission_file(fp))
    {
	fclose(fp);
	return -1;
    }
    fclose(fp);

    show_mission();
    
    log_event("Updating parameters from %s\n", file);
    ht_foreach_elem(mission.params, update_parameters, (void*)0);
    
    return mission.duration;
}

/**
 * Read parameters from a file.
 * Read the parameters from file and set the corresponding
 * values in the parameter table.
 *
 * @param  file  filename.
 * @return 1 if successful, 0 on error
 */
int
params_read(const char *file)
{
    FILE	*fp;
    
    if((fp = fopen(file, "r")) == NULL)
	return 0;
    if(!read_mission_file(fp))
    {
	fclose(fp);
	return 0;
    }
    fclose(fp);

    log_event("Updating parameters from %s\n", file);
    ht_foreach_elem(mission.params, update_parameters, (void*)0);
    return 1;
}
