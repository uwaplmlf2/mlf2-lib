/**@file
** $Id: iridium.h,v ee1ccc9723e7 2008/05/27 18:48:36 mikek $
*/
#ifndef _IRIDIUM_H_
#define _IRIDIUM_H_

#include <stddef.h>

#define IRIDIUM_BAUD	19200L
#define IRIDIUM_DEVICE	11

/** Chat "script" data structure */
typedef struct {
    char	*send;		/**< string to send */
    char	*expect;	/**< expect response */
    long	timeout;	/**< response timeout in seconds */
} chat_t;

typedef struct {
    char data[20];
} snumber_t;

#define sn_to_str(sn)	((sn).data)

int iridium_init(long baud);
int iridium_dev_ready(void);
void iridium_shutdown(void);
void iridium_passthru(void);
int iridium_chat(const char *send, const char *expect, long timeout);
size_t iridium_write(const char *data, size_t n);
size_t iridium_read(char *data, size_t n);
int iridium_get_sn(snumber_t *sn);
int iridium_get_sq(void);
int iridium_get_reg(void);
int iridium_serialdev(void);
int iridium_get_file(const char *filename);
long iridium_put_file(const char *filename);
int iridium_try_dial(const char *pn, long timeout);
int iridium_put_data(char *data, int size);
int iridium_put_file_chunks(const char *filename, unsigned chunksize, 
			    long start, const char *ctmpl, const char *rtmpl);
int iridium_login(const char *prefix, const char *pn, chat_t *login);
int iridium_dir_setup(void);
int iridium_send_netcdf(const char *fname, int hdrsize, int recsize, 
			long rstart, long rend, long rinc, int sendhdr);
void iridium_set_com1(void);
void iridium_set_com2(void);
size_t iridium_get_buffer(char *buf, size_t size);

#endif /* _IRIDIUM_H_ */
