/**@file
** $Id: adcp.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _ADCP_H_
#define _ADCP_H_

#define ADCP_DEVICE	1
#define ADCP_BAUD	9600L
#define ADCP_TIMEOUT	2L
#define ADCP_PINGS	2

#define MAX_ADCP_PINGS	10
#define MAX_BEAMS	3


/*
 * New ADCP output file format.  Each file is a series of one or
 * more records.  Each record has the following format.
 *
 *    AdcpSampleHdr
 *    [PktHdrType,
 *     FloatHdrType,
 *     FloatDataType]*nr_samples
 * 
 */
/** File header structure */
typedef struct {
    long	nr_samples; /**< sample count */
    long	timestamp; /**< time in seconds since 1/1/1970 UTC */
    long	interval; /**< time between samples (seconds)*/
} AdcpSampleHdr;

/** Sample header structure */
typedef struct {
    unsigned short	pad;	/**< always zero */
    unsigned short	index;	/**< sample index number */
    unsigned short	count;	/**< always 1 */
} PktHdrType;

/** Data structure used by Sontek (must be packed) */
typedef struct {
    unsigned char	SyncChar;
    unsigned char	Nbytes;
    unsigned char	Npulses;
    unsigned char	NpingsPerPulse;
    int			Heading;
    int			Pitch;
    int			Roll;
    int			FluxX;
    int			FluxY;
    int			FluxZ;
} FloatHdrType __attribute__((packed));

/** Data structure used by Sontek (must be packed) */
typedef struct {
    short		Vel[MAX_BEAMS];
    unsigned char	Amp[MAX_BEAMS];
    unsigned char	Cor[MAX_BEAMS];
} FloatDataType __attribute__((packed));

int adcp_init(void);
void adcp_shutdown(void);
int adcp_dev_ready(void);
void adcp_test(void);
int adcp_set_params(char **extra_cmds);
int adcp_sample_start(void);
int adcp_data_ready(void);
void adcp_get_stats(unsigned long *errs, unsigned long *reads);
int adcp_store_data(FILE *ofp, int ns, long ts, long dt);
int adcp_set_pulse(const char *pname, int vals[], int n);
int adcp_sample_mode(void);
int adcp_set_sampling(long si_ms, int nr_samples);
int adcp_save_setup(void);

#endif
