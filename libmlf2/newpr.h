/*
** arch-tag: newpr header file
*/
#ifndef _NEWPR_H_
#define _NEWPR_H_

#define NEWPR_CMD_GETCOUNT  0x01
#define NEWPR_CMD_GETNEXT   0x02
#define NEWPR_CMD_CLEARBUF  0x04
#define NEWPR_CMD_OVERRUNS  0x08
#define NEWPR_CMD_ADRESET   0x10

#define NEWPR_GAIN      64
#define NEWPR_VREF      2.5

typedef enum {PR_TOP=0x01, PR_HULL=0x02, PR_BOTTOM=0x04, PR_BOTH=0x03, PR_ALL=0x07} pr_channel_t;

int newpr_init(void);
void newpr_spi_init(void);
void newpr_shutdown(void);
int newpr_read_data(pr_channel_t which, int n, long data[]);
int newpr_get_count(void);
int newpr_adreset(void);
int newpr_clear(void);
double newpr_mv_to_psi(pr_channel_t which, double mv);
double newpr_counts_to_mv(long counts);

#endif /* _NEWPR_H_ */
