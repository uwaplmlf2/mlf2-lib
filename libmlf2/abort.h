/*
** arch-tag: 9fb0dd0a-4e33-4c35-ac22-0d7ce8972083
** Time-stamp: <2006-09-18 10:52:27 mike>
*/
#ifndef _ABORT_H_
#define _ABORT_H_

#ifndef DEF_WATCHDOG_TIMEOUT
#define DEF_WATCHDOG_TIMEOUT	900L
#endif

#ifndef __GNUC__
#define __attribute__(x)
#endif

void abort_set_timeout(long seconds);
void abort_mission(int now) __attribute__ ((noreturn));
void abort_prog(void) __attribute__ ((noreturn));
void restart_sys(void) __attribute__ ((noreturn));
void enter_safe_mode(long duration, long timeout);
void enter_recovery_mode(void);

#endif /* _ABORT_H_ */
