#ifndef SBE63_H
#define SBE63_H

#include "ioports.h"

#define SBE63_DEVICE(n)         (5)
#define SBE63_BAUD              9600

typedef struct {
    float   oxy;        /**< O2 concentration in ml/l */
    float   temp;       /**< thermistor temperature in degC */
    float   phase;      /**< raw phase delay in usec */
    float   voltage;    /**< raw thermistor voltags */
} SBE63Data;

int sbe63_init(int which);
void sbe63_shutdown(int which);
void sbe63_test(int which);
int sbe63_dev_ready(int which);
int sbe63_setup(int which);
int sbe63_read_data(int which, SBE63Data *sd, long timeout);
#endif /* SBE63_H */
