/**@file
** arch-tag: GTD header file.
*/
#ifndef _GTD_H_
#define _GTD_H_

#define GTD_DEVICE	5
#define GTD_MINOR	0
#define GTD_BAUD	1200L


/** Scale factor to convert temperature integration-time counts to seconds */
#define T_COUNTS_PER_SEC	1200L

/** Scale factor to convert pressure integration-time counts to seconds */
#define P_COUNTS_PER_SEC	300L

/** pump_mode constants */
enum {PUMP_PULSED=0, PUMP_CONTINUOUS=1};

/** GTD sampling parameters */
typedef struct {
    short	pres_res;	/**< b0 pressure integration time (counts) */
    short	temp_res;	/**< b1 temperature int. time (counts) */
    short	pump_mode;	/**< b2 pump mode */
    short	pump_on;	/**< b3 pump on time (ms) */
    short	pump_off;	/**< b4 pump off time (ms) */
    short	pump_cycle;	/**< b5 number of pumping cycles */
    short	pump_delay;	/**< b6 pump delay (ms) */
    short	bb_delay;	/**< b7 "button board" delay (ms) */
    short	bb_warmup;	/**< b8 button board warm-up (ms) */
    short	bb_interrupt;	/**< b9 button board interrupts */
    short	bb_sample;	/**< b10 number of button board samples */
} GTDsample_t;

/**
 * Calculate the GTD sample cycle time.
 * @param s pointer to sample-parameter data structure.
 * @return sample time in milliseconds.
 */
static __inline__ unsigned long GTD_CYCLE_TIME(GTDsample_t *s)
{
    long	T;

    /* pressure sample time (ms) */
    T = (s->pres_res*1000L/P_COUNTS_PER_SEC);
    /* temperature sample time (ms) */
    T += (s->temp_res*1000L/T_COUNTS_PER_SEC);

    if(s->pump_mode == PUMP_PULSED)
    {
	/* pulsed mode */
	T += s->pump_cycle*(s->pump_on + s->pump_off);
    }
    else
    {
	/* continuous mode */
	T += (s->pump_delay + s->bb_delay + s->bb_warmup);
    }

    return T;
}


/** GTD data sample */
typedef struct {
    float	pressure;	/**< pressure in millibars */
    float	temperature;	/**< temperature in degrees C */
} GTDdata_t;

/** Default sample parameters */
#define GTD_DEFAULT_SAMPLE {\
      pres_res: 600,\
      temp_res: 2400,\
      pump_mode: 0,\
      pump_on: 1000,\
      pump_off: 3000,\
      pump_cycle: 5,\
      pump_delay: 1000,\
      bb_delay: 8000,\
      bb_warmup: 7000,\
      bb_interrupt: 2,\
      bb_sample: 3\
    }


int gtd_init(void);
void gtd_shutdown(void);
int gtd_dev_ready(void);
int gtd_start_sample(GTDsample_t *gs);
int gtd_data_ready(void);
int gtd_read_data(GTDdata_t *gd);
void gtd_test(void);
int gtd_sampling(void);

#endif
