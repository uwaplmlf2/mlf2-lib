/*
** $Id: iostream.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Implement a stdio-like stream on top of a serial I/O device.
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <assert.h>
#include "serial.h"
#include "iostream.h"

/*
 * Fill the input buffer from the character source (if necessary) and 
 * return the next character.
 *
 * Returns next character or EOF
 */
static int
_iosfilbuf(register IOstream *ios)
{
    register struct stream	*s = &ios->in;
    register int		len;
    
    if(s->bp >= s->bend)
    {
	if(ios->flags & IOS_BUFDIRTY)
	    iosflush(ios);

	if((len = serial_read(ios->desc, s->buf, s->bufsize)) <= 0)
	    return EOF;
	s->bend = (s->bp = s->buf) + len;
    }

    return *s->bp++;
}

/*
 * _iosflushbuf - add char to end of buffer and flush if full
 * @ios: pointer to IOstream
 * @c: new character.
 *
 * Insert a character at the end of the output buffer and flush
 * if the buffer is full.  If @c is -1, buffer is flushed 
 * unconditionally.
 *
 * Returns character inserted.
 */
static int
_iosflushbuf(IOstream *ios, int c)
{
    register size_t		len;
    register struct stream	*s = &ios->out;

    /*
    ** Flush the buffer and reset the pointers.
    */
    if(c == -1 || s->bp >= s->bend)
    {
	len = s->bp - s->buf;
	if(len > 0)
	    serial_write(ios->desc, s->buf, len);
	s->bp = s->buf;
	s->bend = s->buf + s->bufsize;
	ios->flags &= ~IOS_BUFDIRTY;
	if(c == -1)
	    return 0;
	/* If a "real" character was passed, fall through */
    }

    /*
    ** Flush at end of line for line-buffered case.
    */
    if(c == '\n' && (ios->flags & IOS_BUFMODE) == IOS_LINEBUF)
    {
	*s->bp++ = c;
	_iosflushbuf(ios, -1);
	return c;
    }

    /*
    ** Mark buffer as dirty and insert the character.
    */
    ios->flags |= IOS_BUFDIRTY;
    
    *s->bp++ = c;
    return c;
}


static void
init_stream(struct stream *s, char *buf, int n)
{
    s->bp = s->buf = s->bend = buf;
    s->bufsize = n;
}

/**
 * iosopen -  open a new IOstream for input and output.
 * @desc: serial device descriptor (from serial_open())
 * @bufmode: buffering mode, one of the following constants:
 *
 * Open a new IOstream for input and output.  An IOstream provides a
 * stdio-like interface on top of a serial device.  @bufmode should
 * be one of the following constants defined in iostream.h.
 *
 * %IOS_UNBUF - stream is unbuffered.
 *
 * %IOS_LINEBUF - stream is line buffered.
 *
 * %IOS_FULLBUF - stream is fully buffered.
 *
 * Returns a pointer to a new IOstream or NULL if it cannot be opened.
 */
IOstream*
iosopen(int desc, int bufmode)
{
    int		bufsize;
    IOstream	*ios;
    long	nbytes = sizeof(IOstream);
    
    bufsize = (bufmode == IOS_UNBUF) ? 1 : IOS_BUFSIZE;
    nbytes += (2*bufsize);
    
    if((ios = (IOstream*)malloc(nbytes)) == NULL)
	return (IOstream*)0;

    init_stream(&ios->in, (char*)ios + sizeof(IOstream), bufsize);
    init_stream(&ios->out, ios->in.buf + bufsize, bufsize);
    ios->desc = desc;
    ios->flags = bufmode;

    serial_inmode(desc, SERIAL_NONBLOCKING);
    
    return ios;
}

/**
 * iosclose - close an open IOstream
 * @ios: pointer to IOstream.
 *
 * Close an open IOstream.  Using a stream after it has been closed 
 * will probably crash the system.
 *
 * Returns 0
 */
int
iosclose(IOstream *ios)
{
    iosflush(ios);
    free(ios);
    return 0;
}


/**
 * iosflush - flush all buffered output.
 * @ios: pointer to open IOstream.
 *
 * Flush any pending output to the underlying serial device.  Returns 0
 */
int
iosflush(IOstream *ios)
{
    return _iosflushbuf(ios, -1);
}

/**
 * iosgetc - return next character from a IOstream
 * @ios: pointer to open IOstream
 *
 * Returns next character.  Note that this read blocks until a character is 
 * available.
 */
int
iosgetc(register IOstream *ios)
{
    register int	c;

    assert(ios->in.bufsize != 0);
    
    /* Block until we get a character */
    while((c = _iosfilbuf(ios)) == EOF)
	;
    
    return c;
}

/**
 * iosgets - read a line from an IOstream.
 * @s: buffer for returned string.
 * @size: maximum size of @s.
 * @ios: pointer to open IOstream.
 *
 * Read the next line from the open IOstream.  Reading stops at
 * the next '\n' character or after @size - 1 characters,
 * whichever comes first.  The trailing '\n' (if any) is not
 * included in the returned string.  The string will be terminated
 * with a '\0'.  Returns a pointer to the filled string.
 */
char*
iosgets(char *s, register int size, IOstream *ios)
{
    register char	*ptr;
    register int	c;
    
    assert(ios->in.bufsize != 0);
    
    ptr = s;
    while(--size > 0)
    {
	if((c = iosgetc(ios)) == EOF || c == '\n')
	    break;
	*ptr++ = c;
    }
    *ptr = '\0';
    return s;
}

/**
 * iosungetc - push a character back to the IOstream.
 * @c: character to push back.
 * @ios: pointer to open IOstream.
 *
 * Push a character back into an open IOstream where it will be returned by 
 * the next read operation.  Returns @c or EOF if the character cannot be
 * pushed back.
 */
int 
iosungetc(int c, IOstream *ios)
{
    register struct stream	*s = &ios->in;

    assert(ios->in.bufsize != 0);
    
    if(s->bp == s->buf)
    {
	/*
	** We're at the beginning of the buffer.  If 'bend' is greater
	** than 'buf', we have just filled the buffer but have not yet
	** read a character.  There is no way to "unget" so return an
	** error.
	*/
	if(s->bend > s->buf)
	    return EOF;
	s->bp++;
	s->bend = s->bp;
    }

    return (*--s->bp = c);
}

/**
 * iosread - read an array of values from an open IOstream.
 * @ptr: array to store input values.
 * @size: size (in bytes) of each array element.
 * @nmemb: number of elements.
 * @ios: pointer to open IOstream.
 *
 * This function operates like fread().  It reads an array of elements from
 * an open IOstream.  The return value is the number of elements read.
 */
int
iosread(void *ptr, size_t size, size_t nmemb, IOstream *ios)
{
    register size_t	n = size*nmemb;
    register char	*p = (char*)ptr;

    assert(ios->in.bufsize != 0);
    
    while(--n)
	*p++ = iosgetc(ios);
    
    return nmemb;
}

/**
 * iosputc - write a character to an open IOstream.
 * @c: character.
 * @ios: pointer to open IOstream.
 *
 * Writes the character @c to the IOstream.  Returns output character.
 */
int
iosputc(int c, IOstream *ios)
{
    assert(ios->out.bufsize != 0);
    
    return _iosflushbuf(ios, c);
}

/**
 * iosputs - write a string to an open IOstream.
 * @s: string.
 * @ios: pointer to open IOstream.
 *
 * Writes the string @s to an IOstream.  Returns 0.
 */
int 
iosputs(register const char *s, IOstream *ios)
{
    assert(ios->out.bufsize != 0);
    
    while(*s)
	(void)iosputc(*s++, ios);
    return 0;
}


/**
 * ioswrite - write an array of values to an open IOstream.
 * @ptr: array of output values.
 * @size: size (in bytes) of each array element.
 * @nmemb: number of elements.
 * @ios: pointer to open IOstream.
 *
 * This function operates like fwrite().  It writes an array of elements to
 * an open IOstream.  The return value is the number of elements written.
 */
int
ioswrite(const void *ptr, size_t size, size_t nmemb, IOstream *ios)
{
    register size_t	n = size*nmemb;
    register const char	*p = (char*)ptr;
    
    assert(ios->out.bufsize != 0);
    while(--n)
	iosputc(*p++, ios);
    
    return nmemb;
}


