/*
** $Id: cstar.h,v 0fb828fe41c8 2007/10/17 22:54:58 mikek $
*/
#ifndef _CSTAR_H_
#define _CSTAR_H_

#define CSTAR_DEVICE	5
#define CSTAR_BAUD	9600L

int cstar_init(void);
void cstar_shutdown(void);
int cstar_dev_ready(void);
void cstar_test(void);
int cstar_read_data(unsigned *rval, long timeout);

/* Constants to describe the s-expression output format */
#define CSTAR_DATA_FMT		""
#define CSTAR_DATA_DESC		"Counts"

#endif /* _CSTAR_H_ */
