/**@file
** Functions to read and write NMEA sentences.
**
** $Id: nmea.c,v c992eb2d937a 2008/01/09 20:28:31 mikek $
**
** Functions to handle NMEA 0183 (National Marine Electronics Assoc.)
** format "sentences".
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#ifdef linux
#include <unistd.h>
#include <signal.h>
#else
#include "salarm.h"
#endif
#include "nmea.h"

#ifdef linux
#define ALARM_OFF()	{ alarm(0); sigaction(SIGALRM, &old_act, NULL); }
#else
#define ALARM_OFF()	(alarm_shutdown())
#endif

#define	START_TOK	'$'
#define END_TOK		'*'
#define SEP_TOK		','
#define EOL		'\r'

static jmp_buf		recover;

static void
catch_alarm(void)
{
#ifdef __GNUC__
    longjmp(recover, 1L);
#else
    longjmp(recover, 1);
#endif
}

static int
hex(int c)
{
    return isdigit(c) ? (c - '0') : ((tolower(c) - 'a') + 10);
}


/**
 * Read the next NMEA sentence from an input source.
 * Read the next NMEA format sentence from the source function getbyte.
 * Returns sentence length (# of fields) or one of the following negative 
 * error codes defined in nmea.h.
 *
 *   - NMEA_ERR_TIMEOUT - maximum time exceeded
 *   - NMEA_ERR_CSUM - checksum verification failed.
 *
 * @param  ns  returned sentence.
 * @param  getbyte  source function to return next sentence byte.
 * @param  handle  opaque parameter passed to getbyte.
 * @param  timeout  maximum time allowed to read the sentence (seconds).
 * @param  gettime  function which will return a timestamp
 * @return sentence length (number of fields).
 */
int
nmea_read_next(NMEAsentence *ns, fsource_t getbyte, void *handle, 
		   unsigned timeout, ftstamp_t gettime)
{
    register int	c, n;
    register char	*ptr, **fptr, *bufend;
    unsigned		sum = 0, csum;
#ifdef linux
    struct sigaction	act, old_act;
#endif    

#ifdef linux
    act.sa_handler = catch_alarm;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGALRM, &act, &old_act);
    alarm(timeout);
#else    
    alarm_set((long)timeout, ALARM_ONESHOT, catch_alarm);
#endif
    
    if(setjmp(recover) != 0)
	return NMEA_ERR_TIMEOUT;

findstart:    
    while((c = (getbyte)(handle)) != START_TOK)
	;

    if(gettime)
	ns->timestamp = (*gettime)();
    
    n      = 0;
    ptr    = ns->buf;
    fptr   = ns->fields;
    bufend = ptr + NMEA_MAX_LEN;
    
    while((c = (*getbyte)(handle)) != END_TOK && c != EOL)
    {
	if(ptr == bufend)
	    goto findstart;
	
	/* Have we found the start of a new field? */
	if((*ptr++ = c) == SEP_TOK)
	{
	    *fptr++ = ptr;	/* save pointer to this field */
	    ptr[-1] = '\0';	/* "erase" the separator */
	    n++;
	}
	
	sum ^= c;
    }

    *ptr = '\0';
    ns->nfields = n;
    
    if(c != EOL)
    {
	/*
	** Read the hex checksum characters
	*/
	c = (*getbyte)(handle);
	csum = hex(c) * 16 + hex((*getbyte)(handle));
	ALARM_OFF();
	return (sum == csum) ? n : NMEA_ERR_CSUM;
    }

    ALARM_OFF();
    
    return n;
}


/**
 * Read the next NMEA sentence from an input source.
 * Read the next NMEA format sentence, from the source function getbyte,
 * which starts with the string 'match'. Returns sentence length (# of fields) 
 * or one of the following negative error codes defined in nmea.h.
 *
 *   - NMEA_ERR_TIMEOUT - maximum time exceeded
 *   - NMEA_ERR_CSUM - checksum verification failed.
 *
 * @param  ns  returned sentence.
 * @param  match  string to match at start of sentence.
 * @param  getbyte  source function to return next sentence byte.
 * @param  handle  opaque parameter passed to getbyte.
 * @param  timeout  maximum time allowed to read the sentence (seconds).
 * @param  gettime  function which will return a timestamp
 * @return sentence length (number of fields).
 */
int
nmea_read_match(NMEAsentence *ns, char *match, fsource_t getbyte,
		void *handle, unsigned timeout, ftstamp_t gettime)
{
    register int	c, n;
    register char	*ptr, *mptr, **fptr, *bufend;
    unsigned		sum = 0, csum;
#ifdef linux
    struct sigaction	act, old_act;
#endif    

#ifdef linux
    act.sa_handler = catch_alarm;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGALRM, &act, &old_act);
    alarm(timeout);
#else 
    alarm_set((long)timeout, ALARM_ONESHOT, catch_alarm);
#endif
    
    if(setjmp(recover) != 0)
	return NMEA_ERR_TIMEOUT;

findstart:    
    while((c = (*getbyte)(handle)) != START_TOK)
	;

    if(gettime)
	ns->timestamp = (*gettime)();
    
    n      = 0;
    sum    = 0;
    mptr   = match;
    ptr    = ns->buf;
    bufend = ptr + NMEA_MAX_LEN;
    fptr   = ns->fields;
    
    while((c = (*getbyte)(handle)) != END_TOK && c != EOL)
    {
	if(*mptr != 0 && *mptr++ != c)
	    goto findstart;
	if(ptr == bufend)
	    goto findstart;
	
	/* Have we found the start of a new field? */
	if((*ptr++ = c) == SEP_TOK)
	{
	    *fptr++ = ptr;	/* save pointer to this field */
	    ptr[-1] = '\0';	/* "erase" the separator */
	    n++;
	}
	
	sum ^= c;
    }

    *ptr = '\0';
    ns->nfields = n;
    
    if(c != EOL)
    {
	/*
	** Read the hex checksum characters
	*/
	c = (*getbyte)(handle);
	csum = hex(c) * 16 + hex((*getbyte)(handle));
	ALARM_OFF();
	return (sum == csum) ? n : NMEA_ERR_CSUM;
    }

    ALARM_OFF();
    
    return n;
}

/**
 * Write an NMEA format sentence.
 * Write the string 's' as a valid NMEA format sentence using the transmit
 * function putbyte.
 *
 * @param  s  sentence to transmit (not including the leading $ character).
 * @param  putbyte  function to transmit a byte.
 * @param  handle  opaque parameter passed to putbyte.
 * @param  inc_csum  if non-zero, append a checksum to the sentence.
 */
void
nmea_write_str(const char *s, fsink_t putbyte, void *handle,
	       int inc_csum)
{
    register const char	*ptr;
    register unsigned	sum;
    char		str[8];
    
    (*putbyte)((int)'$', handle);
    
    sum = 0;
    for(ptr = s;*ptr;ptr++)
    {
	sum ^= *ptr;
	(*putbyte)((int)*ptr, handle);
    }

    if(inc_csum)
    {
	sprintf(str, "*%02X\r\n", sum);
	for(ptr = str;*ptr;ptr++)
	    (*putbyte)((int)*ptr, handle);
    }
    
}
