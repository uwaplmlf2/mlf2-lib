/*
** File queue.
*/
#ifndef _FILEQ_H_
#define _FILEQ_H_

#define FQ_DUMP_FILE    "fileq.txt"

void fq_init(void);
int fq_add(const char *file);
int fq_add_next(const char *file);
int fq_send(int (*fsend)(const char*), int keep);
int fq_len(void);
int fq_add_uncompressed(const char *file);
int fq_dump(const char *filename);
int fq_load(const char *filename);
void fq_show(FILE *fp);
int fq_pop(char *next, size_t namelen);
#endif /* _FILEQ_H_ */
