/**@file
** Driver for the i490 and LiCOR light sensors.
**
** $Id: i490.c,v 0c4ee2f43fc8 2008/01/10 17:46:50 mikek $
**
**
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <tt8.h>
#include <tt8lib.h>
#include "power.h"
#include "adc.h"
#include "atod.h"
#include "i490.h"

#define WARMUP_MS	300L

static int ref = 0;
static unsigned long init_time;

/**
 * Initialize the I490 sensor interface.
 *
 * Power-on and initialize the I490 sensor interface.  Returns 1 if successful,
 * otherwise 0.
 */
int
i490_init(void)
{
    /* Are we already initialized? */
    if(ref > 0)
	goto initdone;

    atod_init();
    power_on(ALGDEV_POWER2);
    init_time = MilliSecs();
    
initdone:
    ref++;
    
    return 1;
}

/**
 * Shutdown the I490 interface.
 *
 */
void
i490_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    power_off(ALGDEV_POWER2);
    atod_shutdown();
}

/**
 * i490_dev_ready - check if device is ready.
 *
 * Returns true if device is ready to be sampled.
 */
int
i490_dev_ready(void)
{
    return ((MilliSecs() - init_time) >= WARMUP_MS);
}

/**
 * Read a sample from the I490 sensor.
 *
 * The upper 16-bits contains the high gain sample
 * and the lower 16-bits contain the low gain sample.
 *
 * @returns next sample.
 */
long
i490_read_data(void)
{
    long	x;
    
    x = atod_read(I490_LOW_GAIN);
    x |= ((unsigned long)atod_read(I490_HIGH_GAIN) << 16);
    
    return x;
}
