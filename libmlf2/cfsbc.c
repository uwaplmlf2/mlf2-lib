/*
** Interface to the Camera Float single-board computer.
*/
#include <stdio.h>
#ifdef __GNUC__
#include <unistd.h>
#else
#include <fcntl.h>
#endif
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "iofuncs.h"
#include "xmodem.h"
#include "lpsleep.h"
#include "ptable.h"
#include "cfsbc.h"

#define SHELL_PROMPT    "CF>"

#define PURGE_INPUT(d, t) while(sgetc_timed(d, MilliSecs(), (t)) != -1)

#ifndef __GNUC__
#define __inline__
#endif

#define RESPONSE_LEN    1024
#define CMD_LEN         256
#define RESPONSE_END    '>'

#define MAX_RX_COMMAND          32
#define RX_RESP_TEMPLATE        "receive %s"
#define MAX_RX_RESPONSE         40

static int              ref = 0;
static int              serial_dev;


/**
 * Callback to reopen the device interface upon return from low-power sleep.
 */
static void
cfsbc_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
    PURGE_INPUT(serial_dev, 100);
    log_event("Reopened CF SBC serial interface\n");
}

/**
 * cfsbc_init - initialize the interface.
 */
int
cfsbc_init(void)
{
    if(ref > 0)
        goto initdone;

    if((serial_dev = serial_open(CFSBC_DEVICE, 0, CFSBC_BAUD)) < 0)
    {
        log_error("aux", "Cannot open serial device (code = %d)\n",
                  serial_dev);
        return 0;
    }

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to the device.
    */
    add_after_hook("AUX-reopen", cfsbc_reopen);

initdone:
    ref++;
    return 1;
}

int
cfsbc_serialdev(void)
{
    if(ref == 0)
        return -1;
    return serial_dev;
}

/**
 * cfsbc_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
cfsbc_shutdown(void)
{
    if(ref == 0)
        return;
    ref = 0;
    remove_after_hook("AUX-reopen");
    serial_close(serial_dev);
}

/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
cfsbc_dev_ready(void)
{
    PURGE_INPUT(serial_dev, 100);
    return (serial_chat(serial_dev, "\n", SHELL_PROMPT, 2L) == 1);
}

/**
 * Interactive pass-thru mode.
 */
void
cfsbc_test(void)
{
    printf("Pass-through mode: CTRL-c to exit\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

static char shell_response[RESPONSE_LEN+1];
static char shell_command[CMD_LEN+1];

/**
 * Send a shell command. Wait for the command to
 * execute (if timeout > 0).
 *
 * @param  command  shell command string (with trailing '\n')
 * @param  timeout  number of milliseconds to wait for a response.
 */
int
cfsbc_shell_command(const char *command, long timeout)
{
    PURGE_INPUT(serial_dev, 100);
    memset(shell_response, 0, RESPONSE_LEN);
    sputz(serial_dev, command);
    return (cfsbc_shell_response(timeout) == NULL) ? 0 : 1;
}

/**
 * Return the most recent response from the shell.
 *
 * @param  timeout  number of milliseconds to wait for a response.
 */
char*
cfsbc_shell_response(long timeout)
{
    long        t;
    char        *p;
    int         c, i;

    if(strlen(shell_response) == 0 && timeout > 0)
    {
        p = shell_response;

        // Skip input until the first linefeed (echoed input)
        t = MilliSecs();
        while((c = sgetc_timed(serial_dev, t, timeout)) != '\n')
        {
            if(c < 0)
            {
                log_error("cfsbc", "Timeout waiting for echo\n");
                return NULL;
            }
        }

        i = 0;
        while((c = sgetc_timed(serial_dev, t, timeout)) != RESPONSE_END)
        {
            if(c < 0)
            {
                log_error("cfsbc", "Timeout waiting for response\n");
                return NULL;
            }

            if(i < RESPONSE_LEN)
            {
                *p = c;
                p++;
                i++;
            }
        }
        // Search backward for the final linefeed
        while(--i > 0)
        {
            p--;
            if(*p == '\n')
                break;
            *p = '\0';
        }
        *p = '\0';
    }

    return shell_response;
}

/**
 * Convert the most recent shell response to an integer and write the
 * value to the location pointed to by rval.
 */
int
cfsbc_int_response(long timeout, long *rval)
{
    char        *resp;

    resp = cfsbc_shell_response(timeout);
    if(resp == NULL)
        return 0;
    return sscanf(resp, "%ld", rval);
}

static int
upload_file(char *buf, int size, void *arg)
{
    FILE        *fp = (FILE*)arg;

    if(feof(fp))
        return 0;

    SerPutByte('.');
    return (int)fread(buf, 1L, (size_t)size, fp);
}

static long
xmodem_put_file(const char *filename)
{
    FILE        *fp;
    int         r, psize = 1024;
    long        bytes = 0;

    if((fp = fopen(filename, "rb")) == NULL)
    {
        log_error("cfsbc", "Cannot open file %s\n", filename);
        return 0;
    }

    /*
    ** Use the file size to determine an appropriate packet size.
    */
    if(fseek(fp, 0L, SEEK_END) == 0 || fseek(fp, 0L, SEEK_END) == 0)
    {
        bytes = lseek(fileno(fp), 0L, SEEK_CUR);
        if(fseek(fp, 0L, SEEK_SET) < 0)
            fseek(fp, 0L, SEEK_SET);

        psize = (bytes > 512L) ? 1024 : 128;
    }
    else
        log_error("cfsbc", "fseek failed\n");


    printf("Uploading %s (packet size = %d) ", filename, psize);
    fflush(stdout);

    r = xmodem_send(serial_dev, upload_file, (void*)fp, psize);
    fclose(fp);
    fputs(" done\n", stdout);

    return r > 0 ? bytes : (long)r;
}

/**
 * cfsbc_put_file - use XMODEM to transfer a file to the SBC
 *
 * @param filename: local file name
 * @param dest: remote file name, if NULL use @filename.
 */
int
cfsbc_put_file(const char *filename, const char *dest)
{
    long              bytes;
    const char        *name;

    PURGE_INPUT(serial_dev, 100);

    name = (dest != NULL && strlen(dest) > 0) ? dest : filename;

    log_event("Uploading file %s\n", filename);
    sprintf(shell_command, "put %s\n", name);
    sprintf(shell_response, RX_RESP_TEMPLATE, name);
    if(serial_chat(serial_dev, shell_command, shell_response, 5L) == 1)
    {
        bytes = xmodem_put_file(filename);
        log_event("File sent (%ld bytes)\n", bytes);
        return (serial_chat(serial_dev, "", SHELL_PROMPT, 3L) == 1);
    }
    else
        log_error("cfsbc", "Cannot start file transfer\n");

    return 0;
}

/**
 * Use XMODEM to transfer a file to the archive on the SBC.
 */
int
cfsbc_archive_file(const char *filename)
{
    long        bytes;

    PURGE_INPUT(serial_dev, 100);

    log_event("Archiving file %s\n", filename);
    sprintf(shell_command, "archive %s\n", filename);
    sprintf(shell_response, RX_RESP_TEMPLATE, filename);
    if(serial_chat(serial_dev, shell_command, shell_response, 5L) == 1)
    {
        bytes = xmodem_put_file(filename);
        log_event("File sent (%ld bytes)\n", bytes);
        return (serial_chat(serial_dev, "", SHELL_PROMPT, 3L) == 1);
    }
    else
        log_error("cfsbc", "Cannot start file transfer\n");

    return 0;
}

/**
 * Set the SBC clock to the current TT8 time.
 */
int
cfsbc_set_clock(void)
{
    time_t      now;
    struct tm   *t;

    PURGE_INPUT(serial_dev, 100);
    time(&now);
    t = localtime(&now);
    strftime(shell_command, sizeof(shell_command)-1,
             "time %Y-%m-%dT%H:%M:%S\n", t);
    return serial_chat(serial_dev, shell_command, SHELL_PROMPT, 5L) == 1;
}

/**
 * Upload the current parameter settings to the SBC.
 */
int
cfsbc_send_params(const char *filename)
{
    FILE *pfp;
    long bytes = 0;

    PURGE_INPUT(serial_dev, 100);
    if((pfp = fopen(filename, "a")) != NULL)
    {
        dump_params(pfp);
        fclose(pfp);
        if(serial_chat(serial_dev, "params\n", "receive .params.incoming", 5L) == 1)
        {
            bytes = xmodem_put_file(filename);
            log_event("File %s sent (%ld bytes)\n", filename, bytes);
            return (serial_chat(serial_dev, "", SHELL_PROMPT, 6L) == 1);
        }
        else
            log_error("cfsbc", "Cannot start file transfer\n");
    }

    return 0;
}

/**
 * Send the current status information and instruct the SBC to start a
 * communication session with the shore server. If imei is NULL or an
 * empty string, the IMEI value will be read from the modem.
 */
int
cfsbc_start_comms(const char *status_file, const char *imei)
{
    FILE        *ifp;
    int         c;

    PURGE_INPUT(serial_dev, 100);

    if((ifp = fopen(status_file, "r")) != NULL)
    {
        PET_WATCHDOG();
        sputz(serial_dev, "status\napplication/xml\n");
        while((c = fgetc(ifp)) != EOF)
        {
            sputc(serial_dev, c);
            DelayMilliSecs(1L);
        }
        sputc(serial_dev, '\n');
        fclose(ifp);
    }

    PURGE_INPUT(serial_dev, 100);

    // Get the shell prompt
    if(serial_chat(serial_dev, "\n", SHELL_PROMPT, 3L) != 1)
        return 0;

    // Start the communication process in the background
    if(imei != NULL && *imei != '\0')
    {
        char    cmd[24];
        sprintf(cmd, "call %s\n", imei);
        return cfsbc_shell_command(cmd, 7000L);
    }
    else
        return cfsbc_shell_command("call\n", 7000L);
}

int
cfsbc_done(void)
{
    return (CFSBC_MODEM_INUSE() == 0);
}

void
cfsbc_halt(void)
{
    PURGE_INPUT(serial_dev, 100);
    sputz(serial_dev, "halt\n");
}


int
cfsbc_sleep(long seconds)
{
    PURGE_INPUT(serial_dev, 100);
    if(serial_chat(serial_dev, "\n", SHELL_PROMPT, 2L) != 1)
    {
        log_error("cfsbc", "No response\n");
        return 0;
    }
    snprintf(shell_command, sizeof(shell_command)-1, "sleep %ld\n", seconds);
    sputz(serial_dev, shell_command);
    return 1;
}

static int xm_last_index = -1;

static int
download_file(char *buf, int size, int index, void *arg)
{
    FILE        *fp = (FILE*)arg;

    if(index != xm_last_index)
    {
        SerPutByte('.');
        if(fwrite(buf, (size_t)1, (size_t)size, fp) != (size_t)size)
        {
            log_error("download", "File write error\n");
            return 0;
        }

        xm_last_index = index;
    }

    return 1;
}

static int
cfsbc_xmodem_get(const char *cmd, const char *filename)
{
    FILE        *fp;
    int         r;

    r = serial_chat(serial_dev, cmd, "command now.|CF>", 5L);
    switch(r)
    {
        case 0:
            log_error("cfsbc", "Timeout in file download\n");
            return 0;
        case 2:
            log_error("cfsbc", "File not found (%s)\n", cmd);
            return 0;
    }

    if((fp = fopen(filename, "wb")) == NULL)
    {
        log_error("cfsbc", "Cannot open file %s\n", filename);
        return 0;
    }

    printf("Downloading %s ", filename);
    fflush(stdout);
    xm_last_index = -1;
    r = xmodem_recv(serial_dev, download_file, (void*)fp);
    fclose(fp);
    fputs(" done\n", stdout);

    return r;
}

int
cfsbc_download_file(const char *filename, int remove)
{
    if(remove)
        sprintf(shell_command, "get -r %s\n", filename);
    else
        sprintf(shell_command, "get %s\n", filename);
    return cfsbc_xmodem_get(shell_command, filename);
}

/**
 * Take a photo using the SBC camera.
 */
int
cfsbc_photo(struct camera_setup_t *csp, long timeout, int bg)
{
    long        nchars, i, n;
    size_t      limit;
    char        *p;

    p = shell_command;
    limit = sizeof(shell_command);

    if(csp->aoi != (char*)0 && csp->aoi[0] != '\0')
        nchars = snprintf(shell_command, sizeof(shell_command),
                          "%sphoto crop=%s", bg ? "bg" : "", csp->aoi);
    else
        nchars = snprintf(shell_command, sizeof(shell_command),
                          "%sphoto", bg ? "bg" : "");

    if(csp->settings != (char*)0)
    {
        p += nchars;
        limit -= nchars;
        nchars = snprintf(p, limit, " settings=%s", csp->settings);
    }

    if(csp->exposure > 0)
    {
        p += nchars;
        limit -= nchars;
        nchars = snprintf(p, limit, " exposure=%.3lf", csp->exposure);
    }

    n = (csp->n_scales > 3) ? 3 : csp->n_scales;

    p += nchars;
    limit -= nchars;
    for(i = 0; i < n;i++)
    {
        nchars = snprintf(p, limit, " scale=%dq", csp->scales[i]);
        p += nchars;
        limit -= nchars;
    }
    log_event("Photo command: \"%s\"\n", shell_command);
    *p++ = '\n';
    *p = '\0';

    return cfsbc_shell_command(shell_command, timeout);
}

int
cfsbc_oneshot(const char *aoi, double exposure, unsigned int scale, long timeout,
              char **filename, long n, post_func_t fpost)
{
    char                        *resp, *p, *q;
    long                        i;
    unsigned                    scales[2];
    struct camera_setup_t       cs;

    scales[0] = 25;
    scales[1] = scale;
    cs.scales = scales;
    cs.aoi = aoi;
    cs.exposure = exposure;
    cs.n_scales = 2;

    if(cfsbc_photo(&cs, timeout, 0) == 0)
    {
        log_error("cfsbc", "No photo taken\n");
        return 0;
    }

    if(fpost != NULL)
        fpost();

    for(i = 0; i < 2; i++)
    {
        if(cfsbc_shell_command("next\n", 3000L) == 0)
        {
            log_error("cfsbc", "No response\n");
            return 0;
        }

        resp = cfsbc_shell_response(0L);
        if(resp[0] == '\0' || (strncmp(resp, "CF", 2L) == 0))
        {
            log_error("cfsbc", "No file stored\n");
            break;
        }

        if(filename[i] != NULL)
        {
            p = filename[i];
            q = resp;
            while(*q != '\0')
            {
                if(isalnum(*q) || *q == '.')
                {
                    *p = *q;
                    p++;
                }
                q++;
            }
            *p = '\0';
        }

        if(cfsbc_download_file(filename[i], 1) == 0)
        {
            log_error("cfsbc", "File download failed\n");
            return 0;
        }
    }

    return 1;
}
