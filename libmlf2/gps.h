/**@file
** $Id: gps.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _GPS_H_
#define _GPS_H_

#define GPS_MAX_SAT	32

//** Degrees, minutes, fractional minutes */
typedef struct _dms {
    short	deg;		/**< Degrees */
    short	min;		/**< Minutes */
    short	frac;		/**< 1/10000 minutes */
    char	dir;		/**< N, S, E, or W */
} DMS;

/** GPS data record */
typedef struct _gpsdata {
    DMS			lat;  /**< latitude */
    DMS			lon; /**< longitude */
    int			satellites; /**< number of satellites used */
    int			status; /**< fix status */
#ifdef EXPERIMENTAL
    unsigned char	snr[GPS_MAX_SAT];
#endif
} GPSdata;

int gps_init(void);
void gps_shutdown(void);
int gps_read_data(GPSdata *gpd);
int gps_set_clock(void);
int gps_dev_ready(void);


#endif
