/**@file
** Extended motor control interface for MLF2.
**
*/
#include <stdio.h>
#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <qsm332.h>
#include <sim332.h>
#include "ioports.h"
#include "power.h"
#include "log.h"
#include "mc.h"
#include "xmotor.h"

/* Delay after a motor move before MC is ready to accept commands (ms) */
static long mc_command_delay = 1000L;

long
xmotor_set_delay(long ms)
{
    long    val = mc_command_delay;

    mc_command_delay = ms;
    return val;
}

/**
 * Return the current motor position
 *
 * @returns position in counts
 */
long
xmotor_pos(void)
{
    return mc_get_counter() / MC_SCALE_FACTOR;
}

/**
 * Initialize the external motor subsystems
 */
void
xmotor_sys_init()
{
    mc_shutdown();
    DelayMilliSecs(1000L);
    /* Prevent automatic homing of secondary piston */
    mc_set_inhibit();
    mc_init(MC_SERIAL_DEVICE);
    DelayMilliSecs(500L);
    mc_clear_inhibit();

}

/**
 * Power down both motor subsystems
 */
void
xmotor_sys_shutdown()
{
    mc_shutdown();
}

/**
 * Move external piston to the HOME position.
 *
 * @return motor status, 1 for running, 0 for stopped.
 */
int
xmotor_home(void)
{
    long xp = mc_get_counter();

    mc_shutdown();
    DelayMilliSecs(1000L);
    mc_init(MC_SERIAL_DEVICE);
    DelayMilliSecs(mc_command_delay);

    return mc_isrunning();
}

/**
 * Move external piston to a new position.
 *
 * @param  target  desired position specified in counts.
 * @return motor status, 1 for running, 0 for stopped.
 */
int
xmotor_move(long target)
{
    if(target > XBALLAST_MAX)
        target = XBALLAST_MAX;
    else if(target < XBALLAST_MIN)
        target = XBALLAST_MIN;

    mc_goto(target * MC_SCALE_FACTOR);
    DelayMilliSecs(mc_command_delay);

    return mc_isrunning();
}

/**
 * Wait for the latest motor move to complete.
 *
 * @param  timeout  maximum wait time in milliseconds.
 * @return motor status, 1 for running, 0 for stopped.
 */
int
xmotor_wait(long timeout)
{
    long    start = MilliSecs();

    if(mc_isrunning())
    {
        while(mc_isrunning())
        {
            if((MilliSecs() - start) >= timeout)
                break;
            PET_WATCHDOG();
        }
        DelayMilliSecs(mc_command_delay);
    }

    return mc_isrunning();
}
