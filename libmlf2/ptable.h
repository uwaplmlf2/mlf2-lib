/*
** $Id: ptable.h,v 41b47946b4f5 2007/11/09 22:41:24 mikek $
*/
#ifndef _PTABLE_H_
#define _PTABLE_H_

#include "hash.h"

#define NR_CELLS	51	/* Number of hash-table cells */

#define PTYPE_SHORT	0
#define PTYPE_LONG	1
#define PTYPE_DOUBLE	2
#define PTYPE_STRING	4
#define PTYPE_READ_ONLY	0x80
#define PTYPE_DIRTY	0x40
#define PTYPE_TYPEMASK	0x0f
#define PTYPE_SIZEMASK	0xff00
#define PTYPE_SIZESHIFT	8

#define PTYPE_GETSIZE(t)	(((t) & PTYPE_SIZEMASK) >> PTYPE_SIZESHIFT)
#define PTYPE_SETSIZE(t, n)	((t) | ((unsigned)(n) << PTYPE_SIZESHIFT))

struct param {
    unsigned	type;
    void	*loc;
};

void add_param(const char *name, int type, void *addr);
void dump_params(FILE *fp);
int init_param_table(void);
int get_param(const char *name, struct param *param);
long get_param_as_int(const char *name);
double get_param_as_double(const char *name);
int set_param_str(const char *name, const char *str);
int set_param_int(const char *name, long value);
int set_param_double(const char *name, double value);

#endif /* _PTABLE_H_ */
