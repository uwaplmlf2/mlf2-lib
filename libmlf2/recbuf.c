/**@file
 *
 * Functions for working with comma or tab-separated data records
 */
#include "recbuf.h"


void recbuf_init(RecBuf *rbp, char *buf, int n, char **fbuf, int nf)
{
    rbp->buf = buf;
    rbp->bufsize = n;
    rbp->field = fbuf;
    rbp->maxfields = nf;
    rbp->nfields = 0;
}

int recbuf_split(RecBuf *rbp, char sep)
{
    char        *p;

    if(rbp->maxfields <= 0)
        return 0;

    p = rbp->buf;
    rbp->field[0] = p;
    rbp->nfields = 1;

    while(rbp->nfields < rbp->maxfields && *p != '\0')
    {
        if(*p == sep)
        {
            *p++ = '\0';
            rbp->field[rbp->nfields++] = p;
        }
        else
            p++;
    }

    return rbp->nfields;
}
