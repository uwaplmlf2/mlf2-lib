/*
** arch-tag: c261cbbf-7565-47e4-adef-1432255595d9
** Time-stamp: <2007-04-05 11:50:00 mike>
*/
#ifndef _ANALOGOUT_H_
#define _ANALOGOUT_H_

#define VOLTS_TO_COUNTS(v) (short)((v)*4095./5.)

void da_output(short value);


#endif /* _ANALOGOUT_H_ */
