/**@file
 * Driver for Anderaa Optode oxygen sensor.
 *
 * $Id: optode.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
 *
 */
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "ioports.h"
#include "serial.h"
#include "log.h"
#include "optode.h"

#include "iofuncs.h"

#define WARM_UP_MSECS   0L
#define RECORD_SIZE     360

#define REC_START       0x11 /* record start char, ^Q */
#define REC_END         0x0d /* record end char, ^M */

static int serial_dev = -1;
static int old_format = 0;
static long init_time;
static char recbuf[RECORD_SIZE];

#define STREQ(s0, s1)   ((s0)[0] == (s1)[0] && !strcmp((s0), (s1)))
#define STRENDSWITH(s, c)       ((s)[strlen(s)-1] == (c))

static int record_parse(char *record, float *values, int n_vals)
{
    char        *rptr, *savep, *token;
    int         i, int_count;
    enum {ST_START=0, ST_READINT, ST_READFIELD, ST_READVAL, ST_DONE} state;

    rptr = record;
    i = 0;
    int_count = 2;
    state = ST_START;

    while((token = strtok_r(rptr, " \t", &savep)) != NULL)
    {
        rptr = NULL;
        switch(state)
        {
            case ST_START:
                if(STREQ(token, "MEASUREMENT"))
                    state = ST_READINT;
                break;
            case ST_READINT:
                if(isdigit(token[0]))
                    int_count--;
                if(int_count == 0)
                    state = ST_READFIELD;
                break;
            case ST_READFIELD:
                if(STRENDSWITH(token, ']'))
                    state = ST_READVAL;
                else if(STRENDSWITH(token, ':'))
                {
                    state = ST_READVAL;
                    old_format = 1;
                }
                break;
            case ST_READVAL:
                if(sscanf(token, "%f", &values[i]) == 1)
                {
                    i++;
                    state = (i == n_vals) ? ST_DONE : ST_READFIELD;
                }
                break;
            case ST_DONE:
                break;
        }
    }

    return i;
}

/**
 * Open connection to the device and initialize. Errors are logged.
 *
 *
 * @return 1 if successful, otherwise 0.
 */
int
optode_init(void)
{
    if(serial_dev >= 0)
        goto already_open;

    if((serial_dev = serial_open(OPTODE_DEVICE, 0, OPTODE_BAUD)) < 0)
    {
        log_error("optode", "Cannot open serial device (code = %d)\n",
                  serial_dev);
        return 0;
    }

    init_time = MilliSecs();
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    old_format = 0;
already_open:
    return 1;
}

/**
 * Shutdown the device interface and power off the device.
 */
void
optode_shutdown(void)
{
    if(serial_dev >= 0)
    {
        serial_close(serial_dev);
        serial_dev = -1;
    }

}

/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
optode_dev_ready(void)
{
    return (serial_dev >= 0) && ((MilliSecs() - init_time) > WARM_UP_MSECS);
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 */
void
optode_test(void)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

/**
 * Read a data sample.
 *
 * @param  odp  pointer to returned data structure
 * @param  timeout  maximum time allowed for reading (milliseconds)
 *
 * @return 1 if successful, 0 on error.
 */
int
optode_read_data(OptodeData *odp, long timeout)
{
    char        *p;
    int         c, i;
    long        t;
    float       raw[OPTODE_DATA_FIELDS];

    memset(odp, sizeof(OptodeData), 0);

    if(serial_chat(serial_dev, "", "MEAS", (timeout/1000L)+1) == 0)
    {
        log_error("optode", "Timeout waiting for record start\n");
        return 0;
    }

    p = recbuf;
    i = sprintf(recbuf, "MEAS");
    p += i;
    t = MilliSecs();
    while(i < RECORD_SIZE && (c = sgetc_timed(serial_dev, t, timeout)) != REC_END)
    {
        if(c < 0)
        {
            log_error("optode", "Timeout waiting for record end\n");
            return 0;
        }
        *p = c;
        p++;
        i++;
    }
    *p = '\0';
    if(record_parse(recbuf, raw, OPTODE_DATA_FIELDS) != OPTODE_DATA_FIELDS)
        return 0;

    if(old_format)
    {
        odp->version = 1;
        odp->data.v1.oxy = raw[OPTODE_FIELD_OXY];
        odp->data.v1.bphase = raw[OPTODE_FIELD_BPHASE];
        odp->data.v1.sat = raw[OPTODE_FIELD_SAT];
        odp->data.v1.temp = raw[OPTODE_FIELD_TEMP];
    }
    else
    {
        odp->version = 2;
        odp->data.v2.c1rph = raw[OPTODE_FIELD_C1RPH];
        odp->data.v2.c2rph = raw[OPTODE_FIELD_C2RPH];
        odp->data.v2.c1amp = raw[OPTODE_FIELD_C1AMP];
        odp->data.v2.c2amp = raw[OPTODE_FIELD_C2AMP];
        odp->data.v2.sat = raw[OPTODE_FIELD_SAT];
        odp->data.v2.temp = raw[OPTODE_FIELD_TEMP];
    }

    return 1;
}
