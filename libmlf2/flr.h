/*
** $Id: flr.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _FLR_H_
#define _FLR_H_

#define FLR_DEVICE	4
#define FLR_MINOR	0
#define FLR_BAUD	9600L
#define FLR_TIMEOUT_MS	2500L

int flr_init(void);
void flr_shutdown(void);
int flr_dev_ready(void);
int flr_set_params(int navg);
int flr_read_data(void);
void flr_get_stats(unsigned long *errs, unsigned long *reads);

#endif /* _FLR_H_ */
