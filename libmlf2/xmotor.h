/*
** Motor interface for dual-piston system.
*/
#ifndef _XMOTOR_H_
#define _XMOTOR_H_

#include "motor.h"

/* Ballast position for deployment (~23mm) */
#define XBALLAST_PARK           372165L

#define XBALLAST_MIN            0L
/* Maximum ballast position in counts */
#define XBALLAST_MAX            BALLAST_MAX

/* Serial interface to motor controller */
#define MC_SERIAL_DEVICE        3

/* Scale factor for MC positions */
#define MC_SCALE_FACTOR         2L

int xmotor_home(void);
long xmotor_pos(void);
int xmotor_move(long target);
void xmotor_stop(void);
void xmotor_sys_init(void);
void xmotor_sys_shutdown(void);
long xmotor_set_delay(long ms);
int xmotor_wait(long timeout);

#endif

