/*
** $Id: eco.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Interface to Wetlabs ECO Scattering Meter
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <setjmp.h>
#include <ctype.h>
#include "serial.h"
#include "salarm.h"
#include "log.h"
#include "eco.h"

#ifndef __GNUC__
#define __inline__
#endif

#define WARM_UP_MSECS	350L

static int ref = 0;
static int serial_dev = -1;
static jmp_buf	recover;
static long init_time;
static unsigned long nr_reads, nr_errors;

static __inline__ int serial_getc(int desc)
{
    unsigned char	c;
    return (serial_read(desc, &c, (size_t)1) == 1) ? ((int)c)&0xff : -1;
}

static void
catch_alarm(void)
{
#ifdef __GNUC__
    longjmp(recover, 1L);
#else
    longjmp(recover, 1);
#endif
}

/**
 * eco_init - initialize the fluorometer interface.
 *
 * Open a connection to the fluorometer and initialize the device.  Returns
 * 1 if successful, otherwise 0.
 */
int
eco_init(void)
{
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(ECO_DEVICE, ECO_MINOR, ECO_BAUD)) < 0)
    {
	log_error("eco", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }
    
    init_time = MilliSecs();
    
initdone:
    ref++;

    return 1;
}

/**
 * eco_shutdown - shutdown the fluorometer interface.
 *
 * Shutdown the fluorometer interface and power off the device.
 */
void
eco_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    serial_close(serial_dev);
}


/**
 * eco_dev_ready - check if device is ready.
 *
 * Return true if the device is ready to accept commands.
 */
int
eco_dev_ready(void)
{
    return ref && ((MilliSecs() - init_time) > WARM_UP_MSECS);
}

/**
 * eco_set_params - set fluorometer parameters.
 * @navg: number of samples to average.
 *
 * Set sampling parameters for the fluorometer device, currently there
 * is only one.  Returns 1 if successful, 0 if an error occurs.
 */
int
eco_set_params(int navg)
{
    int		r;
    char	cmd[12], resp[12];
    
    if(navg < 1 || navg > 255)
    {
	log_error("eco", "Bad parameter, navg = %d\n", navg);
	return 0;
    }

    r = 1;
    sprintf(cmd, "$ave %d\r\n", navg);
    sprintf(resp, "Ave %d\r\n", navg);
    
    if(!serial_chat(serial_dev, cmd, resp, 2L))
    {
	log_error("eco", "Cannot set averaging value, %s\n", cmd);
	r = 0;
    }
    
    serial_inflush(serial_dev);
    
    return r;
}

/**
 * eco_get_stats - fetch read error statistics
 * @errs: returned error count
 * @reads: returned read count.
 */
void
eco_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}

/**
 * eco_read_data - read scattering data values.
 * @edp: pointer to returned data.
 *
 * Reads the next sample value from the ECO (this may actually be
 * an average of many samples, see eco_set_params()) as raw counts.
 * Returns 1 if successful, 0 if an error occurs.
 */
int
eco_read_data(ECOdata *edp)
{
    int		c, i;
    short	num;
    short	buf[ECO_FIELDS];
    
    if(ref <= 0)
	return 0;
   
    nr_reads++;
    
    alarm_set_ms((long)ECO_TIMEOUT_MS, ALARM_ONESHOT, catch_alarm);
    if(setjmp(recover) != 0)
    {
	log_error("eco", "Timeout reading Scattering Meter\n");
	nr_errors++;
	return 0;
    }

    /*
    ** Send an 'r' command to read a data sample.  The device will respond
    ** with the following string:
    **
    **	\r\nDATE\tTIME\tSAMPLE[1-6]\r\n
    **
    */
    serial_write(serial_dev, "$run\r\n", 6L);

    /*
    ** Look for two TABs which will put us at the start of SAMPLE.  
    */
    while(serial_getc(serial_dev) != '\t')
	;
        
    while(serial_getc(serial_dev) != '\t')
	;
    
    num = 0;
    i = 0;
    while(i < ECO_FIELDS && (c = serial_getc(serial_dev)) != '\r')
    {
	if(isdigit(c))
	    num = num*10 + (c - '0');
	else if(c == '\t')
	{
	    buf[i] = num;
	    i++;
	    num = 0;
	}
    }

    alarm_shutdown();

    edp->blue[ECO_REF] = buf[0];
    edp->blue[ECO_SIG] = buf[1];
    edp->red[ECO_REF]  = buf[2];
    edp->red[ECO_SIG]  = buf[3];
    edp->flr           = buf[4];
    edp->therm         = buf[5];
    
    serial_inflush(serial_dev);
    
    return 1;
}


