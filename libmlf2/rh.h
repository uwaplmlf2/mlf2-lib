/*
** $Id: rh.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _RH_H_
#define _RH_H_

#define RH_CHANNEL	8	/* A/D channel */

int rh_init(void);
void rh_shutdown(void);
int rh_dev_ready(void);
short rh_read_data(void);
double rh_mv_to_percent(int mv);

#endif
