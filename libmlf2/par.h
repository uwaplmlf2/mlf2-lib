/*
** $Id: par.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _PAR_H_
#define _PAR_H_

#define PAR_CHANNEL0	14	/* A/D channel */
#define PAR_CHANNEL1	15

int par_init(void);
void par_shutdown(void);
int par_dev_ready(void);
long par_read_data(void);

#endif
