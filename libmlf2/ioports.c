/**@file
** Interface to external 8-bit I/O ports.
**
** $Id: ioports.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
**
**
*/
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8lib.h>
#include "ioports.h"

/** Base address for the ports */
#define PORTBASE	0x400000

#define SEGMENT_DESC	((PORTBASE >> 8) | SZ2K)

/*
** Options:  upper-byte, assert CS on R/W, synchronize CS with data 
** strobe, no wait states, supervisor space access, user space access.
*/
#define SELECT_OPTS  ((M_BYTEU & SET) |\
                      (M_CSWRITE & SET) |\
                      (M_CSREAD & SET) |\
                      (M_DSSTRB & SET) |\
                       M_WAIT0 |\
                      (M_SPCS & SET) |\
                      (M_SPCU & SET))
/** Number of ports */
#define NPORTS		6

/*
** Maintain virtual "registers" for the write-only ports
*/
static unsigned char iop_state[NPORTS];
static int iop_init = 0;

#define IOP_CHECK()	if((*CSBAR0 != SEGMENT_DESC) || (*CSOR0 != SELECT_OPTS)) init_ioports()

/**
 * Initialize I/O port interface.
 * Initialize the external 8-bit I/O ports on the MLF2 board.  This
 * function will be called automatically the first time one of the other
 * module functions is called.
 *
 * Each port is backed by a "virtual register" which holds the current
 * value of the port and allows the reading of output ports.  To insure
 * we start from a known state, all ports are set to zero.
 */
void
init_ioports(void)
{
    register int	i;

    /* 8-bit port */
    _CSPAR0->CS0 = PORT8;

    /* Set base address and block size (2k bytes) */
    *CSBAR0 = SEGMENT_DESC;

    /* set options */
    *CSOR0 = SELECT_OPTS;

    if(!iop_init)
    {
	iop_init = 1;
	for(i = 0;i < NPORTS;i++)
	    iop_write(i, (unsigned char)0);
    }
    
}

/**
 * Read value from I/O port.
 * Read the value from the specified port.  port must be one of the IO_*
 * constants defined in ioports.h.  By default, the virtual
 * register for the port is read.  In order to read the real input,
 * port must be ORed with the constant REAL_INPUT.  Note that
 * port A is the only one with a real input.
 *
 * @param  port  port ID.
 * @return port value.
 */
unsigned char
iop_read(int port)
{
    IOP_CHECK();
    
    /*
    ** If REAL_INPUT bit is set read the physical port, otherwise read
    ** the virtual register
    */
    return (port&REAL_INPUT) ? *((unsigned char*)PORTBASE+(port&ADDR_MASK)) : 
      ((port < 0 || port >= NPORTS) ? 0 : iop_state[port]);
}

/**
 * Write a byte to an output port
 *
 * @param  port  port ID
 * @param  value  value to write
 */
void
iop_write(int port, unsigned char value)
{
    IOP_CHECK();
    
    if(port < 0 || port >= NPORTS)
	return;
    *((unsigned char*)PORTBASE+port) = value;
    iop_state[port] = value;
}

/**
 * Set bits in an output port.
 *
 * @param  port  port ID
 * @param  mask  mask of bits to set.
 */
void
iop_set(int port, unsigned char mask)
{
    IOP_CHECK();
    
    if(port < 0 || port >= NPORTS)
	return;
    iop_state[port] |= mask;
    *((unsigned char*)PORTBASE+port) = iop_state[port];
}


/**
 * Clear bits in an output port.
 *
 * @param  port  port ID
 * @param  mask  mask of bits to clear.
 */
void
iop_clear(int port, unsigned char mask)
{
    IOP_CHECK();
    
    if(port < 0 || port >= NPORTS)
	return;
    mask = ~mask;
    iop_state[port] &= mask;
    *((unsigned char*)PORTBASE+port) = iop_state[port];
}

/**
 * Clear all output ports.
 */
void
iop_clear_all(void)
{
    register int	i;

    IOP_CHECK();
    
    for(i = 0;i < NPORTS;i++)
    {
	iop_state[i] = 0;
	*((unsigned char*)PORTBASE+i) = 0;
    }
}

    
    

      
