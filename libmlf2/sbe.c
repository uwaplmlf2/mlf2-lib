/**@file
 * Common driver code for a Seabird CTD
 *
 */
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "sbe.h"
#include "iofuncs.h"

#define SAMPLE_TIMEO_MS     3000L
#define PURGE_INPUT(d) while(sgetc_timed(d, MilliSecs(), 100L) != -1)
#define WAKEUP(d) (serial_chat(d, "\r", "S>", 3L) == 1)

/**
 * sbe_init - initialize the interface.
 *
 * @param devnum  serial device number
 * @param baud    serial baud rate
 * @return serial device ID or -1 on error.
 */
int
sbe_init(int devnum, long baud)
{
    int     sdev;

    if((sdev = serial_open(devnum, 0, baud)) < 0)
    {
        log_error("sbe", "Cannot open serial device (code = %d)\n",
                  sdev);
        return -1;
    }

    serial_inmode(sdev, SERIAL_NONBLOCKING);
    return sdev;
}

/**
 * sbe_shutdown - shutdown the device interface.
 *
 * @param sdev  serial device ID.
 */
void
sbe_shutdown(int sdev)
{
    serial_close(sdev);
}

/**
 * sbe_dev_ready - check if the device is ready for commands.
 *
 * @param sdev  serial device ID
 * @return 1 (yes) or 0 (no)
 */
int
sbe_dev_ready(int sdev)
{
    return WAKEUP(sdev);
}

/**
 * sbe_test - pass through mode.
 *
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 *
 * @param sdev  serial device number.
 */
void
sbe_test(int sdev)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(sdev, 0x03, 0x02);
}

/**
 * sbe_upload - upload non-averaged data in engineering units
 *
 * Upload stored data from the device and pass each line of data
 * (scan) to a call-back function. If START or END are less than
 * or equal to zero, all scans are returned.
 *
 * @param sdev          serial device ID
 * @param start         starting scan
 * @param end           ending scan
 * @param callback      function to process each scan
 * @param cbdata        opaque data pointer passed to callback
 * @returns number of uploaded samples, -1 on error.
 */
int
sbe_upload(int sdev, int start, int end,
           void (*callback)(void *obj, char *linebuf),
           void *cbdata)
{
    int     mode, i, c, n, n_samples;
    ulong   t_start;
    char    linebuf[80], cmdbuf[20];
    enum {ST_WAIT_START=1,
          ST_PROCESS,
          ST_GET_PROMPT,
          ST_DONE} state;

    PURGE_INPUT(sdev);
    if(!WAKEUP(sdev))
        return -1;

    // Set the interface to not block on input
    mode = serial_inmode(sdev, SERIAL_NONBLOCKING);
    if(start > 0 && end > 0)
    {
        snprintf(cmdbuf, sizeof(cmdbuf), "dd%d,%d\r", start, end);
        sputz(sdev, cmdbuf);
    }
    else
        sputz(sdev, "dd\r");

    t_start = MilliSecs();
    i = 0;
    n = sizeof(linebuf) - 1;
    n_samples = 0;

    state = ST_WAIT_START;
    while(state != ST_DONE)
    {
        c = sgetc_timed(sdev, t_start, SAMPLE_TIMEO_MS);
        if(c < 0)
            break;
        switch(state)
        {
            case ST_WAIT_START:
                if(c == '\n')
                {
                    state = ST_PROCESS;
                    i = 0;
                }
                break;
            case ST_PROCESS:
                switch(c)
                {
                    case '\n':
                        // Scan complete
                        linebuf[i] = '\0';
                        if(i > 0)
                        {
                            if(strncmp(linebuf, "upload complete", 15L) != 0)
                            {
                                callback(cbdata, linebuf);
                                n_samples++;
                            }
                        }
                        i = 0;
                        // Reset the timer after each scan
                        t_start = MilliSecs();
                        break;
                    case 'S':
                        state = ST_GET_PROMPT;
                        break;
                    default:
                        if(i < n && isprint(c))
                            linebuf[i++] = c;
                        break;
                }
                break;
            case ST_GET_PROMPT:
                if(c == '>')
                    state = ST_DONE;
                break;
            case ST_DONE:
                break;
        }
    }
    serial_inmode(sdev, mode);

    return n_samples;
}

/**
 * sbe_cmd - send a command and process each line of the response
 *
 * Each LF terminated line read from the device is passed to a call-back
 * function. When the call-back returns a zero or the device prompt ('S>')
 * is seen, the function returns.
 *
 * @param sdev      serial device ID
 * @param cmd       command string
 * @param timeout   response timeout in milliseconds
 * @param callback  function to process each line
 * @param cbdata    opaque data pointer passed to callback
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe_command(int sdev, const char *cmd, long timeout,
            CBstatus_t (*callback)(void *obj, char *linebuf), void *cbdata)
{
    int     mode, i, c, n, lines, rval, r;
    ulong   t_start;
    char    linebuf[80];
    enum {ST_WAIT_START=1,
          ST_PROCESS,
          ST_ERROR,
          ST_GET_PROMPT,
          ST_DONE} state;

    PURGE_INPUT(sdev);

    if(strncmp(cmd, "sl", 2L) == 0)
        state = ST_PROCESS;   // No command echo
    else
    {
        if(!WAKEUP(sdev))
            return 0;
        state = ST_WAIT_START;
    }

    // Set the interface to not block on input
    mode = serial_inmode(sdev, SERIAL_NONBLOCKING);
    sputz(sdev, cmd);

    t_start = MilliSecs();
    i = 0;
    lines = 0;
    n = sizeof(linebuf) - 1;
    rval = 1;

    while(state != ST_DONE)
    {
        c = sgetc_timed(sdev, t_start, timeout);
        if(c < 0)
        {
            rval = 0;
            break;
        }

        switch(state)
        {
            case ST_WAIT_START:
                if(c == '\n')
                {
                    state = ST_PROCESS;
                    i = 0;
                }
                break;
            case ST_PROCESS:
                switch(c)
                {
                    case '\n':
                        linebuf[i] = '\0';
                        if(callback)
                        {
                            r = callback(cbdata, linebuf);
                            if((r & CB_WAIT_PROMPT) == 0)
                                state = ST_DONE;
                            if((r & CB_ERROR))
                                rval = 0;  // Error in callback
                        }
                        i = 0;
                        break;
                    case 'S':
                        state = ST_GET_PROMPT;
                        break;
                    case '?':
                        state = ST_ERROR;
                        rval = 0;
                        break;
                    default:
                        if(i < n && isprint(c))
                            linebuf[i++] = c;
                        break;
                }
                break;
            case ST_ERROR:
                if(c == 'S')
                    state = ST_GET_PROMPT;
                break;
            case ST_GET_PROMPT:
                if(c == '>')
                    state = ST_DONE;
                break;
            case ST_DONE:
                break;
        }
    }
    serial_inmode(sdev, mode);

    return rval;
}
