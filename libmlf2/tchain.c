#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "lpsleep.h"
#include "serial.h"
#include "log.h"
#include "tchain.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define PURGE_INPUT(d) while(sgetc_timed(d, MilliSecs(), 100L) != -1)

/* CCITT CRC polynomial representation */
#define CRC_POLY    0x1021

#define MAX_DEVS    2
#define WARMUP_MS   1000L

// Maintain the state of each device
typedef struct devstate {
    int             sd; /**< serial device descriptor */
    int             ref; /**< reference count */
    long            t0; /** < start time in ms */
} devstate_t;
static devstate_t dstab[MAX_DEVS];
static int devs_active = 0;

static char linebuf[80];
static char databuf[512];

static __inline__ int WAKEUP(int sd)
{
    sputc(sd, '\r');
    DelayMilliSecs(10L);
    return (serial_chat(sd, "\r", "Ready: ", 2L) == 1 || serial_chat(sd, "\r", "Ready: ", 2L) == 1);
}

/*
 * update_crc - incrementally update a CCITT CRC calculation.
 * @crc: current CRC
 * @c: character to add to CRC
 *
 * Returns the new CRC.
 */
static __inline__ unsigned short update_crc(unsigned short crc, int c)
{
    int     i;

    crc = crc ^ (c << 8);
    for(i = 0;i < 8;i++)
    {
        if(crc & 0x1)
            crc = (crc >> 1) ^ CRC_POLY;
        else
            crc >>= 1;
    }

    return crc;
}

static void
tchain_reopen(void)
{
    int  i;
    for(i = 0;i < MAX_DEVS;i++)
    {
        if(dstab[i].ref > 0)
        {
            serial_reopen(dstab[i].sd);
            serial_inmode(dstab[i].sd, SERIAL_NONBLOCKING);
            serial_inflush(dstab[i].sd);
        }
    }
}

/*
 * Read the response from the device up to the first CR-LF.
 */
static int
read_response(int sd, char *buf, size_t n, long timeout)
{
    int             c;
    size_t          i;
    unsigned long   t_start;
    enum {ST_START=1,
          ST_READING,
          ST_SEENCR,
          ST_DONE} state;

    t_start = MilliSecs();
    state = ST_START;
    i = 0;
    while(state != ST_DONE)
    {
        c = sgetc_timed(sd, t_start, timeout);
        if(c < 0)
        {
            log_error("tchain", "Read timeout\n");
            return 0;
        }

        switch(state)
        {
            case ST_START:
                if(isalnum(c))
                {
                    state = ST_READING;
                    buf[i++] = c;
                }
                break;
            case ST_READING:
                if(c == '\r')
                    state = ST_SEENCR;
                else
                {
                    if(i < n)
                        buf[i++] = c;
                }
                break;
            case ST_SEENCR:
                if(c == '\n')
                    state = ST_DONE;
                break;
            case ST_DONE:
                break;
        }

    }
    buf[i] = '\0';

    return 1;
}

/*
 * Read N bytes of data from the device.
 */
static size_t
read_buffer(int sd, char *buf, size_t n, long timeout, unsigned short *pcrc)
{
    int             c;
    size_t          i;
    unsigned long   t_start;

    i = 0;
    t_start = MilliSecs();
    while(i < n)
    {
        c = sgetc_timed(sd, t_start, timeout);
        if(c < 0)
        {
            log_error("tchain", "Read timeout\n");
            break;
        }
        buf[i++] = c;
        if(pcrc)
            *pcrc = update_crc(*pcrc, c);
    }

    return i;
}

/*
 * Transfer the contents of a logged data-set to the MLF2 CF card.
 */
static int
download_dataset(int sd, FILE *ofp, int dset, long nbytes)
{
    long            start, returned, from;
    int             param;
    unsigned short  crc, calc_crc;

    start = 0;
    while(nbytes > 0)
    {
        PURGE_INPUT(sd);
        snprintf(linebuf, sizeof(linebuf)-1, "read data %d %ld %ld\r",
                 dset, sizeof(databuf), start);
        sputz(sd, linebuf);
        if(read_response(sd, linebuf, sizeof(linebuf)-1, 3000L) == 0)
        {
            log_error("tchain", "Data transfer failed\n");
            return 0;
        }

        if(sscanf(linebuf, "data %d %ld %ld", &param, &returned, &from) != 3)
        {
            log_error("tchain", "Invalid response from logger (%s)\n", linebuf);
            return 0;
        }

        calc_crc = 0;
        if(read_buffer(sd, databuf, returned, 3000L, &calc_crc) != returned ||
           read_buffer(sd, (char*)&crc, 2L, 2000L, NULL) != 2L)
        {
            log_error("tchain", "Block read failed\n");
            return 0;
        }

        log_event("Tchain: read %ld bytes; offset=%ld; crc=%04x (%04x)\n",
                  returned, from, crc, calc_crc);
        fwrite(databuf, 1L, returned, ofp);
        nbytes -= returned;
        start += returned;
    }

    return 1;
}

/**
 * tchain_init - initialize the interface.
 */
int
tchain_init(int which)
{
    devstate_t  *dsp;

    dsp = &dstab[which];
    if(dsp->ref > 0)
        return 1;

    if((dsp->sd = serial_open(TCHAIN_DEVICE(which), 0, TCHAIN_BAUD)) < 0)
        return 0;

    dsp->t0 = MilliSecs();
    devs_active++;
    if(devs_active == 1)
        add_after_hook("TCHAIN-reopen", tchain_reopen);

    serial_inmode(dsp->sd, SERIAL_NONBLOCKING);
    dsp->ref = 1;

    return 1;
}

/**
 * tchain_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
tchain_shutdown(int which)
{
    if(dstab[which].ref == 0)
        return;
    dstab[which].ref = 0;
    serial_close(dstab[which].sd);
    devs_active--;
    if(devs_active == 0)
        remove_after_hook("TCHAIN-reopen");
}

/**
 * Check if device is ready.
 *
 * @param  which  device index, 0 or 1.
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
tchain_dev_ready(int which)
{
    return (MilliSecs() - dstab[which].t0) > WARMUP_MS;
}

/**
 * tchain_test - pass through mode.
 *
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 *
 */
void
tchain_test(int which)
{
    if(dstab[which].ref)
    {
        printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
        serial_passthru(dstab[which].sd, 0x03, 0x02);
    }
}

/**
 * tchain_cmd - send a command and read the response.
 *
 * Purge the input line, wakeup the device, send the command and read the
 * response.
 */
int
tchain_cmd(int which, const char *cmd, char *reply, size_t n, long timeout)
{
    devstate_t  *dsp = &dstab[which];

    if(dsp->ref == 0)
    {
        log_error("tchain", "Device %d not enabled\n", which);
        return 0;
    }

    PURGE_INPUT(dsp->sd);
    WAKEUP(dsp->sd);
    sputz(dsp->sd, cmd);
    if(read_response(dsp->sd, reply, n, timeout) == 0)
    {
        log_error("tchain", "Cannot read response\n");
        return 0;
    }

    if(reply[0] == 'E' && isdigit(reply[1]))
    {
        log_error("tchain", "Internal error: %s\n", reply);
        return 0;
    }

    return 1;
}

/**
 * tchain_fast_cmd - send a command and read the response.
 *
 * Similar to tchain_cmd but assumes the device is already awake.
 */
int
tchain_fast_cmd(int which, const char *cmd, char *reply, size_t n, long timeout)
{
    devstate_t  *dsp = &dstab[which];

    if(dsp->ref == 0)
    {
        log_error("tchain", "Device %d not enabled\n", which);
        return 0;
    }

    sputz(dsp->sd, cmd);
    if(read_response(dsp->sd, reply, n, timeout) == 0)
    {
        log_error("tchain", "Cannot read response\n");
        return 0;
    }

    if(reply[0] == 'E' && isdigit(reply[1]))
    {
        log_error("tchain", "Internal error: %s\n", reply);
        return 0;
    }

    return 1;
}

/**
 * tchain_sample - read a data sample.
 */
int
tchain_sample(int which, TchainData *td)
{
    char    *dptr, *savep, *token;
    int     i;

    memset((void*)td->data, 0, sizeof(td->data));

    if(tchain_cmd(which, "fetch\r", databuf, sizeof(databuf)-1, 5000L) == 0)
    {
        log_error("tchain", "Fetch command failed\n");
        return 0;
    }

    dptr = databuf;
    /* Skip the first field (date/time) */
    token = strtok_r(dptr, ",", &savep);
    if(token == NULL)
    {
        log_error("tchain", "Cannot parse data record\n");
        return 0;
    }

    /*
     * Parse the comma-separated temperature values
     */
    i = 0;
    while((token = strtok_r(NULL, ",", &savep)) != NULL)
    {
        if(sscanf(token, " %f", &(td->data[i])) == 1)
            i++;
    }

    return i;
}

int
tchain_sync_clock(int which)
{
    time_t      now;
    struct tm   *t;

    time(&now);
    t = localtime(&now);
    strftime(linebuf, sizeof(linebuf)-1, "now = %Y%m%d%H%M%S\r", t);
    return tchain_cmd(which, linebuf, databuf, sizeof(databuf)-1, 5000L);
}

int
tchain_download(int which, int dset, const char *filename)
{
    long        nbytes;
    int         r;
    devstate_t  *dsp = &dstab[which];
    FILE        *ofp;

    if(tchain_cmd(which, "meminfo used\r", databuf, sizeof(databuf)-1, 5000L) == 0)
    {
        log_error("tchain", "Meminfo command failed\n");
        return 0;
    }

    if(sscanf(databuf, "meminfo used = %ld", &nbytes) != 1)
    {
        log_error("tchain", "Invalid response from logger (%s)\n", databuf);
        return 0;
    }

    log_event("%ld bytes stored on Tchain logger %d\n", nbytes, which);
    if((ofp = fopen(filename, "w")) == NULL)
    {
        log_error("tchain", "Cannot open output file %s\n", filename);
        return 0;
    }

    r = download_dataset(dsp->sd, ofp, dset, nbytes);
    fclose(ofp);

    return r;
}

int
tchain_enable(int which, long interval_ms)
{
    time_t      now;
    struct tm   *t;

    time(&now);
    t = localtime(&now);
    strftime(linebuf, sizeof(linebuf)-1, "now = %Y%m%d%H%M%S\r", t);
    if(tchain_cmd(which, linebuf, databuf, sizeof(databuf)-1, 5000L) == 0)
    {
        log_error("tchain", "Cannot set clock\n");
        return 0;
    }

    strftime(linebuf, sizeof(linebuf)-1, "starttime = %Y%m%d%H%M%S\r", t);
    if(tchain_fast_cmd(which, linebuf, databuf, sizeof(databuf)-1, 5000L) == 0)
    {
        log_error("tchain", "Cannot set start time\n");
        return 0;
    }

    t->tm_year += 2;
    strftime(linebuf, sizeof(linebuf)-1, "endtime = %Y%m%d%H%M%S\r", t);
    if(tchain_fast_cmd(which, linebuf, databuf, sizeof(databuf)-1, 5000L) == 0)
    {
        log_error("tchain", "Cannot set end time\n");
        return 0;
    }

    snprintf(linebuf, sizeof(linebuf)-1, "sampling mode = continuous,period = %ld\r",
             interval_ms);
    if(tchain_fast_cmd(which, linebuf, databuf, sizeof(databuf)-1, 5000L) == 0)
    {
        log_error("tchain", "Cannot set sampling mode\n");
        return 0;
    }

    return tchain_fast_cmd(which, "enable erasememory = true\r", databuf,
                           sizeof(databuf)-1, 5000L);
}

int
tchain_disable(int which)
{
    return tchain_cmd(which, "stop\r", databuf, sizeof(databuf)-1, 5000L);
}
