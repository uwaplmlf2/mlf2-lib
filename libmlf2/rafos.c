/**@file
** Driver for RAFOS board.
**
** arch-tag: 3b39d2bf-10cb-452c-8306-88644920a6ad SERIAL
**
** 
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "lpsleep.h"
#include "log.h"
#include "ioports.h"
#include "rafos.h"

#ifndef __GNUC__
#define __inline__
#endif

#define WARM_UP_MSECS		500L
#define WAKEUP_PULSE_LEN	10L
#define WAKEUP_TIME		1500L

/** Maximum receive window (seconds) */
#define MAX_WINDOW		20150

#define WAKEUP_CLOCK()	do {\
	                       int __i;\
                               for(__i=0;__i < 3;__i++) {\
                                   iop_clear(IO_E, 0x10);\
                                   DelayMilliSecs(WAKEUP_PULSE_LEN);\
                                   iop_set(IO_E, 0x10);\
                                   DelayMilliSecs(WAKEUP_PULSE_LEN);\
                               }\
                               DelayMilliSecs(WAKEUP_TIME);\
                           } while(0)
#define WAKEUP_RECV()	do { iop_set(IO_E, 0x20);\
                             DelayMilliSecs(WAKEUP_TIME);\
                           } while(0)
#define SLEEP_RECV()    do { iop_clear(IO_E, 0x20); } while(0)

                              
enum { T_START=0,
       T_SAMPLE,
       NR_TIMES };
static long	timestamps[NR_TIMES];

static int 		ref = 0;
static int 		serial_dev;
static unsigned long	nr_reads, nr_errors;
static long		rafos_sample_time;
static unsigned char	parity[128];

/*
 * Quick hack to count the number of 1 bits in an integer. I found this
 * on "HACKMEM" many years ago.
 */
static int nbits_set(unsigned long n)
{
    unsigned long  rval;

    rval = n - ((n >> 1) & 033333333333) - ((n >> 2) & 011111111111);
    return(((rval + (rval >> 3)) & 030707070707) % 63);
}

static __inline__ int HEX(int c)
{
    if(isdigit(c))
	return c - '0';
    else
	return tolower(c) - 'a' + 10;
}

static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char	c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
	if((MilliSecs() - start) >= timeout)
	    return -1;
    }
    return ((int) c)&0x7f;
}

static __inline__ void sputc(int desc, int out)
{
    char	c = out | parity[out];
    
    serial_write(desc, &c, 1);
}

static void sputs(int desc, char *str, long delay)
{
    while(*str)
    {
	sputc(desc, *str);
	str++;
	DelayMilliSecs(delay);
    }
}


/**
 * Initialize the device interface.
 * 
 * @return 1 if successful, 0 on error.
 */
int
rafos_init(void)
{
    int		i, nset;
    
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(RAFOS_DEVICE, 0, RAFOS_BAUD)) < 0)
    {
	log_error("rafos", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    DelayMilliSecs(WARM_UP_MSECS);

    /* Generate the parity bit table, the device uses even parity */
    for(i = 0;i < 128;i++)
    {
	nset = nbits_set((unsigned long)i);
	parity[i] = (nset & 1) ? 0x80 : 0;
    }

    iop_set(IO_E, 0x10);
    
    
    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to rafos_sample_start and rafos_read_data.
    */
    add_after_hook("RAFOS-reopen", rafos_reopen);
    
    memset(timestamps, 0, sizeof(timestamps));
    timestamps[T_START] = RtcToCtm();
initdone:
    ref++;

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
    
    return 1;
}

void
rafos_reopen(void)
{
    iop_set(IO_E, 0x10);
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
}


/**
 * Shutdown the device interface.
 */
void
rafos_shutdown(void)
{
    if(ref == 0)
	return;
    ref = 0;
    iop_clear(IO_E, 0x10);
    remove_after_hook("RAFOS-reopen");
    serial_close(serial_dev);
}

static int
find_prompt(void)
{
    int		c;
    ulong	t0;
    enum {START=0, GOT_COLON, DONE} state;

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    state = START;
    t0 = MilliSecs();
    while(state != DONE)
    {
	c = sgetc_timed(serial_dev, t0, 3000L);
	if(c == -1)
	{
	    serial_inflush(serial_dev);
	    return 0;
	}

	if(c == 0x03)
	    state = DONE;
#if 0	
	else
	{
	    if(isprint(c))
		printf("got '%c'\n", (char)c);
	    else
		printf("got 0x%02x\n", c);
	}
	
	

	switch(state)
	{
	    case START:
		if(c == ':')
		    state = GOT_COLON;
		break;
	    case GOT_COLON:
		if(c == ' ' || c == 0x03)
		    state = DONE;
		break;
	    case DONE:
		break;
	}
#endif
    }

    serial_inflush(serial_dev);
    
    return 1;
}

/*
 * Address a sub-device
 *
 * @param  dev device code
 * @return 1 if successful, otherwise 0.
 */
static int
rafos_address(int dev)
{
    char	*send;

    serial_inflush(serial_dev);
    
    if(dev == RAFOS_CLOCK_DEV)
	send = "#CK00?ID\r";
    else
	send = "#R01 ";

    sputs(serial_dev, send, 1L);

    return find_prompt();
}


/**
 * Check if device is ready.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
rafos_dev_ready(void)
{
    return ref;
}

/**
 * Set RAFOS clock to tt8 time.
 * 
 * @return 1 if sucessful, 0 on error.
 */
int
rafos_set_clock(void)
{
    time_t	t;
    struct tm	*tm;
    char	sync = '@';
    char	tstr[24];

    WAKEUP_CLOCK();
    
    if(!rafos_address(RAFOS_CLOCK_DEV))
    {
	log_error("rafos", "Cannot address clock\n");
	return 0;
    }
    

    /* Unlock the set-time command */
    sputs(serial_dev, "!U\r", 5L);
    if(!find_prompt())
    {
	log_error("rafos", "Cannot send clock unlock command\n");
	return 0;
    }
    

    /* Set timestamp for 3 seconds in the future and send it */
    t = RtcToCtm() + 3L;
    tm = localtime(&t);
    sprintf(tstr, "!T0000%02d%02d%02d", tm->tm_hour, tm->tm_min,
	    tm->tm_sec);
    log_event("Setting rafos clock to %s\n", tstr);
    
    sputs(serial_dev, tstr, 50L);

    /* Wait for time to arrive and send SYNC character */
    while(RtcToCtm() < t)
	;
    sputc(serial_dev, sync);
    serial_inflush(serial_dev);
    
    /* Clock automatically de-addresses and goes to sleep */

    return 1;
}

/**
 * Sync system clock with RAFOS.
 * Set the TT8 clock from the RAFOS clock. Note that the RAFOS clock 
 * does not maintain the date so we can only update the time of day.
 * This function should not be called near a day boundary.
 *
 * @return second-of-the-day if successful, 0 if an error occurs.
 */
long
rafos_set_tt8_clock(void)
{
    time_t	t0;
    int		c, n;
    char	*tp;
    struct tm	*tm;
    long	t_pseudo;
    char	tstr[16];

    WAKEUP_CLOCK();
    
    if(!rafos_address(RAFOS_CLOCK_DEV))
    {
	log_error("rafos", "Cannot address clock\n");
	return 0;
    }

    tp = tstr;
    n = sizeof(tstr) - 1;
    sputs(serial_dev, "?T\r", 5L);

    /*
    ** Wait for the SYNC character and save the timestamp in a
    ** buffer for later parsing.
    */
    t0 = MilliSecs();
    while((c = sgetc_timed(serial_dev, t0, 4000L)) != '@')
    {
	if(isdigit(c))
	{
	    n--;
	    if(n >= 0)
		*tp++ = c;
	}
	else if(c == -1)
	    return 0;
    }

    /* 
    ** Save the sync time. We will use this time to determine the current date
    ** because the RAFOS clock only counts days. Note that even if the TT8
    ** clock has not drifted, there is a chance that the time can change from
    ** 23:59:59 to 00:00:00 between the time the RAFOS clock sends the SYNC
    ** character and the time we read the TT8 clock.  For that reason, this
    ** function should not be called near a day boundary.
    */
    t0 = RtcToCtm();
    
    /* tstr == DDDDHHMMSS */
    *tp = '\0';

    /* We don't care about the day, convert time to an integer */
    t_pseudo = strtol(&tstr[4], NULL, 10);

    if(strlen(tstr) < 4 || t_pseudo <= 0)
	return 0;
    
    tm = localtime(&t0);
    tm->tm_hour = t_pseudo/10000;
    if(tm->tm_hour >= 24)
	return 0;
    
    t_pseudo %= 10000;
    tm->tm_min = t_pseudo/100;
    if(tm->tm_min >= 60)
	return 0;
    
    t_pseudo %= 100;

    /* Add delta T to seconds, mktime will normalize */
    tm->tm_sec = t_pseudo;
    SetTimeSecs(mktime(tm), NULL);
    log_event("Clock adjustment\n");
    
    serial_inflush(serial_dev);
    
    /* Put clock module back to sleep */
    sputs(serial_dev, "!D\r", 5L);

    t0 = RtcToCtm();
    tm = localtime(&t0);
    return ((tm->tm_hour*60L) + tm->tm_min)*60L + tm->tm_sec;
}

/**
 * Get the second-of-the-day from the RAFOS clock.
 *
 * @return second of the day (0 - 86399).
 */
long
rafos_sod(void)
{
    int		c, n;
    char	*tp;
    long	t_pseudo, hour, min, sod;
    ulong	t0;
    char	tstr[16];

    WAKEUP_CLOCK();
    
    if(!rafos_address(RAFOS_CLOCK_DEV))
	return 0;

    tp = tstr;
    n = sizeof(tstr) - 1;
    sputs(serial_dev, "?T\r", 100L);

    /*
    ** Wait for the SYNC character and save the timestamp in a
    ** buffer for later parsing.
    */
    t0 = MilliSecs();
    while((c = sgetc_timed(serial_dev, t0, 4000L)) != '@')
    {
	if(isdigit(c))
	{
	    n--;
	    if(n >= 0)
		*tp++ = c;
	}
	else if(c == -1)
	    return 0;
    }

    /* tstr == DDDDHHMMSS */
    *tp = '\0';

    /* We don't care about the day, convert time to an integer */
    t_pseudo = strtol(&tstr[4], NULL, 10);

    if(strlen(tstr) < 4 || t_pseudo <= 0)
	return 0;
        
    hour = t_pseudo/10000;
    t_pseudo %= 10000;
    min = t_pseudo/100;
    t_pseudo %= 100;

    sod = (hour*60 + min)*60 + t_pseudo;

    serial_inflush(serial_dev);
    
    /* Put clock module back to sleep */
    sputs(serial_dev, "!D\r", 0L);
 
    return sod;
}


/**
 * Fetch read error statistics.
 *
 * @param  errs  returned error count
 * @param  reads  returned read count.
 */
void
rafos_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}

/**
 * Start the sample process.
 *
 * @param  duration  sample time in seconds
 * @return the sample count or 0 if an error occurs.
 */
unsigned
rafos_start_sample(unsigned duration)
{
    unsigned	count;
    char	cmdbuf[16];
    
    timestamps[T_SAMPLE] = 0L;
    
    if(duration > MAX_WINDOW)
	duration = MAX_WINDOW;
    
    WAKEUP_RECV();
    if(!rafos_address(RAFOS_RECV_DEV))
    {
	log_error("rafos", "Cannot address receiver\n");
	SLEEP_RECV();
	return 0;
    }

    /* Analog power on */
    sputs(serial_dev, "!A\r", 100L);
    if(!find_prompt())
    {
	log_error("rafos", "Cannot turn on analog power\n");
	SLEEP_RECV();
	return 0;
    }
    
    count = (unsigned)((double)duration/.3075);
    
    sprintf(cmdbuf, "?M%04X E\r", count);
    sputs(serial_dev, cmdbuf, 100L);
    if(!find_prompt())
    {
	log_error("rafos", "Cannot start sampling process\n");
	SLEEP_RECV();
	return 0;
    }
    
    timestamps[T_SAMPLE] = RtcToCtm();
    rafos_sample_time = (long)duration;
    
    return count;
}


/**
 * Test whether sampling process is done.
 *
 * @return true if sampling process is done.
 */
int
rafos_data_ready(void)
{
    if(timestamps[T_SAMPLE] == 0)
	return 1;
    return (RtcToCtm() - timestamps[T_SAMPLE]) >= rafos_sample_time;
}

/**
 * Read a data value.
 *
 * @param  rdp  pointer to returned data record.
 * @return 1 if successful, 0 on error.
 */
int
rafos_read_data(RafosData *rdp)
{
    unsigned short	x;
    int			c, i;
    unsigned short	vals[6];
    ulong		t0;
    enum {START=0, RDNUM, READ, DONE} state;

    nr_reads++;
    if(!rafos_address(RAFOS_RECV_DEV))
    {
	log_error("rafos", "Cannot address receiver\n");
	nr_errors++;
	return 0;
    }

    /* retrieve the correlation values */
    sputs(serial_dev, "?C\r", 100L);

    /*
     * The response from the receiver is in the following format:
     *
     * CC IIII CC IIII CC IIII
     *
     * Where CC is a correlation value and IIII is the corresponding
     * index within the data window. All values are base-16. Note that
     * the parsing algorithm first looks for a carriage-return so as
     * to avoid the echo of the command string.
     */
    x = 0;
    i = 0;
    state = START;
    t0 = MilliSecs();
    while(state != DONE)
    {
	c = sgetc_timed(serial_dev, t0, 5000L);
	if(c == -1)
	    return 0;
	switch(state)
	{
	    case START:
		if(c == '\r')
		    state = READ;
		break;
	    case READ:
		if(isxdigit(c))
		{
		    state = RDNUM;
		    x = HEX(c);
		}
		break;
	    case RDNUM:
		if(isspace(c))
		{
		    vals[i++] = x;
		    if(i == 6)
			state = DONE;
		    else
			state = READ;
		}
		else if(isxdigit(c))
		    x = x*16 + HEX(c);
		break;
	    case DONE:
		break;
	}
    }

    for(i = 0;i < 3;i++)
    {
	rdp->corr[i] = vals[i*2];
	rdp->index[i] = vals[i*2+1];
    }
    rdp->t = timestamps[T_SAMPLE];
    
    /* turn off analog power */
    find_prompt();
    sputs(serial_dev, "!N\r", 100L);

    /* put device to sleep */
    find_prompt();
    sputs(serial_dev, "!S\r", 50L);
    SLEEP_RECV();
    
    return 1;
}


/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
rafos_test(void)
{
    int		c, burst;
    
    printf("Pass-through mode: CTRL-c to exit\n");
    while(1)
    {
	burst = 20;
	while(SerByteAvail() && burst--)
	{
	    c = SerGetByte();
	    if(c == 0x03)
		return;
	    else
		sputc(serial_dev, c);
	}
	
	burst = 20;
	while((c = sgetc_timed(serial_dev, MilliSecs(), 100)) != -1 && burst--)
	    SerPutByte(c);
    }
}
