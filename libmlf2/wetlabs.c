/**@file
** Generic interface to Wetlabs Fluorometer. Used as a base module
** for sensor specific drivers.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <setjmp.h>
#include <ctype.h>
#include "serial.h"
#include "log.h"
#include "wetlabs.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define PURGE_INPUT(d) while(sgetc_timed(d, MilliSecs(), 100L) != -1)

#define START_CHAR  '\n'
#define END_CHAR    '\r'


/**
 * Initialize the device interface.
 *
 * @param devnum  serial device number.
 * @param baud    serial baud rate.
 * @return serial device ID or -1 on error
 */
int
wetlabs_init(int devnum, long baud)
{
    int         sdev;

    if((sdev = serial_open(devnum, 0, baud)) < 0)
    {
        log_error("wetlabs", "Cannot open serial device (code = %d)\n",
                  sdev);
        return -1;
    }

    serial_inmode(sdev, SERIAL_NONBLOCKING);
    return sdev;
}

/**
 * Shutdown the fluorometer interface and power off the device.
 *
 * @param sdev  serial device number.
 */
void
wetlabs_shutdown(int sdev)
{
    serial_close(sdev);
}



/**
 * Close the anti-fouling shutter.
 *
 * The shutter will reopen automatically when the device is powered back
 * on.
 *
 * @param sdev  serial device number.
 * @returns 1
 */
int
wetlabs_close_shutter(int sdev)
{
    int     i;

    log_event("Closing WETLABS shutter\n");

    /*
    ** The shutter is closed when a "stop" command is received. The
    ** stop command is a sequence of five '!' characters.  The device
    ** seems to have some buffering issues so we send a sequence of
    ** ten '!' and add a small pause in-between.
    */
    for(i = 0;i < 10;i++)
    {
        sputc(sdev, '!');
        DelayMilliSecs(150L);
    }
    serial_inflush(sdev);

    return 1;
}

/**
 * Pass through mode.
 *
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 *
 * @param sdev  serial device number.
 */
void
wetlabs_test(int sdev)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(sdev, 0x03, 0x02);
}

int
wetlabs_dev_ready(int sdev, long timeout)
{
    int c;

    c = sgetc_timed(sdev, MilliSecs(), timeout);
    return c == START_CHAR;
}

/**
 * Reads the next data record from the device.
 *
 * @param  sdev  serial device ID
 * @param  ncols  number of data columns per record
 * @param  vals  array of output data values
 * @param  timeout  read timeout in milliseconds
 * @return number of columns read
 */
int
wetlabs_read_data(int sdev, int ncols, short *vals, long timeout)
{
    int     c, i;
    ulong   t;
    short   num;
    enum {ST_1=1,
          ST_2,
          ST_3,
          ST_DONE} state;

    /*
    ** State machine to try and locate the start of the data. We wait
    ** for a linefeed (START_CHAR) to indicate the start of a line and
    ** then for two TABs to skip the first two fields (date and time).
    */
    t = MilliSecs();
    state = ST_1;
    while(state != ST_DONE)
    {
        c = sgetc_timed(sdev, t, timeout);

        if(c < 0)
        {
            log_error("wetlabs", "Timeout waiting for record start\n");
            return 0;
        }

        switch(state)
        {
            case ST_1:
                if(c == START_CHAR)
                    state = ST_2;
                break;
            case ST_2:
                if(c == '\t')
                    state = ST_3;
                break;
            case ST_3:
                if(c == START_CHAR)
                    state = ST_2;
                else if(c == '\t')
                    state = ST_DONE;
                break;
            case ST_DONE:
                break;
        }
    }


    num = 0;
    i = 0;
    while(i < ncols &&
          (c = sgetc_timed(sdev, t, timeout)) != END_CHAR)
    {
        if(c < 0)
        {
            log_error("wetlabs", "Error reading sample (field = %d)\n", i);
            return 0;
        }

        if(isdigit(c))
            num = num*10 + (c - '0');
        else if(c == '\t')
        {
            vals[i] = num;
            i++;
            num = 0;
        }
    }

    if(i < ncols)
    {
        vals[i] = num;
        i++;
    }

    return i;
}
