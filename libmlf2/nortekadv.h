/*
** arch-tag: 6407c799-d5c6-4839-8af4-1f15c3080df1
** Time-stamp: <2006-03-20 14:18:56 mike>
*/
#ifndef _NORTEKADV_H_
#define _NORTEKADV_H_

#define NOR_DEVICE	6
#define NOR_BAUD	19200L
#define NOR_TIMEOUT	2L

int nor_init(void);
void nor_shutdown(void);
int nor_dev_ready(void);
void nor_test(void);
void nor_get_stats(unsigned long *errs, unsigned long *reads);
int nor_set_clock(void);
int nor_start(void);
int nor_read_clock(struct tm *t);
int nor_stop(void);
int nor_powerdown(void);
void nor_serial_off(void);
void nor_serial_on(void);

#endif
