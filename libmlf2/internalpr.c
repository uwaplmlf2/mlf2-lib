/**@file
** Driver for internal pressure sensor.
**
** $Id: internalpr.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
*/
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <time.h>
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include "power.h"
#include "atod.h"
#include "adc.h"
#include "internalpr.h"

#define WARM_UP_MSECS	200L

/** Conversion factor, psi/mV */
#define IPR_SCALE	0.004878

/** Offset value, psi */
#define IPR_OFFSET	0.

static int ref = 0;
static long init_time;

/**
 * Initialize the internal pressure sensor interface.
 *
 * Power-on and initialize the pressure sensor interface.  Returns 1 if 
 * successful, otherwise 0.
 */
int
ipr_init(void)
{
    /* Are we already initialized? */
    if(ref > 0)
	goto initdone;

    atod_init();

    init_time = MilliSecs();
    
initdone:
    ref++;
    
    return 1;
}

/**
 * Power down the internal pressure sensor interface.
 */
void
ipr_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    atod_shutdown();
}

/**
 * Check if device is ready.
 *
 * Returns true if device is ready for sampling.
 */
int
ipr_dev_ready(void)
{
    return ((MilliSecs() - init_time) >= WARM_UP_MSECS);
}

/**
 * Read a sample from the sensor.
 *
 * @return next sample in millivolts (0 - 4095).
 */
short
ipr_read_data(void)
{
    return atod_read(IPR_CHANNEL);
}

/**
 * Convert millivolts to psi.
 *
 * @param  mv sensor reading in millivolts.
 *
 * @return psi
 */
double
ipr_mv_to_psi(int mv)
{
    return (double)mv*IPR_SCALE - IPR_OFFSET;
}
