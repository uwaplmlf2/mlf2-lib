/*
** 
*/
#ifndef _QCP_H_
#define _QCP_H_

#include "serialpar.h"

#define QCP_DEVICE         5
#define QCP_BAUD           9600

int qcp_init(void);
void qcp_shutdown(void);
int qcp_dev_ready(void);
int qcp_read_data(ParData *pd, long timeout);
void qcp_test(void);

#endif