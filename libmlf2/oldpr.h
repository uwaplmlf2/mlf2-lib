/*
**
*/
#ifndef _OLDPR_H_
#define _OLDPR_H_

#define OLDPR_GAIN      64
#define OLDPR_VREF      2.5

int oldpr_init(long cmd_interval);
void oldpr_spi_init(void);
void oldpr_shutdown(void);
int oldpr_read_data(int n, long data[]);
int oldpr_get_count(void);
int oldpr_adreset(void);
int oldpr_overruns(void);
int oldpr_clear(void);
double oldpr_mv_to_psi(double mv);
double oldpr_counts_to_mv(long counts);

#endif /* _OLDPR_H_ */
