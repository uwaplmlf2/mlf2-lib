/**@file
 ** $Id: drogue.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
 */
#ifndef _DROGUE_H_
#define _DROGUE_H_

/** Drogue status constants */
typedef enum {DROGUE_OPENING=0, /**< drogue is opening */
              DROGUE_OPENED=1,  /**< drogue is opened */
              DROGUE_CLOSING=2, /**< drogue is closing */
              DROGUE_CLOSED=3,  /**< drogue is closed */
              DROGUE_UNKNOWN} drstate_t;

int drogue_start_open(long check_time, int autostop);
int drogue_start_close(long check_time, int autostop);
drstate_t drogue_check(void);

/**
 * Check if drogue is open.
 *
 * @return true if drogue is opened or in the process of opening.
 */
static __inline__ int drogue_isopen(void)
{
    drstate_t   ds = drogue_check();
    return (ds == DROGUE_OPENED || ds == DROGUE_OPENING);
}

/**
 * Wait for latest operation to complete.
 * Wait for the most recent open/close operation to complete.
 */
static __inline__ void drogue_wait(void)
{
    switch(drogue_check())
    {
        case DROGUE_OPENING:
            while(drogue_check() != DROGUE_OPENED)
                ;
            break;
        case DROGUE_CLOSING:
            while(drogue_check() != DROGUE_CLOSED)
                ;
            break;
        default:
            break;
    }
}

#endif /* _DROGUE_H_ */
