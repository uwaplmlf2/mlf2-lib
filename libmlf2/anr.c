/**@file
 *
 * $Id: anr.c,v b6d86840376a 2009/01/14 00:08:08 mikek $
 *
 * Driver for the Acoustic Noise Recorder.
 *
 */
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "ioports.h"
#include "serial.h"
#include "log.h"
#include "anr.h"
#include "lpsleep.h"

#include "iofuncs.h"

/** Purge all characters from the serial input */
#define PURGE_INPUT(d, t) while(sgetc_timed(d, MilliSecs(), t) != -1)

#define WARM_UP_MSECS	20000L

#define ACK		"$OK\r"
#define SYNC		"x\r"
#define ERR		"$ERR\r"

static int serial_dev = -1;
static long init_time;

/**
 * Synchronize the ANR clock to the TT8 clock.
 * @return 1 if successful, 0 on communication error.
 */
static int
_clock_sync(void)
{
    long	t;
    char	cmd[24];
    
    log_event("Synchronizing ANR clock\n");
    t = RtcToCtm() + 4L;
    sprintf(cmd, "$time=%ld\r", t);
    
    if(serial_chat(serial_dev, cmd, ACK, 3L) == 1)
    {
	/* Wait for the sync time */
	while(RtcToCtm() < t)
	    ;
	return serial_chat(serial_dev, SYNC, ACK, 3L);
    }
    
    log_error("anr", "Clock sync failed\n");
    
    return 0;
}

static void
anr_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
}

/**
 * Open connection to the device and initialize. Errors are logged.
 *
 *
 * @return 1 if successful, otherwise 0.
 */
int
anr_init(void)
{
    if(serial_dev >= 0)
	goto already_open;
    
    if((serial_dev = serial_open(ANR_DEVICE, 0, ANR_BAUD)) < 0)
    {
	log_error("anr", "Cannot open serial device (code = %d)\n",
		  serial_dev);
	return 0;
    }
    
    init_time = MilliSecs();    
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    if(serial_chat(serial_dev, "", ACK, WARM_UP_MSECS/1000L) != 1)
    {
	log_error("anr", "No response from device\n");
	anr_shutdown();
	return 0;
    }

    PURGE_INPUT(serial_dev, 1000L);
    
    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to anr_start and anr_stop.
    */
    add_after_hook("ANR-reopen", anr_reopen);
    
already_open:
    return 1;
}

/**
 * Shutdown the device interface and power off the device.
 */
void
anr_shutdown(void)
{
    if(serial_dev >= 0)
    {
	remove_after_hook("ANR-reopen");
	serial_close(serial_dev);
	serial_dev = -1;
    }
    
}

/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
anr_dev_ready(void)
{
    return (serial_dev >= 0);
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
anr_test(void)
{
    if(serial_dev < 0)
    {
	log_error("anr", "Device is not powered-on!\n");
	return;
    }

    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

/**
 * Start the sampling process. The ANR clock is synchronized before the
 * sampling is started.
 *
 * @param  interval  sampling interval in milliseconds.
 * @param  no_gtd  if non-zero, ANR will not record while GTD pump is active.
 * @return 1 if sucessful, 0 on error.
 */
int
anr_start(long interval, int no_gtd)
{
    char	cmd[24];
    
    PURGE_INPUT(serial_dev, 200L);

    _clock_sync();

    PURGE_INPUT(serial_dev, 200L);

    sprintf(cmd, "$sampint=%ld\r", interval);
    
    if(serial_chat(serial_dev, cmd, ACK, 3L) != 1)
    {
	log_error("anr", "$sampint command failed\n");
	return 0;
    }
    
    if(no_gtd && serial_chat(serial_dev, "$gtdnr\r", ACK, 3L) != 1)
    {
	log_error("anr", "$gtdnr command failed\n");
	/* Failure of this command is not fatal */
    }
	
    PURGE_INPUT(serial_dev, 200L);
    return serial_chat(serial_dev, "$start\r", ACK, 3L);
}

/**
 * Stop the sampling process.
 *
 * @return 1 if sucessful, 0 on error.
 */
int
anr_stop(void)
{
    return serial_chat(serial_dev, "$stop\r", ACK, 3L);
}

/**
 * Send a command to the device.
 *
 * @param  cmd  command string.
 * @return 1 if successful, 0 on error.
 */
int
anr_cmd(const char *cmd)
{
    int		n;
    char 	buf[24];

    if(serial_dev < 0)
    {
	log_error("anr", "Device is not powered-on!\n");
	return 0;
    }
    
    /*
    ** Do not try to purge the input before sending the $stop command
    ** because the device is transmitting a '.' on the serial line
    ** every time it takes a sample (i.e. PURGE_INPUT will never return).
    */
    if(strcmp(cmd, "$stop"))
	PURGE_INPUT(serial_dev, 100L);

    if(!strcmp(cmd, "$sync"))
	return _clock_sync();

    /* make sure the buffer is null-terminated */
    buf[22] = buf[23] = '\0';
    strncpy(buf, cmd, sizeof(buf)-2);
    
    /* terminate the string with a carriage-return */
    n = strlen(buf);
    buf[n] = '\r';
    buf[n+1] = '\0';
    
    return serial_chat(serial_dev, buf, ACK, 3L);
}
