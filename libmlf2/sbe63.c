#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "sbe.h"
#include "sbe63.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define MAX_DEVS    2

// Maintain the state of each device
typedef struct devstate {
    int         sd; /**< serial device descriptor */
    int         ref; /**< reference count */
} devstate_t;
static devstate_t dstab[MAX_DEVS];

typedef struct {
    float   p;
    float   v;
    float   o;
    float   t;
} Format1_t;

static CBstatus_t
parse_fmt1(void *obj, char *linebuf)
{
    Format1_t *s = (Format1_t*)obj;
    if(sscanf(linebuf, "%f, %f, %f, %f", &s->p, &s->v, &s->o, &s->t) != 4)
    {
        log_error("sbe63", "Error parsing data: (%s)\n", linebuf);
        return CB_ERROR;
    }

    return CB_OK;
}

/**
 * sbe63_init - initialize the interface.
 */
int
sbe63_init(int which)
{
    devstate_t  *dsp;

    dsp = &dstab[which];
    if(dsp->ref > 0)
        return 1;

    if((dsp->sd = sbe_init(SBE63_DEVICE(which), SBE63_BAUD)) < 0)
        return 0;
    dsp->ref = 1;
    DelayMilliSecs(2000L);
    sbe63_setup(which);

    return 1;
}

/**
 * sbe63_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
sbe63_shutdown(int which)
{
    if(dstab[which].ref == 0)
        return;
    dstab[which].ref = 0;
    sbe_shutdown(dstab[which].sd);
}

/**
 * sbe_dev_ready - check if the device is ready for commands.
 *
 * @return 1 (yes) or 0 (no)
 */
int
sbe63_dev_ready(int which)
{
    if(dstab[which].ref)
        return sbe_dev_ready(dstab[which].sd);
    else
        return 0;
}

/**
 * sbe63_test - pass through mode.
 *
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 *
 */
void
sbe63_test(int which)
{
    if(dstab[which].ref)
        sbe_test(dstab[which].sd);
}

/**
 * sbe63_setup - send initialization commands.
 *
 */
int
sbe63_setup(int which)
{
    devstate_t  *dsp = &dstab[which];

    if(sbe_command(dsp->sd, "setformat=1\r", 3000L, NULL, NULL) == 0)
    {
        log_error("sbe63", "No response to \"setformat\" command\n");
        return 0;
    }

    if(sbe_command(dsp->sd, "setavg=2\r", 3000L, NULL, NULL) == 0)
    {
        log_error("sbe63", "No response to \"setavg\" command\n");
        return 0;
    }

    return 1;
}

/**
 * sbe63_read_data - read a single data sample.
 *
 */
int
sbe63_read_data(int which, SBE63Data *sd, long timeout)
{
    int         rval;
    Format1_t   sample;
    devstate_t  *dsp = &dstab[which];

    memset(&sample, 0, sizeof(sample));
    rval = sbe_command(dsp->sd, "ts\r", 5000L, parse_fmt1, &sample);
    if(rval)
    {
        sd->phase = sample.p;
        sd->voltage = sample.v;
        sd->temp = sample.t;
        sd->oxy = sample.o;
    }

    return rval;
}
