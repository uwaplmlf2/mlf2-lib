/**@file
** PIC-based pressure-sensor. The SPI interface is now handled in the picadc module.
**
**
*/
#include <stdio.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8lib.h>
#include "nvram.h"
#include "ioports.h"
#include "newpr.h"
#include "picadc.h"

/*
** This first set of constants is for the 500psi sensors, the
** second is for the 300psi.
*/
#ifdef USE_500
#define PSI_PER_MV  10.0695
#define PSI_OFFSET  -15.904
#else
#define PSI_PER_MV  5.787
#define PSI_OFFSET  -15.0018
#endif

static double psi_per_mv[] = { PSI_PER_MV, PSI_PER_MV, PSI_PER_MV };
static double psi_offset[] = { PSI_OFFSET, PSI_OFFSET, PSI_OFFSET };

static spi_clocks_t clocks = SPI_SETTINGS;

#define MIN(a, b)   ((a) < (b) ? (a) : (b))

/**
 * Initialize the interface to the sensor.
 *
 * @return 1
 */
int
newpr_init(void)
{
    int         index, nchans;
    nv_value    nv;
    char        name[20];

    nchans = sizeof(psi_per_mv)/sizeof(double);

    picadc_init(&clocks);

    /*
    ** Lookup the calibration constants in NVRAM.
    */
    for(index = 0;index < nchans;index++)
    {
        sprintf(name, "PIC%1d:psi/mv", index);
        if(nv_lookup(name, &nv) != -1)
            psi_per_mv[index] = nv.d;
        sprintf(name, "PIC%1d:psi-offset", index);
        if(nv_lookup(name, &nv) != -1)
            psi_offset[index] = nv.d;
    }

    return 1;
}

/**
 * Reinitialize the SPI bus.
 */
void
newpr_spi_init(void)
{
    picadc_spi_init(&clocks);
}

/**
 * Shutdown the device.
 *
 */
void
newpr_shutdown(void)
{
    picadc_shutdown();
}

/**
 * Get the buffer sample count.
 *
 * @return the number of samples queued on the PIC.
 */
int
newpr_get_count(void)
{
    return picadc_get_count();
}

/**
 * Reset the A/D controller.
 *
 * @return 1
 */
int
newpr_adreset(void)
{
    return picadc_adreset();
}


/**
 * Clear the sample buffer.
 *
 * @return 1
 */
int
newpr_clear(void)
{
    return picadc_clear();
}

/**
 * Read the buffered data samples.
 * Read the most recent n samples of data from the PIC pressure sensor.
 * The sample values are in counts, use newpr_counts_to_mv() to convert
 * to millivolts.  The return value is the actual number of samples read
 * which will be <= n.
 *
 * @param  which  channel, PR_TOP, PR_HULL, or PR_BOTH
 * @param  n  number of samples to read.
 * @param  data  array of returned samples in channel order.
 * @return sample count.
 */
int
newpr_read_data(pr_channel_t which, int n, long data[])
{
    int         i, j, chan, n_read;
    unsigned        mask;
    static adc_sample_t buf[PICADC_MAX_SAMPLES];

    n_read = picadc_read_data(n, buf);

    for(i = 0,j = 0;i < n_read;i++)
    {
        for(chan = 0,mask = 1;chan < PICADC_CHANNELS;chan++,mask<<=1)
        {
            if(which & mask)
                data[j++] = buf[i].channel[chan];
        }
    }

    return n_read;
}

/**
 * Convert counts to millivolts.
 *
 * @param  counts  A/D input value.
 * @return millivolts.
 */
double
newpr_counts_to_mv(long counts)
{
    double      volts_per_count;

    volts_per_count = NEWPR_VREF/((double)0x1000000 * (double)NEWPR_GAIN);
    return (volts_per_count * counts * 1000.);
}

/**
 * Convert pressure-sensor millivolts to psi
 * Converts millivolts to psi using the calibration constants.  The constants are
 * looked-up in NVRAM by newpr_init(), if they are not found, compiled in
 * values are used.
 *
 * @param  which  channel, PR_TOP or PR_HULL.
 * @param  mv  pressure sensor sample in millivolts.
 * @return psi
 */
double
newpr_mv_to_psi(pr_channel_t which, double mv)
{
    int     index;

    if(which & 0x01)
        index = 0;
    else if(which & 0x02)
        index = 1;
    else if(which & 0x04)
        index = 2;
    else
        return 0.;

    return mv*psi_per_mv[index] + psi_offset[index];
}
