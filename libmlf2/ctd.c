/**@file
** Seabird ctd module
**
** @deprecated
** Interface to the Seabird SBE-41 CTD module.  The float may have two CTD
** sensors but they are combined into a single device because the sampling
** spec calls for them to be sampled as close together as possible.
**
** @sa newctd.c
** 
*/
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <setjmp.h>
#include "salarm.h"
#include "serial.h"
#include "iostream.h"
#include "log.h"
#include "ctd.h"

static int		devmask, active;


/**
 * Initialize the CTD interface.
 * Open connection to the CTD interface and initialize.  The interface may 
 * have up to two CTDs attached, CTD_A and CTD_B.  The parameter which is
 * used to select the devices and may be set to one of the following.
 * - CTD_A (use the first CTD only)
 * - CTD_B (use the second CTD only)
 * - CTD_A | CTD_B (use both CTDs)
 *
 * @param  which  device selector.
 * @return number of initialized devices (1 or 2), 0 on error.
 */
int
ctd_init(int which)
{
    int		r = 0;
    
    if((which & CTD_A) && newctd_init(0))
    {
	devmask = CTD_A;
	r++;
    }

    if((which & CTD_B) && newctd_init(1))
    {
	devmask |= CTD_B;
	r++;
    }
    
    return r;
}

/**
 * Shutdown the CTD interface.
 * Shutdown the CTD device interface.  The devices are powered off.
 */
void
ctd_shutdown(void)
{
    if(devmask & CTD_A)
	newctd_shutdown(0);
    if(devmask & CTD_B)
	newctd_shutdown(1);
    devmask = 0;
}

/**
 * Check if device is ready.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
ctd_dev_ready(void)
{
    return 1;
}

/**
 * Fetch read error statistics.
 *
 * @param  errs  returned error count
 * @param  reads  returned read count.
 */
void
ctd_get_stats(unsigned long *errs, unsigned long *reads)
{
    if(devmask & CTD_A)
	newctd_get_stats(0, errs, reads);
    else if(devmask & CTD_B)
	newctd_get_stats(1, errs, reads);
    else
    {
	*errs = 0;
	*reads = 0;
    }
}

/**
 * Start the sampling process.
 *
 * @param  p  array of two pressure values in decibars
 * @return 1 if successful or 0 if an error occurs.
 */
int
ctd_start_sample(double p[])
{
    int	r = 0;

    if((devmask & CTD_A) && newctd_start_sample(0, p[0]))
    {
	active |= CTD_A;
	r++;
    }
    
    if((devmask & CTD_B) && newctd_start_sample(1, p[1]))
    {
	active |= CTD_B;
	r++;
    }
    
    return r;
}

/**
 * Test whether sampling process is done.
 *
 * @return a count of the number of samples ready.
 */
int
ctd_data_ready(void)
{
    int		r = 0;
    
    if(active & CTD_A)
	r += newctd_data_ready(0);
    if(active & CTD_B)
	r += newctd_data_ready(1);

    return r;
}

/**
 * Read the most recent data sample.
 *
 * @param  ctd  pointer to output data structure.
 * @return 1 if successful or 0 if an error occurs.
 */
int
ctd_read_data(CTDdata *ctd)
{
    int		r = 0;
    NewCTDdata	nctd;
    
    ctd->t[0] = ctd->t[1] = 0;
    ctd->s[0] = ctd->s[1] = 0;
    ctd->which = 0;
    ctd->delta_t = 0;
    
    if((active & CTD_A) && newctd_read_data(0, &nctd))
    {
	r++;
	ctd->t[0] = nctd.t;
	ctd->s[0] = nctd.s;
	ctd->which |= CTD_A;
	active &= ~CTD_A;
    }

    if((active & CTD_B) && newctd_read_data(1, &nctd))
    {
	r++;
	ctd->t[1] = nctd.t;
	ctd->s[1] = nctd.s;
	ctd->which |= CTD_B;
	active &= ~CTD_B;
    }
    
	
    return r;
}

/**
 * Synchronous data sampling.
 * Start the sampling process, wait until it is complete
 * and return the data record.
 *
 * @param  p  array of two pressure values in decibars
 * @param  ctd  pointer to output data structure.
 * @return 1 if successful or 0 if an error occurs.
 */
int
ctd_read_data_sync(double p[], CTDdata *ctd)
{
    int		n, r;
    
    if((n = ctd_start_sample(p)) == 0)
	return 0;
    while(ctd_data_ready() != n)
	;
    r = ctd_read_data(ctd);
    return r;
}


/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 *
 * @param  which  device selector, CTD_A or CTD_B.
 */
void
ctd_test(int which)
{
    if(which & CTD_A)
	newctd_test(0);
    else if(which & CTD_B)
	newctd_test(1);
}
