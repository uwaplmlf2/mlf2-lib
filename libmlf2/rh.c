/**@file
** Relative humidity sensor driver.
**
** $Id: rh.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
**
**
*/
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <time.h>
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include "power.h"
#include "atod.h"
#include "adc.h"
#include "rh.h"

#define WARM_UP_MSECS	200L
#define RH_SCALE	0.039376
#define RH_OFFSET	25.806

static int ref = 0;
static long init_time;

/**
 * Initialize the sensor interface.
 * Power-on and initialize the humidity sensor interface.  
 *
 * @return 1 if successful, otherwise 0.
 */
int
rh_init(void)
{
    /* Are we already initialized? */
    if(ref > 0)
	goto initdone;

    atod_init();

    init_time = MilliSecs();
    
initdone:
    ref++;
    
    return 1;
}

/**
 * Power down the sensor interface.
 */
void
rh_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    atod_shutdown();
}

/**
 * Check if device is ready.
 *
 * @return 1 if device is ready for sampling.
 */
int
rh_dev_ready(void)
{
    return ((MilliSecs() - init_time) >= WARM_UP_MSECS);
}

/**
 * Read a sample from the sensor.
 *
 * @return next sample in millivolts (0 - 4095).
 */
short
rh_read_data(void)
{
    return atod_read(RH_CHANNEL);
}

/**
 * Convert millivolts to percent RH.
 *
 * @param  mv  sensor reading in millivolts.
 * @return percent RH.
 */
double
rh_mv_to_percent(int mv)
{
    return (double)mv*RH_SCALE - RH_OFFSET;
}
