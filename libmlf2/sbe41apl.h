#ifndef SBE41APL_H
#define SBE41APL_H

#include "newctd.h"

#define SBE41APL_DEVICE(n)    (CTD_DEVICE+n)
#define SBE41APL_BAUD         CTD_BAUD

int sbe41apl_init(int which);
void sbe41apl_shutdown(int which);
void sbe41apl_test(int which);
int sbe41apl_sample(int which, long timeout, float p, NewCTDdata *ctd);
int sbe41apl_sample_hold(int which, long timeout, float p);
int sbe41apl_data_ready(int which);
int sbe41apl_read(int which, long timeout, NewCTDdata *ctd);
int sbe41apl_dev_ready(int which);
int sbe41apl_start_pump(int which, long timeout);
int sbe41apl_stop_pump(int which, long timeout);
#endif /* SBE41APL_H */
