/*
** arch-tag: ADV header file
** Time-stamp: <2003-11-14 11:08:36 mike>
*/
#ifndef _ADV_H_
#define _ADV_H_

#define ADV_DEVICE	1
#define ADV_BAUD	19200L
#define ADV_TIMEOUT	2L

int adv_init(void);
void adv_shutdown(void);
int adv_dev_ready(void);
void adv_test(void);
int adv_send_cmds(FILE *fp, int ignore_errors);
void adv_get_stats(unsigned long *errs, unsigned long *reads);
int adv_set_clock(void);
int adv_deploy(void);

#endif
