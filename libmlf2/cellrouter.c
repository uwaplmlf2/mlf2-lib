#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "iofuncs.h"
#include "log.h"
#include "ioports.h"
#include "cellrouter.h"


/** Prompt issued by host computer (RUDICS server) */
#define HOST_PROMPT     "irsn: "


static int      ref = 0;
static int      serial_dev;

/**
 * cellrouter_init - initialize the interface.
 */
int
cellrouter_init(void)
{
    if(ref > 0)
        goto initdone;

    if((serial_dev = serial_open(CELLROUTER_DEVICE, 0,
                                 CELLROUTER_BAUD)) < 0)
    {
        log_error("cellrouter", "Cannot open serial device (code = %d)\n",
                  serial_dev);
        return 0;
    }

    /* Activate the serial comm interface chip */
    iop_set(IO_E, (unsigned char)0x08);
    DelayMilliSecs(250L);

    /* Shutdown serial interface chip */
    iop_set(IO_E, (unsigned char)0x02);

    /* Assert DTR */
    iop_clear(IO_E, (unsigned char)0x01);

initdone:
    ref++;
    return 1;
}

/**
 * cellrouter_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
cellrouter_shutdown(void)
{
    if(ref == 0)
        return;
    ref = 0;

    /* Disconnect COM2 */
    iop_clear(IO_E, (unsigned char)0x08);

    /* Shutdown serial interface chip */
    iop_clear(IO_E, (unsigned char)0x02);

    serial_close(serial_dev);
}

/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
cellrouter_dev_ready(void)
{
    return (serial_dev >= 0) && serial_chat(serial_dev, "", HOST_PROMPT, 3L);
}

/**
 * Return serial device.
 */
int
cellrouter_serialdev(void)
{
    return serial_dev;
}
