/**@file
** Driver for Sontek ADV.
**
**
*/
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include "salarm.h"
#include "serial.h"
#include "log.h"
#include "lpsleep.h"
#include "adv.h"

#define COMMAND_PROMPT	">"
#define ADV_INIT_TIMEOUT	10L
#define WARM_UP_MSECS		1900L


static int ref = 0;
static int serial_dev = -1;
static long init_time;
static unsigned long nr_reads, nr_errors;

static __inline__ int sgetc(int desc)
{
    unsigned char	c;
    return (serial_read(desc, &c, (size_t)1) == 1) ? (int)c : -1;
}

static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char	c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
	if((MilliSecs() - start) >= timeout)
	    return -1;
    }
    return ((int) c)&0xff;
}

static char*
serial_gets(char *buf, int n, int desc)
{
    char	*p = buf;
    int		c;
    
    while(--n)
    {
	if((c = sgetc(desc)) == '\n' || c == -1)
	    break;
	*p++ = c;
    }
    *p = '\0';
    
    return buf;
}

static int
command_mode(void)
{
    if(serial_chat(serial_dev, "\r\n", COMMAND_PROMPT, 2L))
	return 1;
    serial_inflush(serial_dev);
    
    /*
    ** Send a BREAK to force the device into Command Mode.
    */
    serial_break(serial_dev);
    DelayMilliSecs(2000L);
    
    /*
    ** Now send a CR and wait for the command prompt.
    */
    if(!serial_chat(serial_dev, "\r\n", COMMAND_PROMPT, 4L))
    {
	log_error("adcp", "Cannot get command prompt\n");
	return 0;
    }

    return 1;
}


void
adv_reopen(void)
{
    serial_reopen(serial_dev);
}

/**
 * Initialize Sontek ADV interface.
 * Open connection to ADV device and initialize. Errors are logged.
 *
 * @return 1 if successful, otherwise 0.
 */
int
adv_init(void)
{
    
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(ADV_DEVICE, 0, ADV_BAUD)) < 0)
    {
	log_error("adv", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    init_time = MilliSecs();

    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to adv_sample_start and adv_read_data.
    */
    add_after_hook("ADV-reopen", adv_reopen);

initdone:
    ref++;
    
    return 1;
}


/**
 * Shutdown the ADV interface.
 * Shutdown the ADV interface and power off the device.
 */
void
adv_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    
    remove_after_hook("ADV-reopen");

    serial_close(serial_dev);
}


/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
adv_dev_ready(void)
{
    return ((MilliSecs() - init_time) >= WARM_UP_MSECS);
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
adv_test(void)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

static void
parse_chat(const char *in, char *cmd, int cmdlen, char *resp, int resplen)
{
    const char	*src;
    char	*dst;
    int		limit, i;

    /* initialize pointers */
    src = in;
    dst = cmd;
    limit = cmdlen;
    i = 0;
    
    while(*src)
    {
	/* end of line terminates the input string */
	if(*src == '\r' || *src == '\n')
	    break;
	if(i == limit)
	    break;
	if(*src == ':')
	{
	    /* start parsing the response string */
	    *dst = '\0';
	    dst = resp;
	    limit = resplen;
	    i = 0;
	    src++;
	    continue;
	}

	if(*src == '\\')
	{
	    /* handle C-style backslash escapes */
	    src++;
	    switch(*src)
	    {
		case 'r':
		    *dst++ = '\r';
		    break;
		case 'n':
		    *dst++ = '\n';
		    break;
		case '\\':
		    *dst++ = '\\';
		    break;
		default:
		    *dst++ = *src;
		    break;
	    }
	    src++;
	}
	else
	{
	    *dst++ = *src++;
	}
	
	i++;
    }

    *dst = '\0';
}

/**
 * Send commands from a file to the ADV.
 * Read each line from the file fp and send it to the ADV and wait for a
 * response.  The default response expected is "OK".  If a different response
 * is expected, the response should appear on the same line as the command,
 * separated by a colon.
 *
 * @param  fp  pointer to file of commands
 * @param  ignore_errors  if non-zero, errors are ignored
 * @return 1 if successful, 0 on error.
 */
int
adv_send_cmds(FILE *fp, int ignore_errors)
{
    char	line[128], cmd[80], resp[48];
    
    if(!command_mode())
	return 0;

    while(fgets(line, (int)sizeof(line), fp) != NULL)
    {
	if(line[0] == '#')
	    continue;
	/* set default response */
	resp[0] = 'O';
	resp[1] = 'K';
	resp[2] = '\0';

	/* parse the line */
	parse_chat(line, cmd, sizeof(cmd)-1, resp, sizeof(resp)-1);
	log_event("Sending ADV command: %s", cmd);
	
	if(!serial_chat(serial_dev, cmd, resp, 5L))
	{
	    log_error("adv", "Command failed: %s\n", cmd);
	    if(!ignore_errors)
		return 0;
	}
    }
    

    return 1;
}

/**
 * Fetch read error statistics.
 *
 * @param  errs  returned error count
 * @param  reads  returned read count.
 */
void
adv_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}

/**
 * Synchronize the ADV clock with the TT8.
 *
 * @return 1 if successful, 0 on error.
 */
int
adv_set_clock(void)
{
    struct tm	*tnow;
    time_t	t;
    char	cmd[48];

    if(!command_mode())
	return 0;
    
    serial_inflush(serial_dev);

    t = RtcToCtm();
    tnow = localtime(&t);
    sprintf(cmd, "Date %d/%02d/%02d\r\n", tnow->tm_year+1900,
	    tnow->tm_mon+1, tnow->tm_mday);
    log_event("Setting ADV date: %s", cmd);
    
    if(!serial_chat(serial_dev, cmd, "OK", 5L))
	log_error("adv", "Command failed: %s", cmd);
    
    sprintf(cmd, "Time %d:%02d:%02d\r\n", tnow->tm_hour,
	    tnow->tm_min, tnow->tm_sec);
    log_event("Setting ADV time: %s", cmd);
    return serial_chat(serial_dev, cmd, "OK", 5L);
}

/**
 * Start the data collection process.
 *
 * @return 1 if successful, 0 on error.
 */
int
adv_deploy(void)
{
    if(!command_mode())
	return 0;
    serial_write_zstr(serial_dev, "Deploy\r\n");
    return 1;
}
