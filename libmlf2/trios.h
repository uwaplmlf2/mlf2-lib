/*
** arch-tag: 1028d805-4faf-43f6-9124-27c281b0274c
** 
*/
#ifndef _TRIOS_H_
#define _TRIOS_H_

#define TRIOS_DEVICE(n)	((n) == 0 ? 6 : 10)
#define TRIOS_BAUD	9600L

typedef struct {
    int			module;
    int			size;
    int			index;
    unsigned char 	data[64];
} DATAFRAME_t;

/** Callback function for trios_read_data */
typedef int (*trios_datasink)(DATAFRAME_t *, void *);

/** Integration time constants */
typedef enum {T_AUTO=0, T_4MS,
	      T_8MS, T_16MS, T_32MS, T_64MS, T_128MS,
	      T_256MS, T_512MS, T_1024MS, T_2048MS,
	      T_4096MS, T_8192MS} inttime_t;

/** Convert time code to milliseconds */
#define T_CODE_TO_MS(c)	(1L << ((c)+1))

int trios_init(int which);
void trios_shutdown(int which);
int trios_dev_ready(int which);
void trios_test(int which);
int trios_command(int which, int code, int p0, int p1, int module);
void trios_set_inttime(int which, inttime_t tcode);
int trios_start(int which);
unsigned long trios_start_time(int which);
int trios_data_ready(int which);
int trios_get_reply(int which, int max_frames,
		    long timeout, trios_datasink handler,
		    void *cbdata);
unsigned trios_get_sn(int which);

#define trios_read_data	trios_get_reply

#endif
