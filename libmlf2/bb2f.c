/*
** $Id: bb2f.c,v 990cc1ffca70 2007/11/27 00:55:14 mikek $
**
** Interface to Wetlabs Fluorometer/Turbidity Meter
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <setjmp.h>
#include <ctype.h>
#include "serial.h"
#include "log.h"
#include "bb2f.h"
#include "wetlabs.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define START_CHAR	'\n'
#define END_CHAR	'\r'

#define WARM_UP_MSECS	350L

static int ref = 0;
static int serial_dev = -1;
static long init_time;


/**
 * Initialize the fluorometer interface.
 * Open a connection to the fluorometer and initialize the device.
 *
 * @return 1 if successful, otherwise 0.
 */
int
bb2f_init(void)
{
    if(ref > 0)
	goto initdone;

    if((serial_dev = wetlabs_init(BB2F_DEVICE, BB2F_BAUD)) < 0)
	return 0;

    init_time = MilliSecs();
initdone:
    ref++;

    return 1;
}

/**
 * Shutdown the fluorometer interface and power off the device.
 */
void
bb2f_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    wetlabs_shutdown(serial_dev);
}


/**
 * Check if device is ready.
 *
 * @return true if the device is ready to accept commands.
 */
int
bb2f_dev_ready(void)
{
    return ref && ((MilliSecs() - init_time) > WARM_UP_MSECS);
}


/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 */
void
bb2f_test(void)
{
    wetlabs_test(serial_dev);
}

/**
 * Reads the next sample value from the device.
 *
 * @param  dp  pointer to returned data.
 * @param  timeout  read timeout in milliseconds
 * @return 1 if successful, 0 if an error occurs.
 */
int
bb2f_read_data(Bb2fData *dp, long timeout)
{
    int		n;
    short	buf[BB2F_FIELDS];

    if(ref <= 0)
	return 0;

    n = wetlabs_read_data(serial_dev, BB2F_FIELDS, buf, timeout);
    if(n < BB2F_FIELDS)
    {
        log_error("bb2f", "Error reading data record\n");
        return 0;
    }

    dp->blue_ref = buf[0];
    dp->blue     = buf[1];
    dp->red_ref  = buf[2];
    dp->red      = buf[3];
    dp->chl      = buf[4];

    return 1;
}
