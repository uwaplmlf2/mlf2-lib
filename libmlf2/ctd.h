/**@file
 ** @deprecated
** $Id: ctd.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _CTD_H_
#define _CTD_H_

#include "newctd.h"

/** Device selectors */
#define CTD_A		0x01
#define CTD_B		0x02

/** CTD data record */
typedef struct ctd {
    int			which; /**< device selector */
    float		t[2];	/**< temperature (degrees) */
    float		s[2];	/**< salinity (ppt) */
    unsigned long	delta_t; /**< time between samples */
} CTDdata;

int ctd_init(int which);
void ctd_shutdown(void);
int ctd_read_data(CTDdata *ctd);
int ctd_start_sample(double *p);
int ctd_dev_ready(void);
void ctd_get_stats(unsigned long *errs, unsigned long *reads);
void ctd_test(int which);
int ctd_read_data_sync(double p[], CTDdata *ctd);
int ctd_data_ready(void);

#endif
