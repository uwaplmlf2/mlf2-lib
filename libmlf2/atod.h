/*
** $Id: atod.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _ATOD_H_
#define _ATOD_H_

void atod_init(void);
void atod_shutdown(void);
short atod_read(int chan);
int atod_read_buf(int chan, int bipolar, int period, register long n, 
	      short *databuf);

#endif
