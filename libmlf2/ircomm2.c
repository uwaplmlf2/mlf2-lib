/**@file
** High level Iridium communication functions.
**
** @sa iridium.c
** @sa ircomm.c
** @ingroup Comm
** $Id: ircomm2.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
**
**
*/
#include <stdio.h>
#ifdef __GNUC__
#include <unistd.h>
#else
#include <fcntl.h>
#endif
#include <tt8.h>
#include <tt8lib.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "serial.h"
#include "log.h"
#include "xmodem.h"
#include "iridium.h"

#define MIN(a, b) ((a) < (b)) ? (a) : (b)

struct upload_desc_t {
    FILE	*fp;
    int		hdrsize;
    int		recsize;
    int		sendhdr;
    long	rstart;
    long	rend;
    long	rinc;
    long	rcurrent;
};

/*
 * upload_netcdf - xmodem_send callback to upload a netcdf data file.
 */
static int
upload_netcdf(char *buf, int size, void *arg)
{
    struct upload_desc_t	*up = (struct upload_desc_t*)arg;
    long			offset, ncopy, nr_bytes, n, where;
    
    /*
    ** This function is called by xmodem_send when it is ready for a new
    ** block of data.
    */
    nr_bytes = 0;
    while(size > 0 && up->rcurrent <= up->rend)
    {
	where = ftell(up->fp);
	n = 0;
	if(up->sendhdr && where < up->hdrsize)
	{
	    /* We are still sending the header */
	    ncopy = up->hdrsize - where;
	    n = fread(buf, 1L, MIN(ncopy, size), up->fp);
	}
	else if(where == up->hdrsize && up->rstart > 0)
	{
	    /*
	    ** We just finished with the header, position the FILE
	    ** pointer at the first record.
	    */
	    fseek(up->fp, up->hdrsize+up->rstart*up->recsize, SEEK_SET);
	    continue;
	}
	else
	{
	    offset = (where - up->hdrsize) % up->recsize;
	    ncopy = up->recsize - offset;
	    if(ncopy > size)
	    {
		/* The remaining section of the record exceeds block size */
		n = fread(buf, 1L, (size_t)size, up->fp);
	    }
	    else
	    {
		/* We are crossing a record boundary with this block */
		n = fread(buf, 1L, (size_t)ncopy, up->fp);
		up->rcurrent += up->rinc;
		if(up->rinc > 1)
		    fseek(up->fp, up->rcurrent*up->recsize+up->hdrsize, 
			  SEEK_SET);
	    }
	}

	/* If we read no data, we must be at the end of the file */
	if(n == 0)
	    break;
	
	nr_bytes += n;
	size -= n;
	buf += n;	
	
    }
    SerPutByte('.');
    
    return nr_bytes;
}

/**
 * Dialup and login to a remote system.
 * This function dials the specified phone number and "executes" the chat
 * script in order to login to the remote system.  login is an array of
 * chat_t data structures terminated by a NULL entry (both the 'send' and
 * 'expect' fields are NULL).
 *
 * @param  prefix  phone number prefix.
 * @param  pn  phone number (including area code).
 * @param  login  chat script to automate the login process.
 * @return 1 if successful, otherwise 0.
 */
int
iridium_login(const char *prefix, const char *pn, chat_t *login)
{
    chat_t	*cp;
    char	number[64];
    
    /* Sanity check */
    if(strlen(prefix)+strlen(pn) > sizeof(number))
	return 0;
    sprintf(number, "%s%s", prefix, pn);
    
    log_event("Dialing %s\n", number);
    
    if(iridium_try_dial(number, 60L))
    {
	log_event("Connected\n");
	
	cp = &login[0];
	while(cp->send || cp->expect)
	{
	    if(iridium_chat(cp->send, cp->expect, cp->timeout) != 1)
	    {
		log_error("iridium", "Error in chat script\n");
		return 0;
	    }
	    cp++;
	}
    }
    else
    {
	log_error("iridium", "Dialing error (pn = %s)\n", number);
	return 0;
    }
    
    return 1;
}


/**
 * Setup working directory on remote system.
 * This function is used to setup the working directory for this float on
 * the remote system.  The Iridium serial number is used for the directory
 * name.
 *
 * @return 1 if successful, 0 if an error occurs.
 */
int
iridium_dir_setup(void)
{
    snumber_t	sn;
    char 	cmd[64];

    iridium_get_sn(&sn);

    /* 
    ** Use the '-p' option to mkdir so it won't fail if the directory
    ** already exists.
    */
    sprintf(cmd, "mkdir -p %s && cd %s\n", sn_to_str(sn), sn_to_str(sn));
    /* TODO: fix the hardcoded shell prompt */
    return (iridium_chat(cmd, "]$ |NO CARRIER", 30L) == 1);
}


/**
 * Send data from a netcdf file.
 * The function can be used to send a subset of a netCDF file.
 *
 * @param  fname  filename
 * @param  hdrsize  header size in bytes
 * @param  recsize  record size in bytes
 * @param  rstart  first record to send
 * @param  rend  last record to send
 * @param  rinc  record increment (stride)
 * @param  sendhdr  header flag.
 * @return 1 if successful, otherwise 0.
 * @warning This is still experimental.
 */
int
iridium_send_netcdf(const char *fname, int hdrsize, int recsize, long rstart,
		    long rend, long rinc, int sendhdr)
{
    int				sd, r;
    struct upload_desc_t	upd;

    if((sd = iridium_serialdev()) == -1)
    {
	log_error("iridium", "File transfer attempted on closed device\n");
	return 0;
    }
    
    if((upd.fp = fopen(fname, "rb")) == NULL)
    {
	log_error("iridium", "Cannot open file %s\n", fname);
	return 0;
    }

    upd.sendhdr = sendhdr;
    upd.hdrsize = hdrsize;
    upd.recsize = recsize;
    upd.rcurrent = upd.rstart = rstart;
    upd.rend = rend;
    upd.rinc = rinc;

    /*
    ** If the header is not being sent, position the file pointer at
    ** the start of the first record.
    */
    if(!sendhdr)
	fseek(upd.fp, hdrsize+recsize*rstart, SEEK_SET);
    
    log_event("Uploading %s (%ld-%ld,%ld)\n", fname, rstart, rend, rinc);
    r = xmodem_send(sd, upload_netcdf, (void*)&upd, 128);
    fclose(upd.fp);
    
    return r;
}
