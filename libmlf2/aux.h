/*
 *
 */
#ifndef AUX_H
#define AUX_H

#define AUX_DEVICE      5
#define AUX_BAUD        38400L

int aux_init(void);
void aux_shutdown(void);
int aux_dev_ready(void);
void aux_send(const char *text);
int aux_login(void);
int aux_shell_command(const char *command, long timeout);
char* aux_shell_response(long timeout);
int aux_xmodem_get_file(const char *filename, int remove);
int aux_xmodem_get(const char *cmd, const char *filename);

#endif /* AUX_H */
