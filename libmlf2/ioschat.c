/*
** $Id: ioschat.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <setjmp.h>
#include <string.h>
#include "serial.h"
#include "iostream.h"
#include "salarm.h"

#define STR_COMPARE(a,b)	(*(a) == *(b) && strcmp((a), (b)) == 0)

static jmp_buf	recover;

static void
catch_alarm(void)
{
#ifdef __GNUC__
    longjmp(recover, 1L);
#else
    longjmp(recover, 1);
#endif
}


/**
 * ios_chat - automate a "conversational" interaction through an IOstream.
 * @ios: pointer to IOstream.
 * @send: string to send.
 * @expect: response string to wait for.
 * @timeout: maximum number of seconds to wait for a response.
 *
 * This function automates a "conversational" interaction through an IOstream
 * interface (see iostream.c).  The @send string is sent and the @expect
 * string is waited for.  Returns 1 if successful, otherwise 0.
 */
int
ios_chat(IOstream *ios, const char *send, const char *expect, long timeout)
{
    register char		*p, *q;
    register int		i, n;
    static char			response_buf[80];
    
    if((n = strlen(expect)) >= sizeof(response_buf))
	return 0;

    if(send)
    {
	serial_write(ios->desc, send, strlen(send));
	DelayMilliSecs(10L);
    }

    if(n == 0)
	return 1;

    alarm_set(timeout, ALARM_ONESHOT, catch_alarm);

    if(setjmp(recover) != 0)
	return 0;

#ifdef DEBUG_CHAT
    printf("Waiting for \"%s\" ... ", expect);
    fflush(stdout);
#endif
    
    for(i = 0;i < n;i++)
	response_buf[i] = iosgetc(ios);
    response_buf[i] = '\0';
    
    while(!STR_COMPARE(response_buf, expect))
    {
	/*
	** Shift the buffer to the left
	*/
	p = response_buf;
	q = response_buf + 1;
	for(i = 1;i < n;i++)
	    *p++ = *q++;

	/*
	** Load the next character
	*/
	*p++ = iosgetc(ios);
	*p = 0;
#ifdef DEBUG_CHAT
	fputs(response_buf, stderr);
	fputc('\n', stderr);
#endif	    
    }

    alarm_shutdown();
#ifdef DEBUG_CHAT
    printf("got it\n");
#endif    
    
    return 1;
}
