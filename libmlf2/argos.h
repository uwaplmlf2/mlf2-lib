/*
** $Id: argos.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _ARGOS_H_
#define _ARGOS_H_

#define ARGOS_DEVICE		8
#define ARGOS_MINOR		0
#define ARGOS_BAUD		4800L
#define ARGOS_MSG_LEN		31
#define ARGOS_MSG_INTERVAL	120L
#define ARGOS_MSG_TIME		15L

int argos_init(void);
void argos_shutdown(void);
int argos_dev_ready(void);
int argos_wait(long timeout);
int argos_send_msg(const unsigned char *msg, int n);
long argos_next(void);
int argos_build_packet(GPSdata *gdp, const char *fmt, void **dp, int n, 
		   unsigned char *pkt);

#endif /* _ARGOS_H_ */
