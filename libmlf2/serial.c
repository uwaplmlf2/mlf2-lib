/**@file
** Generic serial device interface.
**
** $Id: serial.c,v df4a2adb185c 2008/05/28 20:48:47 mikek $
**
**
*/
#include <stdio.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "ioports.h"
#include "power.h"
#include "tpuart.h"
#include "lpsleep.h"
#include "serial.h"

#define MUX_SWITCH_TIME 60L
#define MUX_BITS    0x0f

/** Serial device */
struct serial_device {
    int     power_switch;       /**< Power switch code (see power.h) */
    int     rchan;          /**< TPU read channel */
    int     wchan;          /**< TPU write channel */
    int     muxcode;        /**< serial MUX setting */
    int     muxshift;               /**< bits to shift in mux code */
    int     enable_port;    /**< I/O port for RS232 enable (FORCEON) */
    int     enable_mask;    /**< Mask for RS232 enable, 0 == not used */
    int     inmode;                 /**< Input mode flag */
    long    baud;                   /**< baud rate */
    TPUart  **devp;         /**< points to cache_entry.dev */
};

/** TPU Uart device */
struct cache_entry {
    int     ref; /**< Reference count (serial_device count) */
    TPUart  *dev;
};

static struct cache_entry _uart_cache[16];

static struct serial_device _sd_tab[] = {
    { GPS_POWER,        0,  1,  0},
    { SERDEV_POWER1,    2,  3,  0},
    { SERDEV_POWER2,    4,  5,  0},
    { SERDEV_POWER3,    7,  6,  0x80,   2 },
    { SERDEV_POWER4,    7,  6,  0x81,   2 },
    { SERDEV_POWER5,    7,  6,  0x82,   2 },
    { SERDEV_POWER6,    7,  6,  0x83,   2, IO_E, 0x10 },
    { SERDEV_POWER7,    10, 11, 0x80,   0 },
    { SERDEV_POWER8,    10, 11, 0x81,   0 },
    { SERDEV_POWER9,    10, 11, 0x82,   0 },
    { SERDEV_POWER10,   10, 11, 0x83,   0, IO_E, 0x20 },
    { COMM_POWER,       14, 13, 0},
};

static void
_pet_watchdog(void)
{
    PET_WATCHDOG();
}

static void
mux_set(int code)
{
    unsigned char   val;

    val = iop_read(IO_F);
    val &= ~MUX_BITS;
    val |= code;
    iop_write(IO_F, val);
}

static __inline__ int mux_get(void)
{
    return (iop_read(IO_F) & MUX_BITS);
}

/*
 * Set all RS-232 FORCEON lines.
 */
static void
force_on(void)
{
    int                     n = sizeof(_sd_tab)/sizeof(struct serial_device);
    int                     i;
    struct serial_device    *sd;

    for(i = 0;i < n;i++)
    {
        sd = &_sd_tab[i];
        if(sd->devp != 0 && sd->enable_mask != 0)
            iop_set(sd->enable_port, (unsigned char)sd->enable_mask);
    }
}

/*
 * Clear all RS-232 FORCEON lines.
 */
static void
force_off(void)
{
    int                     n = sizeof(_sd_tab)/sizeof(struct serial_device);
    int                     i;
    struct serial_device    *sd;

    for(i = 0;i < n;i++)
    {
        sd = &_sd_tab[i];
        if(sd->devp != 0 && sd->enable_mask != 0)
            iop_clear(sd->enable_port, (unsigned char)sd->enable_mask);
    }
}

/**
 * Open a serial device.
 * Open a serial device and return a device descriptor.  A serial device
 * encapsulates a TPU uart and power control.  Opening a serial device
 * initializes the TPU uart and applies power to the associated hardware.
 * major serves as a index into the device table and must be 0-11.  Each
 * major number is associated with an interface as follows.
 * - 0  -  GPS interface.
 * - 1-10  -  Serial interfaces 1 through 10.
 * - 11  -  Iridium modem interface.
 *
 * minor is not used and only exists to provide compatability with the
 * old serial interface.
 *
 * Returns integer device descriptor for use in I/O operations or one of the
 * following negative error codes defined in serial.h.
 * - SERIAL_EBADNUM - bad major number.
 * - SERIAL_EMINOR - bad minor number.
 * - SERIAL_EOPEN - device open failed.
 *
 * @param  major  major number (0-11).
 * @param  minor  minor number.
 * @param  baud  baud rate
 * @return device descriptor or negative error code.
 */
int
serial_open(int major, int minor, long baud)
{
    struct serial_device    *sd;
    struct cache_entry      *ue;

    if(major < 0 || major >= SERIAL_NDEVS)
        return SERIAL_EBADNUM;
    sd = &_sd_tab[major];
    ue = &_uart_cache[sd->rchan];

    /*
     * Multiple serial devices may share the same TPU-uart through
     * a multiplexor.  The uart need only be opened once.
     */
    if(ue->ref == 0)
    {
        if((ue->dev = tpuart_open(sd->rchan, sd->wchan, baud, TPU_NO_PARITY,
                                  8, 1, 0)) == 0)
        {
            return SERIAL_EOPEN;
        }

    }

    sd->inmode = SERIAL_BLOCKING;
    sd->baud = baud;
    sd->devp = &ue->dev;
    ue->ref++;

    if(sd->muxcode)
        mux_set((sd->muxcode & 0x0f) << sd->muxshift);
    else
        tpuart_inflush(*sd->devp);

    if(sd->enable_mask != 0)
    {
        iop_set(sd->enable_port, (unsigned char)sd->enable_mask);
        /* Call before entering LP sleep mode */
        add_before_hook("serial-devs", force_off);
    }

    tpuart_set_speed(*sd->devp, sd->baud);
    power_on(sd->power_switch);

    return major;
}

/**
 * Close and reopen the uart.
 * Close and reopen the TPU uart interface associated with a serial
 * device.  This is useful if the device remains powered-on while
 * the TT8 is in LP-sleep mode.  Upon return from LP-sleep, the TPU
 * uart must be reinitialized.
 *
 * @param  desc  serial device descriptor.
 * @return 1 if successful, otherwise 0.
 */
int
serial_reopen(int desc)
{
    register struct serial_device   *sd;

    if(desc < 0 || desc >= SERIAL_NDEVS)
        return 0;

    sd = &_sd_tab[desc];

    if(sd->devp == 0)
        return 0;

    tpuart_close(*sd->devp);

    if((*sd->devp = tpuart_open(sd->rchan, sd->wchan, sd->baud,
                                TPU_NO_PARITY, 8, 1, 0)) == 0)
        return 0;

    if(sd->enable_mask != 0)
        iop_set(sd->enable_port, (unsigned char)sd->enable_mask);

    serial_inmode(desc, sd->inmode);

    return 1;
}

/**
 * Close a serial device.
 * Close and power down an open serial device.
 *
 * @param  desc  serial device descriptor.
 */
void
serial_close(int desc)
{
    struct serial_device    *sd;
    struct cache_entry      *ue;

    if(desc < 0 || desc >= SERIAL_NDEVS)
        return;

    /* Remove the link to the uart and shutdown the device */
    sd = &_sd_tab[desc];
    if(sd->enable_mask != 0)
        iop_clear(sd->enable_port, (unsigned char)sd->enable_mask);
    sd->devp = 0;
    power_off(sd->power_switch);

    ue = &_uart_cache[sd->rchan];

    /* If this is the last device, close the uart */
    if(--ue->ref <= 0)
    {
        if(ue->dev)
            tpuart_close(ue->dev);
        ue->dev = 0;
        ue->ref = 0;
    }
}

/**
 * Flush input buffers.
 *
 * @param  desc  serial device descriptor.
 */
void
serial_inflush(int desc)
{
    struct serial_device    *sd;

#ifdef CHECK_DESC
    if(desc < 0 || desc >= SERIAL_NDEVS)
        return 0;
#endif
    sd = &_sd_tab[desc];

    if(sd->muxcode)
    {
        int code;

        code = (sd->muxcode & 0x0f) << sd->muxshift;
        if(code != mux_get())
        {
            mux_set(code);
            DelayMilliSecs(MUX_SWITCH_TIME);
        }
    }

    tpuart_inflush(*sd->devp);
}

/**
 * Set the serial device input mode.
 * Sets the input mode of desc to be blocking (wait for input) or non-blocking
 * (reads return immediately if no input is available).  mode must be set to
 * SERIAL_BLOCKING or SERIAL_NONBLOCKING.
 *
 * @param  desc  serial device descriptor.
 * @param  mode  new input mode.
 * @return previous mode setting.
 */
int
serial_inmode(int desc, int mode)
{
    int     r;

    r = _sd_tab[desc].inmode;
    _sd_tab[desc].inmode = mode;
    switch(mode)
    {
        case SERIAL_BLOCKING:
            tpuart_set_block(*_sd_tab[desc].devp);
            break;
        case SERIAL_NONBLOCKING:
            tpuart_set_noblock(*_sd_tab[desc].devp);
            break;
    }

    return r;
}

/**
 * Change the baud rate of a serial device.
 *
 * @param  desc  serial device descriptor.
 * @param  baud  new baud rate.
 * @return the previous setting.
 */
long
serial_speed(int desc, long baud)
{
    long    r;
    struct serial_device    *sd;

    sd = &_sd_tab[desc];
    r = sd->baud;

    if(baud > 0L)
    {
        sd->baud = baud;
        tpuart_set_speed(*sd->devp, baud);
    }

    return r;
}

/**
 * Reset the baud rates of all ports to their current value. This
 * function should be called after changing the CPU clock frequency.
 *
 */
void
serial_reset_speed(void)
{
    int     n = sizeof(_sd_tab)/sizeof(struct serial_device);
    int     i;
    struct serial_device    *sd;

    for(i = 0;i < n;i++)
    {
        sd = &_sd_tab[i];
        if(sd->devp && sd->baud > 0)
            tpuart_set_speed(*sd->devp, sd->baud);

    }
}

/**
 * Put serial device into "pass through" mode.
 * Pass-thru monitor function to let the console user interact directly with
 * the hardware attached to the device.  Reads characters from the console and
 * writes them to the serial device and vice-versa.  Entering stopchar from
 * the console will exit the function.
 *
 * @param  desc  serial device descriptor.
 * @param  stopchar  character which will exit the function
 * @param  breakchar  character which will send a BREAK
 */
void
serial_passthru(int desc, int stopchar, int breakchar)
{
    struct serial_device    *sd;

    sd = &_sd_tab[desc];
    assert(sd->devp != 0);

    if(sd->muxcode)
    {
        int code;

        code = (sd->muxcode & 0x0f) << sd->muxshift;
        if(code != mux_get())
        {
            mux_set(code);
            DelayMilliSecs(MUX_SWITCH_TIME);
            serial_reopen(desc);
        }
    }

    tpu_pass_thru(*sd->devp, stopchar, breakchar, NULL);
}


/**
 * Read bytes from a serial device.
 * Read up to n bytes of data from the device and store in buf.
 *
 * @param  desc  serial device descriptor.
 * @param  buf  array to store input data.
 * @param  n  number of bytes to read.
 * @return number of bytes read.
 */
int
serial_read(int desc, char *buf, size_t n)
{
    struct serial_device    *sd;

    sd = &_sd_tab[desc];
    assert(sd->devp != 0);

    /* Adjust mux setting if necessary */
    if(sd->muxcode)
    {
        int code;

        code = (sd->muxcode & 0x0f) << sd->muxshift;
        if(code != mux_get())
        {
            mux_set(code);
            DelayMilliSecs(MUX_SWITCH_TIME);
            serial_reopen(desc);
        }
    }

    return tpu_read(*sd->devp, buf, n);
}

/**
 * Write bytes to the serial device.
 * Write up to n bytes from buf to the serial device.
 *
 * @param  desc  serial device descriptor.
 * @param  buf  array containing data to output.
 * @param  n  number of bytes to write.
 * @return number of bytes written.
 */
int
serial_write(int desc, const char *buf, size_t n)
{
    struct serial_device    *sd;

#ifdef CHECK_DESC
    if(desc < 0 || desc >= SERIAL_NDEVS)
        return 0;
#endif
    sd = &_sd_tab[desc];
    assert(sd->devp != 0);

    /* Adjust mux setting if necessary */
    if(sd->muxcode)
    {
        int code;

        code = (sd->muxcode & 0x0f) << sd->muxshift;
        if(code != mux_get())
        {
            mux_set(code);
            DelayMilliSecs(MUX_SWITCH_TIME);
            tpuart_set_speed(*sd->devp, sd->baud);
        }
    }

    return tpu_write(*sd->devp, buf, n);
}

/**
 * Write a null-terminated string to the serial device.
 *
 * @param  desc  serial device descriptor.
 * @param  buf  string to write.
 * @return number of bytes written.
 */
int
serial_write_zstr(int desc, const char *buf)
{
    return serial_write(desc, buf, strlen(buf));
}

/**
 * Send a BREAK signal to a serial device.
 *
 * @param  desc  serial device descriptor.
 */
void
serial_break(int desc)
{
    struct serial_device    *sd;

#ifdef CHECK_DESC
    if(desc < 0 || desc >= SERIAL_NDEVS)
        return 0;
#endif
    sd = &_sd_tab[desc];
    assert(sd->devp != 0);

    /* Adjust mux setting if necessary */
    if(sd->muxcode)
    {
        int code;

        code = (sd->muxcode & 0x0f) << sd->muxshift;
        if(code != mux_get())
        {
            mux_set(code);
            DelayMilliSecs(MUX_SWITCH_TIME);
            tpuart_set_speed(*sd->devp, sd->baud);
        }
    }

    tpu_send_break(*sd->devp);
}
