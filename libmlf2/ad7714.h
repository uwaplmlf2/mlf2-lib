/*****************************************************************************
*  Module: ad7714.h             Programmer: Russ Light       Date: 03/07/97  *
*                                                                            *
*  Description:                                                              *
*    Include file with definiitions to support AD7714 A/D converter.         *
*                                                                            *
*  Modification Record:                                                      *
*                                                                            *
*                                                                            *
*****************************************************************************/
#ifndef _AD7714_H_
#define _AD7714_H_

#define TPU_AD7714_DRDY	15	/* TT8 TPU line used for DRDY signal */

#define DEFAULT_VREF	1.6876	/* default value of AD7714 reference
				   voltage */
#define AD7714_FCLK	2457600 /* freq of Fclk for AD7714 */

/*****************************************************************************
* Communications Register Definitions                                        *
 ****************************************************************************/
 				/* channel select codes - 
				   OR with other codes */
#define AIN1_AIN2	0x04	/* channel AIN1-AIN2 fully differential */
#define AIN3_AIN4	0x05	/* channel AIN1-AIN2 fully differential */
#define AIN5_AIN6	0x06	/* channel AIN1-AIN2 fully differential */

#define VM_PRESSURE_CH	AIN1_AIN2	/* VM AD7714 channel for pressure
					   xdcr */

#define READ		0x08	/* perform read
				   OR with other codes */
#define WRITE		0x00	/* perform write
				   OR with other codes */

				/* register select codes
				   OR with other codes */
#define MODE_REG	0x10	/* access mode register */
#define FILTER_H_REG	0x20	/* access filter high register */
#define FILTER_L_REG	0x30	/* access filter low register */
#define DATA_REG	0x50	/* access data register */
#define	ZERO_SCALE_REG	0x60	/* access zero-scale cal register */
#define	FULL_SCALE_REG	0x70	/* access full-scale cal register */

/*****************************************************************************
*  Mode Register Definitions                                                 *
 ****************************************************************************/
#define AD7714_OP_MODE	0x0E0	/* AD7714 operation mode definition for
				   VM - upper three bits of high filter
				   register
				   Bit 7 = 1, Unipolar
				   Bit 6 = 1, 24-bit word length
				   Bit 5 = 1, Current Boost On */

#define AD7714_NORM_MODE     0x00	/* default operating mode - OR with
				   gain to create mode register value
				   Bit 7 = 0, MD2 - Normal Mode
				   Bit 6 = 0, MD1
				   Bit 5 = 0, MD0
				   Bit 4 = 0, G2  - Gain

				   Bit 3 = 0, G1
				   Bit 2 = 0, G0
				   Bit 1 = 0, Burn out current off
				   Bit 0 = 0, FSYNC = not used */

#define SELF_CAL_MODE   0x20	/* self calibration operating mode - OR with
				   gain to create mode register value
				   Bit 7 = 0, MD2 - Self Calibration
				   Bit 6 = 0, MD1
				   Bit 5 = 1, MD0
				   Bit 4 = 0, G2  - Gain

				   Bit 3 = 0, G1
				   Bit 2 = 0, G0
				   Bit 1 = 0, Burn out current off
				   Bit 0 = 0, FSYNC = not used */

/*
** Extract gain value from mode register value.
*/
#define GAIN_VALUE(x)	(((x) & 0x1c) >> 2)

				/* gain values for mode register - bits in
				   proper position, OR with other bits */
#define GAIN_1		0x00	/* gain value = 1 */
#define GAIN_2		0x04	/* gain value = 2 */
#define GAIN_4		0x08	/* gain value = 4 */
#define GAIN_8		0x0C	/* gain value = 8 */
#define GAIN_16		0x10	/* gain value = 16 */
#define GAIN_32		0x14	/* gain value = 32 */
#define GAIN_64		0x18	/* gain value = 64 */
#define GAIN_128	0x1C	/* gain value = 128 */
				   
#define AD7714_DEF_GAIN     GAIN_32 /* default gain - OR with 
				    operating mode to create mode register 
				    value */

#define PRESSURE_GAIN    GAIN_32 /* gain value for pressure xdcr - OR with 
				    operating mode to create mode register 
				    value */

#define AD7714_GAIN     32

/* Default Filter Code */
#define	AD7714_FC_DEFAULT	0x3C0	/* Output rate = 20Hz,
				   12-bit Filter code = 960 = 0x3C0,
				   Bit 11 = 0, FS11
				   Bit 10 = 0, FS10
				   Bit 9  = 1, FS9
				   Bit 8  = 1, FS8

				   Bit 7 = 1, FS7
				   Bit 6 = 1, FS6
				   Bit 5 = 0, FS5
				   Bit 4 = 0, FS4

				   Bit 3 = 0, FS3
				   Bit 2 = 0, FS2
				   Bit 1 = 0, FS1
				   Bit 0 = 0, FS0 */

#define AD7714_FC_LOW		19
#define AD7714_FC_HIGH		4000

unsigned long get_data_ad7714(unsigned short channel, int sync);
int setup_AD7714(int gainval);
double ad7714_to_mv(unsigned long c);
void switch_channel_ad7714(unsigned short channel);
unsigned long self_cal_ad7714(unsigned short channel, unsigned short gain);
void set_filter_ad7714(unsigned int filter);
void set_mode_ad7714(unsigned short channel, unsigned short mode);
void reset_ad7714(void);
void cfg_qspi_ad7714(void);

#endif /* _AD7714_H_ */
