/*
*/
#ifndef _WETLABS_H_
#define _WETLABS_H_

int wetlabs_init(int devnum, long baud);
void wetlabs_shutdown(int sdev);
int wetlabs_read_data(int sdev, int ncols, short *vals, long timeout);
int wetlabs_close_shutter(int sdev);
void wetlabs_test(int sdev);
int wetlabs_dev_ready(int sdev, long timeout);

#endif /* _WETLABS_H_ */
