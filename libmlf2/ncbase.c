/*
** arch-tag: base netcdf functions.
** Time-stamp: <2007-02-27 22:25:52 mike>
**
** Write netCDF format data files for MLF2.  The file formats are
** predefined so the headers are constant.  There are four different
** file types which are defined by the following CDL descriptions:
**
** Environmental Data
**
** netcdf env {
**     dimensions:
** 	ctd = 2, time = unlimited;
**     variables:
** 	int	time(time);
** 		time:units		= "seconds since 1970-01-01 UTC";
** 	float	pressure(time);
** 		pressure:units		= "decibars";
**	float	piston(time);
**		piston:units		= "cm";
** 	float	temp(time, ctd);
** 		temp:units		= "degreesC";
** 	float	sal(time, ctd);
** 		sal:units		= "ppt";
** }
**
**
**
** GPS Data
**
** netcdf gps {
**     dimensions:
**	time = unlimited;
**     variables:
** 	int	time(time);
** 		time:units		= "seconds since 1970-01-01 UTC";
**      float 	lat(time);
**		lat:units		= "degrees";
**	float	lon(time);
**		lon:units		= "degrees";
**	int	nsats(time);
** }
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <setjmp.h>
#include <assert.h>
#include <time.h>
#ifdef __GNUC__
#include <unistd.h>
#endif
#if defined(TEST) && defined(__TT8__)
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#endif
#include "netcdf.h"
#include "adcp.h"
#ifdef __i386__
#define SWAP_BYTES
#endif

/* How often to update record count (see flush_record()) */
#define UPDATE_RC_INTERVAL	10

#define writerec(nd, fp)	(fwrite((nd)->data, 1L, (long)((nd)->size), fp) == (long)((nd)->size))


#include "ncattr.h"

/*
** Dimension descriptions for each file type.
*/

#ifdef WAVE_NETCDF
/* Wave spectra */
static struct dim wave_dims[] = {
{ _SAMPLE,	64L },
{ _DIRECTION, 	3L },
{ _TIME,	0L }
};

static int32 wave_dim_index_scalar[] = { 2 };
static int32 wave_dim_index_std[] = { 2, 0 };
static int32 wave_dim_index_xyz[] = { 2, 0, 1 };
#endif

#ifdef NOISE_NETCDF
/* Ambient noise */
static struct dim noise_dims[] = {
{ _CHANNEL,	2L},
{ _SAMPLE,	52L},
{ _TIME,	0L }
};

static int32 noise_dim_index_scalar[] = { 2 };
static int32 noise_dim_index_psd[] = { 2, 1, 0 };
#endif

/* GPS data */
static struct dim gps_dims[] = {
{ _TIME,	0L }
};

static int32 gps_dim_index_scalar[] = { 0 };

#ifdef FPR_NETCDF
/* Fast pressure data */
#define fpr_dims		gps_dims
#define fpr_dim_index_scalar	gps_dim_index_scalar
#endif

#ifdef NOISE_NETCDF
static struct var noise_vars[] = {
{ _TIME,	1, noise_dim_index_scalar,	1, &attr_table[TIME_UNITS],
    NC_INT,	0,	0,	0,	0 },
{ _PSD,		3, noise_dim_index_psd,		1, &attr_table[PSD_UNITS],
    NC_BYTE,	0,	0,	0,	0 },
  };
#endif

#ifdef WAVE_NETCDF
static struct var wave_vars[] = {
{ _TIME,	1, wave_dim_index_scalar,	1, &attr_table[TIME_UNITS],
    NC_INT,	0,	0,	0,	0 },
{ _PRESSURE,	2, wave_dim_index_std,		1, &attr_table[PRESSURE_UNITS],
    NC_FLOAT,	0,	0,	0,	0 },
{ _ROTATION,	3, wave_dim_index_xyz,		2, &attr_table[ROTATION_UNITS],
    NC_SHORT,	0,	0,	0,	0 },
{ _ACCEL,	3, wave_dim_index_xyz,		2, &attr_table[ACCEL_UNITS],
    NC_SHORT,	0,	0,	0,	0 },
  };
#endif

static struct var gps_vars[] = {
{ _TIME,	1, gps_dim_index_scalar,	1, &attr_table[TIME_UNITS],
    NC_INT,	0,	0,	0,	0 },
{ _LAT,		1, gps_dim_index_scalar,	1, &attr_table[ANGLE_UNITS],
    NC_FLOAT,	0,	0,	0,	0 },
{ _LON,		1, gps_dim_index_scalar,	1, &attr_table[ANGLE_UNITS],
    NC_FLOAT,	0,	0,	0,	0 },
{ _NSATS,	1, gps_dim_index_scalar,	0, 0,
    NC_INT,	0,	0,	0,	0 },
  };

#ifdef FPR_NETCDF
static struct var fpr_vars[] = {
{ _TIME,	1, fpr_dim_index_scalar,	1, &attr_table[TIME_UNITS],
    NC_INT,	0,	0,	0,	0 },
{ _PRESSURE,	1, fpr_dim_index_scalar,	1, &attr_table[PRESSURE_UNITS],
    NC_FLOAT,	0,	0,	0,	0 },
  };
#endif

static jmp_buf	wr_error;

/*
 * Write a 4-byte integer to the open stream in MSB first
 * order.  A write error is indicated by jumping to the
 * 'wr_error' jmp_buf which must be initialized by the
 * caller (or caller's caller etc.).
 *
 */
static void
write_long(FILE *ofp, uint32 val)
{
#ifdef SWAP_BYTES
    val = (val & 0xff000000) >> 24 |
      (val & 0xff0000) >> 8 |
      (val & 0xff00) << 8 |
      (val & 0xff) << 24;
#endif    
    if(fwrite(&val, 4L, 1L, ofp) != 1L)
	longjmp(wr_error, 1L);
}

/*
 * Write a 2-byte integer to the open stream in MSB first
 * order.  A write error is indicated by jumping to the
 * 'wr_error' jmp_buf which must be initialized by the
 * caller (or caller's caller etc.).
 *
 */
static void
write_short(FILE *ofp, uint16 val)
{
#ifdef SWAP_BYTES
    val = (val & 0xff00) >> 8 | (val & 0xff) << 8;
#endif     
    if(fwrite(&val, 2L, 1L, ofp) != 1L)
	longjmp(wr_error, 1L);
}

/*
 * Write a specified number of zero bytes to the open stream.  Usually
 * used to pad a field to a 4-byte boundary.  A write error is indicated 
 * by jumping to the 'wr_error' jmp_buf which must be initialized by the
 * caller (or caller's caller etc.).
 *
 */
static void
write_padding(FILE *ofp, register int nr_pad)
{
    while(nr_pad--)
	if(fputc(0, ofp) == EOF)
	    longjmp(wr_error, 1L);
}

/*
 * Write a netCDF format string to the open stream.  The string is
 * preceeded by a 4-byte length value and is padded to a 4-byte
 * boundary.
 */
static void
write_string(FILE *ofp, const char *str)
{
    int32	n = strlen(str);

    /*
    ** Write the length
    */
    write_long(ofp, n);

    /*
    ** Write the string contents.
    */
    if(fwrite(str, 1L, n, ofp) != n)
	longjmp(wr_error, 1L);

    /*
    ** Pad to the next 4-byte boundary.
    */
    n &= 3;
    if(n > 0)
	write_padding(ofp, (int)(4 - n));
}

/*
 * Write an array of netCDF attributes to an open stream.  An attribute
 * is a name/value pair.
 */
static void
write_attributes(FILE *ofp, register struct attr *attr, int32 n)
{
    register int32	i;
    union 
    {
	float	f;
	int32	l[2];
	int16	s;
	double	d;
    } val;
      
    write_long(ofp, NC_ATTRIBUTE);
    write_long(ofp, n);
    
    for(i = 0;i < n;i++,attr++)
    {
	write_string(ofp, attr->name);
	write_long(ofp, attr->type);
	switch(attr->type)
	{
	    case NC_CHAR:
		write_string(ofp, attr->value);
		break;
	    case NC_FLOAT:
		sscanf(attr->value, "%f", &val.f);
		write_long(ofp, 1L);
		write_long(ofp, val.l[0]);
		break;
	    case NC_DOUBLE:
		sscanf(attr->value, "%lf", &val.d);
		write_long(ofp, 1L);
#ifdef SWAP_BYTES
		write_long(ofp, val.l[1]);
		write_long(ofp, val.l[0]);
#else
		write_long(ofp, val.l[0]);
		write_long(ofp, val.l[1]);
#endif
		break;
	    case NC_INT:
		sscanf(attr->value, "%ld", &val.l[0]);
		write_long(ofp, 1L);
		write_long(ofp, val.l[0]);
		break;
	}
	
    }
}

/*
 * Write a pair of 4-byte zeros to the open stream.  This construct is
 * used to indicate that a variable, dimension, or attribute array is
 * not present in the netCDF header.
 */
static void
write_absent(FILE *ofp)
{
    write_long(ofp, 0L);
    write_long(ofp, 0L);
}

/*
 * Update the record count of an open netCDF file.  The record count
 * is a 4-byte value starting at offset 4.
 */
static void
write_record_count(FILE *ofp, uint32 nrecs)
{
    int32	pos;
    
    fflush(ofp);
    pos = ftell(ofp);
    if(fseek(ofp, 4L, SEEK_SET) < 0)
	fseek(ofp, 4L, SEEK_SET);
    
    write_long(ofp, nrecs);
    fflush(ofp);
    if(fseek(ofp, pos, SEEK_SET) < 0)
	fseek(ofp, pos, SEEK_SET);
}

/*
 * Determine the size of a netCDF data type.
 */
static int32
sizeof_nc_type(int32 type)
{
    int32	size = 0;
    
    switch(type)
    {
	case NC_BYTE:
	    size = 1;
	    break;
	case NC_SHORT:
	    size = 2;
	    break;
	case NC_INT:
	case NC_FLOAT:
	    size = 4;
	    break;
	case NC_DOUBLE:
	    size = 8;
	    break;
    }

    return size;
}

/*
 * write_nc_header - write a netCDF header to a file.
 * @ofp: pointer to open output file
 * @dims: array of dimension descriptors
 * @ndims: number of dimensions (assumed to be > 0)
 * @vars: array of variable descriptors
 * @nvars: number of variables (assumed to be > 0)
 * @desc: descriptive string which will be associated
 *			with the global 'source' attribute.
 *
 * Returns record size if successful, otherwise 0.
 */
long
write_nc_header(FILE *ofp, struct dim *dims, int32 ndims, struct var *vars,
		int32 nvars, const char *desc)
{
    int32		datastart, begin, vsize;
    long		rsize, non_rsize;
    register int32	i, j;
    struct attr		globals[2];

    /*
    ** Use setjmp/longjmp to avoid having to check return codes from
    ** every call to write_long/write_string.
    */
    if(setjmp(wr_error) != 0)
	return 0;
    
    write_long(ofp, NC_MAGIC);		/* magic number */
    write_long(ofp, 0L);		/* record count (fill in later) */

    /*
    ** Dimension array.
    */
    write_long(ofp, NC_DIMENSION);
    write_long(ofp, ndims);
    for(i = 0;i < ndims;i++)
    {
	write_string(ofp, dims[i].name);
	write_long(ofp, dims[i].length);
    }

    /*
    ** Global attributes
    */
    globals[0].name = "revision";
    globals[0].type = NC_CHAR;
    globals[0].value = "$Revision: 5f2cb9a04677 $";
    
    if(desc)
    {
	globals[1].name = "source";
	globals[1].type = NC_CHAR;
	globals[1].value = desc;
	write_attributes(ofp, globals, 2L);
    }
    else
	write_attributes(ofp, globals, 1L);
    
    /*
    ** Variable descriptions.
    */
    rsize = non_rsize = 0L;
    write_long(ofp, NC_VARIABLE);
    write_long(ofp, nvars);
    for(i = 0;i < nvars;i++)
    {
	register struct var	*v;
	
	v = &vars[i];
	write_string(ofp, v->name);
	write_long(ofp, (long)v->ndims);
	vsize = sizeof_nc_type(v->type);

	/*
	** Write the dimension indicies and calculate the VSIZE
	** parameter for this variable.
	*/
	v->isrec = 0;
	for(j = 0;j < v->ndims;j++)
	{
	    write_long(ofp, v->dim_id[j]);
	    if(dims[v->dim_id[j]].length > 0)
		vsize *= dims[v->dim_id[j]].length;
	    else
		v->isrec = 1;
	}
	
	/*
	** Record offset of this variable.
	*/
	if(v->isrec)
	    v->offset = rsize;
	
	/* Roundup VSIZE to next 4-byte boundary */
	v->vsize = (vsize + 3) & ~3;
	
	if(v->nattrs > 0)
	    write_attributes(ofp, v->attr, (long)v->nattrs);
	else
	    write_absent(ofp);
	write_long(ofp, v->type);
	write_long(ofp, v->vsize);

	if(v->isrec)
	    rsize += v->vsize;
	else
	    non_rsize += v->vsize;
	
	v->begin = ftell(ofp);	/* remember this position */
	write_long(ofp, 0L);		/* placeholder for BEGIN field */
    }

    begin = ftell(ofp);

    /*
    ** Reserve space for the non-record variables first.
    */
    if(non_rsize)
    {
	i = non_rsize/4;
	while(i--)
	    write_long(ofp, 0L);
    
	fflush(ofp);
    }
    
    datastart = ftell(ofp);
    
    /*
    ** Rewind and write the BEGIN fields for each variable.  The BEGIN
    ** field of the 'var' structure currently contains the file position
    ** of the BEGIN field in the netCDF header.  After this operation,
    ** the struct field and the header field will both contain the file
    ** position of the start of the data for the corresponding variable.
    */

    /*
    ** Non-record variables come first.
    */
    if(non_rsize)
    {
	for(i = 0;i < nvars;i++)
	{
	    if(vars[i].isrec)
		continue;
	    fseek(ofp, vars[i].begin, SEEK_SET);
	    write_long(ofp, begin);
	    vars[i].begin = begin;
	    begin += vars[i].vsize;
	}
    }
    
    for(i = 0;i < nvars;i++)
    {
	if(!vars[i].isrec)
	    continue;
	
	fseek(ofp, vars[i].begin, SEEK_SET);
	write_long(ofp, begin);
	vars[i].begin = begin;
	begin += vars[i].vsize;
    }
    
    fflush(ofp);
    fseek(ofp, datastart, SEEK_SET);
    
    return rsize;
}

/*
 * find_variable - return the index of a variable.
 * @vars: array of variable descriptions
 * @n: size of @vars
 * @name: variable name
 *
 * Returns array index of the named variable.
 */
static int32
find_variable(register struct var *vars, register int32 n, const char *name)
{
    while(n--)
	if(!strcmp(name, vars[n].name))
	    return n;
    return -1;
}

/*
 * Copy variable data into the record buffer.
 *
 */
int
add_to_record(void *data, int size, unsigned char *rec, struct var *vars, 
	      int32 n, const char *name)
{
    int32	offset, vsize, i;
    
    if((i = find_variable(vars, n, name)) == -1)
	return 0;

    if(!vars[i].isrec)
	return 0;
    
    offset = vars[i].offset;
    vsize  = vars[i].vsize;

#ifdef DEBUG_NETCDF
    fprintf(stderr, "%s starts at %ld (size = %ld)\n", name, offset, vsize);
#endif

    assert(offset >= 0);
    assert(size > 0);
    assert(size <= vsize);

#ifdef DEBUG_NETCDF
    fprintf(stderr, "copying %d bytes to 0x%lx\n", size, (uint32)(rec+offset));
#endif    
    memcpy(rec+offset, data, (long)size);
    if(vsize > size)
	memset(rec+offset+size, 0, vsize-size);

    return 1;
}

int
write_variable(FILE *ofp, void *data, int size, struct var *vars, int32 n, 
	       const char *name)
{
    int32	vsize, i;
    long	pos;
    int		r = 1;
    
    if((i = find_variable(vars, n, name)) == -1)
	return 0;

    vsize = vars[i].vsize;
    assert(size <= vsize);
    
    fflush(ofp);
    pos = ftell(ofp);
    fseek(ofp, vars[i].begin, SEEK_SET);
    if(fwrite(data, 1L, (long)size, ofp) != size)
	r = 0;
    fflush(ofp);
    fseek(ofp, pos, SEEK_SET);
    
    return r;
}

/**
 * flush_record - write the netCDF record to the output file.
 * @nd: pointer to netCDF record descriptor
 * @ofp: pointer to the netCDF output file.
 *
 * Flushes the filled output record to the open file.  This function also
 * updates the record count (see sync_file()) every %UPDATE_RC_INTERVAL times
 * that it is called.  Returns 1 if successful, otherwise 0.  
 */
int
flush_record(netCDFdesc *nd, FILE *ofp)
{
    if(setjmp(wr_error) != 0)
	return 0;

    assert(nd->size > 0);
    if(writerec(nd, ofp) && (fflush(ofp) == 0))
    {
	nd->count++;
#if 0
	if((nd->count % UPDATE_RC_INTERVAL) == 0)
	    write_record_count(ofp, nd->count);
#endif
	return 1;
    }
    
    return 0;
}

/**
 * sync_file - update data file record count.
 * @nd: pointer to netCDF record descriptor
 * @ofp: pointer to the netCDF output file.
 *
 * Synchronizes the output file by updating the record count field in
 * the file header.
 */
void
sync_file(netCDFdesc *nd, FILE *ofp)
{
    write_record_count(ofp, nd->count);
}

#ifdef WAVE_NETCDF
/**
 * write_wave_variable - write a wave-spectra variable.
 * @nd: pointer to netCDF record descriptor
 * @name: name of the variable
 * @data: pointer to data value
 * @size: size (in bytes) of the data value
 *
 * Write the value of a wave-spectra variable to the current
 * wave-spectra record.  Returns 1 if successful, otherwise 0.
 */
int
write_wave_variable(netCDFdesc *nd, const char *name, void *data, int size)
{
    assert(nd && nd->size > 0 && nd->vars > 0);
    return add_to_record(data, size, nd->data, nd->vars, 
			 sizeof(wave_vars)/sizeof(struct var), name);
}


/**
 * write_wave_header - write the netCDF header for a wave-spectra data file.
 * @ofp: pointer to the netCDF output file.
 * @desc: descriptive string for the file.
 * @ns: number of samples in each record.
 * @zfile: name of compressed data file.
 *
 * Writes the netCDF header for a wave-spectra data file to the output
 * file pointed to by @ofp.  The string @desc will be stored in the
 * 'source' attribute in the header.  Returns a pointer to a netCDF record 
 * descriptor if successful, otherwise NULL.  The pointer is allocated from 
 * the heap and the caller is responsible for freeing the pointer.
 *
 */
netCDFdesc*
write_wave_header(FILE *ofp, const char *desc, long ns)
{
    netCDFdesc	*nd;
    long	nbytes, total_size;
    
    if(ns > 0)
	wave_dims[0].length = ns;
    
    nbytes = write_nc_header(ofp, wave_dims, 
			     sizeof(wave_dims)/sizeof(struct dim),
			     wave_vars, sizeof(wave_vars)/sizeof(struct var),
			     desc);
    ALLOC_STRUCTURE(wave);

    return nd;
}
#endif /* WAVE_NETCDF */

#ifdef NOISE_NETCDF
/**
 * write_noise_variable - write an ambient noise variable.
 * @nd: pointer to netCDF record descriptor
 * @name: name of the variable
 * @data: pointer to data value
 * @size: size (in bytes) of the data value
 *
 * Write the value of an ambient noise variable to the current
 * ambient noise record.  Returns 1 if successful, otherwise 0.
 */
int
write_noise_variable(netCDFdesc *nd, const char *name, void *data, int size)
{
    assert(nd && nd->size > 0 && nd->vars > 0);
    return add_to_record(data, size, nd->data, nd->vars, 
			 sizeof(noise_vars)/sizeof(struct var), name);
}


/**
 * write_noise_header - write the netCDF header for a noise data file.
 * @ofp: pointer to the netCDF output file.
 * @desc: descriptive string for the file.
 * @ns: number of samples per record.
 *
 * Writes the netCDF header for an ambient noise data file to the output
 * file pointed to by @ofp.  The string @desc will be stored 
 * in the 'source' attribute in the header.  Returns a pointer to a netCDF 
 * record descriptor if successful, otherwise NULL.  The pointer is allocated 
 * from the heap and the caller is responsible for freeing the pointer.
 */
netCDFdesc*
write_noise_header(FILE *ofp, const char *desc, long ns)
{
    netCDFdesc	*nd;
    long	nbytes, total_size;

    if(ns > 0)
	noise_dims[1].length = ns;

    nbytes = write_nc_header(ofp, noise_dims, 
			     sizeof(noise_dims)/sizeof(struct dim),
			     noise_vars, sizeof(noise_vars)/sizeof(struct var),
			     desc);

    ALLOC_STRUCTURE(noise);
    
    return nd;
}
#endif /* NOISE_NETCDF */

/**
 * write_gps_variable - write a GPS variable.
 * @nd: pointer to netCDF record descriptor
 * @name: name of the variable
 * @data: pointer to data value
 * @size: size (in bytes) of the data value
 *
 * Write the value of a GPS variable to the current GPS record.  Returns 1 
 * if successful, otherwise 0.
 */
int
write_gps_variable(netCDFdesc *nd, const char *name, void *data, int size)
{
    assert(nd && nd->size > 0 && nd->vars > 0);
    return add_to_record(data, size, nd->data, nd->vars, 
			 sizeof(gps_vars)/sizeof(struct var), name);
}


/**
 * write_gps_header - write the netCDF header for a GPS data file.
 * @ofp: pointer to the netCDF output file.
 * @desc: descriptive string for the file.
 *
 * Writes the netCDF header for a GPS data file to the output
 * file pointed to by @ofp.  The string @desc will be stored 
 * in the 'source' attribute in the header.  Returns a pointer to a netCDF 
 * record descriptor if successful, otherwise NULL.  The pointer is allocated 
 * from the heap and the caller is responsible for freeing the pointer.
 */
netCDFdesc*
write_gps_header(FILE *ofp, const char *desc)
{
    netCDFdesc	*nd;
    long	nbytes, total_size;

    nbytes = write_nc_header(ofp, gps_dims, 
			     sizeof(gps_dims)/sizeof(struct dim),
			     gps_vars, sizeof(gps_vars)/sizeof(struct var),
			     desc);

    ALLOC_STRUCTURE(gps);
    
    return nd;
}

#ifdef FPR_NETCDF
/**
 * write_fpr_variable - write a fast-pressure variable.
 * @nd: pointer to netCDF record descriptor
 * @name: name of the variable
 * @data: pointer to data value
 * @size: size (in bytes) of the data value
 *
 * Write the value of a variable to the current record.  Returns 1 
 * if successful, otherwise 0.
 */
int
write_fpr_variable(netCDFdesc *nd, const char *name, void *data, int size)
{
    assert(nd && nd->size > 0 && nd->vars > 0);
    return add_to_record(data, size, nd->data, nd->vars, 
			 sizeof(fpr_vars)/sizeof(struct var), name);
}


/**
 * write_fpr_header - write the netCDF header for a fast-pressure data file.
 * @ofp: pointer to the netCDF output file.
 * @desc: descriptive string for the file.
 *
 * Writes the netCDF header for a fast-pressure data file to the output
 * file pointed to by @ofp.  The string @desc will be stored 
 * in the 'source' attribute in the header.  Returns a pointer to a netCDF 
 * record descriptor if successful, otherwise NULL.  The pointer is allocated 
 * from the heap and the caller is responsible for freeing the pointer.
 */
netCDFdesc*
write_fpr_header(FILE *ofp, const char *desc)
{
    netCDFdesc	*nd;
    long	nbytes, total_size;

    nbytes = write_nc_header(ofp, fpr_dims, 
			     sizeof(fpr_dims)/sizeof(struct dim),
			     fpr_vars, sizeof(fpr_vars)/sizeof(struct var),
			     desc);

    ALLOC_STRUCTURE(fpr);
    
    return nd;
}
#endif /* FPR_NETCDF */
