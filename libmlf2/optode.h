/*
** $Id: optode.h,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
*/
#ifndef _OPTODE_H_
#define _OPTODE_H_

#define OPTODE_DEVICE   9
#define OPTODE_BAUD     9600L

/**Optode data structure */
typedef struct optode_data {
    float       c1rph;            /**< blue phase */
    float       c2rph;            /**< red phase */
    float       c1amp;            /**< blue amplitude */
    float       c2amp;            /**< red amplitude */
    float       sat;              /**< air saturation */
    float       temp;             /**< in-situ temperature */
} OptodeV2;

typedef struct old_optode_data {
    float       oxy;            /**< oxygen concentration */
    float       sat;
    float       temp;           /**< in-situ temperature */
    float       bphase;         /**< phase angle */
} OptodeV1;

typedef struct {
    int         version;
    union {
        OptodeV1        v1;
        OptodeV2        v2;
    } data;
} OptodeData;

#define OPTODE_DATA_FIELDS      10

#define OPTODE_FIELD_C1RPH      5
#define OPTODE_FIELD_C2RPH      6
#define OPTODE_FIELD_C1AMP      7
#define OPTODE_FIELD_C2AMP      8
#define OPTODE_FIELD_SAT        1
#define OPTODE_FIELD_TEMP       2
#define OPTODE_FIELD_OXY        0
#define OPTODE_FIELD_BPHASE     4


int optode_init(void);
void optode_shutdown(void);
int optode_dev_ready(void);
void optode_test(void);
int optode_read_data(OptodeData *odp, long timeout);

#endif /* _OPTODE_H_ */
