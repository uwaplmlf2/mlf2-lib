/**@file
 * Driver for Wetlabs C-Star Transmissometer.
 *
 * $Id: cstar.c,v fc0b8d196869 2008/01/24 15:40:36 mikek $
 *
 */
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "ioports.h"
#include "serial.h"
#include "log.h"
#include "cstar.h"

#include "iofuncs.h"

#define WARM_UP_MSECS   10000L

#define START_CHAR      '\n'
#define END_CHAR        '\r'

static int serial_dev = -1;
static long init_time;

/**
 * Open connection to the device and initialize.
 *
 *
 * @return 1 if successful, otherwise 0.
 */
int
cstar_init(void)
{
    if(serial_dev >= 0)
        goto already_open;

    if((serial_dev = serial_open(CSTAR_DEVICE, 0, CSTAR_BAUD)) < 0)
    {
        log_error("cstar", "Cannot open serial device (code = %d)\n",
                  serial_dev);
        return 0;
    }

    init_time = MilliSecs();
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
 already_open:
    return 1;
}

/**
 * Shutdown the device interface and power off the device.
 */
void
cstar_shutdown(void)
{
    if(serial_dev >= 0)
    {
        serial_close(serial_dev);
        serial_dev = -1;
    }

}

/**
 * Check if device is ready.
 *
 * @return 1 if device is ready, otherwise 0.
 */
int
cstar_dev_ready(void)
{
    return (serial_dev >= 0) && ((MilliSecs() - init_time) > WARM_UP_MSECS);
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 */
void
cstar_test(void)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

/**
 * Read a data sample.
 *
 * @param  rval  pointer to returned data value
 * @param  timeout  read timeout in milliseconds.
 * @return 1 if sucessful, 0 on error.
 */
int
cstar_read_data(unsigned *rval, long timeout)
{
    long        t;
    int         c;
    unsigned    val;

    serial_inflush(serial_dev);

    t = MilliSecs();
    while((c = sgetc_timed(serial_dev, t, timeout)) != START_CHAR)
    {
        if(c < 0)
        {
            log_error("cstar", "Timeout waiting for record start\n");
            return 0;
        }
    }

    val = 0L;
    while((c = sgetc_timed(serial_dev, t, timeout)) != END_CHAR)
    {
        if(c < 0)
        {
            log_error("cstar", "Timeout reading data value\n");
            return 0;
        }

        if(isdigit(c))
            val = val*10 + (c - '0');
    }

    *rval = val;

    return 1;
}
