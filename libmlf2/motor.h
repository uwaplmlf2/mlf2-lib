/*
**$Id: motor.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _MOTOR_H_
#define _MOTOR_H_

/* Maximum ballast position in counts */
#ifndef BALLAST_MAX
#define BALLAST_MAX         5420670L    /* ~33.5 cm */
#endif

/* Minimum position */
#ifndef BALLAST_MIN
#define BALLAST_MIN         41100L      /* 0.254 cm */
#endif

/*
** Allowable position error in counts
*/
#ifndef POSITION_ERROR
#define POSITION_ERROR      64
#endif

/*
** TPU channel used by motor encoder.
*/
#define ENCODER_TPU         12
#define QDEC_PRI_TPU        8
#define QDEC_SEC_TPU        9

#define MOTOR_CURRENT_CHAN  9
#define MOTOR_VOLTAGE_CHAN  6


/*
** Return codes from 'motor_move'
*/
#define MTR_CURRENT_MAX     -1  /* maximum current exceeded */
#define MTR_TIMEOUT         -2  /* timed-out */
#define MTR_STALLED         -3  /* motor not moving */
#define MTR_UNKNOWN_POS     -4  /* motor not HOME'ed */
#define MTR_INTERRUPTED     -5  /* interrupt from keyboard */
#define MTR_USER_ABORT      -6  /* aborted by user callback function */
#define MTR_POS_ERROR       -7  /* home limit switch set during "normal"
                                   operation */

#define MTR_OK              1


long motor_home(long timeout);
long motor_pos(void);
long motor_move(long target, long err, long timeout,
       int (*func)(long, unsigned long));
double counts_to_cm(long);
long cm_to_counts(double);
void motor_set_position(long pos);
void motor_trace(int status);
double motor_energy(void);
double motor_speed(void);
long motor_overshoot(void);
int motor_move_async(long target, long interval, void (*callback)(long, int));
void motor_fwd(void);
void motor_rev(void);
void motor_stop(void);
void motor_sys_init(void);
void motor_sys_shutdown(void);
void motor_set_encoder(long cpt, long gr);
void motor_ignore_limit(void);
void motor_bump(long ms);
#endif
