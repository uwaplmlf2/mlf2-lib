/*
*/
#ifndef _VALEPORT_H_
#define _VALEPORT_H_

#define VP_MAX_CHARS    64
#define VP_MAX_FIELDS   8

typedef struct vprec {
    char        buf[VP_MAX_CHARS+1];
    char        *field[VP_MAX_FIELDS];
} VpRec;

int valeport_init(int devnum, long baud);
void valeport_shutdown(int sdev);
int valeport_dev_ready(int sdev);
int valeport_read_data(int sdev, VpRec *rec, long timeout);
void valeport_test(int sdev);

#endif /* _VALEPORT_H_ */
