/**@file
** SPI interface to the D/A converter.
**
**  - converter is addressed by chip-select 0 (CS0).
**  - clock (SCK) is active high.
**  - data is changed on the leading edge and captured on the following edge.
**  - CS0 and SCK must remain low between transfers.
*/
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8lib.h>

#define QSPI_BAUD       500000L

static
ushort basic_cmd = (M_CONT  & CLR)    /* CS driven by QPDR after transfer */
| (M_BITSE & SET)       /* bit count determined by BITS field in SPCR0 */
| (M_DT    & SET)       /* use SPCR1 DTL delay after xfer */
| (M_DSCK  & SET)       /* use SPCR1 DSCKL delay, SCK to PCS */
| (M_CS3   & SET)       /* PCS3, Max186 A/D, SET == disabled */
| (M_CS2   & CLR)       /* PCS2, PICpr, CLR == disabled */
| (M_CS1   & CLR)       /* PCS1, not used */
| (M_CS0   & SET);      /* PCS0, D/A, SET == enabled */

/*
 * Initialize the QSPI interface.
 *
 * @param  baud  serial clock baud rate
 * @param  clkdelay  delay between chip-select valid and clock transition (usecs)
 * @param  cmdinterval  interval between commands (usecs)
 *
 *
 */
static void
qspi_setup(long baud, long clkdelay, long cmdinterval)
{
    int                 dsckl, dtl;
    unsigned            rate_div;
    ulong               fclk = SimGetFSys();

    /* Convert timing parameters from microseconds */
    dsckl = (ulong)clkdelay*fclk/1000000L;
    dtl = (ulong)cmdinterval*fclk/(32L*1000000L);

    /*
    ** D/A uses CS0.
    **
    ** Output values of GP-I/O pins AND the state of the chip-select lines
    ** between QSPI transfers.  PCS0 and SCK must remain low between transfers.
    */
    *QPDR = M_PCS3;  // Keep CS3 high to disable Max186 ADC

    /* QSPI needs MOSI, MISO, and chip-select 0 */
    *QPAR |= (M_MOSI | M_MISO | M_PCS0);
    /* All chip-selects, MOSI and SCK are outputs */
    *QDDR = M_PCS3 | M_PCS2 | M_PCS1 | M_PCS0 | M_SCK | M_MOSI;


    /*
    ** Specify baud rate and set Master Mode. Clock is active high, data is
    ** changed on the leading edge and captured on the following edge.
    */
    rate_div = SimGetFSys()/(2*baud);
    *SPCR0 = rate_div | (M_MSTR & SET) | (M_CPOL & CLR) | (M_CPHA & SET);

    /* Disable Loop Mode, HALTA interrupts, and Halt */
    *SPCR3 = 0;

    /* Set delays */
    *SPCR1 = ((dsckl & 0x7f) << 8) | (dtl & 0xff);
}

/*
 * Transmit a single word to the slave device.
 */
static int
xfer_single(short data, int get_response)
{
    int         endqp;

    endqp = 0;

    SPIXMT[0] = data;
    SPICMD[0] = basic_cmd;
    if(get_response)
    {
        SPIXMT[1] = 0;
        SPICMD[1] = basic_cmd;
        endqp = 1;
    }

    *SPCR2 = (M_WREN & CLR) |
      (M_WRTO & CLR) |
      (endqp << 8) | 0x0;

    *SPSR &= ~M_SPIF;   /* clear flag */
    *SPCR1 |= M_SPE;    /* enable QSPI transfer */
    /* wait for pointer to reach ENDQP */
    while(!(*SPSR & M_SPIF))
        ;
    return get_response ? (int)SPIRCV[1] : 1;
}

/**
 * Output the specified value to the D/A converter.
 *
 * @param  value  output value in counts (0 - 4095)
 */
void
da_output(short value)
{
    qspi_setup(QSPI_BAUD, 10L, 500L);
    (void)xfer_single(value & 0xfff, 0);
}
