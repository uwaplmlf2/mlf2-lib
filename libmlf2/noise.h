/*
** $Id: noise.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _NOISE_H_
#define _NOISE_H_

#include "netcdf.h"

#define LOW_GAIN_CHAN	10
#define HIGH_GAIN_CHAN	11

#define FFTLEN_EX	10
#define FFTLEN		(1L << FFTLEN_EX)
#define BIN_SIZE	10L
#define NR_BINS		(FFTLEN/(2*BIN_SIZE) + 1)

/*
** Nominal receiver sensitivity (dB)
*/
#define RECEIVER_SENS	160

int noise_sample(void);
FILE* noise_open_file(const char *fname);
void noise_close_file(FILE *fp);
int noise_write_psd(FILE *ofp);
int noise_write_psd2(FILE *ofp, netCDFdesc *nd);
void noise_init_tables(void);
void noise_clear_psd(void);
int noise_get_psd(char *buf);
int noise_raw_sample(int chan, short *buf, int np);
long noise_get_fft_time(void);
void noise_init(long t_warmup);
int noise_dev_ready(void);
void noise_shutdown(void);

#endif
