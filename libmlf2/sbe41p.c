#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "sbe.h"
#include "sbe41p.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define MAX_DEVS    2

// Maintain the state of each device
typedef struct devstate {
    int         sd; /**< serial device descriptor */
    int         ref; /**< reference count */
    int         profile_active; /**< in profile mode */
} devstate_t;
static devstate_t dstab[MAX_DEVS];

typedef struct {
    float   p;
    float   t;
    float   s;
} PTS_t;

/*
 * Various response parsing functions used as call-backs
 * to sbe_command
 */
static CBstatus_t
parse_pts(void *obj, char *linebuf)
{
    PTS_t *psample = (PTS_t*)obj;
    if(sscanf(linebuf, "%f, %f, %f", &psample->p, &psample->t, &psample->s) != 3)
    {
        log_error("sbe41p", "Error parsing PTS data: (%s)\n", linebuf);
        return CB_ERROR;
    }

    return CB_OK;
}

static CBstatus_t
parse_pt(void *obj, char *linebuf)
{
    PTS_t *psample = (PTS_t*)obj;
    if(sscanf(linebuf, "%f, %f", &psample->p, &psample->t) != 2)
    {
        log_error("sbe41p", "Error parsing PTS data: (%s)\n", linebuf);
        return CB_ERROR;
    }

    return CB_OK;
}

static CBstatus_t
parse_pr(void *obj, char *linebuf)
{
    float *p = (float*)obj;
    if(sscanf(linebuf, "%f", p) != 1)
    {
        log_error("sbe41p", "Error parsing FP data: (%s)\n", linebuf);
        return CB_ERROR;
    }

    return CB_OK;
}

static CBstatus_t
parse_int(void *obj, char *linebuf)
{
    long        *ip = (long*)obj;
    CBstatus_t  rval;

    rval = CB_WAIT_PROMPT;
    if(sscanf(linebuf, "%ld", ip) != 1)
    {
        log_error("sbe41p", "Error parsing response: (%s)\n", linebuf);
        rval |= CB_ERROR;
    }
    return rval;
}

/**
 * sbe41p_init - initialize the interface.
 */
int
sbe41p_init(int which)
{
    devstate_t  *dsp;

    dsp = &dstab[which];
    if(dsp->ref > 0)
        return 1;

    if((dsp->sd = sbe_init(SBE41P_DEVICE(which), SBE41P_BAUD)) < 0)
        return 0;

    DelayMilliSecs(2000L);
    if(sbe_command(dsp->sd, "tswait=2\r", 3000L, NULL, NULL) == 0)
        log_error("sbe41p", "No response to \"tswait\" command\n");

    dsp->ref = 1;

    return 1;
}

/**
 * sbe41p_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
sbe41p_shutdown(int which)
{
    if(dstab[which].ref == 0)
        return;
    dstab[which].ref = 0;
    sbe_shutdown(dstab[which].sd);
}

/**
 * sbe_dev_ready - check if the device is ready for commands.
 *
 * @return 1 (yes) or 0 (no)
 */
int
sbe41p_dev_ready(int which)
{
    if(dstab[which].ref)
        return sbe_dev_ready(dstab[which].sd);
    else
        return 0;
}

/**
 * sbe41p_test - pass through mode.
 *
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 *
 */
void
sbe41p_test(int which)
{
    if(dstab[which].ref)
        sbe_test(dstab[which].sd);
}

/**
 * sbe41p_isprofiling - return non-zero if profiling.
 *
 * @param which     device index, 0 or 1
 */
int
sbe41p_isprofiling(int which)
{
    return dstab[which].profile_active;
}

/**
 * sbe41p_sample - read a single CTD sample
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @param *ctd      if non-NULL will contain returned temperature and salinity
 * @param *p        if non-NULL will contain returned pressure
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41p_sample(int which, long timeout, NewCTDdata *ctd, float *p)
{
    PTS_t       pts;
    int         rval;
    devstate_t  *dsp = &dstab[which];

    rval = 0;
    if(sbe41p_isprofiling(which))
    {
        log_error("sbe41p", "Sample attempted during profile\n");
        return rval;
    }

    if(ctd != NULL)
    {
        memset(&pts, 0, sizeof(pts));
        rval = sbe_command(dsp->sd, "pts\r", timeout, parse_pts, &pts);
        if(rval)
        {
            ctd->t = pts.t;
            ctd->s = pts.s;
            if(p != NULL)
                *p = pts.p;
        }
    }
    else if(p != NULL)
    {
        rval = sbe_command(dsp->sd, "fp\r", timeout, parse_pr, p);
    }

    return rval;
}

/**
 * sbe41_slpt - return the most recent pressure and temperature
 *
 * Return the most recent temperature and pressure acquired during a profile.
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @param *ctd      if non-NULL will contain returned temperature
 * @param *p        if non-NULL will contain returned pressure
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41p_slpt(int which, long timeout, NewCTDdata *ctd, float *p)
{
    PTS_t       pts;
    int         rval;
    devstate_t  *dsp = &dstab[which];

    if(!sbe41p_isprofiling(which))
    {
        log_error("sbe41p", "Profile not started\n");
        return 0;
    }

    memset(&pts, 0, sizeof(pts));
    rval = sbe_command(dsp->sd, "slpt\r", timeout, parse_pt, &pts);
    if(ctd != NULL)
        ctd->t = pts.t;
    if(p != NULL)
        *p = pts.p;

    return 1;
}

/**
 * sbe41p_start_profile - start profile mode.
 *
 * In profile mode, the CTD samples temperature, conductivity, and pressure
 * at 1 Hz and stores the values internally.
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41p_start_profile(int which, long timeout)
{
    int         rval = 1;
    devstate_t  *dsp = &dstab[which];

    if(sbe_command(dsp->sd, "outputrt=n\r", timeout, NULL, NULL) == 0)
        log_error("sbe41p", "No response to \"outputrt=n\" command\n");

    if(sbe_command(dsp->sd, "startprofile\r", timeout, NULL, NULL) > 0)
    {
        dsp->profile_active = 1;
    }
    else
    {
        rval = 0;
        log_error("sbe41p", "No response to \"startprofile\" command\n");
    }

    return rval;
}

/**
 * sbe41p_upload - upload data recors from the latest profile.
 *
 * @param which         device index, 0 or 1
 * @param start         starting scan
 * @param end           ending scan
 * @param callback      function to process each scan
 * @param cbdata        opaque data pointer passed to callback
 * @returns number of uploaded samples, -1 on error.
 */
int
sbe41p_upload(int which, int start, int end,
           void (*callback)(void *obj, char *linebuf),
           void *cbdata)
{
    devstate_t  *dsp = &dstab[which];

    if(dsp->profile_active)
        return 0;

    return sbe_upload(dsp->sd, start, end, callback, cbdata);
}

/**
 * sbe41p_stop_profile - stop profile mode.
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41p_stop_profile(int which, long timeout)
{
    devstate_t  *dsp = &dstab[which];

    if(!dsp->profile_active)
        return 1;
    dsp->profile_active = 0;
    return sbe_command(dsp->sd, "stopprofile\r", timeout, NULL, NULL);
}

/**
 * sbe41p_start_pump - run the pump.
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41p_start_pump(int which, long timeout)
{
    devstate_t  *dsp = &dstab[which];

    return sbe_command(dsp->sd, "pumpon\r", timeout, NULL, NULL);
}

/**
 * sbe41p_stop_pump - turn off the pump
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41p_stop_pump(int which, long timeout)
{
    devstate_t  *dsp = &dstab[which];

    return sbe_command(dsp->sd, "pumpoff\r", timeout, NULL, NULL);
}
