/**@file
 * Driver module for the SUNA (Submersible Ultraviolet Nitrate Analyzer).
 *
 * This driver has fairly limited capabilities. It can power the device on,
 * start the sampling process, and read the data records. No device
 * configuration is done.
 *
 * IMPORTANT:
 * Before using this driver, insure that the SUNA is configured to operate in
 * triggered mode and to output "full binary" data frames to the serial
 * connection

 */
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "lpsleep.h"
#include "ioports.h"
#include "serial.h"
#include "log.h"
#include "suna.h"

#include "iofuncs.h"

#define PURGE_INPUT(d, t) while(sgetc_timed(d, MilliSecs(), (t)) != -1)

/** Start-up time in milliseconds */
#define WARM_UP_MSECS   2000L
/** Sampling time in milliseconds */
#define SUNA_SAMPLE_MS  21000L
/** Polled-mode prompt */
#define SUNA_PROMPT     "CMD?"

static int serial_dev = -1;
static long init_time;
static unsigned long t_sample;
static unsigned long t_start;

static void
suna_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
}

/**
 * Open connection to the device and initialize. Errors are logged.
 *
 *
 * @return 1 if successful, otherwise 0.
 */
int
suna_init(void)
{
    if(serial_dev >= 0)
        goto already_open;

    if((serial_dev = serial_open(SUNA_DEVICE, 0, SUNA_BAUD)) < 0)
    {
        log_error("suna", "Cannot open serial device (code = %d)\n",
                  serial_dev);
        return 0;
    }

    init_time = MilliSecs();
    t_sample = 0;

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode. This will allow the device to remain
    ** open across calls to lpsleep.
    */
    add_after_hook("SUNA-reopen", suna_reopen);

 already_open:
    return 1;
}

/**
 * Shutdown the device interface and power off the device.
 */
void
suna_shutdown(void)
{
    if(serial_dev >= 0)
    {
        remove_after_hook("SUNA-reopen");
        serial_close(serial_dev);
        serial_dev = -1;
    }

}


/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
suna_dev_ready(void)
{
    return (serial_dev >= 0) && serial_chat(serial_dev, "STAT\n", SUNA_PROMPT, 3L);
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 */
void
suna_test(void)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

int
suna_start(void)
{
    PURGE_INPUT(serial_dev, 500L);

    sputz(serial_dev, "STRT\n");
    t_sample = MilliSecs();
    return 1;
}

/**
 * Test whether the data sample is ready. The SUNA requires approximately
 * 20 seconds of warm-up time for the lamp before it begins outputting
 * light data frames.
 *
 * @return true if data is ready to be sampled.
 */
int
suna_data_ready(void)
{
    return (MilliSecs() - t_sample) >= SUNA_SAMPLE_MS;
    return 1;
}

/**
 * Return the sample start time.
 *
 */
unsigned long
suna_start_time(void)
{
    return t_start;
}

/**
 * Read a data sample.
 *
 * @param  idp  pointer to returned data.
 * @param  timeout  read timeout in ms.
 *
 * @return bitmask listing the data frames read (should be 0x03)
 */
int
suna_read_data(SunaData *idp, long timeout)
{
    unsigned long       t;
    int                 c, frames, i;
    long                n, remaining;
    unsigned char       hdr[SUNA_HDRLEN];
    enum {ST_START=0,
          ST_HDR_CHK,
          ST_READ_DARK,
          ST_READ_LIGHT} state;

    t_start = RtcToCtm();

    memset(idp, 0, sizeof(SunaData));

    memcpy(&idp->dark[0], SUNA_DARK_HEADER, SUNA_HDRLEN);
    memcpy(&idp->light[0], SUNA_LIGHT_HEADER, SUNA_HDRLEN);

    frames = 0;
    i = 0;
    remaining = SUNA_BINARY_FULL_SIZE;
    state = ST_START;
    t = MilliSecs();
    while(frames != SUNA_BOTH_FRAMES &&
          (c = sgetc_timed(serial_dev, t, timeout)) >= 0)
    {
        switch(state)
        {
            case ST_START:
                i = 0;
                remaining = SUNA_BINARY_FULL_SIZE;
                memset(hdr, 0, sizeof(hdr));
                state = ST_HDR_CHK;
                hdr[i++] = c;
                break;
            case ST_HDR_CHK:
                hdr[i++] = c;
                if(i == SUNA_HDRLEN)
                {
                    if(!memcmp(hdr, SUNA_DARK_HEADER, SUNA_HDRLEN))
                        state = ST_READ_DARK;
                    else if(!memcmp(hdr, SUNA_LIGHT_HEADER, SUNA_HDRLEN))
                        state = ST_READ_LIGHT;
                    else
                    {
                        /* shift the header buffer left */
                        memmove(&hdr[0], &hdr[1], i-1);
                        i -= 1;
                    }
                }
                break;
            case ST_READ_DARK:
                idp->dark[i++] = c;
                remaining -= 1;
                log_event("Reading dark data frame\n");
                n = serial_read(serial_dev, (char*)&idp->dark[i],
                                remaining);
                log_event("Read %ld bytes\n", n);
                i += n;
                remaining -= n;
                if(remaining == 0)
                {
                    frames |= SUNA_DARK_FRAME;
                    state = ST_START;
                }
                break;
            case ST_READ_LIGHT:
                idp->light[i++] = c;
                remaining -= 1;
                log_event("Reading light data frame\n");
                n = serial_read(serial_dev, (char*)&idp->light[i],
                                remaining);
                log_event("Read %ld bytes\n", n);
                i += n;
                remaining -= n;
                if(remaining == 0)
                {
                    frames |= SUNA_LIGHT_FRAME;
                    state = ST_START;
                }
                break;
        }

    }

    sputz(serial_dev, "STOP\n");

    return frames;
}
