/*
** $Id: inertial.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _INERTIAL_H_
#define _INERTIAL_H_

#define INERTIAL_DEVICE		2
#define INERTIAL_BAUD		38400L
#define INERTIAL_TIMEOUT	5

/*
** Command codes for inertial_dev_control.
*/
#define INERTIAL_CALIBRATE	1
#define INERTIAL_USEVOLTS	2
#define INERTIAL_USESCALED	3

typedef struct _tuple3d {
    short	x;
    short	y;
    short	z;
} Tuple3d;

typedef struct _ftuple3d {
    float	x;
    float	y;
    float	z;
} FTuple3d;

typedef struct _inertial_data {
    long	timestamp;
    Tuple3d	ang_rate;
    Tuple3d	accel;
    short	temp;
    short	t;
    char	type;
} InertialData;

int inertial_init(void);
void inertial_shutdown(void);
int inertial_dev_ready(void);
int inertial_read_data(InertialData *id);
int inertial_dev_control(int cmd);
void cvt_ang_rate(InertialData *id, FTuple3d *ar);
void cvt_accel(InertialData *id, FTuple3d *accel);
void inertial_get_stats(unsigned long *errs, unsigned long *reads);

#endif


