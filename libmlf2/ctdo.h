/**@file
** $Id: ctdo.h,v 724d1829be81 2007/11/05 06:03:25 mikek $
*/
#ifndef _CTDO_H_
#define _CTDO_H_

#define CTDO_DEVICE	7
#define CTDO_MINOR	0
#define CTDO_BAUD	9600L
#define CTDO_TIMEOUT	5L


/** Sample times (seconds) */
#define CTDO_SAMPLE_TIME	3	/* T and S only */
#define CTDO_OXY_SAMPLE_TIME	24	/* T, S, and Oxygen */

/** CTDO data record */
typedef struct ctdo {
    float		t;		/**< temperature (degrees) */
    float		s;		/**< salinity (ppt) */
    long		oxy;		/**< dissolved oxygen (freq counts)*/
} CTDOdata;

int ctdo_init(void);
void ctdo_shutdown(void);
int ctdo_dev_ready(void);
long ctdo_start_pump(unsigned slow, unsigned fast, unsigned off, unsigned ncycles);
int ctdo_cancel_pump(void);
int ctdo_start_oxy(void);
int ctdo_stop_oxy(void);
int ctdo_start_sample(double p);
int ctdo_data_ready(void);
int ctdo_read_data(CTDOdata *ctdo);
void ctdo_get_stats(unsigned long *errs, unsigned long *reads);
void ctdo_test(void);
int ctdo_cmd(const char *cmd);
unsigned long ctdo_start_time(void);
int ctdo_wakeup(void);
#endif
