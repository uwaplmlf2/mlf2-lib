/*
**
*/
#ifndef _ECOPAR_H_
#define _ECOPAR_H_

#define ECOPAR_DEVICE	4
#define ECOPAR_BAUD	19200L

/* Time required to close shutter (ms) */
#define ECOPAR_SHUTTER_WAIT_MS  4000L

int ecopar_init(void);
void ecopar_shutdown(void);
int ecopar_dev_ready(void);
int ecopar_read_data(long timeout);
int ecopar_close_shutter(void);
void ecopar_test(void);

#endif /* _ECOPAR_H_ */
