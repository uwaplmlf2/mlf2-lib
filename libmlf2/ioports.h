/**@file
** $Id: ioports.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _IOPORTS_H_
#define _IOPORTS_H_

#define	REAL_INPUT	0x80
#define ADDR_MASK	0x7f

/** Port constants */
#define IO_A		0
#define IO_B		1
#define IO_C		2
#define IO_D		3
#define IO_E		4
#define IO_F		5
#define IO_A_REAL	(IO_A | REAL_INPUT)

void init_ioports(void);
unsigned char iop_read(int port);
void iop_write(int port, unsigned char value);
void iop_set(int port, unsigned char mask);
void iop_clear(int port, unsigned char mask);
void iop_clear_all(void);

/** Macro to stroke the watchdog via Port E bit 7 */
#define PET_WATCHDOG()	do { iop_set(IO_E, (unsigned char)0x80);\
                             LMDelay(2000);\
                             iop_clear(IO_E, (unsigned char)0x80);\
                           } while(0);

/** Force the watchdog to blow the explosive bolt */
#define EMERGENCY_ABORT() do { iop_set(IO_F, (unsigned char)0x10);\
                               DelayMilliSecs(5100);\
                               iop_clear(IO_F, (unsigned char)0x10);\
                             } while(0);

#endif
