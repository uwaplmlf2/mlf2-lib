/**@file
** Driver for Nortek ADV.
**
** arch-tag: 90e93461-492c-4386-af22-13eac3ebc760
**
**
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include <setjmp.h>
#include "salarm.h"
#include "serial.h"
#include "log.h"
#include "lpsleep.h"
#include "ioports.h"
#include "nortekadv.h"

#define ACK	0x0606
#define NAK	0x1515

#define ADV_INIT_TIMEOUT	10L
#define WARM_UP_MSECS		2000L

#define BCD(x)\
    ({short __ones = (x)%10, __tens = (x)/10;	\
	(__tens << 4) | __ones;})

#define DEC(x)\
    ({short __ones = (x) & 0xf, __tens = (x) >> 4;\
        (__tens*10) + __ones;})

typedef struct {
    char*		code;
    int			param_len;
    int			resp_len;
} COMMAND_t;

enum {CMD_RC=0, CMD_SC, CMD_INQ, CMD_GA,
      CMD_GC, CMD_BV, CMD_PD, CMD_ID, CMD_SD, CMD_MC};

static COMMAND_t nor_cmds[] = {
    [CMD_RC] = {"RC", 0, 6},
    [CMD_SC] = {"SC", 6, 0},
    [CMD_INQ] = {"II", 0, 2},
    [CMD_GA] = {"GA", 0, 784},
    [CMD_GC] = {"GC", 0, 512},
    [CMD_BV] = {"BV", 0, 4},
    [CMD_PD] = {"PD", 0, 0},
    [CMD_ID] = {"ID", 0, 14},
    [CMD_SD] = {"SD", 0, 0},
    [CMD_MC] = {"MC", 0, 0},
};

static int ref = 0;
static int serial_dev = -1;
static long init_time;
static unsigned long nr_reads, nr_errors;

static __inline__ int sgetc(int desc)
{
    unsigned char	c;
    return (serial_read(desc, &c, (size_t)1) == 1) ? (int)c : -1;
}

static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char	c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
	if((MilliSecs() - start) >= timeout)
	    return -1;
    }
    return ((int) c)&0xff;
}

static __inline__ void sputc(int desc, int out)
{
    char	c = out;
    
    serial_write(desc, &c, 1);
}

static void sputs(int desc, char *str)
{
    while(*str)
    {
	serial_write(desc, str, 1);
	str++;
    }
}



static int
send_command(COMMAND_t *cmdp, void *params, void *resp)
{
    unsigned short	status;
    long		t0;
    int			c, n, i;
    char		*rp, *p;
    
    serial_write(serial_dev, cmdp->code, 2L);
    if(cmdp->param_len)
    {
	DelayMilliSecs(100L);
	p = params;
	for(i = 0;i < cmdp->param_len;i++)
	    printf("%02x ", (int)p[i]);
	printf("\n");
	serial_write(serial_dev, params, cmdp->param_len);
    }
    
    if(cmdp->resp_len)
    {
	n = cmdp->resp_len;
	t0 = MilliSecs();
	rp = resp;
	while(n && (c = sgetc_timed(serial_dev, t0, 5000L)) != -1)
	{
	    *rp++ = c;
	    n--;
	}
	
	if(n != 0)
	    return 0;
    }

    t0 = MilliSecs();
    if((c = sgetc_timed(serial_dev, t0, 1500L)) == -1)
	return 0;
    status = c & 0xff;
    if((c = sgetc_timed(serial_dev, t0, 1500L)) == -1)
	return 0;
    status |= ((c & 0xff) << 8);

    return (status == ACK);
}

static int
soft_break(void)
{
    int		r;
    
    sputs(serial_dev, "@@@@@@");
    DelayMilliSecs(100L);
    r = serial_chat(serial_dev, "K1W%!Q", "firm:\006\006|mode\r\n\006\006", 3L);
    if(r == 1)
    {
	/* We interrupted data-mode, send confirmation code */
	return send_command(&nor_cmds[CMD_MC], NULL, NULL);
    }
    return r;
}


void
nor_reopen(void)
{
    iop_set(IO_E, 0x10);
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
}

void
nor_serial_off(void)
{
    iop_clear(IO_E, 0x10);
}

void
nor_serial_on(void)
{
    iop_set(IO_E, 0x10);
}

/**
 * Initialize Sontek ADV interface.
 * Open connection to ADV device and initialize. Errors are logged.
 *
 * @return 1 if successful, otherwise 0.
 */
int
nor_init(void)
{
    unsigned char	id[14];
    
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(NOR_DEVICE, 0, NOR_BAUD)) < 0)
    {
	log_error("adv", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    /* Force serial interface on */
    iop_set(IO_E, 0x10);
    
    init_time = MilliSecs();
    DelayMilliSecs(WARM_UP_MSECS);

    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to nor_sample_start and nor_read_data.
    */
    add_after_hook("ADV-reopen", nor_reopen);
    add_before_hook("ADV-close", nor_serial_off);

initdone:
    ref++;

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    soft_break();
    serial_inflush(serial_dev);
    
    if(!send_command(&nor_cmds[CMD_ID], NULL, id))
    {
	log_error("adv", "Device not responding\n");
	nor_shutdown();
	return 0;
    }

    id[13] = '\0';

    return 1;
}


/**
 * Shutdown the ADV interface.
 * Shutdown the ADV interface and power off the device.
 */
void
nor_shutdown(void)
{
    if(ref == 0)
	return;
    ref = 0;
    remove_after_hook("ADV-reopen");
    remove_before_hook("ADV-close");
    iop_clear(IO_E, 0x10);
    serial_close(serial_dev);
}


/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
nor_dev_ready(void)
{
    return ref;
}

/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
nor_test(void)
{
    printf("Pass-through mode: CTRL-c to exit, CTRL-b to send a BREAK\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}



/**
 * Fetch read error statistics.
 *
 * @param  errs  returned error count
 * @param  reads  returned read count.
 */
void
nor_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}

/**
 * Synchronize the ADV clock with the TT8.
 *
 * @return 1 if successful, 0 on error.
 */
int
nor_set_clock(void)
{
    struct tm	*tnow;
    time_t	t;
    char	params[48];

    //soft_break();
    serial_inflush(serial_dev);

    t = RtcToCtm();
    tnow = localtime(&t);
    params[0] = BCD(tnow->tm_min);
    params[1] = BCD(tnow->tm_sec);
    params[2] = BCD(tnow->tm_mday);
    params[3] = BCD(tnow->tm_hour);
    params[4] = BCD(tnow->tm_year % 100);
    params[5] = BCD(tnow->tm_mon+1);
    
    return send_command(&nor_cmds[CMD_SC], params, NULL);
}

/**
 * Read the ADV clock.
 *
 * @param  t  pointer to returned time structure.
 * @return 1 if successful, 0 on error.
 */
int
nor_read_clock(struct tm *t)
{
    char	resp[6];
    
    if(!send_command(&nor_cmds[CMD_RC], NULL, resp))
	return 0;
    
    t->tm_min = DEC(resp[0]);
    t->tm_sec = DEC(resp[1]);
    t->tm_mday = DEC(resp[2]);
    t->tm_hour = DEC(resp[3]);
    t->tm_year = DEC(resp[4]) + 100;
    t->tm_mon = DEC(resp[5]) - 1;

    return 1;
}

/**
 * Start the data collection process.
 *
 * @return 1 if successful, 0 on error.
 */
int
nor_start(void)
{
    soft_break();
    serial_inflush(serial_dev);
    return send_command(&nor_cmds[CMD_SD], NULL, NULL);
}

/**
 * Stop the data collection process.
 *
 * @return 1 if successful, 0 on error.
 */
int
nor_stop(void)
{
    return soft_break();
}

/**
 * Power the device down.
 *
 * @return 1 if successful, 0 on error.
 */
int
nor_powerdown(void)
{
    return send_command(&nor_cmds[CMD_PD], NULL, NULL);
}
