/**@file
 * Interface to the APL PAL-ARGO acoustic receiver board.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "lpsleep.h"
#include "serial.h"
#include "iofuncs.h"
#include "log.h"
#include "palargo.h"


#ifndef __GNUC__
#define __inline__
#endif

#define PURGE_INPUT(d, t) while(sgetc_timed(d, MilliSecs(), (t)) != -1)

/**< Time from power-on until device is ready for commands (ms) */
#define WARMUP_MS       8500L

/**< Delimiters for data records */
#define DATA_START  '$'
#define DATA_END    '\n'

static int      ref = 0;
static int      serial_dev;
static long     t_start;

static void
wakeup(void)
{
    sputc(serial_dev, ' ');
    DelayMilliSecs(50L);
    sputc(serial_dev, ' ');
    DelayMilliSecs(50L);
    PURGE_INPUT(serial_dev, 500L);
}

static void
palargo_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
}

/**
 * Initialize the device interface.
 */
int
palargo_init(void)
{
    if(ref > 0)
        goto initdone;

    if((serial_dev = serial_open(PALARGO_DEVICE, 0,
                                 PALARGO_BAUD)) < 0)
    {
        log_error("palargo", "Cannot open serial device (code = %d)\n",
                  serial_dev);
        return 0;
    }

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    t_start = MilliSecs();

    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.
    */
    add_after_hook("PALARGO-reopen", palargo_reopen);

initdone:
    ref++;
    return 1;
}

/**
 * Shutdown the device interface.  The device is powered off.
 */
void
palargo_shutdown(void)
{
    if(ref == 0)
        return;
    PURGE_INPUT(serial_dev, 200L);
    if(serial_chat(serial_dev, "PowerOff\r", ">", 4L) == 0)
        log_error("palargo", "No response to PowerOff command\n");
    ref = 0;
    remove_after_hook("PALARGO-reopen");
    serial_close(serial_dev);
}

/**
 * Check that the device is ready for commands.
 *
 * @returns 1 if ready, otherwise 0.
 */
int
palargo_check(void)
{
    if(ref == 0)
        return 0;

    PURGE_INPUT(serial_dev, 200L);

    if((MilliSecs() - t_start) >= WARMUP_MS)
    {
        wakeup();
        return serial_chat(serial_dev, "ok?\r", "ok?,ack\r\n", 3L);
    }

    return 0;
}

/**
 * Turn hydrophone on or off.
 *
 * @param state  1 (on) or 0 (off).
 * @returns 1 if successful, 0 on error.
 */
int
palargo_hydrophone(int state)
{
    if(ref == 0)
        return 0;

    PURGE_INPUT(serial_dev, 200L);

    if(state)
    {
        if(serial_chat(serial_dev, "hydro,on\r", "hydro,on,ack\r\n", 4L) == 0)
        {
            log_error("palargo", "No response to 'hydro,on' command\n");
            return 0;
        }
    }
    else
    {
        if(serial_chat(serial_dev, "hydro,off\r", "hydro,off,ack\r\n", 4L) == 0)
        {
            log_error("palargo", "No response to 'hydro,off' command\n");
            return 0;
        }
    }

    return 1;
}

/**
 * Synchronize the PAL-ARGO clock to the TT8 clock.
 *
 * @returns 1 if successful, 0 on error.
 */
int
palargo_sync_clock(void)
{
    long    t;
    char            cmd[32], resp[32];

    if(ref == 0)
        return 0;

    time(&t);
    sprintf(cmd, "time,%ld\r", t);
    sprintf(resp, "time,ack,%ld\r\n", t);

    PURGE_INPUT(serial_dev, 200L);
    if(serial_chat(serial_dev, cmd, resp, 4L) == 0)
    {
        log_error("palargo", "Cannot set clock\n");
        return 0;
    }

    return 1;
}

/**
 * Start the acoustic sampling process.
 *
 * @param store_rawdata  if non-zero, store the raw acoustic data
 * @returns 1 if successful, 0 on error.
 */
int
palargo_start_sample(int store_rawdata)
{
    char    *cmd, *response;

    if(ref == 0)
        return 0;

    if(store_rawdata)
    {
        cmd = "mlf2,1\r";
        response = "mlf2,ack,1\r\n";
    }
    else
    {
        cmd = "mlf2,0\r";
        response = "mlf2,ack,0\r\n";
    }

    PURGE_INPUT(serial_dev, 200L);
    if(serial_chat(serial_dev, cmd, response, 4L) == 0)
    {
        log_error("palargo", "No response to 'mlf2' command\n");
        return 0;
    }

    return 1;
}

/**
 * Read a PAL ARGO data sample.
 *
 * @param pdp  pointer to returned data
 * @param timeout  read timeout in milliseconds
 * @returns 1 if successful, 0 on error.
 */
int
palargo_read_sample(PalData *pdp, long timeout)
{
    long            t;
    int             c, i;
    char            *p;
    static char     linebuf[128];

    enum {ST_1=1,
          ST_2,
          ST_3,
          ST_DONE} state;

    if(ref == 0)
        return 0;

    t = MilliSecs();
    state = ST_1;
    i = 0;
    while(state != ST_DONE)
    {
        c = sgetc_timed(serial_dev, t, timeout);

        if(c < 0)
        {
            log_error("palargo", "Timeout waiting for record start\n");
            return 0;
        }

        switch(state)
        {
            case ST_1:
                if(c == DATA_START)
                    state = ST_2;
                break;
            case ST_2:
                i = 0;
                if(c == DATA_START)
                    state = ST_3;
                else
                    state = ST_1;
                break;
            case ST_3:
                if(c == DATA_END)
                {
                    state = ST_DONE;
                    linebuf[i] = '\0';
                }
                else
                {
                    if(i < (sizeof(linebuf) - 1))
                        linebuf[i++] = c;
                }
                break;
            case ST_DONE:
                break;
        }
    }

    p = strtok(linebuf, " \t");
    i = 0;
    while(p && i < PALARGO_FREQ_BINS)
    {
        pdp->spl[i++] = (float)atof(p);
        p = strtok(NULL, " \t");
    }
    pdp->nbins = i;

    /* Check for saturation flag */
    if(p)
        pdp->is_saturated = (unsigned)atoi(p);
    else
        pdp->is_saturated = 0;


    return i;
}

/**
 * Start and read a PAL ARGO data sample.
 *
 * @param pdp  pointer to returned data
 * @param timeout  read timeout in milliseconds
 * @param store_rawdata  if non-zero, store the raw acoustic data
 * @returns 1 if successful, 0 on error.
 */
int
palargo_sample(PalData *pdp, long timeout, int store_rawdata)
{
    if(palargo_start_sample(store_rawdata))
    {
        pdp->t = RtcToCtm();
        return palargo_read_sample(pdp, timeout);
    }

    return 0;
}
