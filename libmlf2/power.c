/**@file
** Power switch manager.
**
** Manage the power switches on the second-gen MLF2 board.
**
*/
#include <tt8.h>
#include <tt8lib.h>
#include "power.h"
#include "ioports.h"
#include "log.h"

/**Power-switch data structure */
typedef struct _pswitch {
    int			port;		/**< output port */
    int			state;		/**< reference counter */
    unsigned char	mask;		/**< port bit */
    unsigned long	ontime;		/**< timestamp of last power-on */
} PowerSwitch;

static PowerSwitch _switches[NSWITCHES] = {
[SERDEV_POWER1] = 	{ IO_A,	0,	0x01 },
[SERDEV_POWER2] = 	{ IO_A,	0,	0x02 },
[SERDEV_POWER3] = 	{ IO_B,	0,	0x08 },
[SERDEV_POWER4] = 	{ IO_B, 0,	0x10 },
[SERDEV_POWER5] = 	{ IO_B, 0,	0x20 },
[SERDEV_POWER6] = 	{ IO_B, 0,	0x40 },
[SERDEV_POWER7] = 	{ IO_A, 0,	0x04 },
[SERDEV_POWER8] = 	{ IO_B, 0,	0x01 },
[SERDEV_POWER9] = 	{ IO_A, 0,	0x08 },
[SERDEV_POWER10] = 	{ IO_A, 0,	0x10 },
[GPS_POWER] = 		{ IO_A,	0,	0x20 },
[COMM_POWER] = 		{ IO_B,	0,	0x04 },
[POS_POWER] = 		{ IO_A,	0,	0x80 },
[QSPIDEV_POWER1] = 	{ IO_B,	0,	0x80 },
[QSPIDEV_POWER3] = 	{ IO_A,	0,	0x40 },
[ANALOG_POWER] = 	{ IO_D, 0,	0x01 },
[ALGDEV_POWER1] = 	{ IO_D,	0,	0x04 },
[ALGDEV_POWER2] =	{ IO_D,	0,	0x08 },
[ALGDEV_POWER3] =	{ IO_D,	0,	0x10 },
[HIGHV_POWER] = 	{ IO_C, 0,	0x40 },
[LED_POWER] =           { IO_C, 0,	0x04 },
[ARGOS_POWER] =         { IO_C, 0,	0x08 },
};

/**
 * Activate a power line.
 * Switch on a specified power line.  Each line has an associated
 * reference count.  If this count is zero, the line is switched
 * on.  The count is then incremented.  n must be one of the following
 * constants defined in power.h.
 *
 *   - SERDEV_POWER* - power for serial devices.	
 *   - GPS_POWER - power for GPS device.	
 *   - COMM_POWER - modem power
 *   - POS_POWER - motor position sensor power
 *   - QSPIDEV_POWER* - QSPI device power.
 *   - ANALOG_POWER - +7 volt analog power.
 *   - ALGDEV_POWER* - power for analog devices.	
 *   - HIGHV_POWER - power from the higher voltage battery pack.
 *
 * @param  n  line ID code.
 *
 */
void 
power_on(int n)
{
    register PowerSwitch	*s = &_switches[n];
	
    if(n >= NSWITCHES || n < 0)
	return;
    
    if(s->state == 0)
    {
	iop_set(s->port, s->mask);
	s->ontime = MilliSecs();
    }

    s->state++;
}

/**
 * Deactivate a power line.
 * Switch off the specified power line.  The reference count is
 * decremented and if it has reached zero, the power is turned
 * off.  See power_on() for line IDs.
 *
 * @param  n  line ID code.
 */
void 
power_off(int n)
{
    register PowerSwitch	*s = &_switches[n];

    if(n >= NSWITCHES || n < 0 || s->state == 0)
	return;
    
    if(--s->state == 0)
	iop_clear(s->port, s->mask);
}

/**
 * When was power switch turn on?
 *
 * @param  n  line ID code.
 * @return the elapsed time in milliseconds since switch was
 * turned on.
 */
unsigned long
power_on_time(int n)
{
    register PowerSwitch	*s = &_switches[n];

    if(n >= NSWITCHES || n < 0 || s->state == 0)
	return 0;

    return (MilliSecs() - s->ontime);
}
