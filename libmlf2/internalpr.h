/*
** arch-tag: header for internal pressure module
*/
#ifndef _INTERNALPR_H_
#define _INTERNALPR_H_

#define IPR_CHANNEL	0	/* A/D channel */

int ipr_init(void);
void ipr_shutdown(void);
int ipr_dev_ready(void);
short ipr_read_data(void);
double ipr_mv_to_psi(int mv);

#endif
