/*
** 
*/
#ifndef _SPAR_H_
#define _SPAR_H_

typedef struct spdata {
    float   light;      /**< irradiance, units vary with sensor */
    float   temp;       /**< internal temperature in degC */
    float   volts;      /**< supply voltage */
} ParData;

int spar_init(const char *name, int devnum, long baud, long warmup_ms);
void spar_shutdown(int dev);
int spar_dev_ready(int dev);
int spar_read_data(int dev, ParData *pd, long timeout);
void spar_test(int dev);

#endif
