/**@file
 * Utility function to quote a string.
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "quote.h"


/**
 * Quote an ASCII string by replacing linefeeds with "\n", carriage
 * returns with "\r", and non-printing characters with their
 * value in hex, "\xXX".
 *
 * Returns the length of the quoted string.
 */
size_t
quote_string(const char *str, char *quoted, size_t n)
{
    const char  *p;
    char        *q;
    int         c, nw;
    size_t      limit = n - 6, i;

    p = str;
    q = quoted;
    *q++ = '"';
    i = 1;
    while(*p && i < limit)
    {
        c = *p;
        switch(c)
        {
            case '\n':
                *q++ = '\\';
                *q++ = 'n';
                i += 2;
                break;
            case '\r':
                *q++ = '\\';
                *q++ = 'r';
                i += 2;
                break;
            default:
                if(isprint(c))
                {
                    *q++ = c;
                    i++;
                }
                else
                {
                    nw = sprintf(q, "\\x%02x", c);
                    q += nw;
                    i += nw;
                }
                break;
        }
        p++;
    }

    // Append ellipsis to show that the content was trimmed
    if(*p != '\0')
    {
        strcpy(q, "...");
        q += 3;
        i += 3;
    }
    *q++ = '"';
    *q = '\0';

    return i+1;
}

/**
 * Quote an ASCII string to a file by replacing linefeeds with "\n",
 * carriage returns with "\r", and non-printing characters with their value
 * in hex, "\xXX".
 *
 */
void
quote_to_file(const char *str, FILE *fp)
{
    const char  *p;
    int         c;

    p = str;
    while(*p)
    {
        c = *p;
        switch(c)
        {
            case '\n':
                fputc('\\', fp);
                fputc('n', fp);
                break;
            case '\r':
                fputc('\\', fp);
                fputc('r', fp);
                break;
            default:
                if(isprint(c))
                    fputc(c, fp);
                else
                    fprintf(fp, "\\x%02x", c);
                break;

        }
        p++;
    }
    fprintf(fp, "'\n");
}
