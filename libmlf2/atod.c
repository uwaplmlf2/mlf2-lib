/**@file
** Interface to the "internal" A/D on the MLF2 board.
**
** $Id: atod.c,v 19ce53ee667a 2008/10/09 16:10:27 mikek $
**
** The standard TT8 8-channel A/D is connected to a 16-8 channel mux.  Port
** D1 is used to select between the upper/lower 8 channels.
**
**
*/
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <qsm332.h>
#include "ioports.h"
#include "power.h"
#include "adintf.h"
#include "adc.h"

static int ref = 0;
    
/**
 * Initialize the internal A/D.
 * Initialize the interface to the internal A/D.  On the MLF2 board, the
 * internal A/D has been extended with a multiplexor from 8 to 16 channels.
 */
void
atod_init(void)
{
    /* Clear the mux control bit */
    iop_clear(IO_D, (unsigned char)0x02);
    power_on(V7_POWER);
}

/**
 * Shutdown internal A/D.
 * Shutdown the internal A/D by switching off power to the mux.
 */
void
atod_shutdown(void)
{

    /*
    ** The mux control line must be low before we switch off the
    ** power to the mux.
    */
    iop_clear(IO_D, (unsigned char)0x02);
    power_off(V7_POWER);
}

/**
 * Read a sample from internal A/D.
 *
 * @param  chan  channel number, 0 <= chan <= 15
 * @return channel value in millivolts (0-4095) or -1 on error.
 */
short
atod_read(int chan)
{
    register short	val;
    
    if(chan & 0x08)
	iop_set(IO_D, (unsigned char)0x02);		/* upper 8 channels */
    else
	iop_clear(IO_D, (unsigned char)0x02);	/* lower 8 channels */

    val = read_ad(chan&0x07);
    
    return val;
}


/**
 * Read a series of samples from an A/D channel.
 * This function reads a series of samples from the A/D converter. Due to the 
 * method used to read the samples, n should be a multiple of 8.  The value 
 * of n will be truncated to the nearest multiple of 8 before sampling.
 *
 * @param  chan  A/D channel number (0 <= chan <= 15).
 * @param  bipolar  if non-zero, sample in bipolar mode.
 * @param  period  desired sampling period in microseconds.
 * @param  n  number of samples to read.  
 * @param  databuf buffer to hold the sampled data.
 * @returns actual sampling period or 0 on error.
 */
int
atod_read_buf(int chan, int bipolar, int period, register long n, 
	      short *databuf)
{
    int		r;
    
    if(chan & 0x08)
	iop_set(IO_D, (unsigned char)0x02);		/* upper 8 channels */
    else
	iop_clear(IO_D, (unsigned char)0x02);	/* lower 8 channels */

    r = adburst(chan & 0x07, bipolar, period, n, databuf);
    return r;
}
