/*
** Interface to PIC-based Motor Controller
*/
#ifndef _MC_H_
#define _MC_H_

#include "ioports.h"

typedef enum {
    MC_MOTOR_FWD=0x31,
    MC_MOTOR_REV=0x32,
    MC_MOTOR_STOP=0x33,
    MC_GET_COUNTER=0x34,
    MC_GOTO=0x35,
    MC_SET_COUNTER=0x36,
} mc_command_t;

#define mc_fwd()		mc_send_command(MC_MOTOR_FWD, 0)
#define mc_rev()		mc_send_command(MC_MOTOR_REV, 0)
#define mc_stop()		mc_send_command(MC_MOTOR_STOP, 0)
#define mc_goto(c)		mc_send_command(MC_GOTO, (c))
#define mc_set_counter(c)	mc_send_command(MC_SET_COUNTER, (c))
#define mc_isrunning()		(iop_read(IO_A_REAL) & 0x80)
#define mc_set_inhibit()	iop_set(IO_F, 0x80)
#define mc_clear_inhibit()	iop_clear(IO_F, 0x80)

int mc_init(int serial_port);
void mc_spi_init(void);
void mc_shutdown(void);
unsigned long mc_send_command(mc_command_t code, unsigned long arg);
void mc_spi_init(void);
int mc_serial_init(int port);
unsigned long mc_get_counter(void);

#endif
