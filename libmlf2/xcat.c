/**@file
** Xcat ARGOS transmitter utility functions.
**
** arch-tag: 00963d19-a956-4524-bd70-aade35770a8b
**
**
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "xcat.h"

#define WARM_UP_MSECS	500

#define ACK	0x06
#define NAK	0x21

#include "iofuncs.h"

static int 		serial_dev;

/**
 * Load a default message packet into the Xcat.
 *
 * @param  packet  message packet contents.
 * @param  n  packet length.
 * @return 1 if successful, 0 on error.
 */
int
xcat_setup(const unsigned char *packet, size_t n)
{
    int 	csum = 0;
    int		i, r, c, code;

    if(n > 31)
	return 0;
    
    if((serial_dev = serial_open(XCAT_DEVICE, 0, XCAT_BAUD)) < 0)
    {
	log_error("xcat", "Cannot open serial device (code = %d)\n",
		  serial_dev);
	return 0;
    }

    DelayMilliSecs(WARM_UP_MSECS);

    serial_inflush(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    if(!serial_chat(serial_dev, "\r\n", "Parsing Com", 2L))
    {
	log_error("xcat", "No response\n");
	serial_close(serial_dev);
	return 0;
    }
    
    
    /* Load default message packet */
    sputc(serial_dev, 'U');
    sputc(serial_dev, '$');
    sputc(serial_dev, 'S'); csum = 'S';
    sputc(serial_dev, 'D'); csum += 'D';
    sputc(serial_dev, n); csum += n;
    
    for(i = 0;i < n;i++)
    {
	sputc(serial_dev, packet[i]);
	csum += packet[i];
    }
    sputc(serial_dev, csum & 0xff);
    serial_chat(serial_dev, "\r\n", "\r\n", 2L);
    
    r = 1;
    c = sgetc_timed(serial_dev, MilliSecs(), 2500);
    switch(c)
    {
	case NAK:
	    code = sgetc_timed(serial_dev, MilliSecs(), 2000);
	    log_error("xcat", "Command failed, error code = 0x%02x\n", code);
	    r = 0;
	    break;
	case ACK:
	    r = 1;
	    break;
	default:
	    r = 1;
	    break;
    }
    
    serial_close(serial_dev);
    
    return r;
}
