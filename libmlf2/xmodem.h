/**@file
** $Id: xmodem.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _XMODEM_H_
#define _XMODEM_H_

/** Sink function for xmodem_recv */
typedef int (*xmrcv_callback)(char*, int, int, void*);
/** Source function for xmodem_send */
typedef int (*xmsnd_callback)(char*, int, void*);

int xmodem_recv(int desc, xmrcv_callback fsink, void *call_data);
int xmodem_send(int desc, xmsnd_callback fsource, void *call_data, int psize);

#endif /* _XMODEM_H_ */
