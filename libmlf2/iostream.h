/*
** $Id: iostream.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _IOSTREAM_H_
#define _IOSTREAM_H_

#include <stddef.h>

#define IOS_UNBUF	0
#define IOS_LINEBUF	1
#define IOS_FULLBUF	2
#define IOS_BUFMODE	0x03
#define IOS_BUFDIRTY	0x80

#define IOS_BUFSIZE	128L

struct stream {
    char	*bp;
    char	*bend;
    char	*buf;
    size_t	bufsize;
};

typedef struct _serial_io {
    void	*handle;
    int		(*read)(void *handle, char *buf, size_t n);
    int		(*write)(void *handle, char *buf, size_t n);
    int		(*ioctl)(void *handle, int flag, void *value);
} SerialIO;


typedef struct _io_stream {
    int			flags;
    int			desc;
    struct stream	in;
    struct stream	out;
} IOstream;

IOstream *iosopen(int desc, int bufmode);
int iosclose(IOstream *ts);
int iosgetc(IOstream *ts);
int iosungetc(int c, IOstream *ts);
int iosputc(int c, IOstream *ts);
char *iosgets(char *s, int size, IOstream *ts);
int iosflush(IOstream *ts);
int iosputs(const char *s, IOstream *ts);
#ifdef AZTEC_C
int iosprintf(IOstream *ts, const char *fmt, ...);
#endif
int ioswrite(const void *ptr, size_t size, size_t nmemb, IOstream *ios);
int iosread(void *ptr, size_t size, size_t nmemb, IOstream *ios);
int ios_chat(IOstream *ios, const char *send, const char *expect, 
	     long timeout);

#endif

    
