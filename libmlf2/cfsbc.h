/*
 *
 */
#ifndef _CFSBC_H_
#define _CFSBC_H_

#include "ioports.h"

#define CFSBC_DEVICE            2
#define CFSBC_BAUD              38400L
#define CFSBC_MODEM_DIO         0x10
#define CFSBC_MODEM_INUSE()     (iop_read(IO_A|REAL_INPUT) & CFSBC_MODEM_DIO)

struct camera_setup_t {
    const char  *aoi;  /**< Area Of Interest geometry string */
    const char  *settings; /**< settings filename */
    double      exposure;  /**< exposure time in ms */
    unsigned    *scales; /**< scales (percentage < 100) for each thumbnail */
    int         n_scales; /**< size of the scales array */
};

/*
 * Function called by cfsbc_oneshot after photo is taken but before files
 * are downloaded
 */
typedef void  (*post_func_t)(void);

int cfsbc_init(void);
int cfsbc_serialdev(void);
void cfsbc_shutdown(void);
int cfsbc_dev_ready(void);
void cfsbc_test(void);
int cfsbc_put_file(const char *filename, const char *dest);
int cfsbc_archive_file(const char *filename);
int cfsbc_set_clock(void);
int cfsbc_send_params(const char *filename);
int cfsbc_start_comms(const char *status_file, const char *imei);
int cfsbc_done(void);
int cfsbc_sleep(long secs);
void cfsbc_halt(void);
int cfsbc_download_file(const char *filename, int remove);
int cfsbc_shell_command(const char *command, long timeout);
char* cfsbc_shell_response(long timeout);
int cfsbc_int_response(long timeout, long *rval);
int cfsbc_photo(struct camera_setup_t *csp, long timeout, int bg);
int cfsbc_oneshot(const char *aoi, double exposure, unsigned int scale, long timeout,
                  char **filename, long n, post_func_t fpost);
#endif /* _CFSBC_H_ */
