/**@file
** Various functions to abort the mission.
**
** arch-tag: 83fd3049-9617-4ae2-99c4-a426d08f4cd3
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tt8lib.h>
#include <tt8.h>
#include <picodcf8.h>
#include "util.h"
#include "nvram.h"
#include "ioports.h"
#include "lpsleep.h"
#include "log.h"
#include "motor.h"
#include "abort.h"

static char *safe_mode_tmpl = "<mission>
<duration units='seconds' value='%ld' />
<param name='safe.timeout_sec' value='%ld' />
</mission>
";

static long watchdog_timeout = DEF_WATCHDOG_TIMEOUT;

/**
 * Specify the watchdog timeout.
 * Informs the module of the watchdog timeout. This is only
 * needed for displaying the "countdown" on the console.
 *
 * @param  seconds  timeout in seconds
 */
void
abort_set_timeout(long seconds)
{
    watchdog_timeout = seconds;
}

/**
 * Allow watchdog to timeout and abort the mission.
 * Abort the mission by allowing the watchdog timer to expire and
 * release the drop weight (and reset the TT8). Writes a countdown
 * message to the console every twenty seconds. Upon reset, the system
 * starts the Recovery Mode program.
 *
 * @param  now  if non-zero, abort immediately rather than wait for
 *              the watchdog to timeout.
 *
 * This function never returns.
 */
void
abort_mission(int now)
{
    nv_value	nv;
    long	reset_time;
    
    PET_WATCHDOG();
    reset_time = RtcToCtm() + watchdog_timeout;
    
    /*
    ** Save the current piston position.
    */
    nv.l = motor_pos();
    if(nv.l > 0)
	nv_insert("piston", &nv, NV_INT, 0);
    
    /*
    ** Set a flag to indicate we are forcing an abort.
    */
    nv.l = 1;
    nv_insert("force-abort", &nv, NV_INT, 0);
    nv_write();
    
    /* Close log file and clear all output lines */
    closelog();
    iop_clear_all();

    if(now)
    {
	EMERGENCY_ABORT();
	while(1)
	    Reset();
    }
    

    while(1)
    {
	printf("Reset in %ld seconds. <CR><CR> to interrupt\n", 
	       reset_time - RtcToCtm());
	if(isleep(20L))
	{
	    PET_WATCHDOG();
	    printf("Clearing ABORT flag in NVRAM, please wait ...\n");
	    nv.l = 1;
	    nv_insert("power-cycle", &nv, NV_INT, 0);
	    nv.l = 0;
	    nv_insert("force-abort", &nv, NV_INT, 1);
	    /* Truncate autoexec.bat so we don't restart */
	    if(fileexists("autoexec.bat"))
		fclose(fopen("autoexec.bat", "w"));
	    Reset();
	}
    }

}

/**
 * Abort the program.
 *
 * Abort the program and reset the CPU. Upon reset, the system will
 * return to the Boot Manager menu. This function never returns.
 */
void
abort_prog(void)
{
    nv_value	nv;

    PET_WATCHDOG();
    
    closelog();
    iop_clear_all();
    nv.l = 1;
    nv_insert("power-cycle", &nv, NV_INT, 1);
    while(1)
	Reset();
}

/**
 * Restart the system.
 *
 * Reboots the system an restarts the mission program. This
 * function never returns.
 */
void
restart_sys(void)
{
    nv_value	nv;

    PET_WATCHDOG();
    
    closelog();
    iop_clear_all();
    nv.l = -1;
    nv_insert("power-cycle", &nv, NV_INT, 1);
    while(1)
	Reset();
}

/**
 * Enter safe-mode.
 *
 * @param duration  safe-mode duration in seconds
 * @param timeout  safe-mode timeout in seconds
 */
void
enter_safe_mode(long duration, long timeout)
{
    FILE	*ofp;
    nv_value	nv;

    PET_WATCHDOG();

    if(!fileexists("safemode.run"))
    {
	log_error("safe", "Safe-mode program not found\n");
	return;
    }
    
    if((ofp = fopen("safemode.xml", "w")) != NULL)
    {
	fprintf(ofp, safe_mode_tmpl, duration, timeout);
	fclose(ofp);
    }
    else
	log_error("safe", "Cannot open safe-mode parameter file\n");

    /*
    ** Save the current piston position.
    */
    nv.l = motor_pos();
    if(nv.l > 0)
    {
	nv_insert("piston", &nv, NV_INT, 0);
	nv_write();
    }

    /* Close log file and clear all output lines */
    log_event("Entering safe-mode\n");
    closelog();
    iop_clear_all();
    execstr("safemode");

    openlog(NULL);
    log_error("safe", "Safe-mode program failed to start\n");
}

void
enter_recovery_mode(void)
{
    nv_value	nv;

    PET_WATCHDOG();

    if(!fileexists("recover.run"))
    {
	log_error("safe", "Recovery-mode program not found\n");
	return;
    }

    /*
    ** Save the current piston position.
    */
    nv.l = motor_pos();
    if(nv.l > 0)
    {
	nv_insert("piston", &nv, NV_INT, 0);
	nv_write();
    }
    
    log_event("Entering recovery-mode\n");
    closelog();
    iop_clear_all();
    execstr("recover");

    openlog(NULL);
    log_error("recover", "Recovery-mode program failed to start\n");
}
