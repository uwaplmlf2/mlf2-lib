/**@file
** SPI interface to PIC-based A/D converter.
**
**
** SPI interface to the PIC microcontroller which manages the pressure
** sensors. The PIC reads three 24-bit A/D channels at 1hz and buffers
** the last 60 seconds of data.
**
**  - CS lines are active high.
**  - CS lines must be low between transfers.
**  - SCLK is active low.
**  - data is changed on the leading edge and captured on the following edge.
**
** The PIC supports the following commands:
**
**  - GETCOUNT (0x01) -- returns current buffer size.
**  - GETNEXT (0x02) -- returns the next (oldest) data sample from the buffer.
**  - CLEARBUF (0x03) -- clear buffer contents, no return value.
**  - ADRESET (0x04) -- reset the A/D, no return value.
**
*/
#include <stdio.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8lib.h>
#include "nvram.h"
#include "power.h"
#include "picadc.h"


#define MIN(a, b)   ((a) < (b) ? (a) : (b))

#define PIC_BAUD    100000L
#define PIC_CLKDELAY    500L
#define PIC_CMDINTERVAL 500L

static
ushort basic_cmd[] = { (M_CONT  & CLR)    /* CS driven by QPDR after transfer */
| (M_BITSE & CLR)   /* always transfer 8 bits */
| (M_DT    & SET)   /* use SPCR1 DTL delay after xfer */
| (M_DSCK  & SET)   /* use SPCR1 DSCKL delay, SCK to PCS */
| (M_CS3   & SET)   /* PCS3, Max186 A/D, SET == disabled */
| (M_CS2   & SET)   /* PCS2, Picadc0, SET == enabled */
| (M_CS1   & CLR)   /* PCS1, UNUSED, CLR == disabled */
| (M_CS0   & CLR),  /* PCS0, UNUSED, CLR == disabled */
};

/*
 * Initialize the QSPI interface.
 *
 * @param  baud  serial clock baud rate
 * @param  clkdelay  delay between chip-select valid and clock transition (usecs)
 * @param  cmdinterval  interval between commands (usecs)
 *
 *
 */
static void
qspi_setup(long baud, long clkdelay, long cmdinterval)
{
    int         dsckl, dtl;
    unsigned    rate_div;
    ulong       fclk = SimGetFSys();
    unsigned    cs;

    cs = M_PCS2;

    /* Convert timing parameters from microseconds */
    dsckl = (ulong)clkdelay*fclk/1000000L;
    dtl = (ulong)cmdinterval*fclk/(32L*1000000L);

    /*
    ** Output values of GP-I/O pins AND the state of the chip-select lines
    ** between QSPI transfers.  Chip select must remain low between transfers.
    */
    *QPDR = (M_SCK | M_MISO | M_PCS3);

    /* QSPI needs MOSI, MISO, and chip-select */
    *QPAR = (M_MOSI | M_MISO | cs);
    /* All chip-selects, MOSI and SCK are outputs */
    *QDDR = M_PCS3 | M_PCS2 | M_PCS1 | M_PCS0 | M_SCK | M_MOSI;


    /*
    ** Specify baud rate and set Master Mode. Clock is active low, data is
    ** changed on the leading edge and captured on the following edge.
    */
    rate_div = SimGetFSys()/(2*baud);
    *SPCR0 = rate_div | (M_MSTR & SET) | (M_CPOL & SET) | (M_CPHA & SET);

    /* Disable Loop Mode, HALTA interrupts, and Halt */
    *SPCR3 = 0;

    /* Set delays */
    *SPCR1 = ((dsckl & 0x7f) << 8) | (dtl & 0xff);
}

static int
simple_cmd(int cmdcode, int get_response)
{
    int     endqp;

    endqp = 0;

    SPIXMT[0] = cmdcode;
    SPICMD[0] = basic_cmd[0];
    if(get_response)
    {
        SPIXMT[1] = 0;
        SPICMD[1] = basic_cmd[0];
        endqp = 1;
    }

    *SPCR2 = (M_WREN & CLR) |
      (M_WRTO & CLR) |
      (endqp << 8) | 0x0;

    *SPSR &= ~M_SPIF;   /* clear flag */
    *SPCR1 |= M_SPE;    /* enable QSPI transfer */
    /* wait for pointer to reach ENDQP */
    while(!(*SPSR & M_SPIF))
        ;
    return get_response ? (int)SPIRCV[1] : 1;
}

/**
 * Power-on and initialize the interface to the sensor.
 *
 * @return 1
 */
int
picadc_init(spi_clocks_t *clks)
{
    power_on(QSPIDEV_POWER1);
    picadc_spi_init(clks);
    return 1;
}

/**
 * Initialize the SPI interface to the sensor.
 *
 */
void
picadc_spi_init(spi_clocks_t *clks)
{
    qspi_setup(clks->baud, clks->delay, clks->interval);
}

/**
 * Shutdown the device.
 *
 */
void
picadc_shutdown(void)
{
    power_off(QSPIDEV_POWER1);
}

/**
 * Get the buffer sample count.
 *
 * @return the number of samples queued on the PIC.
 */
int
picadc_get_count(void)
{
    return simple_cmd(PICADC_CMD_GETCOUNT, 1);
}

/**
 * Reset the A/D controller.
 *
 * @return 1
 */
int
picadc_adreset(void)
{
    return simple_cmd(PICADC_CMD_ADRESET, 0);
}

int
picadc_old_adreset(void)
{
    return simple_cmd(PICADC_CMD_OLDADRESET, 0);
}

/**
 * Clear the sample buffer.
 *
 * @return 1
 */
int
picadc_clear(void)
{
    return simple_cmd(PICADC_CMD_CLEARBUF, 0);
}

int
picadc_old_clear(void)
{
    return simple_cmd(PICADC_CMD_OLDCLEARBUF, 0);
}

/**
 * Check number of missed samples (old board only)
 *
 * @param  index  sensor index, 0 or 1.
 * @return the number of samples missed by the PIC when servicing
 * the last data transfer request.
 */
int
picadc_overruns(void)
{
    return simple_cmd(PICADC_CMD_OVERRUNS, 1);
}

/*
 * Helper function for picadc_read_data. Reads a single A/D sample record from
 * the QSPI receive buffer.
 */
static void
receive_adc_sample(adc_sample_t *samp, int source_index)
{
    int     i, j, k;
    long    val;

    j = source_index;
    for(i = 0;i < PICADC_CHANNELS;i++)
    {
        /*
         * Each A/D value is stored in the QSPI receive buffer as
         * a 3-byte value, most-significant byte first.
         */
        val = 0;
        for(k = 0;k < 3;k++)
            val = (val << 8) + SPIRCV[j++];
        samp->channel[i] = val;
    }
}

/**
 * Read the buffered data samples.
 * Read the most recent N samples of data from the PIC A/D converter.
 * The sample values are in counts, use picadc_counts_to_mv() to convert
 * to millivolts.  The return value is the actual number of samples read
 * which will be <= nr_samples. Note that if nr_samples is less than the
 * number of samples in the buffer, the extra samples are discarded.
 *
 * @param  nr_samples  number of samples to read.
 * @param  data  array of returned samples.
 * @return sample count.
 */
int
picadc_read_data(int nr_samples, adc_sample_t data[])
{
    int         i, n, first;
    int         endqp, newqp;

    /**
       ### Data Transfer

       The PIC will return the entire contents of its data-buffer preceeded by
       a single byte specifying the number of samples.

       Setup the SPI registers for the transfer of 9-element data blocks on
       each wrap of the queue.

       Register layout:

       | Index |  XMIT  |  RECV  |
       | :---: | :----: | :----: |
       |   0   |    0   | HIGH[i]|
       |   1   |    0   | MID[i] |
       |   2   |    0   | LOW[i] |
       |   3   |    0   | HIGH[i+1] |
       |   4   |    0   | MID[i+1] |
       |   5   |    0   | LOW[i+1] |
       |   6   |    0   | HIGH[i+2] |
       |   7   |    0   | MID[i+2] |
       |   8   |    0   | LOW[i+2] |
       |   9   |    0   | <IGNORED> |
       |   A   |    0   | <IGNORED> |
       |   B   |    0   | <IGNORED> |
       |   C   |    0   | <IGNORED> |
       |   D   |    0   | <IGNORED> |
       |   E   | PICADC_CMD_GETNEXT | <IGNORED> |
       |   F   |    0   | NSAMPLES |

       On the first transfer:

       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           ENDQP <- 0x0f
           NEWQP <- 0x0e
           WREN <- 0
       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

       After the first transfer, NSAMPLES is read and the subsequent transfers proceed
       as follows:

       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           ENDQP <- 0x08
           NEWQP <- 0x0
           WREN <- 1
           WRTO <- 0
           n <- RCV[0x0f]
           i <- 0
           while n > 0:
               SAMPLE[i++] <- RCV[0:8]
               n--
               if n == 1:
                   WREN <- 0
       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

     */

    endqp = 0x0f;
    newqp = 0x0e;

    i = 0;
    while(i <= endqp)
    {
        SPIXMT[i] = 0;
        SPICMD[i] = basic_cmd[0];
        i++;
    }

    SPIXMT[newqp] = PICADC_CMD_GETBUFFER;
    SPICMD[newqp] = basic_cmd[0];

    *SPCR2 = (M_WREN & CLR) |
      (M_WRTO & CLR) |
      (endqp << 8) | newqp;

    *SPSR &= ~M_SPIF;   /* clear flag */
    *SPCR1 |= M_SPE;    /* enable QSPI transfer */

    i = 0;
    n = PICADC_MAX_SAMPLES;
    first = 1;

    while(n > 0)
    {
        /* wait for pointer to reach ENDQP */
        while(!(*SPSR & M_SPIF))
            ;

        if(first)
        {
            /*
             * Read the sample count value and reset the queue pointers.
             */
            first = 0;
            n = SPIRCV[endqp];
            endqp = PICADC_CHANNELS*3 - 1;
            newqp = 0;
            *SPCR2 = (M_WREN & SET) |
              (M_WRTO & CLR) |
              (endqp << 8) | newqp;
            *SPCR1 |= M_SPE;    /* enable QSPI transfer */
        }
        else
        {
            /*
             * The caller is allowed to request fewer samples than the buffer
             * currently holds. The excess values will be discarded.
             */
            if(nr_samples > 0)
            {
                receive_adc_sample(&data[i++], 0);
                nr_samples--;
            }

            n--;
        }

        /* clear flag */
        *SPSR &= ~M_SPIF;
    }

    *SPCR1 &= ~M_SPE;

    /* Return the number of samples read */
    return i;
}

/**
 * Read the buffered data samples from the old 1-channel board.
 *
 * @param  n  number of samples to read.
 * @param  data  array of returned samples.
 * @return sample count.
 */
int
picadc_old_read_data(int n, long data[])
{
    int             i, j, first;
    int             endqp, newqp, sample_incr;
    unsigned char   *bp;


    /**
       Summary of the data transfer procedure.

       Setup the SPI registers for an efficient pipelined transfer of
       5-element data blocks on each wrap of the queue.

       Register layout:

       | Index |  XMIT  |  RECV  |
       | :---: | :----: | :----: |
       |   0   |    0   | HIGH[i]|
       |   1   |    0   | MID[i] |
       |   2   | PICADC_CMD_GETNEXT | LOW[i] |
       |   3   |    0   | HIGH[i+1] |
       |   4   |    0   | MID[i+1] |
       |   5   | PICADC_CMD_GETNEXT | LOW[i+1] |
       |   6   |    0   | HIGH[i+2] |
       |   7   |    0   | MID[i+2] |
       |   8   | PICADC_CMD_GETNEXT | LOW[i+2] |
       |   9   |    0   | HIGH[i+3] |
       |   A   |    0   | MID[i+3] |
       |   B   | PICADC_CMD_GETNEXT | LOW[i+3] |
       |   C   |    0   | HIGH[i+4] |
       |   D   |    0   | MID[i+4] |
       |   E   | PICADC_CMD_GETNEXT | LOW[i+4] |

       On first iteration:

       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        E -> NEWQP
        E -> ENDQP
        1 -> WREN   // enable queue wrapping
        0 -> WRTO   // wrap to zero
        when SPIF == 1:
             if nr_samples <= 5:
                (nr_samples*3)-1 -> ENDQP
                0 -> WREN
                XMIT[ENDQP] = 0
            clear SPIF
            continue
       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

       On subsequent iterations

       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        when SPIF == 1:
            read data from 0 to ENDQP
            nr_samples -= 5
            if nr_samples <= 0:
                break
             if nr_samples <= 5:
                (nr_samples*3)-1 -> ENDQP
                0 -> WREN
                XMIT[ENDQP] = 0
            clear SPIF
            continue
       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

     */

    endqp = 0x0e;
    /* Number of samples read per queue wrap */
    sample_incr = (endqp + 1)/3;

    i = 0;
    while(i <= endqp)
    {
        SPIXMT[i] = 0;
        SPICMD[i] = basic_cmd[0];
        i++;
        SPIXMT[i] = 0;
        SPICMD[i] = basic_cmd[0];
        i++;
        SPIXMT[i] = PICADC_CMD_GETNEXT;
        SPICMD[i] = basic_cmd[0];
        i++;
    }

    *SPCR2 = (M_WREN & SET) |
      (M_WRTO & CLR) |
      (endqp << 8) | endqp;

    i = 0;
    first = 1;

    *SPSR &= ~M_SPIF;   /* clear flag */
    *SPCR1 |= M_SPE;    /* enable QSPI transfer */

    while(n > 0)
    {
        bp = (unsigned char*)&data[i];

        /* wait for pointer to reach ENDQP */
        while(!(*SPSR & M_SPIF))
            ;
        if(first)
        {
            first = 0;
            goto adjust;
        }

        /* Unpack the data into the caller's buffer, MSB first */
        for(j = 0;j <= endqp;j+=3)
        {
            *bp++ = 0;
            *bp++ = SPIRCV[j];
            *bp++ = SPIRCV[j+1];
            *bp++ = SPIRCV[j+2];
        }

        /*
        ** sample_incr is the number of samples read on each wrap of the queue
        */
        n -= sample_incr;
        i += sample_incr;

    adjust:
        if(n <= sample_incr && n > 0)
        {
            /*
             * We need to adjust the queue pointers.  It is prudent to
             * first stop the QSPI to avoid race conditions.
             */
            *SPCR3 |= M_HALT;       /* request HALT */
            while(!(*SPSR & M_HALTA))   /* wait for acknowledgement */
                ;
            *SPCR1 &= ~M_SPE;       /* disable QSPI */

            /*
             * The lower 4 bits of SPSR is the pointer to the last completed
             * command.  Increment it to get the new starting point.
             */
            newqp = ((*SPSR & 0x0f) + 1) % (endqp+1);

            /* adjust ENDQP and clear WREN */
            endqp = (n*3) - 1;
            sample_incr = (endqp + 1)/3;
            SPIXMT[endqp] = 0;

            *SPCR2 = (M_WREN & CLR) |
              (M_WRTO & CLR) |
              (endqp << 8) | newqp;

            /* clear HALTA and HALT */
            *SPSR &= ~M_HALTA;
            *SPCR3 &= ~M_HALT;

            /* re-enable the QSPI */
            *SPCR1 |= M_SPE;
        }

        /* clear flag */
        *SPSR &= ~M_SPIF;
    }

    *SPCR1 &= ~M_SPE;

    return i;
}

/**
 * Convert counts to millivolts.
 *
 * @param  counts  A/D input value.
 * @return millivolts.
 */
double
picadc_counts_to_mv(long counts)
{
    double      volts_per_count;

    volts_per_count = PICADC_VREF/((double)0x1000000 * (double)PICADC_GAIN);
    return (volts_per_count * counts * 1000.);
}
