/**@file
 */
#ifndef _PALARGO_H_
#define _PALARGO_H_

#define PALARGO_DEVICE      2
#define PALARGO_BAUD        9600

/**< Maximum time to allow for power-up (seconds) */
#define PALARGO_POWER_UP    10L
/**< Worst case DAQ time (seconds) */
#define PALARGO_DAQ_TIME    15L
/**< Maximum number of frequency bins */
#define PALARGO_FREQ_BINS   10

typedef struct {
    long        t;  /**< sample timestamp in seconds */
    unsigned    nbins; /**< number of SPL samples */
    float       spl[PALARGO_FREQ_BINS]; /**< sound pressure level in dB */
    unsigned    is_saturated; /**< 1 or 0 */
} PalData;

int palargo_init(void);
void palargo_shutdown(void);
int palargo_check(void);
int palargo_hydrophone(int state);
int palargo_start_sample(int store_rawdata);
int palargo_read_sample(PalData *pdp, long timeout);
int palargo_sample(PalData *pdp, long timeout, int store_rawdata);
int palargo_sync_clock(void);
#endif /* PALARGO_H */
