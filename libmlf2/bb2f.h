/*
*/
#ifndef _BB2F_H_
#define _BB2F_H_

#define BB2F_DEVICE	2
#define BB2F_BAUD	19200L

#define BB2F_FIELDS	6

typedef struct bb2f {
    short	blue_ref;	/**< 470nm reference */
    short	blue;		/**< 470nm value */
    short	red_ref;	/**< 650nm reference */
    short	red;		/**< 650nm value */
    short       chl;            /**< Chlorophyll */
} __attribute__((packed)) Bb2fData;

/* Constants to describe the s-expression output format */
#define BB2F_DATA_FMT		">5h"
#define BB2F_DATA_DESC		"blueref,blue,redref,red,chl"


int bb2f_init(void);
void bb2f_shutdown(void);
int bb2f_dev_ready(void);
int bb2f_read_data(Bb2fData *fdp, long timeout);
void bb2f_test(void);

#endif /* _BB2F_H_ */
