/**@file
** arch-tag: 37b269a6-f342-4f0b-8547-f29647b15cf6 SERIAL
** Time-stamp: <2006-03-07 11:08:32 mike>
*/
#ifndef _RAFOS_H_
#define _RAFOS_H_

#define RAFOS_DEVICE		10
#define RAFOS_BAUD		9600

/** Clock device code */
#define RAFOS_CLOCK_DEV		1
/** Receiver device code */
#define RAFOS_RECV_DEV		2

/** RAFOS data record */
typedef struct {
    long		t;  /**< Start time (seconds since 1/1/1970 UTC). */
    unsigned short	index[3]; /**< Indicies of 3 largest correlation values. */
    unsigned short	corr[3]; /**< Largest correlation values. */
} RafosData;

int rafos_init(void);
void rafos_shutdown(void);
void rafos_reopen(void);
int rafos_dev_ready(void);
int rafos_read_data(RafosData *rdp);
void rafos_get_stats(unsigned long *errs, unsigned long *reads);
void rafos_test(void);
int rafos_set_clock(void);
void rafos_reopen(void);
long rafos_set_tt8_clock(void);
unsigned rafos_start_sample(unsigned duration);
int rafos_data_ready(void);
long rafos_sod(void);

#endif
