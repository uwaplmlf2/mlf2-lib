/**@file
** XMODEM send/receive functions.
**
** @ingroup Comm
** $Id: xmodem.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
**
**
*/
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <string.h>
#include <ctype.h>
#include "serial.h"
#include "log.h"
#include "ioports.h"
#include "xmodem.h"
#define WAIT_FOR_EOT_ACK
#ifndef __GNUC__
#define __inline__
#endif

#define SOH             0x01
#define STX             0x02
#define EOT             0x04
#define ACK             0x06
#define NAK             0x15
#define CAN             0x18
#define START_CSUM      NAK
#define START_CRC       'C'

/* Packet padding character (ctrl-z) */
#define PAD_CHAR        26

/** Character timeout in milliseconds */
#define CHAR_TIMEO      1000L
/** Packet timeout in milliseconds */
#define PKT_TIMEO       20000L
/** Session start timeout in milliseconds */
#define START_TIMEO     60000L
/** EOT character timeout in milliseconds */
#define EOT_TIMEO       8000L

#define MAX_CRC_WAITS   4
#define MAX_CSUM_WAITS  10
#define MAX_RESEND      2

/* CCITT CRC polynomial representation */
#define CRC_POLY        0x1021

static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char       c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
        if((MilliSecs() - start) >= timeout)
            return -1;
    }
    return ((int) c)&0xff;
}

static __inline__ int sputc(int desc, int c)
{
    unsigned char       byte = c;
    return serial_write(desc, &byte, 1L);
}

static long nread(int desc, char *buf, long n, long timeout_ms)
{
    long        nr, n1, start;

    n1 = 0;
    start = MilliSecs();
    while(n1 < n || (MilliSecs() - start) >= timeout_ms)
    {
        nr = serial_read(desc, buf, n-n1);
        if(nr > 0)
        {
            start = MilliSecs();
            buf += nr;
            n1 += nr;
        }
    }

    return n1;
}

/*
** Read characters until more than 2*CHAR_TIMEO milliseconds expire between
** them.
*/
#define PURGE_INPUT(d) while(sgetc_timed(d, MilliSecs(), 2*CHAR_TIMEO) != -1)

/*
 * update_crc - incrementally update a CCITT CRC calculation.
 * @crc: current CRC
 * @c: character to add to CRC
 *
 * Returns the new CRC.
 */
static __inline__ unsigned short update_crc(unsigned crc, int c)
{
    int         i;

    crc = crc ^ (c << 8);
    for(i = 0;i < 8;i++)
    {
        if(crc & 0x8000)
            crc = (crc << 1) ^ CRC_POLY;
        else
            crc <<= 1;
    }

    return crc;
}


static char pktdata[1024];

/**
 * Receive XMODEM packets from a remote system.
 * This function handles an XMODEM receive process through a serial port.
 * The packets received are passed to fsink which is called with the following
 * arguments:
 *
 *    (*fsink)(char *pktdata, int psize, int pnum, void* call_data)
 *
 * Where "pktdata" is the data section of the packet, "psize" is the length
 * of the packet data in bytes, and "pnum" is the packet number modulo 256.
 * This callback function is responsible for the final storage of the data and
 * should return 1 if the receive process should continue or zero if it
 * should be aborted. Errors are logged.
 *
 * @param  desc  serial port descriptor.
 * @param  fsink  data "sink" function.
 * @param  call_data  arbitrary data passed to fsink.
 * @return 1 if successful, 0 on error.
 */
int
xmodem_recv(int desc, xmrcv_callback fsink, void *call_data)
{
    int         psize, pnum, cpnum, use_crc, count, i;
    int         c, csend, r, mode;
    unsigned    csum;
    ulong       tstart, total_bytes;
    char        *p;
    enum {XMR_START=0,
          XMR_WAIT,
          XMR_RD_HDR,
          XMR_RD_DATA,
          XMR_STORE_DATA,
          XMR_CHECK_CSUM,
          XMR_SEND_ACK,
          XMR_SEND_NAK,
          XMR_ACK_DONE,
          XMR_DO_ABORT,
          XMR_FAST_READ,
          XMR_DONE} state;

    /* Set the interface to not block on input */
    mode = serial_inmode(desc, SERIAL_NONBLOCKING);

    PURGE_INPUT(desc);

    state = XMR_START;
    count = 0;
    pnum = 0;
    r = 1;
    use_crc = 1;
    total_bytes = 0;
    tstart = MilliSecs();
    csend = START_CRC;
    psize = 0;
    csum = 0;

    while(state != XMR_DONE)
    {
        switch(state)
        {
            case XMR_START:
                /* Send a start character to the sender */
                if(count > MAX_CRC_WAITS)
                {
                    csend = START_CSUM;
                    use_crc = 0;
                }
                else
                    csend = START_CRC;
            case XMR_WAIT:
                /* Wait for the start of a packet */
                sputc(desc, csend);
                count++;
                PET_WATCHDOG();
                c = sgetc_timed(desc, MilliSecs(), PKT_TIMEO);
                switch(c)
                {
                    case SOH:
                        psize = 128;
                        state = XMR_RD_HDR;
                        break;
                    case STX:
                        psize = 1024;
                        state = XMR_RD_HDR;
                        break;
                    case EOT:
                        state = XMR_ACK_DONE;
                        break;
                    case CAN:
                        state = XMR_DONE;
                        log_error("xmodem-recv", "Sender aborted\n");
                        r = 0;
                        break;
                    default:
                        if(count >= MAX_CSUM_WAITS)
                        {
                            log_error("xmodem-recv",
                                      "Timeout waiting for packet %d\n",
                                      pnum+1);
                            r = 0;
                            state = XMR_DO_ABORT;
                        }
                        break;
                }
                break;
            case XMR_RD_HDR:
                /* Read the packet header */
                count = 0;
                pnum = sgetc_timed(desc, MilliSecs(), CHAR_TIMEO);
                cpnum = sgetc_timed(desc, MilliSecs(), CHAR_TIMEO);
                state = XMR_FAST_READ;
                break;
            case XMR_FAST_READ:
                csum = 0;
                if(nread(desc, pktdata, (long)psize, CHAR_TIMEO) != (long)psize)
                {
                    log_error("xmodem-recv",
                              "Timeout reading packet %d\n", pnum);
                    PURGE_INPUT(desc);
                    state = XMR_SEND_NAK;
                }
                else
                {
                    state = XMR_CHECK_CSUM;
                    if(use_crc)
                    {
                        for(i = 0;i < psize;i++)
                            csum = update_crc(csum, (int)pktdata[i]);
                    }
                    else
                    {
                        for(i = 0;i < psize;i++)
                            csum += (int)pktdata[i];
                    }
                }
                break;
            case XMR_RD_DATA:
                /* Read the packet data */
                p = pktdata;
                csum = 0;
                state = XMR_CHECK_CSUM;
                for(i = 0;i < psize;i++)
                {
                    if((c = sgetc_timed(desc, MilliSecs(), CHAR_TIMEO)) < 0)
                    {
                        log_error("xmodem-recv",
                                  "Timeout reading packet %d\n", pnum);
                        PURGE_INPUT(desc);
                        state = XMR_SEND_NAK;
                        break;
                    }

                    if(use_crc)
                        csum = update_crc(csum, c);
                    else
                        csum += c;
                    *p++ = c;
                }
                break;
            case XMR_CHECK_CSUM:
                /* Verify the checksum or CRC */
                state = XMR_SEND_ACK;
                switch(use_crc)
                {
                    case 1:
                        c = sgetc_timed(desc, MilliSecs(), CHAR_TIMEO);
                        if(c < 0 || (c != (csum >> 8)))
                            state = XMR_SEND_NAK;
                        /* fall through and read second byte of CRC */
                    case 0:
                        c = sgetc_timed(desc, MilliSecs(), CHAR_TIMEO);
                        if(c < 0 || (c != (csum & 0xff)))
                            state = XMR_SEND_NAK;
                }
                break;
            case XMR_SEND_ACK:
                csend = ACK;
                state = XMR_STORE_DATA;
                break;
            case XMR_ACK_DONE:
                sputc(desc, ACK);
                state = XMR_DONE;
                log_event("Xmodem receive: %ld bytes/ %ld ms\n", total_bytes,
                          MilliSecs()-tstart);
                break;
            case XMR_SEND_NAK:
                csend = NAK;
                state = XMR_WAIT;
                break;
            case XMR_DO_ABORT:
                sputc(desc, CAN);
                sputc(desc, CAN);
                sputc(desc, CAN);
                state = XMR_DONE;
                break;
            case XMR_STORE_DATA:
                total_bytes += psize;
                if((*fsink)(pktdata, psize, pnum, call_data) == 0)
                {
                    log_error("xmodem-recv", "User abort\n");
                    r = 0;
                    state = XMR_DO_ABORT;
                }
                else
                    state = XMR_WAIT;
                break;
            case XMR_DONE:
                break;
        }

    }

    /* Restore serial input mode */
    serial_inmode(desc, mode);

    return r;
}


/**
 * Send XMODEM packets to a remote system.
 * This function handles an XMODEM send process through a serial port.  The
 * data to send is provided by fsource which is called as follows:
 *
 *    (*fsource)(char *pktdata, int psize, void *call_data);
 *
 * Where "pktdata" is the buffer for the packet data and "psize" is the number
 * of bytes to store in the buffer.  The return value is the number of bytes
 * stored.  If the return value is 0, the transmission is halted.  If the
 * return value is < 0, the transmission is aborted. Errors are logged.
 *
 * @param  desc  serial port descriptor.
 * @param  fsource  data source function.
 * @param  call_data  arbitrary data passed to fsink.
 * @param  psize  packet size (128 or 1024)
 * @return 1 if successful, 0 on error.
 */
int
xmodem_send(int desc, xmsnd_callback fsource, void *call_data, int psize)
{
    int         pnum, use_crc, count, i;
    int         c, r, mode, n;
    unsigned    csum;
    char        *p = 0;
    ulong       tstart, total_bytes, start, t0;
    enum {XMS_START=0,
          XMS_WAIT,
          XMS_TIMEOUT,
          XMS_FILL_PKT,
          XMS_CALC_CSUM,
          XMS_SEND_PKT,
          XMS_SEND_EOT,
          XMS_DO_ABORT,
          XMS_DONE} state;

    /* Set the interface to not block on input */
    mode = serial_inmode(desc, SERIAL_NONBLOCKING);

    state = XMS_START;
    count = 0;
    pnum = 0;
    r = 1;
    use_crc = 1;
    total_bytes = 0;
    tstart = MilliSecs();
    csum = 0;
    n = 0;

    while(state != XMS_DONE)
    {
        switch(state)
        {
            case XMS_START:
                /* Wait for start character */
                count++;
                PET_WATCHDOG();
                c = sgetc_timed(desc, tstart, 60000L);
                switch(c)
                {
                    case START_CRC:
                        use_crc = 1;
                        state = XMS_FILL_PKT;
                        break;
                    case START_CSUM:
                        use_crc = 0;
                        state = XMS_FILL_PKT;
                        break;
                    case EOT:
                        log_error("xmodem-send", "Receiver abort\n");
                        r = 0;
                        state = XMS_DONE;
                        break;
                    case -1:
                        state = XMS_TIMEOUT;
                        break;
                    default:
                        if(count >= MAX_CSUM_WAITS)
                            state = XMS_TIMEOUT;
                        break;
                }
                break;
            case XMS_TIMEOUT:
                /* Handle timeout while waiting for start char. */
                r = 0;
                state = XMS_DONE;
                log_error("xmodem-send", "Timeout waiting for START\n");
                break;
            case XMS_FILL_PKT:
                /* Fill data section of packet */
                count = 0;
                PET_WATCHDOG();
                pnum++;
                n = (*fsource)(pktdata, psize, call_data);
                p = pktdata;
                if(n == 0)
                    state = XMS_SEND_EOT;
                else if(n < 0)
                    state = XMS_DO_ABORT;
                else
                {
                    if(n < psize)  /* Pad the packet */
                        memset(&p[n], PAD_CHAR, psize-n);
                    state = XMS_CALC_CSUM;
                }
                break;
            case XMS_CALC_CSUM:
                /* Calculate data checksum or CRC */
                csum = 0;
                if(use_crc)
                    for(i = 0;i < psize;i++)
                        csum = update_crc(csum, p[i]);
                else
                    for(i = 0;i < psize;i++)
                        csum += p[i];
                state = XMS_SEND_PKT;
                break;
            case XMS_SEND_PKT:
                /* Send the current packet */
                count++;
                if(count > MAX_RESEND)
                {
                    log_error("xmodem-send",
                              "Timeout waiting for ACK (pnum=%d)\n", pnum);
                    r = 0;
                    state = XMS_DO_ABORT;
                    break;
                }
                PET_WATCHDOG();
                sputc(desc, psize == 128 ? SOH : STX);
                sputc(desc, pnum);
                sputc(desc, ~pnum);
                for(i = 0;i < psize;i++)
                {
                    /* add pauses to avoid overruns */
                    if((i > 0) && (i & 0x7f) == 0)
                        DelayMilliSecs(1L);
                    sputc(desc, p[i]);
                }

                if(use_crc)
                    sputc(desc, (csum >> 8)&0xff);
                sputc(desc, csum & 0xff);
                state = XMS_WAIT;
                break;
            case XMS_WAIT:
                /* Wait for ACK/NAK */
                state = XMS_SEND_PKT;
                t0 = MilliSecs();
                while((c = sgetc_timed(desc, t0, PKT_TIMEO)) != -1)
                {
                    if(c == ACK)
                    {
                        state = XMS_FILL_PKT;
                        break;
                    }
                    else if(c == NAK)
                    {
                        log_event("NAK received (packet=%d, attempt=%d)\n",
                                  pnum, count);
                        state = XMS_SEND_PKT;
                        break;
                    }

                    /*
                    ** Unknown characters received from the modem, continue to listen
                    ** until the timeout expires.
                    */
                }
                break;
            case XMS_DO_ABORT:
                /* Abort the data transfer */
                sputc(desc, CAN);
                sputc(desc, CAN);
                sputc(desc, CAN);
                state = XMS_DONE;
                break;
            case XMS_SEND_EOT:
                /* Send the end-of-transmission character */
                PURGE_INPUT(desc);
                sputc(desc, EOT);
                PET_WATCHDOG();
                log_event("Xmodem send EOT\n");
#ifdef WAIT_FOR_EOT_ACK
                start = MilliSecs();
                while((c = sgetc_timed(desc, start, EOT_TIMEO)) != -1)
                {
                    if(c == ACK)
                        break;
                    else
                        sputc(desc, EOT);
                }

                if(c == -1)
                {
                    log_error("xmodem-send", "Timeout on EOT ACK (ignored)\n");
                    r = 1;
                }
#endif
                state = XMS_DONE;
                break;
            case XMS_DONE:
                break;
        }
    }

    serial_inmode(desc, mode);
    return r;
}
