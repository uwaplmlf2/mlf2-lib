/**@file
**
*/
#ifndef _SUNA_H_
#define _SUNA_H_

#define SUNA_DEVICE     6
#define SUNA_BAUD       19200L

/**Header for Dark data frame */
#define SUNA_DARK_HEADER        "SATSDB"
/**Header for Light data frame */
#define SUNA_LIGHT_HEADER       "SATSLB"

/**Header length */
#define SUNA_HDRLEN             6L
/**Data length for full-data frame */
#define SUNA_BINARY_FULL_SIZE   505L
/**Data length for concentration-only frame */
#define SUNA_BINARY_CONC_SIZE   95L

#define SUNA_FULL_FRAME         (SUNA_HDRLEN+SUNA_BINARY_FULL_SIZE)
#define SUNA_CONC_FRAME         (SUNA_HDRLEN+SUNA_BINARY_CONC_SIZE)

#define SUNA_DARK_FRAME         0x01
#define SUNA_LIGHT_FRAME        0x02
#define SUNA_BOTH_FRAMES        (SUNA_DARK_FRAME|SUNA_LIGHT_FRAME)

#define SUNA_DATA_SIZE          SUNA_FULL_FRAME

/**Output data structure */
typedef struct suna_data {
    unsigned char       dark[SUNA_DATA_SIZE];
    unsigned char       light[SUNA_DATA_SIZE];
} SunaData;

int suna_init(void);
void suna_shutdown(void);
int suna_dev_ready(void);
void suna_test(void);
int suna_start(void);
int suna_data_ready(void);
int suna_read_data(SunaData *idp, long timeout);
unsigned long suna_start_time(void);


#endif /* _SUNA_H_ */
