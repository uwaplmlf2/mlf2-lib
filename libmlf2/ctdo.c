/**@file
** Driver for the Seabird CTD+Oxygen device.
**
** $Id: ctdo.c,v df4a2adb185c 2008/05/28 20:48:47 mikek $
**
**
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "lpsleep.h"
#include "ctdo.h"


#ifndef __GNUC__
#define __inline__
#endif

/** Sample timeout (milliseconds) */
#define SAMPLE_TIMEO_MS		4000L

enum { T_START=0,
       T_PUMPON,
       T_OXYON,
       T_SAMPLE,
       NR_TIMES };
static unsigned long	timestamps[NR_TIMES];

/** pump states */
typedef enum {PUMP_OFF=0, PUMP_ON} pump_state_t;

static int 		serial_dev = -1;
static int		oxy_on;
static unsigned long	nr_reads, nr_errors;

#include "iofuncs.h"

#define PURGE_INPUT(d) while(sgetc_timed(d, MilliSecs(), 100L) != -1)

static void
ctdo_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
}

/**
 * Initialize the device interface.
 * Open connection to the CTD+O interface and initialize.
 *
 * @return 1 if successful, 0 on error.
 */
int
ctdo_init(void)
{
    if(serial_dev >= 0)
	goto already_open;

    if((serial_dev = serial_open(CTDO_DEVICE, CTDO_MINOR, CTDO_BAUD)) < 0)
    {
	log_error("ctdo", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    DelayMilliSecs(1000L);

    if(!serial_chat(serial_dev, "\r\n", "S>", 4L))
    {
	log_error("ctdo", "Cannot initialize device\n");
	serial_close(serial_dev);
	return 0;
    }

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    memset(timestamps, 0, sizeof(timestamps));
    oxy_on = 0;
    timestamps[T_START] = RtcToCtm();

    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to ctdo_start_sample and ctdo_read_data.
    */
    add_after_hook("CTDO-reopen", ctdo_reopen);

already_open:

    return 1;
}

/**
 * Shutdown the CTD+O interface and power off.
 */
void
ctdo_shutdown(void)
{
    if(serial_dev >= 0)
    {
	remove_after_hook("CTDO-reopen");
	serial_close(serial_dev);
	serial_dev = -1;
    }
}


/**
 * Check if device is ready.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
ctdo_dev_ready(void)
{
    return (serial_dev >= 0);
}


/**
 * Fetch read error statistics.
 *
 * @param  errs  returned error count
 * @param  reads  returned read count.
 */
void
ctdo_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}

/**
 * Start a series of pumping cycles.
 *
 * @param  slow  slow pump time (seconds)
 * @param  fast  fast pump time (seconds)
 * @param  off   pump off time (seconds)
 * @param  ncycles  number of times to repeat
 * @return total execution time if successful, 0 on error.
 */
long
ctdo_start_pump(unsigned slow, unsigned fast, unsigned off, unsigned ncycles)
{
    long		total;
    char		cmd[24];

    total = ncycles*(slow+fast+off);
    sprintf(cmd, "PUMP%03u%03u%03u%03u\r\n",
	    slow, fast, off, ncycles);

    serial_write_zstr(serial_dev, cmd);
    timestamps[T_PUMPON] = RtcToCtm();
    return total;
}

/**
 * Cancel a pumping cycle.
 *
 * @return 1 if successful, 0 on error.
 */
int
ctdo_cancel_pump(void)
{
    char	cmd[2];

    serial_inflush(serial_dev);
    cmd[0] = 0x1b;
    cmd[1] = '\0';

    if(!serial_chat(serial_dev, cmd, "S>", 2L) &&
	!serial_chat(serial_dev, cmd, "S>", 2L))
    {
	log_error("ctdo", "Device not responding\n");
	return 0;
    }


    timestamps[T_PUMPON] = 0;
    return serial_chat(serial_dev, "pumpoff", "S>", 2L);
}

/**
 * Wake the device from sleep mode.
 *
 * @return 1 if successful, 0 on error.
 */
int
ctdo_wakeup(void)
{
    PURGE_INPUT(serial_dev);
    return serial_chat(serial_dev, "\r", "S>", 2L);
}

/**
 * Turn on the oxygen sensor.
 *
 * @return 1 if successful, 0 on error.
 */
int
ctdo_start_oxy(void)
{

    PURGE_INPUT(serial_dev);

    if(!serial_chat(serial_dev, "oxon\r\n", "S>", 2L))
	return 0;
    oxy_on = 1;
    timestamps[T_OXYON] = RtcToCtm();
    log_event("O2 sensor on\n");

    return 1;
}

/**
 * Turn off the oxygen sensor.
 *
 * @return 1 if successful, 0 on error.
 */
int
ctdo_stop_oxy(void)
{
    if(oxy_on)
    {
	PURGE_INPUT(serial_dev);
	if(!serial_chat(serial_dev, "oxoff\r\n", "S>", 2L))
	    return 0;
	oxy_on = 0;
	timestamps[T_OXYON] = 0;
	log_event("O2 sensor off\n");
    }

    return 1;
}


/**
 * Start the sampling process.
 * Start the sampling process. If oxygen sensor is on, S, T, and Oxy will be
 * sampled, otherwise, only S and T.
 *
 * @param  p  pressure value in decibars
 * @return 1 if successful or 0 if an error occurs.
 */
int
ctdo_start_sample(double p)
{
    char cbuf[12];

    PURGE_INPUT(serial_dev);

    if(p < 0)
	p = 0.;

    if(oxy_on)
	sprintf(cbuf, "%05dTOS\r\n", (int)(p*10));
    else
	sprintf(cbuf, "%05dTS\r\n", (int)(p*10));
#ifdef SENS_DEBUG
    log_event("CTDO command = %s\n", cbuf);
#endif
    if(!serial_chat(serial_dev, cbuf, cbuf, 3L))
	return 0;
    timestamps[T_SAMPLE] = RtcToCtm();
    return 1;
}

/**
 * Return the sample start time.
 *
 */
unsigned long
ctdo_start_time(void)
{
    return timestamps[T_SAMPLE];
}

/**
 * Test whether sampling process is done.
 *
 * @return true if sampling process is done.
 */
int
ctdo_data_ready(void)
{
    if(timestamps[T_SAMPLE] == 0)
	return 1;
    return (RtcToCtm() - timestamps[T_SAMPLE]) >= CTDO_SAMPLE_TIME;
}


/**
 * Read the most recent data sample.
 *
 * @param  ctdo  pointer to output data structure.
 * @return 1 if successful or 0 if an error occurs.
 */
int
ctdo_read_data(CTDOdata *ctdo)
{
    int		c, i, mode, nr_fields;
    ulong	start;
    char	*fmt;
    float	f[4];
    char	linebuf[64];

    nr_reads++;
    serial_inflush(serial_dev);

    if(!serial_chat(serial_dev, "\r\n", "S>", 2L) &&
	!serial_chat(serial_dev, "\r\n", "S>", 2L))
    {
	log_error("ctdo", "Device not responding\n");
	nr_errors++;
	return 0;
    }

    memset(f, 0, sizeof(f));

    if(oxy_on)
    {
#ifdef SENS_DEBUG
	log_event("CTDO command = SSOX\n");
#endif
	nr_fields = 4;
	fmt = "%f, %f, %f, %f";
	serial_chat(serial_dev, "SSOX\r\n", "SSOX\r\n", 2L);
    }
    else
    {
#ifdef SENS_DEBUG
	log_event("CTDO command = SS\n");
#endif
	nr_fields = 3;
	fmt = "%f, %f, %f";
	serial_chat(serial_dev, "SS\r\n", "SS\r\n", 2L);
    }


    /* Set the interface to not block on input */
    mode = serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    i = 0;
    start = MilliSecs();
    while((c = sgetc_timed(serial_dev, start, SAMPLE_TIMEO_MS)) != -1 &&
	  c != '\r')
	linebuf[i++] = c;
    linebuf[i] = '\0';
    serial_inmode(serial_dev, mode);
#ifdef SENS_DEBUG
    log_event("CTDO response = %s\n", linebuf);
#endif

    if(sscanf(linebuf, fmt, &f[0], &f[1], &f[2], &f[3]) != nr_fields)
    {
	log_error("ctdo", "Error reading sample (%s)\n", linebuf);
	nr_errors++;
	return 0;
    }

    ctdo->t = f[1];
    ctdo->s = f[2];
    ctdo->oxy = (long)f[3];

    return 1;
}


/**
 * Pass through mode.
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 */
void
ctdo_test(void)
{
    printf("Pass-through mode: CTRL-c to exit\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}

/**
 * Send a command to the device.
 * This is convenience function that is intended for use by the
 * systest program. It sends the command string to the device
 * and checks for a successful response.
 *
 * @param  cmd  command string
 * @return 1 if successful, otherwise 0.
 */
int
ctdo_cmd(const char *cmd)
{
    if(!strncmp(cmd, "cancel", 6L))
	return ctdo_cancel_pump();
    else if(!strncmp(cmd, "oxon", 4L))
	return ctdo_start_oxy();
    else if(!strncmp(cmd, "oxoff", 5L))
	return ctdo_stop_oxy();
    else
	return (serial_chat(serial_dev, cmd, "?cmd S>|S>", 3L) == 2);
}
