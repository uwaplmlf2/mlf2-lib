/*
** Interface to the Auxilliary Processor board. This is a Linux
** based system which manages an altimeter and a still camera.
**
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "iofuncs.h"
#include "xmodem.h"
#include "lpsleep.h"
#include "aux.h"

#define LOGIN_PROMPT    "ogin:"
#define SHELL_PROMPT    "$ "

#define PURGE_INPUT(d, t) while(sgetc_timed(d, MilliSecs(), (t)) != -1)

#ifndef __GNUC__
#define __inline__
#endif

#define RESPONSE_LEN    1024
#define RESPONSE_END    '$'

static int              ref = 0;
static int              serial_dev, dev_ready;

static int xm_last_index = -1;

static int
download_file(char *buf, int size, int index, void *arg)
{
    FILE        *fp = (FILE*)arg;

    if(index != xm_last_index)
    {
        SerPutByte('.');
        if(fwrite(buf, (size_t)1, (size_t)size, fp) != (size_t)size)
        {
            log_error("download", "File write error\n");
            return 0;
        }

        xm_last_index = index;
    }

    return 1;
}

/**
 * Callback to reopen the device interface upon return from low-power sleep.
 */
static void
aux_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
    PURGE_INPUT(serial_dev, 100);
    log_event("Reopened Aux Board serial interface\n");
}

/**
 * aux_init - initialize the interface.
 */
int
aux_init(void)
{
    if(ref > 0)
        goto initdone;

    dev_ready = 0;
    if((serial_dev = serial_open(AUX_DEVICE, 0, AUX_BAUD)) < 0)
    {
        log_error("aux", "Cannot open serial device (code = %d)\n",
                  serial_dev);
        return 0;
    }

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.  This will allow the TT8 to sleep between
    ** calls to the device.
    */
    add_after_hook("AUX-reopen", aux_reopen);

initdone:
    ref++;
    return 1;
}

/**
 * aux_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
aux_shutdown(void)
{
    if(ref == 0)
        return;
    ref = 0;
    dev_ready = 0;
    remove_after_hook("AUX-reopen");
    serial_close(serial_dev);
}

/**
 * Check if device is ready.
 *
 * @return true if device is ready.
 */
int
aux_dev_ready(void)
{
    int         c;

    if(ref == 0)
        return 0;

    while(!dev_ready &&
          (c = sgetc_timed(serial_dev, MilliSecs(), 1000L)) != -1)
    {
        if(c == RESPONSE_END)
            dev_ready = 1;
    }

    return dev_ready;
}

int
aux_login(void)
{
    return dev_ready;
}

void
aux_send(const char *text)
{
    PURGE_INPUT(serial_dev, 100);
    sputz(serial_dev, text);
}

static char shell_response[RESPONSE_LEN+1];

/**
 * Send a shell command. Wait for the command to
 * execute (if timeout > 0).
 *
 * @param  command  shell command string (with trailing '\n')
 * @param  timeout  number of milliseconds to wait for a response.
 */
int
aux_shell_command(const char *command, long timeout)
{
    PURGE_INPUT(serial_dev, 100);
    memset(shell_response, 0, RESPONSE_LEN);
    sputz(serial_dev, command);
    return (aux_shell_response(timeout) == NULL) ? 0 : 1;
}

/**
 * Return the most recent response from the shell.
 */
char*
aux_shell_response(long timeout)
{
    long        t;
    char        *p;
    int         c, i;

    if(strlen(shell_response) == 0 && timeout > 0)
    {
        p = shell_response;

        /* Skip input until the first linefeed */
        t = MilliSecs();
        while((c = sgetc_timed(serial_dev, t, timeout)) != '\n')
        {
            if(c < 0)
            {
                log_error("aux", "Timeout waiting for echo\n");
                return NULL;
            }
        }

        i = 0;
        while((c = sgetc_timed(serial_dev, t, timeout)) != RESPONSE_END)
        {
            if(c < 0)
            {
                log_error("aux", "Timeout waiting for response\n");
                return NULL;
            }

            if(i < RESPONSE_LEN)
            {
                *p = c;
                p++;
                i++;
            }
        }
        *p = '\0';
    }

    return shell_response;
}

int
aux_xmodem_get(const char *cmd, const char *filename)
{
    FILE        *fp;
    int         r;

    r = serial_chat(serial_dev, cmd, "command now.|$ ", 5L);
    switch(r)
    {
        case 0:
            log_error("aux", "Timeout in file download\n");
            return 0;
        case 2:
            log_error("aux", "File not found (%s)\n", cmd);
            return 0;
    }

    if((fp = fopen(filename, "wb")) == NULL)
    {
        log_error("iridium", "Cannot open file %s\n", filename);
        return 0;
    }

    printf("Downloading %s ", filename);
    fflush(stdout);
    xm_last_index = -1;
    r = xmodem_recv(serial_dev, download_file, (void*)fp);
    fclose(fp);
    fputs(" done\n", stdout);

    return r;
}

int
aux_xmodem_get_file(const char *filename, int remove)
{
    char        cmd[32];

    if(remove)
        sprintf(cmd, "send -r %s\n", filename);
    else
        sprintf(cmd, "send %s\n", filename);
    return aux_xmodem_get(cmd, filename);
}
