/*
** $Id: flr.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Interface to Wetlabs Fluorometer 
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <setjmp.h>
#include "serial.h"
#include "salarm.h"
#include "iostream.h"
#include "log.h"
#include "flr.h"

#define WARM_UP_MSECS	350L

static int ref = 0;
static int serial_dev = -1;
static IOstream *flr_ios;
static jmp_buf	recover;
static long init_time;
static unsigned long nr_reads, nr_errors;

/*
** Initialization commands and desired responses
*/
static char *cmd_resp[] = {
    "p",	">",
    "b",	"SGALWM.",
    "000010",	">",
    "q",	"Q.",
    "1\r\n",	">",
    "d",	"D.",
    "0\r\n",	">",
    "t",	"T.",
    "200",	">",
    NULL,	NULL
};

static void
catch_alarm(void)
{
#ifdef __GNUC__
    longjmp(recover, 1L);
#else
    longjmp(recover, 1);
#endif
}

/**
 * flr_init - initialize the fluorometer interface.
 *
 * Open a connection to the fluorometer and initialize the device.  Returns
 * 1 if successful, otherwise 0.
 */
int
flr_init(void)
{
    char	**p;
    
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(FLR_DEVICE, FLR_MINOR, FLR_BAUD)) < 0)
    {
	log_error("flr", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }
    
    init_time = MilliSecs();
    
    if((flr_ios = iosopen(serial_dev, IOS_LINEBUF)) == 0)
    {
	serial_close(serial_dev);
	log_error("flr", "Cannot open stream buffer\n");
	return 0;
    }
    
    /*
    ** Allow a warm-up period then send the initialization commands.
    */
    while((MilliSecs() - init_time) < WARM_UP_MSECS)
	;

initdone:
    ref++;

    p = &cmd_resp[0];
    while(p[0] && p[1])
    {
	if(!serial_chat(serial_dev, p[0], p[1], 4L))
	{
	    log_error("flr", "Error sending command, %s\n", p[0]);
	    flr_shutdown();
	    return 0;
	}
	p += 2;
	LMDelay(8000);
    }


    return 1;
}

/**
 * flr_shutdown - shutdown the fluorometer interface.
 *
 * Shutdown the fluorometer interface and power off the device.
 */
void
flr_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    
    iosclose(flr_ios);
    serial_close(serial_dev);
}


/**
 * flr_dev_ready - check if device is ready.
 *
 * Return true if the device is ready to accept commands.
 */
int
flr_dev_ready(void)
{
    return ref;
}

/**
 * flr_set_params - set fluorometer parameters.
 * @navg: number of samples to average.
 *
 * Set sampling parameters for the fluorometer device, currently there
 * is only one.  Returns 1 if successful, 0 if an error occurs.
 */
int
flr_set_params(int navg)
{
    char	buf[4];
    
    if(navg < 1 || navg > 255)
    {
	log_error("flr", "Bad parameter, navg = %d\n", navg);
	return 0;
    }

    sprintf(buf, "%d", navg);
    
    if(!serial_chat(serial_dev, "t", "T.", 2L) ||
       !serial_chat(serial_dev, buf, ">", 2L))
    {
	log_error("flr", "Cannot set time constant, %s\n", buf);
	return 0;
    }
    
    return 1;
}

/**
 * flr_get_stats - fetch read error statistics
 * @errs: returned error count
 * @reads: returned read count.
 */
void
flr_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}

/**
 * flr_read_data - read fluorometer data value.
 *
 * Returns the next sample value from the fluorometer (this may actually be
 * an average of many samples, see flr_set_params()) as millivolts between
 * 0 and 4095.  Returns -1 if an error occurs.
 */
int
flr_read_data(void)
{
    int		c;
    char	*p, buf[8];
    
    if(ref <= 0)
	return 0;
   
    nr_reads++;
    
    alarm_set_ms((long)FLR_TIMEOUT_MS, ALARM_ONESHOT, catch_alarm);
    if(setjmp(recover) != 0)
    {
	log_error("flr", "Timeout reading fluorometer\n");
	nr_errors++;
	return -1;
    }

    /*
    ** Send an 'r' command to read a data sample.  The device will respond
    ** with the following string:
    **
    **	\r\nDATE\tTIME\tSAMPLE\r\n
    **
    */
    serial_write(serial_dev, "r", 1L);

    /*
    ** Look for two TABs which will put us at the start of SAMPLE.  
    */
    while(iosgetc(flr_ios) != '\t')
	;
        
    while(iosgetc(flr_ios) != '\t')
	;
    
    p = buf;
    while((c = iosgetc(flr_ios)) != '\r')
	*p++ = c;
    *p = '\0';
    alarm_shutdown();
    
    return atoi(buf);
}


