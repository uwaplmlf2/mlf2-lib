#ifndef MAG_H
#define MAG_H

#define MAG_DEVICE      3
#define MAG_BAUD        9600

typedef struct mag_data {
    short       x;
    short       y;
    short       z;
    short       m;
} MagData;

int mag_init(void);
void mag_shutdown(void);
int mag_dev_ready(void);
int mag_read_data(MagData *md, long timeout);

#endif /* MAG_H */
