/*
** $Id: alt.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Interface to the Tritech PA200 Sonar Altimeter.  This unit is
** a very simple device.  When powered on, it outputs a stream of
** altitude measurments in meters.
**
*/
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include "salarm.h"
#include "serial.h"
#include "iostream.h"
#include "log.h"
#include "alt.h"

#ifndef __GNUC__
#define __inline__
#endif

#define WARM_UP_MSECS	1000L

#define START_CHAR	'\n'
#define END_CHAR	'm'

static int ref = 0;
static int serial_dev = -1;
static jmp_buf	recover;
static long init_time;
static unsigned long nr_errors, nr_reads;

static __inline__ int serial_getc(int desc)
{
    char	c;
    return (serial_read(desc, &c, (size_t)1) == 1) ? (int)c : -1;
}

/**
 * alt_init - initialize altimeter
 *
 * Power-up the altimeter and initialize the serial interface.
 * Returns 1 if successful, otherwise 0.
 */
int
alt_init(void)
{
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(ALT_DEVICE, 0, ALT_BAUD)) < 0)
    {
	log_error("alt", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    init_time = MilliSecs();

initdone:
    ref++;
    
    return 1;
}

/**
 * alt_shutdown - shutdown the altimeter
 *
 * Close the interface to the altimeter.
 */
void
alt_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    
    serial_close(serial_dev);
}

/**
 * alt_dev_ready - check if device is ready.
 *
 * Returns 1 if device is ready to accept a command, otherwise 0.
 */
int
alt_dev_ready(void)
{
    return ((MilliSecs() - init_time) >= WARM_UP_MSECS);
}

static void
catch_alarm(void)
{
#ifdef __GNUC__
    longjmp(recover, 1L);
#else
    longjmp(recover, 1);
#endif
}

/**
 * alt_get_stats - fetch read error statistics
 * @errs: returned error count
 * @reads: returned read count.
 */
void
alt_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}

/**
 * alt_read_data - read the next altimeter data record.
 *
 * Returns altitude in centimeters (0 to 10000) or -1 if
 * an error occured.  Error message is logged.
 */
int
alt_read_data(void)
{
    register int	c, i;
    float		meters;
    char		buf[8];
    
    if(ref <= 0)
	return 0;

    nr_reads++;
    
    alarm_set((long)ALT_TIMEOUT, ALARM_ONESHOT, catch_alarm);
    if(setjmp(recover) != 0)
    {
	log_error("alt", "Timeout reading altimeter\n");
	nr_errors++;
	return -1;
    }
    
    while((c = serial_getc(serial_dev)) != START_CHAR)
	;
    i = 0;
    while((c = serial_getc(serial_dev)) != END_CHAR && 
	  (isdigit(c) || c == '.'))
    {
	buf[i++] = c;
	if(i >= 7)
	    break;
    }
    alarm_shutdown();
    buf[i] = '\0';

    return (sscanf(buf, "%f", &meters) != 1) ? -1 : (int)(meters * 100);
}

