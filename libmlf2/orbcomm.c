/*
** $Id: orbcomm.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Interface to the Orbcomm Subscriber Communicator (SC).  The serial
** protocol used between the SC and the host is described in the
** "ORBCOMM Serial Interface Specification" document.
**
*/
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <setjmp.h>
#include <ctype.h>
#include <time.h>
#include "serial.h"
#include "log.h"
#include "salarm.h"
#include "orbcomm.h"
#ifdef __TT8__
#include <tt8lib.h>
#include "ioports.h"
#else
#include <errno.h>
#include <unistd.h>
#include "linux_tt8.h"
#endif

#define WARM_UP_MS		3000L
#define ACK_PKTLEN		7

/*
** Table for the handler functions called by orbcomm_dispatch
*/
typedef struct {
    f_packet_handler	f;
    unsigned long	user_data;
} packet_table_entry;

static packet_table_entry	pkt_table[NR_PACKET_TYPES];


/* Status State Messages */
static const char *sc_status_state[] = {
    "Idle",
    "Sending SC-originated message",
    "Sending SC-originated report",
    "Sending SC-originated globalgram",
    "Receiving SC-terminated message",
    "Receiving SC-terminated command",
    "Receiving SC-terminated globalgram",
    "Self-test",
    "Local loopback",
    "Gateway loopback",
};

#define NR_STATE_STR 10

/* Packet "address" */
typedef enum { FROM_SC = 0x05, TO_SC = 0x85 } Paddr;

static unsigned char orb_pkt[ORB_MAX_PKTLEN];
static unsigned char orb_ack_pkt[] = {
    TO_SC, ACK, 0x07, 0x00, 0x00, 0xc1, 0xb2
};

static unsigned char	orb_message_id = 1;

static jmp_buf		recover;
static int		serial_dev = -1;
static long		init_time;
static int		ref = 0;

/*
 * Hex dump the packet contents to stderr.
 */
#ifdef ORB_DEBUG
static void
dump_packet(ORB_packet_t *pkt)
{
    register int	len = PACKET_LEN(pkt);
    register int	i;
    
    fprintf(stderr, "%02x %02x %02x %02x %02x\n", (unsigned)pkt->dir,
	    (unsigned)pkt->type, (unsigned)pkt->lsb_length,
	    (unsigned)pkt->msb_length, (unsigned)pkt->retry_count);
    len -= 5;
    for(i = 0;i < len;i++)
	fprintf(stderr, "%02x", pkt->payload[i]);
    fputc('\n', stderr);
}
#endif

static void
catch_alarm(void)
{
    longjmp(recover, 1L);
}


/*
 * Calculate the Fletcher Checksum for the packet and store the
 * result in the last two bytes.
 */
static void
orb_fill_checksum(ORB_packet_t *pkt)
{
    register int		len;
    register unsigned char	c0, c1;
    register byte		*p = (byte*)pkt;
    
    len = PACKET_LEN(pkt);
    c0 = c1 = 0;

    /* Clear the current checksum */
    p[len-1] = p[len-2] = 0;

#ifdef ORB_DEBUG
    fprintf(stderr, "Checksumming packet: ");
#endif    
    while(len--)
    {
#ifdef ORB_DEBUG
	fprintf(stderr, "%02x ", *p);
#endif
	
	c0 += *p++;
	c1 += c0;
    }
    
    p[-2] = c0 - c1;
    p[-1] = c1 - c0 - c0;
#ifdef ORB_DEBUG
    fprintf(stderr, " (%02x %02x)\n", p[-2], p[-1]);
#endif    
}

/*
 * Verify a packet's checksum.
 */
static int
orb_verify_checksum(ORB_packet_t *pkt)
{
    register int		len;
    register unsigned char	c0, c1;
    register byte		*p = (byte*)pkt;
    
    len = PACKET_LEN(pkt);
    c0 = c1 = 0;
    
    while(len--)
    {
	c0 += *p++;
	c1 += c0;
    }

    /*
    ** Kludge to work around packets from SC with no checksum
    */
    if(p[-2] == 0 && p[-1] == 0)
	return 1;
    
    return (c0 + c1) == 0 ? 1 : 0;
}

static int
ack_ok(ORB_ack_packet_t *pkt)
{
    int		len;
    
    len = PACKET_LEN(pkt);
    return (pkt->type == ACK && len == ACK_PKTLEN && 
	    orb_verify_checksum((ORB_packet_t*)pkt));
}

/*
 * orb_send_packet - send packet and wait for an ACK.
 * @sd: serial device
 * @timeout: number of seconds to wait for an ACK
 * @max_retries: maximum number of packet re-sends.
 *
 * Send the packet buffer to the SC and wait for an ACK.  Returns 1 if 
 * successful or zero if an error occurs.
 *
 */
static int
orb_send_packet(int sd, long timeout, int max_retries, ORB_packet_t *pkt)
{
    int				len, i;
    char			*p;
    ORB_ack_packet_t		ack;
    
    pkt->retry_count = 0;

    if(setjmp(recover) != 0)
    {
	log_error("orbcomm", "Timeout waiting for LL ACK\n");
	
	if(++pkt->retry_count > max_retries)
	    return 0;
    }

    len = PACKET_LEN(pkt);
    
    orb_fill_checksum(pkt);

    /*
    ** Write the packet and wait for the link-level ACK.  Set the alarm
    ** first because 'alarm_set' briefly disables interrupts and may cause
    ** us to miss input characters if we do it after the write.
    */
    alarm_set(timeout, ALARM_ONESHOT, catch_alarm);
    p = (char*)pkt;
    for(i = 0;i < len;i++)
    {
	/*
	** Add a 2ms delay after every 4th byte to compensate for the
	** fact that we have no flow control on the serial interface.
	*/
	serial_write(sd, p, (size_t)1);
	if((i & 0x03) == 0)
	    LMDelay(4000);
	p++;
    }
    
    ack.dir = 0;
    while(serial_read(sd, (char*)&ack.dir, 1L) && ack.dir != FROM_SC)
	;
    
    alarm_reset();

    serial_read(sd, (char*)&ack.type, 6L);

    alarm_shutdown();
    
    return ack_ok(&ack);
}

static ORB_packet_t*
orb_recv_packet(int sd, long timeout)
{
    int				len, bytes_left;
    register ORB_packet_t	*rpkt = (ORB_packet_t*)&orb_pkt[0];

    if(setjmp(recover) != 0)
	return NULL;

    if(timeout > 0)
	alarm_set(timeout, ALARM_ONESHOT, catch_alarm);

    /*
    ** Wait for the "direction" byte which indicates the start of
    ** a command packet.
    */
    while(serial_read(sd, (char*)&rpkt->dir, 1L) && rpkt->dir != FROM_SC)
	;
    
    if(serial_read(sd, (char*)&rpkt->type, (size_t)3) == 3)
    {
	/*
	** The next three bytes contain the packet type code and
	** total packet length.
	*/
	len = PACKET_LEN(rpkt);
	
	if(len == 0 || len > sizeof(orb_pkt))
	{
#ifdef ORB_DEBUG
	    log_error("orbcomm", "Bad packet size, %d\n", len);
#endif	    
	    return NULL;
	}
	
	bytes_left = len - 4;
    
	/*
	** Read the rest of the packet.
	*/
	if(serial_read(sd, (char*)&rpkt->retry_count, 
		       (size_t)bytes_left) == bytes_left)
	{
	    if(timeout > 0)
		alarm_shutdown();
	    
	    if(!orb_verify_checksum(rpkt))
	    {
		log_error("orbcomm", "Checksum error\n");
#ifdef ORB_DEBUG
		dump_packet(rpkt);
#endif
		return NULL;
	    }

            /* Packet is OK, send an ACK */
	    serial_write(sd, (char*)orb_ack_pkt, (size_t)ACK_PKTLEN);
    
	    return rpkt;
	}
	
    }

    log_error("orbcomm", "Read error\n");
    
    if(timeout > 0)
	alarm_shutdown();
    return NULL;
}


/**
 * orbcomm_init - initialize the Orbcomm interface.
 *
 * Open connection to the ORBCOMM Subscriber Communicator (SC).  The packet
 * based communications protocol used between the float and the SC is
 * described in the "ORBCOMM Serial Interface Specification" document.
 *
 * Returns 1 if successful, otherwise 0.
 */
int
orbcomm_init(void)
{

    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(ORBCOMM_DEVICE, 0, ORBCOMM_BAUD)) < 0)
    {
	log_error("orbcomm", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

#ifdef __TT8__
    /* Connect COM2 to the modem */
    iop_set(IO_E, (unsigned char)0x08);
    DelayMilliSecs(500L);
    
    /* Activate the serial comm interface chip */
    iop_set(IO_E, (unsigned char)0x02);
    DelayMilliSecs(500L);
#endif
    
initdone:
    ref++;
    init_time = MilliSecs();
    
    return 1;
}

/**
 * orbcomm_send_ack - send an ACK to the SC.
 *
 * Send an ACK packet to the Subscriber Communicator.
 */
void
orbcomm_send_ack(void)
{
    serial_write(serial_dev, (char*)orb_ack_pkt, (size_t)ACK_PKTLEN);
}

/**
 * orbcomm_dev_ready - check if device is ready.
 *
 * Return true if the device is ready to accept commands.
 */
int
orbcomm_dev_ready(void)
{
    return ((MilliSecs() - init_time) > WARM_UP_MS);
}

/**
 * orbcomm_shutdown - shutdown the Orbcomm interface.
 *
 * Shutdown the Orbcomm interface.  The device is powered-down.
 */
void
orbcomm_shutdown(void)
{
    if(--ref > 0)
	return;

#ifdef __TT8__
    iop_clear(IO_E, (unsigned char)(0x08 | 0x02));
#endif
    
    serial_close(serial_dev);
}

/**
 * orbcomm_add_handler - add a new packet handler function.
 * @type: packet type code.
 * @f: callback function pointer.
 * @user_data: will be passed to @f as its second argument.
 *
 * Add a new packet handler function.  There can be at most one handler for
 * each packet type so calling this function will replace any existing
 * handler.  The callback function @f takes two arguments, a pointer to
 * the packet data structure and @user_data.
 */
void
orbcomm_add_handler(Ptype type, f_packet_handler f, unsigned long user_data)
{
    pkt_table[type-1].f = f;
    pkt_table[type-1].user_data = user_data;
}

/**
 * orbcomm_del_handler - remove a packet handler function.
 * @type: packet type code.
 *
 * Remove the handler for packet type @type.
 */
void
orbcomm_del_handler(Ptype type)
{
    pkt_table[type-1].f = NULL;
}

/**
 * orbcomm_dispatch - read incoming packets and pass to handlers.
 * @timeout: maximum number of seconds to listen for packets.
 *
 * Read incoming packets and pass them to the appropriate handler.  If the
 * handler returns a non-zero value, orbcomm_dispatch returns immediately
 * otherwise it continues to read packets until it times-out.  If @timeout
 * is zero, the function reads packets until the handler returns non-zero.
 *
 * Returns 1 if the handler returns non-zero or 0 on timeout.  
 */
int
orbcomm_dispatch(long timeout)
{
    ORB_packet_t	*pkt;
    int			type;
    register long	start, t;

    start = RtcToCtm();
    t = 0;
    
    while(timeout <= 0 || (t = RtcToCtm() - start) < timeout)
    {
	if((pkt = orb_recv_packet(serial_dev, timeout-t)) == NULL)
	    continue;
	
#if ORB_DEBUG
	fprintf(stderr, "Received packet type %d\n", (int)pkt->type);
#endif	
	type = pkt->type - 1;
	if(type >= NR_PACKET_TYPES)
	    continue;
	
	if(pkt_table[type].f &&
	   (*pkt_table[type].f)(pkt, pkt_table[type].user_data))
	    return 1;
    }

    return 0;
}

/**
 * orbcomm_read_packet - read the next packet of a specified type.
 * @timeout: maximum number of seconds to wait.
 * @type: packet type code.
 * @rpkt: pointer to the returned packet.
 *
 * Reads the next packet of type @type from the SC.  If @type == 0, the
 * function reads the next packet of any type.  @timeout is the maximum
 * number of seconds to wait.  If @timeout == 0, wait forever.  If @rpkt
 * is NULL, memory for the packet is allocated from the heap and must be
 * freed by the caller.  Returns a pointer to the packet or NULL if an
 * error occurs.  Errors are logged.
 */
ORB_packet_t*
orbcomm_read_packet(long timeout, int type, ORB_packet_t *rpkt)
{
    ORB_packet_t	*pkt;
    byte		*pbuf;
    int			len;
    register long	start, t;
    
    start = RtcToCtm();
    t = 0;
    
    while(timeout <= 0 || (t = RtcToCtm() - start) < timeout)
    {
	if((pkt = orb_recv_packet(serial_dev, timeout-t)) == NULL)
	    continue;
	
#if ORB_DEBUG
	fprintf(stderr, "Received packet type %d\n", (int)pkt->type);
#endif	
	if(type == 0 || type == pkt->type)
	{
	    len = PACKET_LEN(pkt);
 
	    if(rpkt)
	    {
		/*
		** Caller has supplied a packet data structure.  Caller
		** is responsible for ensuring the data structure is
		** large enough to hold the requested packet type.
		*/
		memcpy(rpkt, pkt, (long)len);
		return rpkt;
	    }
	    
	    /*
	    ** rpkt not specified.  Allocate heap memory for the new
	    ** packet.  Caller is responsible for freeing the memory.
	    */
	    if((pbuf = (byte*)malloc((long)len)) == NULL)
	    {
#ifdef __TT8__
		log_error("orbcomm", "Cannot allocate %d bytes\n", len);
#endif
		return NULL;
	    }

	    memcpy(pbuf, pkt, (long)len);
    
	    return (ORB_packet_t*)pbuf;
	}
    }
    
    return NULL;
}

/**
 * orbcomm_read_data - read data section of next packet.
 * @buf: buffer for returned data.
 * @n: size of @buf in bytes.
 * @timeout: maximum number of seconds to wait.
 *
 * Read up to @n bytes from the data section (payload) of the next packet 
 * from the SC.  Returns the number of bytes read.
 */
int
orbcomm_read_data(char *buf, int n, long timeout)
{
    ORB_packet_t	*pkt;
    int			payload_size, len;
    
    if((pkt = orb_recv_packet(serial_dev, timeout)) == NULL)
	return 0;

    len = PACKET_LEN(pkt);

    /* Subtract the packet header and the checksum */
    payload_size = len - sizeof(ORB_packet_t) - 1;
    memcpy(buf, &pkt->payload[0], (long)(payload_size > n ? n : payload_size));
    
    return payload_size;
}

static void*
fp_memcpy(register void *dest, void *src, size_t n)
{
    fread(dest, 1L, n, (FILE*)src);
    return dest;
}
    

/**
 * orbcomm_send_msg - send an Orbcomm message to a single recipient.
 * @to: recipient's address.
 * @subj: message subject.
 * @mtype: message type, %MSG_TEXT or %MSG_BINARY.
 * @src: handle for message source.
 * @msglen: length of the message body.
 * @fcpy: message source function.
 *
 * This function calls @fcpy to copy @msglen bytes and sends the resulting
 * message to the recipient specified by @to.  @to may be a standard Internet
 * email address or a single character between 1 and 8 (i.e. "\001" to "\008")
 * specifying the recipient's Orbcomm "speed dial" address.
 *
 * Returns a non-zero message ID if successful, otherwise 0.
 */
int
orbcomm_send_msg(const char *to, const char *subj, MsgBodyType mtype,
		 void *src, int msglen, f_datacpy fcpy)
{
    ORB_outmsg_packet_t		*pkt = (ORB_outmsg_packet_t*)orb_pkt;
    register char		*datap, *data_start;
    unsigned			len, n;
    
    if(msglen > ORB_MAX_MSG_BODY)
    {
#ifdef ORB_DEBUG
	log_error("orbcomm", "Message body too large (%d bytes)\n",
		  msglen);
#endif	
	return 0;
    }
    
    memset(pkt, 0, sizeof(ORB_outmsg_packet_t));
    
    len = sizeof(ORB_outmsg_packet_t) - 1;
    
    if(orb_message_id == 0)
	orb_message_id = 1;
    
    pkt->dir 		= TO_SC;
    pkt->type 		= SC_ORIG_MSG;
    pkt->gateway_id 	= 1;
    pkt->polled     	= 0;
    pkt->ack_level 	= ACK_GWY_DELIVERY;
    pkt->priority 	= PRI_NORMAL;
    pkt->msg_type	= mtype;
    pkt->mha_ref_num	= orb_message_id++;
    pkt->nr_recipients	= 1;
    pkt->subject	= (subj == NULL) ? 0 : 1;

    data_start = datap = (char*)&pkt->payload[0];

    /*
    ** Only a single recipient is allowed.  The recipient may be a
    ** single digit "O/R" indicator or a null-terminated email
    ** address.
    */
    if(!isprint(to[0]))
    {
	*datap++ = to[0];
	len++;
    }
    else
    {
	n = strlen(to) + 1;
	if(n > ORB_MAX_ADDR)
	{
#ifdef ORB_DEBUG
	    log_error("orbcomm", "Address too large (%d bytes)\n", n);
#endif
	    return 0;
	}
	
	strcpy(datap, to);
	datap += n;
	len += n;
    }
    
    if(subj)
    {
	n = strlen(subj) + 1;
	if(n > ORB_MAX_SUBJ)
	{
#ifdef ORB_DEBUG
	    log_error("orbcomm", "Subject too large (%d bytes)\n", n);
#endif
	    return 0;
	}

	strcpy(datap, subj);
	datap += n;
	len += n;
    }

    /*
    ** First byte of the message specifies the message body type
    ** parameter; 5 = IA5TEXT.
    */
    if(mtype == MSG_TEXT)
    {
	*datap++ = 5;
	len++;
    }

    (*fcpy)(datap, src, (long)msglen);
    len += msglen;

    /* Add the size of the checksum */
    len += 2;

    /* Write the length fields */
    pkt->msb_length = (len >> 8) & 0xff;
    pkt->lsb_length = len & 0xff;
    
    return orb_send_packet(serial_dev, ORB_ACK_TIMEOUT, ORB_MAX_RETRIES, 
			   (ORB_packet_t*)pkt) ? 
      ((int)pkt->mha_ref_num & 0xff) : 0;
}

#ifdef ORB_USE_GGRAM
int
orbcomm_send_ggram(int to, const char *ggram)
{
    ORB_outggram_packet_t	*pkt = (ORB_outggram_packet_t*)orb_pkt;
    unsigned			len, n;
    
    memset(pkt, 0, sizeof(ORB_outggram_packet_t));
    
    len = sizeof(ORB_outggram_packet_t) - 1;
    
    pkt->dir 		= TO_SC;
    pkt->type 		= SC_ORIG_GGRAM;
    pkt->gateway_id 	= 1;
    pkt->mha_ref_num	= orb_message_id++;
    pkt->recipient	= to;

    n = strlen(ggram) + 1;
    len += n;
    memcpy(&pkt->payload[0], ggram, (long)n);

    /* Add the size of the checksum */
    len += 2;

    /* Write the length fields */
    pkt->msb_length = (len >> 8) & 0xff;
    pkt->lsb_length = len & 0xff;
    
    return orb_send_packet(serial_dev, ORB_ACK_TIMEOUT, ORB_MAX_RETRIES, 
			   (ORB_packet_t*)pkt);
}
#endif

/**
 * orbcomm_send_text - send a text message.
 * @to: recipient's address.
 * @subj: message subject.
 * @msg: message text.
 *
 * Call orbcomm_send_msg() to send a text message to the recipient.  See
 * orbcomm_send_msg() for details.  Returns a non-zero message ID if 
 * successful or 0 if an error occurs.
 */
int
orbcomm_send_text(const char *to, const char *subj, char *msg)
{
    return orbcomm_send_msg(to, subj, MSG_TEXT, msg, (int)strlen(msg), 
			    (f_datacpy)memcpy);
}

/**
 * orbcomm_send_binary - send a binary message.
 * @to: recipient's address.
 * @subj: message subject.
 * @msg: message contents.
 * @msglen: message length.
 *
 * Call orbcomm_send_msg() to send a binary message to the recipient.  See
 * orbcomm_send_msg() for details.  Returns a non-zero message ID if 
 * successful or 0 if an error occurs.
 */
int
orbcomm_send_binary(const char *to, const char *subj, char *msg,
		    int msglen)
{
    return orbcomm_send_msg(to, subj, MSG_BINARY, msg, msglen, 
			    (f_datacpy)memcpy);
}

/**
 * orbcomm_send_text_file - send a text file.
 * @to: recipient's address.
 * @subj: message subject.
 * @fp: file pointer.
 * @len: message length.
 *
 * Call orbcomm_send_msg() to send the contents of a file as a text message 
 * to the recipient.  See orbcomm_send_msg() for details.  Returns a non-zero 
 * message ID if  successful or 0 if an error occurs.
 */
int
orbcomm_send_text_file(const char *to, const char *subj, FILE *fp, int len)
{
    return orbcomm_send_msg(to, subj, MSG_TEXT, fp, len, fp_memcpy);
}

/**
 * orbcomm_send_binary_file - send a binary file.
 * @to: recipient's address.
 * @subj: message subject.
 * @fp: file pointer.
 * @len: message length.
 *
 * Call orbcomm_send_msg() to send the contents of a file as a binary message 
 * to the recipient.  See orbcomm_send_msg() for details.  Returns a non-zero 
 * message ID if  successful or 0 if an error occurs.
 */
int
orbcomm_send_binary_file(const char *to, const char *subj, FILE *fp, int len)
{
    return orbcomm_send_msg(to, subj, MSG_BINARY, fp, len, fp_memcpy);
}

/**
 * orbcomm_get_status - get SC status.
 * @rpkt: pointer to returned STATUS packet.
 *
 * Send a status-request packet to the SC and read the status packet
 * reply.  Returns 1 if successful, 0 if an error occurs.
 */
int
orbcomm_get_status(ORB_status_packet_t *rpkt)
{
    int				i = 5;
    ORB_comm_packet_t		comm;

    memset(&comm, 0, sizeof(comm));
    
    comm.dir 		= TO_SC;
    comm.type 		= COMM_CMD;
    comm.lsb_length 	= 13;
    comm.msb_length 	= 0;
    comm.type_code 	= CC_STATUS_REQ;
    comm.gateway_id 	= 1;
    
    if(orb_send_packet(serial_dev, ORB_ACK_TIMEOUT, ORB_MAX_RETRIES, 
		       (ORB_packet_t*)&comm))
    {
	while(--i)
	{
	    if(orbcomm_read_packet(10L, STATUS, (ORB_packet_t*)rpkt))
		return 1;
	}
	
    }
    
    return 0;
}

int
orbcomm_send_comm(CommCmdType cctype)
{
    ORB_comm_packet_t		comm;

    memset(&comm, 0, sizeof(comm));
    
    comm.dir 		= TO_SC;
    comm.type 		= COMM_CMD;
    comm.lsb_length 	= 13;
    comm.msb_length 	= 0;
    comm.type_code 	= cctype;
    comm.gateway_id 	= 1;
    
    return orb_send_packet(serial_dev, ORB_ACK_TIMEOUT, ORB_MAX_RETRIES, 
			   (ORB_packet_t*)&comm);
}

const char*
orbcomm_status_str(int code)
{
    return (code >= 0 && code < NR_STATE_STR) ? sc_status_state[code] : "";
}
