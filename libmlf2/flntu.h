/*
** arch-tag: b6b78f1d-b296-49eb-bbe4-c1d2e2c877ca
** $Id: flntu.h,v 990cc1ffca70 2007/11/27 00:55:14 mikek $
*/
#ifndef _FLNTU_H_
#define _FLNTU_H_

#define FLNTU_DEVICE    4
#define FLNTU_BAUD      19200L

#define FLNTU_FIELDS    5

typedef struct flntu {
    short       chl_ref;        /**< Chlorophyll reference */
    short       chl;            /**< Chlorophyll */
    short       ntu_ref;        /**< Turbidity reference */
    short       ntu;            /**< Turbidty */
} __attribute__((packed)) FlntuData;

/* Constants to describe the s-expression output format */
#define FLNTU_DATA_FMT          ">4h"
#define FLNTU_DATA_DESC         "chlref,chl,nturef,ntu"


int flntu_init(void);
void flntu_shutdown(void);
int flntu_dev_ready(void);
int flntu_read_data(FlntuData *fdp, long timeout);
int flntu_close_shutter(void);
void flntu_test(void);

#endif /* _FLNTU_H_ */
