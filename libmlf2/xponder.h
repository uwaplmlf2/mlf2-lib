/*
** arch-tag: 4117cd25-0faa-4fec-9e37-ffb9788537a3 SERIAL
** Time-stamp: <2006-06-30 18:20:02 mike>
*/
#ifndef _XP_H_
#define _XP_H_

#define XP_DEVICE	9
#define XP_BAUD		9600

int xp_init(void);
void xp_shutdown(void);
int xp_dev_ready(void);
void xp_get_stats(unsigned long *errs, unsigned long *reads);
void xp_test(void);
void xp_reopen(void);

int xp_ping(float f_xmit, float f_recv, int pw, long timeo);
long xp_range(void);
int xp_listen(float f_xmit, float f_recv, float pw);
long xp_replies(void);
int xp_response_ready(void);

#endif
