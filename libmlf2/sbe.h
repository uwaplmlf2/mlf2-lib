#ifndef SBE_H
#define SBE_H

typedef enum {
    CB_OK=0,
    CB_ERROR=1,
    CB_WAIT_PROMPT=2
} CBstatus_t;

int sbe_init(int devnum, long baud);
void sbe_shutdown(int sdev);
int sbe_dev_ready(int sdev);
void sbe_test(int sdev);
int sbe_upload(int sdev, int start, int end,
               void (*callback)(void *obj, char *linebuf),
               void *cbdata);
int sbe_command(int sdev, const char *cmd, long timeout,
                CBstatus_t (*callback)(void *obj, char *linebuf), void *cbdata);
#endif /* SBE_H */
