/**@file
 ** Drogue motor control functions.
 **
 ** $Id: drogue.c,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
 **
 **
 */
#include <stdio.h>
#include <tt8lib.h>
#include <tt8.h>
#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <tpu332.h>
#include <qsm332.h>
#include "ioports.h"
#include "timer.h"
#include "log.h"
#include "drogue.h"

/** Motor control port (see ioport)*/
#define DROGUE_PORT IO_C
#define OPEN_MASK   0x01
#define CLOSE_MASK  0x02

#define DEFAULT_TIME    30L

/* Current drogue state */
static volatile drstate_t   __state = DROGUE_UNKNOWN;

/* Motor stop time */
static time_t           __stop_time;

/* Timer ID */
static int  timer_id;

/*
 * Timer interrupt handler.
 */
static int
stop_drogue(void)
{
    iop_clear(DROGUE_PORT, (unsigned char)(OPEN_MASK|CLOSE_MASK));

    /*
    ** The OPENING/OPENED and CLOSING/CLOSED are defined such that
    ** ORing with 1 will change the state appropriately.
    */
    __state |= 1;
    return 0;
}

static void
start_drogue_op(unsigned char mask, long secs, int autostop)
{
    iop_set(DROGUE_PORT, mask);
    if(secs <= 0)
        secs = DEFAULT_TIME;

    if(autostop)
    {
        timer_id = set_timer(secs, stop_drogue);
        __stop_time = 0;
    }
    else
        __stop_time = RtcToCtm() + secs;

}

/**
 * Start drogue opening.
 * Start the drogue open process.  If autostop is non-zero, the check_time
 * parameter is used to set a timer which will trigger a function call to
 * stop the drogue motor.  If autostop is zero, the user must call
 * drogue_check() in order to stop the drogue.
 *
 * @param  check_time  time to allow for opening (seconds).
 * @param  autostop  if true, stop drogue automatically.
 * @return 1 if successful, 0 if the drogue is already opened.
 */
int
drogue_start_open(long check_time, int autostop)
{
    switch(__state)
    {
        case DROGUE_OPENING:
            if(__stop_time > 0 && __stop_time <= RtcToCtm())
                (void)stop_drogue();
            break;
        case DROGUE_CLOSING:    /* abort the closing process */
            if(__stop_time == 0)
                unset_timer(timer_id);
            (void)stop_drogue();
        case DROGUE_UNKNOWN:
        case DROGUE_CLOSED:
            start_drogue_op(OPEN_MASK, check_time, autostop);
            __state = DROGUE_OPENING;
            log_event("Start drogue OPEN\n");
            return 1;
        default:
            break;
    }

    return 0;
}


/**
 * Start drogue closing.
 * Start the drogue close process.  If autostop is non-zero, the check_time
 * parameter is used to set a timer which will trigger a function call to
 * stop the drogue motor.  If autostop is zero, the user must call
 * drogue_check() in order to stop the drogue.
 *
 * @param  check_time  time to allow for closing (seconds).
 * @param  autostop  if true, stop drogue automatically.
 * @return 1 if successful, 0 if the drogue is already opened.
 */
int
drogue_start_close(long check_time, int autostop)
{
    switch(__state)
    {
        case DROGUE_CLOSING:
            if(__stop_time > 0 && __stop_time <= RtcToCtm())
                (void)stop_drogue();
            break;
        case DROGUE_OPENING:    /* abort the opening process */
            if(__stop_time == 0)
                unset_timer(timer_id);
            (void)stop_drogue();
        case DROGUE_UNKNOWN:
        case DROGUE_OPENED:
            start_drogue_op(CLOSE_MASK, check_time, autostop);
            __state = DROGUE_CLOSING;
            log_event("Start drogue CLOSE\n");
            return 1;
        default:
            break;
    }
    return 0;
}


/**
 * Check drogue status.
 * Checks the drogue status and stops the motor if the stop-time specified
 * by the most recent operation has expired.
 *
 * @returns current drogue state.
 */
drstate_t
drogue_check(void)
{
    switch(__state)
    {
        case DROGUE_OPENING:
        case DROGUE_CLOSING:
            if(__stop_time > 0 && __stop_time <= RtcToCtm())
                (void)stop_drogue();
            break;
        default:
            break;
    }

    return __state;
}
