/*
**
*/
#ifndef _PICADC_H_
#define _PICADC_H_

#define PICADC_CMD_GETCOUNT     0x01
#define PICADC_CMD_GETBUFFER    0x02
#define PICADC_CMD_CLEARBUF     0x03
#define PICADC_CMD_ADRESET      0x04

/* Support old single-channel board */
#define PICADC_CMD_GETNEXT      0x02
#define PICADC_CMD_OLDCLEARBUF  0x04
#define PICADC_CMD_OLDADRESET   0x10
#define PICADC_CMD_OVERRUNS     0x08

#define PICADC_GAIN             64
#define PICADC_VREF             2.5

#define PICADC_CHANNELS         3

#define PICADC_MAX_SAMPLES      60

struct adc_sample {
    long    channel[PICADC_CHANNELS];
};

struct spi_clocks {
    long    baud;   /* serial baud rate */
    long    delay;  /* delay btwn chip-select and clk transistion (usecs) */
    long    interval; /* command interval (usecs) */
};

typedef struct adc_sample adc_sample_t;
typedef struct spi_clocks spi_clocks_t;

#define SPI_SETTINGS {.baud = 100000L, .delay = 500L, .interval = 500L}
#define OLDSPI_SETTINGS {.baud = 500000L, .delay = 1L, .interval = 250L}

int picadc_init(spi_clocks_t *clks);
void picadc_spi_init(spi_clocks_t *clks);
void picadc_shutdown(void);
int picadc_read_data(int n, adc_sample_t *data);
int picadc_old_read_data(int n, long data[]);
int picadc_get_count(void);
int picadc_adreset(void);
int picadc_old_adreset(void);
int picadc_old_clear(void);
int picadc_overruns(void);
int picadc_clear(void);
double picadc_counts_to_mv(long counts);

#endif /* _PICADC_H_ */
