/**@file
 ** Motor control interface.
 **
 **
 ** Motor control interface for MLF2.  The ballast piston is driven
 ** by a motor and a linear shaft.  The motor position is measured by
 ** a digital encoder.
 ** @verbatim
        Encoder counts/rev                  =  100
        Motor gearbox ratio                 =  411:1
        Linear travel/rev of gearbox shaft  =  0.1" (0.254 cm)

        linear travel (cm) = counts * 0.254/(100 * 411)
 ** @endverbatim
 ** Position is measured relative to HOME (a point defined by a limit
 ** switch).  Motor must be moved to HOME everytime the float is powered
 ** on.
 **
 */
#define USE_QDEC
#undef MONITOR_STOP
#include <stdio.h>
#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <qsm332.h>
#include <sim332.h>
#include "ioports.h"
#include "power.h"
#include "salarm.h"
#include "timer.h"
#include "log.h"
#include "motor.h"
#include "adintf.h"

#ifdef MOTOR_CHECK_CURRENT
static int motor_current(void);
static int motor_voltage(void);
#endif

/** keyboard characters which will stop a motor operation */
#define is_stop_char(c)     ((c) == 0x0d || (c) == 0x0a)

/** Encoder counts/revolution (default) */
#define COUNTS_PER_REV  100L    /* default value */
/** Gearbox ratio (default) */
#define GEAR_RATIO  411L
/** Turns/inch of the screw */
#define TURNS_PER_INCH  10L


static struct mstate {
    short   dir;        /* direction of motor advance */
    short   valid_pos;  /* set to 1 if motor has been homed */
    short   epsilon;    /* allowable position error (enc. counts) */
    short   trace;      /* trace flag */
    short   status;     /* status flag */
    short   novercurr;  /* number of over-current measurements before
                           an error is flagged. */
    short   ignore_limit;
    long    pos;        /* most recent position (enc. counts) */
    long    target;     /* target position (enc. counts) */
    long    overshoot;  /* motor spindown overshoot (enc. counts) */
    long    last_move;  /* time of last motor movement */
    long    last_pos;   /* position at last motor movement */
    long    home_pos;   /* used when homing the motor */
    long    t_start;    /* motor start time */
    long    counts_per_rev;
    long    gear_ratio;
    long    ballast_min;
    long    ballast_max;
    ulong   t_ticks;    /* timer tick */
    ulong   t_timeout;  /* motor timeout (ticks) */
    double  joules;     /* energy consumed by motor */
    int     (*update)(long, unsigned long);
    void    (*callback)(long, int);
} motor_state = { .counts_per_rev = COUNTS_PER_REV,
                  .gear_ratio = GEAR_RATIO,
                  .ballast_min = BALLAST_MIN,
                  .ballast_max = BALLAST_MAX,
                  .ignore_limit = 0};

/* Motor control bits in output port C */
#define MOTOR_FWD_MASK  (unsigned char)0x10
#define MOTOR_REV_MASK  (unsigned char)0x20

/* Motor state */
#define STOPPED     0
#define MOVING_FWD  1
#define MOVING_REV  2


#ifdef USE_QDEC
#include "qdec.h"
#define ENCODER_START() qdec_start(ENCODER_TPU, QDEC_PRI_TPU, QDEC_SEC_TPU)
#define ENCODER_READ()  qdec_read(ENCODER_TPU)
#define ENCODER_STOP()  qdec_stop(ENCODER_TPU)
#else
#include "itc.h"
#define ENCODER_START() itc_start(ENCODER_TPU)
#define ENCODER_READ()  itc_read(ENCODER_TPU)
#define ENCODER_STOP()  itc_stop(ENCODER_TPU)
#endif

/*
** Subsampling factor for energy consumption calculation.  Higher values
** result in less accurate calculations but more accurate positioning.
*/
#define ENERGY_SUBSAMP      20

#define HOME_TIMEOUT        900

#define MAX_CURRENT_ATOD    2700

/* Length of timer tick */
#define TICKS_PER_SECOND    40L


#if MOTOR_CURRENT_CHAN >= 8
#define SELECT_CURRENT_MUX()    (iop_set(IO_D, (unsigned char)0x02))
#else
#define SELECT_CURRENT_MUX()    (iop_clear(IO_D, (unsigned char)0x02))
#endif

#if MOTOR_VOLTAGE_CHAN >= 8
#define SELECT_VOLTAGE_MUX()    (iop_set(IO_D, (unsigned char)0x02))
#else
#define SELECT_VOLTAGE_MUX()    (iop_clear(IO_D, (unsigned char)0x02))
#endif

#define ABS(x)  ((x) < 0 ? -(x) : (x))

#define mV_to_amps(x)   ((double)(x)/1350.)
#define mV_to_volts(x)  ((double)(x) * 4.322e-3)

static double       speed = 0.;

/**
 * Disable limit switch check during the next motor move.
 */
void
motor_ignore_limit(void)
{
    motor_state.ignore_limit = 1;
}

/**
 * Set motor encoder conversion constants.
 * Set the conversion parameters for converting encoder counts to linear
 * piston travel and vice versa.
 *
 * @param  cpt  encoder counts/turn
 * @param  gr  gear ratio (gr:1)
 */
void
motor_set_encoder(long cpt, long gr)
{
    log_event("Updating motor parameters: (%ld CPT, Gear Ratio = %ld:1)\n",
              cpt, gr);
    motor_state.counts_per_rev = cpt;
    motor_state.gear_ratio = gr;
    motor_state.ballast_max = cm_to_counts(33.5);
    motor_state.ballast_min = cm_to_counts(0.254);
}

/**
 * Convert encoder counts to cm.
 *
 * @param  c  encoder counts.
 * @return linear pistion travel in centimeters.
 */
double
counts_to_cm(long c)
{
    struct mstate   *m = &motor_state;
    return (c * (2.54/TURNS_PER_INCH))/((double)m->counts_per_rev*m->gear_ratio);
}

/**
 * Convert cm. to encoder counts.
 *
 * @param  cm  centimeters
 * @return encoder counts.
 */
long
cm_to_counts(double cm)
{
    struct mstate   *m = &motor_state;
    return (long)((cm*m->counts_per_rev*m->gear_ratio*TURNS_PER_INCH)/2.54);
}

/**
 * Set motor position tracing state.
 * Turn tracing on or off.  When tracing is on, (status == 1), the distance
 * (in counts) between the target position and the current position is
 * continually printed to the console.  Setting status to 0 turns off
 * tracing.
 *
 * @param  status tracing state.
 */
void
motor_trace(int status)
{
    motor_state.trace = status;
}

/**
 * Get motor energy consumption.
 *
 * @return motor energy consumption (in joules) since last reset.
 */
double
motor_energy(void)
{
    return motor_state.joules;
}

/**
 * Get piston speed estimate.
 *
 * @return latest linear speed estimate in counts/msec.
 */
double
motor_speed(void)
{
    return speed;
}

/**
 * Motor overshoot.
 *
 * @return the distance the motor moves during spindown (in counts).
 */
long
motor_overshoot(void)
{
    return motor_state.overshoot;
}

/**
 * Get current motor (piston) position.
 * Find the current piston position.  Read the Transition Counter which
 * maintains the encoder counts since the motor was last started by
 * motor_fwd() or motor_rev() and add that to the last measured position.
 *
 * @returns position in encoder counts.
 */
long
motor_pos(void)
{
    struct mstate  *m = &motor_state;
    long       pos = m->pos;

#ifdef USE_QDEC
    if(m->dir == MOVING_FWD || m->dir == MOVING_REV)
        pos += ENCODER_READ();
#else
    if(m->dir == MOVING_FWD)
        pos += ENCODER_READ();
    else if(m->dir == MOVING_REV)
        pos -= ENCODER_READ();
#endif

    return pos;
}

/**
 * Set motor (piston) position (only for testing).
 *
 * @param  pos  position in encoder counts.
 */
void
motor_set_position(long pos)
{
    struct mstate  *m = &motor_state;

    m->pos = pos;
    m->valid_pos = 1;
}

/**
 * Move the motor forward for a fixed time interval. This operation
 * invalidates the piston position.
 *
 * @param  ms  time interval in milliseconds
 */
void
motor_bump(long ms)
{
    struct mstate  *m = &motor_state;

    motor_fwd();
    DelayMilliSecs(ms);
    motor_stop();
    m->valid_pos = 0;
}

/**
 * Start the motor (piston) moving forward.
 * Start the piston extending.  Motor is first stopped, this also
 * serves to update the position variable.  The Transition Counter is
 * started and will maintain the encoder counts relative to the current
 * position.
 */
void
motor_fwd(void)
{
    struct mstate  *m = &motor_state;

    if(m->dir == MOVING_FWD)
        return;

    motor_stop();
    ENCODER_START();
    m->dir = MOVING_FWD;
    m->t_start = MilliSecs();

    iop_clear(IO_C, MOTOR_REV_MASK);
    iop_set(IO_C, MOTOR_FWD_MASK);
}

/**
 * Start the motor (piston) moving backward.
 * Start the piston retracting.  Motor is first stopped, this also
 * serves to update the position variable.  The Transition Counter is
 * started and will maintain the encoder counts relative to the current
 * position.
 */
void
motor_rev(void)
{
    struct mstate  *m = &motor_state;

    if(m->dir == MOVING_REV)
        return;

    motor_stop();
    ENCODER_START();
    m->dir = MOVING_REV;
    m->t_start = MilliSecs();

    iop_clear(IO_C, MOTOR_FWD_MASK);
    iop_set(IO_C, MOTOR_REV_MASK);
}


/**
 * Stop the motor.
 * Stop the motor and update the position variable.  Motor spindown
 * overshoot is also measured.
 */
void
motor_stop(void)
{
    struct mstate   *m = &motor_state;
    int         i;
    long        p0, p1, stop_point, dx;

    if(m->dir == STOPPED)
        return;

    /*
    ** Stop the motor.
    */
    iop_clear(IO_C, MOTOR_FWD_MASK|MOTOR_REV_MASK);
    stop_point = p1 = ENCODER_READ();

    /*
    ** Wait for the motor to actually stop.
    */
#ifdef MONITOR_STOP
#define POS_BUF_SIZE    500
    {
        int j;
        static unsigned long    buf[POS_BUF_SIZE];

        j = 0;
        while(1)
        {
            p0 = p1;
            for(i = 0;i < 100;i++)
            {
                if(j < POS_BUF_SIZE)
                    buf[j++] = ENCODER_READ();
                LMDelay(9900);
            }
            p1 = ENCODER_READ();
            if(p1 == p0)
                break;
        }

        for(i = 0;i < j;i++)
            printf("%%M %ld\n", buf[i] - stop_point);
    }
#else

    /*
    ** Pulse the motor in the opposite direction for 500ms with a 5% duty
    ** cycle to provide a braking force.
    */
#ifdef USE_MOTOR_BRAKE
    for(i = 0;i < 5;i++)
    {
        if(m->dir == MOVING_FWD)
            iop_set(IO_C, MOTOR_REV_MASK);
        else
            iop_set(IO_C, MOTOR_FWD_MASK);
        DelayMilliSecs(5L);
        iop_clear(IO_C, MOTOR_REV_MASK|MOTOR_FWD_MASK);
        DelayMilliSecs(95L);
    }
#endif

    /*
    ** Check position every 250ms until motor stops.
    */
    do
    {
        p0 = p1;
        DelayMilliSecs(250L);
        p1 = ENCODER_READ();
    } while(p1 != p0);
#endif

    /*
    ** Stop the encoder counter and update the motor position.
    */
    ENCODER_STOP();

    speed = (double)(stop_point - m->pos)/(double)(MilliSecs() - m->t_start);
    speed = ABS(speed);

#ifdef USE_QDEC
    m->pos += p1;
#else
    if(m->dir == MOVING_FWD)
    {
        m->pos += p1;
    }
    else
    {
        m->pos -= p1;
    }
#endif
    dx = p1 - stop_point;
    m->overshoot = ABS(dx);

    m->dir = STOPPED;
}

#ifdef MOTOR_CHECK_CURRENT
/*
 * motor_current - read the current draw.
 *
 * Returns the amount of current being drawn by the motor in A/D
 * counts (0-4095).
 */
static int
motor_current(void)
{
    SELECT_CURRENT_MUX();
    return read_ad(MOTOR_CURRENT_CHAN & 0x07);
}

/*
 * motor_voltage - read motor voltage.
 *
 * Returns the motor battery voltage in A/D counts (0-4095).
 */
static int
motor_voltage(void)
{
    SELECT_VOLTAGE_MUX();
    return read_ad(MOTOR_VOLTAGE_CHAN & 0x07);
}
#endif /* MOTOR_CHECK_CURRENT */

/*
 * Track the motor to the target position by iterating the "goal"
 * function until the goal is achieved -- at which time the goal
 * function will return 0.
 */
static int
track_motor(long (*goalf)(long))
{
    long       result, x, subsamp;
    struct mstate  *m = &motor_state;
    double          I, V;
#ifdef MOTOR_CHECK_CURRENT
    double          watts0 = 0., watts1 = 0.;
#endif

    m->status = MTR_OK;
    subsamp = 0;
#ifdef MOTOR_CHECK_CURRENT
    StopWatchStart();
#endif

    /*
    ** Continue to call the goal function until the goal is
    ** reached.  Goal function returns the "distance" between
    ** the current position and the goal.
    */
    while((result = (goalf)(x = motor_pos())) != 0L)
    {
        if(result < 0L)
            motor_fwd();
        else
            motor_rev();

        PET_WATCHDOG();

        /*
        ** Measure power and integrate over time using the trapezoid rule.
        */
#ifdef MOTOR_CHECK_CURRENT
        if(subsamp == 0)
        {

            watts1 = (I = mV_to_amps(motor_current()))
              * (V = mV_to_volts(motor_voltage()));
            m->joules += (StopWatchTime()/2.) * 1.0e-6 * (watts1 + watts0);
            StopWatchStart();
            watts0 = watts1;

            if(m->trace)
                printf("%%x %ld\n%%P %f,%f\n", x, I, V);

            if(m->update && (*m->update)(x, m->t_ticks))
            {
                m->status = MTR_USER_ABORT;
                break;
            }

            subsamp = ENERGY_SUBSAMP;
        }

        subsamp--;

#endif
    }

    motor_stop();
#ifdef MOTOR_CHECK_CURRENT
    watts1 = mV_to_amps(motor_current()) * mV_to_volts(motor_voltage());
    m->joules += (StopWatchTime()/2.) * 1.0e-6 * (watts1 + watts0);
#endif

    return m->status;
}

/*
 * Goal function for finding the home position.  Since we do not
 * know the absolute position, the input variable represents the
 * position relative to the starting point.
 */
static long
find_home(long position)
{
    struct mstate  *m = &motor_state;
    long       r, dx;
    ulong           dt;
    int             c;

    /*
    ** Check the limit switch.
    */
    if((iop_read(IO_A|REAL_INPUT) & 0x01))
    {
        /*
        ** Switch is on, call the current position HOME.
        */
#ifdef USE_QDEC
        m->home_pos = m->pos + ENCODER_READ();
#else
        m->home_pos = m->pos - ENCODER_READ();
#endif
        return 0L;
    }

    /*
    ** Keep track of when we last moved so stalls
    ** can be detected.
    */
    dx = position - m->last_pos;
    if(ABS(dx) > 0)
    {
        /* The piston has moved */
        dt = m->t_ticks - m->last_move;
        m->last_move += dt;
        m->last_pos = position;
    }

    if((m->t_ticks - m->last_move) > TICKS_PER_SECOND*2L)
    {
        log_error("motor", "Motor stall\n");
        m->status = MTR_STALLED;
        return 0L;
    }


    /*
    ** Check for error conditions and flag the exception.
    */
    r = 1L;

    if(m->t_ticks >= m->t_timeout)
    {
        m->status = MTR_TIMEOUT;
        r = 0L;
    }
#ifdef MOTOR_CHECK_CURRENT
    else if(m->t_ticks > TICKS_PER_SECOND*2L &&
            motor_current() > MAX_CURRENT_ATOD &&
            --m->novercurr <= 0)
    {
        log_error("motor", "Max current exceeded\n");
        m->status = MTR_CURRENT_MAX;
        r = 0L;
    }
#endif
    else if(SerByteAvail())
    {
        c = SerGetByte();
        if(is_stop_char(c))
        {
            m->status = MTR_INTERRUPTED;
            r = 0L;
        }

    }

    return r;
}


/*
 * Goal function to move to an arbitrary target position.  Input
 * variable is the current position.
 */
static long
find_position(long position)
{
    struct mstate  *m = &motor_state;
    long       dx, distance;
    ulong           dt;
    int             c;

    /* Are we there yet? */
    distance = position - m->target;
    if(ABS(distance) < m->epsilon)
        return 0L;

    /*
    ** Keep track of when we last moved so stalls
    ** can be detected.
    */
    dx = position - m->last_pos;
    if(ABS(dx) > 0)
    {
        /* The piston has moved */
        dt = m->t_ticks - m->last_move;
        m->last_move += dt;
        m->last_pos = position;
    }

    if((m->t_ticks - m->last_move) > TICKS_PER_SECOND*2L)
    {
        log_error("motor", "Motor stall\n");
        m->status = MTR_STALLED;
        return 0L;
    }

    /*
    ** Check for error conditions and flag the exception.
    */
    if(m->t_ticks >= m->t_timeout)
    {
        log_event("Motor timeout: position = %ld distance = %ld\n", position,
                  distance);
        m->status = MTR_TIMEOUT;
        distance = 0L;
    }
    else if(SerByteAvail() && SerGetByte() < 0x80)
    {
        c = SerGetByte();
        if(is_stop_char(c))
        {
            m->status = MTR_INTERRUPTED;
            distance = 0L;
        }

    }
    else if((iop_read(IO_A|REAL_INPUT) & 0x01) && !m->ignore_limit)
    {
        if(motor_pos() > m->ballast_min)
        {
            log_error("motor", "Limit switch set at position %ld\n", position);
            m->status = MTR_POS_ERROR;
            distance = 0L;
        }

        if(m->dir == MOVING_REV)
            distance = 0L;
    }
#ifdef MOTOR_CHECK_CURRENT
    else if(m->t_ticks > TICKS_PER_SECOND*2L &&
            motor_current() > MAX_CURRENT_ATOD &&
            --m->novercurr <= 0)
    {
        log_error("motor", "Max current exceeded\n");
        m->status = MTR_CURRENT_MAX;
        distance = 0L;
    }
#endif

    return distance;
}


static void
timer_tick(void)
{
    motor_state.t_ticks++;
}

/**
 * Initialize the motor subsystem.
 *
 */
void
motor_sys_init(void)
{
    /*
    ** Power on the internal A/D mux and the motor
    ** encoder.
    */
    power_on(POS_POWER);
#ifdef MOTOR_CHECK_CURRENT
    power_on(V7_POWER);
    /*
    ** Make PCS1 a general purpose output line and set it high
    ** to prevent activating the external A/D converter.
    */
    _QPDR->PCS1 = SET;
    _QDDR->PCS1 = OUTP;
    _QPAR->PCS1 = CLR;

    /* D5 must be set in order to read the battery voltage */
    iop_set(IO_D, (unsigned char)0x20);
#endif
    DelayMilliSecs(5L);

    /*
    ** Initialize a periodic timer tick which we will use to detect
    ** stalls.
    */
    motor_state.t_ticks = 0;
    alarm_set_ms(1000L/TICKS_PER_SECOND, ALARM_REPEAT, timer_tick);

}

/**
 * Power-down the motor subsystem.
 */
void
motor_sys_shutdown(void)
{
    alarm_shutdown();
    power_off(POS_POWER);
#ifdef MOTOR_CHECK_CURRENT
    iop_clear(IO_D, (unsigned char)0x20);
    iop_clear(IO_D, (unsigned char)0x02);
    power_off(V7_POWER);
#endif
}

#if 0
static void
wait_until_clear(void)
{
    struct mstate  *m = &motor_state;

    motor_fwd();
    while((iop_read(IO_A|REAL_INPUT) & 0x01))
        ;

    m->home_pos = m->pos + ENCODER_READ();

    motor_stop();
}
#endif

/**
 * Move the motor (piston) to the home position.
 *
 * @param  timeout  number of seconds to allow for move.
 * @return the absolute distance, in encoder counts, moved from the starting point.
 */
long
motor_home(long timeout)
{
    struct mstate  *m = &motor_state;
    long       r = 0, start;

    log_event("Begin homing procedure\n");

    motor_sys_init();

    if(iop_read(IO_A|REAL_INPUT) & 0x01)
    {
        m->pos = 0L;
        printf("Limit switch is set, moving motor forward to clear\n");
        motor_fwd();
        DelayMilliSecs(3000L);
        motor_stop();
    }

    if(timeout)
        m->t_timeout = m->t_ticks + timeout*TICKS_PER_SECOND;
    else
        m->t_timeout = 0x7fffffffL;

    start = m->pos;
    /*
    ** Initialize the state structure.
    */
    m->epsilon   = POSITION_ERROR;
    m->last_move = m->t_ticks;
    m->last_pos  = m->pos;
    m->novercurr = 10;
    m->update    = 0;

    /*
    ** Motor can be interrupted by a key press so we must flush
    ** the serial input buffer before starting.
    */
    while(SerByteAvail())
        (void)SerGetByte();

    if((r = (long)track_motor(find_home)) == (long)MTR_OK)
    {
        /*
        ** 'pos' is set to the value at which the piston stopped
        ** moving and is measured relative to the position we
        ** started HOMEing from.  Make the position relative to
        ** 'home_pos' which the (relative) encoder value measured
        ** when the limit switch was activated.
        */
        m->pos -= m->home_pos;
        r = m->home_pos - start;
        m->valid_pos = 1;
        log_event("HOME reached (moved %ld counts, position %ld)\n",
                  r, m->pos);
        r = ABS(r);
    }

    motor_stop();
    motor_sys_shutdown();

    return r;
}

/**
 * Move the piston to a new position.
 * Move the piston to the specified position relative to HOME.  If func is
 * non-NULL, it must point to a function which takes the following arguments:
 * - A long integer containing the shaft position in counts.
 * - An unsigned long integer containing the elapsed time in ticks since
 *   the motor was powered on.
 *
 * The function should return a 0 if the motor motion should continue or
 * a non-zero value if it should be aborted.
 *
 * Return the new position in counts or one of the following negative
 * constants defined in motor.h.
 * - MTR_CURRENT_MAX -  maximum current exceeded.
 * - MTR_TIMEOUT -  timed-out.
 * - MTR_STALLED -  motor not advancing.
 * - MTR_UNKNOWN_POS - motor position unknown (must be HOME'ed first).
 * - MTR_USER_ABORTED - operation aborted by user callback function.
 *
 * @param  target  desired position specified in counts.
 * @param  err  allowed error in final position (counts).
 * @param  timeout  time allowed to accomplish the motion (seconds).
 * @param  func  pointer to a function to be called at every energy calculation.
 * @return new position (counts) or negative error code.
 */

long
motor_move(long target, long err, long timeout,
           int (*func)(long, unsigned long))
{
    struct mstate  *m = &motor_state;
    long            r, diff;

    if(!m->valid_pos)
        return (long)MTR_UNKNOWN_POS;

    diff = target - m->pos;
    if(ABS(diff) < err)
        return m->pos;

    if(target > m->ballast_max)
        target = m->ballast_max;
    else if(target < m->ballast_min)
        target = m->ballast_min;

    motor_sys_init();

    if(timeout)
        m->t_timeout = m->t_ticks + timeout*TICKS_PER_SECOND;
    else
        m->t_timeout = 0x7fffffffL;

    r = 0L;
    /*
    ** Initialize the state structure.
    */
    m->target    = target;
    m->epsilon   = err;
    m->last_move = m->t_ticks;
    m->last_pos  = m->pos;
    m->novercurr = 10;
    m->update    = func;

    speed = 0;

    /*
    ** Motor can be interrupted by a key press so we must flush
    ** the serial input buffer before starting.
    */
    while(SerByteAvail())
        (void)SerGetByte();

    r = (long)track_motor(find_position);

    m->ignore_limit = 0;
    motor_stop();
    motor_sys_shutdown();

    return (r == (long)MTR_OK) ? m->pos : r;
}
