/*
** $Id: nmea.h,v a9fd3739d7f8 2007/12/21 07:01:10 mikek $
*/
#ifndef _NMEA_H_
#define _NMEA_H_

#define NMEA_MAX_LEN		84

#define NMEA_ERR_CSUM		-1
#define NMEA_ERR_TIMEOUT	-2

typedef int (*fsource_t)(void*);
typedef int (*fsink_t)(int, void*);
typedef long (*ftstamp_t)(void);

/**NMEA sentence data structure. */
typedef struct _nmea_sentence {
    short	nfields;		/**< field count */
    long	timestamp;		/**< time sentence was received */
    char	*fields[NMEA_MAX_LEN/2]; /**< field pointers */
    char	buf[NMEA_MAX_LEN];	/**< sentence buffer */
} NMEAsentence;

#define nmea_getfield(n, i)	(n->fields[i])

int nmea_read_next(NMEAsentence *ns, fsource_t getbyte, void *handle, 
	       unsigned timeout, ftstamp_t gettime);
int nmea_read_match(NMEAsentence *ns, char *match, fsource_t getbyte, 
		    void *handle, unsigned timeout, ftstamp_t gettime);
void nmea_write_str(const char *s, fsink_t putbyte, void *handle,
		    int inc_csum);

#endif
