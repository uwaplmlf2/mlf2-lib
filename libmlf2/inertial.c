/*
** $Id: inertial.c,v a8282da34b0f 2007/04/17 20:11:37 mikek $
**
** Interface to the Crossbow 6-axis inertial sensor.
**
*/
#include <stdio.h>
#include <tt8.h>
#include <tt8lib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include "salarm.h"
#include "serial.h"
#include "log.h"
#include "inertial.h"

#ifdef __GNUC__
#define pktcsum(buf, len)\
  ({unsigned char __sum;\
    __asm__("   clr.b     %0\n"\
            "   bra       2f\n"\
            "1: add.b     (%1)+,%0\n"\
            "2: dbf       %3,1b\n"\
            : "=&d" (__sum), "=a" (buf): "1" (buf), "d" (len));\
    __sum;})
#endif

#define WARM_UP_MSECS	1700L
#define PACKET_LEN	18

static int		ref = 0;

static int 		serial_dev;
static long		init_time;
static char		data_type;
static jmp_buf		recover;
static unsigned long	nr_reads, nr_errors;

static unsigned char 	dpacket[PACKET_LEN];


static int
send_expect(char *cmdbuf, int n, int reply)
{
    char	r;

    serial_write(serial_dev, cmdbuf, (size_t)n);
    serial_read(serial_dev, &r, (size_t)1);

    if(r != reply)
    {
	log_error("inertial", "Expected %c, got %c (%x)\n", reply, r,
		  (int)r);
	return 0;
    }

    return 1;
}

static void
catch_alarm(void)
{
    longjmp(recover, 1L);
}
    
/**
 * inertial_init - initialize inertial sensor.
 *
 * Open connection to inertial sensor and initialize.  The device is configured
 * to return the sensor values in scaled engineering units.  Returns 1 if 
 * successful, otherwise 0.
 */
int
inertial_init(void)
{
    int		r = 1;
    char	c[1];

    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(INERTIAL_DEVICE, 0, INERTIAL_BAUD)) < 0)
    {
	log_error("inertial", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    DelayMilliSecs(WARM_UP_MSECS);

initdone:
    ref++;

    /*
    ** Flush any garbage characters on the line.
    */
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    while(serial_read(serial_dev, c, (size_t)1) == 1)
	;
    serial_inmode(serial_dev, SERIAL_BLOCKING);
    
    alarm_set((long)INERTIAL_TIMEOUT, ALARM_ONESHOT, catch_alarm);
    if(setjmp(recover) != 0)
    {
	log_error("inertial", "Timeout initializing sensor\n");
	inertial_shutdown();
	return 0;
    }
  
    if(!send_expect("R", 1, 'H') || !send_expect("c", 1, 'C'))
    {
	inertial_shutdown();
	r = 0;
    }

    alarm_shutdown();
    data_type = 'S';

    init_time = MilliSecs();
    
    return r;
}

/**
 * inertial_shutdown - shutdown the interface.
 *
 * Shutdown the inertial sensor interface.  The device is powered down.
 */
void
inertial_shutdown(void)
{
    if(ref == 0 || --ref > 0)
	return;
    
    serial_close(serial_dev);
}

/**
 * inertial_dev_control - adjust device parameters.
 * @cmd: operation to perform
 *
 * Adjust various sensor parameters, @cmd must be one of the following
 * constants defined in inertial.h.
 *
 * %INERTIAL_CALIBRATE - calibrate and set the zero bias for rate sensors.
 *
 * %INERTIAL_USEVOLTS - data is output as millivolts.
 *
 * %INERTIAL_USESCALED - data is output as scaled engineering values.
 *
 * Returns 1 if sucessful, otherwise 0.
 */
int
inertial_dev_control(int cmd)
{
    char	c[2];
    int		r = 0;

    if(ref <= 0)
	return 0;
    
    alarm_set((long)INERTIAL_TIMEOUT, ALARM_ONESHOT, catch_alarm);
    if(setjmp(recover) != 0)
    {
	log_error("inertial", "Timeout reading sensor\n");
	return 0;
    }

    switch(cmd)
    {
	case INERTIAL_CALIBRATE:
	    c[0] = 'z';
	    c[1] = 100;
	    r = send_expect(c, 2, 'Z');
	    break;
	case INERTIAL_USEVOLTS:
	    c[0] = 'r';
	    if((r = send_expect(c, 1, 'R')) == 1)
		data_type = 'V';
	    break;
	case INERTIAL_USESCALED:
	    c[0] = 'c';
	    if((r = send_expect(c, 1, 'C')) == 1)
		data_type = 'S';
	    break;
    }

    alarm_shutdown();
    
    return r;
}

		
/**
 * inertial_dev_ready - check if device is ready.
 *
 * Returns 1 if device is ready to accept a command, otherwise 0.
 */
int
inertial_dev_ready(void)
{
    return 1;
}

/**
 * inertial_get_stats - fetch read error statistics
 * @errs: returned error count
 * @reads: returned read count.
 */
void
inertial_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}

/**
 * inertial_read_data - read a data record from the inertial sensor
 * @id: pointer to output record
 *
 * Read the next inertial sensor data record.  If the device is configured
 * to return values in scaled engineering units, the following equation
 * may be used to convert to physical units.
 *
 *    Y = (Range * X * 1.5)/32768
 *
 * where Y is the physical value (g's for acceleration and deg/sec for
 * rotation rate, X is the scaled sensor value, and Range is the full
 * range of the sensor in physical units (1 for acceleration and 50
 * for rotation rate).
 *
 * Returns 1 if sucessful, otherwise 0.  Errors are logged.
 */
int
inertial_read_data(InertialData *id)
{
    char		cmd;
    int			n;
    register short	i, sum;
    
    if(ref <= 0)
	return 0;
    
    nr_reads++;
    
    alarm_set((long)INERTIAL_TIMEOUT, ALARM_ONESHOT, catch_alarm);
    if(setjmp(recover) != 0)
    {
	log_error("inertial", "Timeout reading sensor\n");
	nr_errors++;
	return 0;
    }

#ifdef INERTIAL_TIMING
    StopWatchStart();
#endif    
    cmd = 'G';
    serial_write(serial_dev, &cmd, (size_t)1);
    n = serial_read(serial_dev, (char*)dpacket, (size_t)PACKET_LEN);

    
    alarm_shutdown();

    if(n == PACKET_LEN)
    {
	sum = 0;
	for(i = 1;i < n-1;i++)
	    sum += dpacket[i];
	sum &= 0xff;

	if(sum != dpacket[n-1])
	{
	    log_error("inertial", "Packet checksum error %02x != %02x\n",
		      sum, dpacket[n-1]);
	    nr_errors++;
	    return 0;
	}

#ifdef INERTIAL_TIMING
	printf("inertial: read time %lu usecs\n", StopWatchTime());
#endif	
	id->timestamp = MilliSecs() - init_time;

	/*
	** Fill the data structure
	*/
	id->ang_rate.x = (dpacket[1] << 8) | dpacket[2];
	id->ang_rate.y = (dpacket[3] << 8) | dpacket[4];
	id->ang_rate.z = (dpacket[5] << 8) | dpacket[6];
	
	id->accel.x    = (dpacket[7] << 8) | dpacket[8];
	id->accel.y    = (dpacket[9] << 8) | dpacket[10];
	id->accel.z    = (dpacket[11] << 8) | dpacket[12];
	
	id->temp       = (dpacket[13] << 8) | dpacket[14];
	id->t          = (dpacket[15] << 8) | dpacket[16];

	id->type       = data_type;
	
    }
    else
    {
	log_error("inertial", "Bad data packet size, %d\n", n);
	nr_errors++;
	return 0;
    }
    
    return 1;
}
