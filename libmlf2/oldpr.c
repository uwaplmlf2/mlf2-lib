/**@file
** SPI interface to PIC-based pressure-sensor.
**
** This is an interface for the old 1-channel board. It has been rewritten
** to use the new picadc module for the SPI bus interface.
**
** Note that only a single board is supported.
*/
#include <stdio.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8lib.h>
#include <string.h>
#include "nvram.h"
#include "ioports.h"
#include "oldpr.h"
#include "picadc.h"

/*
** This first set of constants is for the 500psi sensors, the
** second is for the 300psi.
*/
#ifdef USE_500
#define PSI_PER_MV  10.0695
#define PSI_OFFSET  -15.904
#else
#define PSI_PER_MV  5.787
#define PSI_OFFSET  -15.0018
#endif

static double psi_per_mv[] = { PSI_PER_MV, PSI_PER_MV };
static double psi_offset[] = { PSI_OFFSET, PSI_OFFSET };

static spi_clocks_t clocks = OLDSPI_SETTINGS;

#define MIN(a, b)   ((a) < (b) ? (a) : (b))


/**
 * Initialize the interface to the sensor.
 *
 * @param  cmd_interval  interval between commands (microseconds)
 * @return 1
 */
int
oldpr_init(long cmd_interval)
{
    nv_value    nv;
    char        name[20];

    clocks.interval = cmd_interval;
    picadc_init(&clocks);

    /*
    ** Lookup the calibration constants in NVRAM.
    */
    strcpy(name, "PIC0:psi/mv");
    if(nv_lookup(name, &nv) != -1)
        psi_per_mv[0] = nv.d;
    strcpy(name, "PIC0:psi-offset");
    if(nv_lookup(name, &nv) != -1)
        psi_offset[0] = nv.d;

    return 1;
}

/**
 * Reinitialize the SPI bus.
 */
void
oldpr_spi_init(void)
{
    picadc_spi_init(&clocks);
}

/**
 * Shutdown the device.
 *
 */
void
oldpr_shutdown(void)
{
    picadc_shutdown();
}

/**
 * Get the buffer sample count.
 *
 * @return the number of samples queued on the PIC.
 */
int
oldpr_get_count(void)
{
    return picadc_get_count();
}

/**
 * Reset the A/D controller.
 *
 */
int
oldpr_adreset(void)
{
    return picadc_old_adreset();
}

/**
 * Check number of missed samples.
 *
 * @return the number of samples missed by the PIC when servicing
 * the last data transfer request.
 */
int
oldpr_overruns(void)
{
    return picadc_overruns();
}

/**
 * Clear the sample buffer.
 *
 * @return 1
 */
int
oldpr_clear(void)
{
    return picadc_old_clear();
}

/**
 * Read the buffered data samples.
 * Read the most recent n samples of data from the PIC pressure sensor.
 * The sample values are in counts, use oldpr_counts_to_mv() to convert
 * to millivolts.  The return value is the actual number of samples read
 * which will be <= n.
 *
 * @param  n  number of samples to read.
 * @param  data  array of returned samples.
 * @return sample count.
 */
int
oldpr_read_data(int n, long data[])
{
    return picadc_old_read_data(n, data);
}

/**
 * Convert counts to millivolts.
 *
 * @param  counts  A/D input value.
 * @return millivolts.
 */
double
oldpr_counts_to_mv(long counts)
{
    double      volts_per_count;

    volts_per_count = OLDPR_VREF/((double)0x1000000 * (double)OLDPR_GAIN);
    return (volts_per_count * counts * 1000.);
}

/**
 * Convert pressure-sensor millivolts to psi
 * Converts millivolts to psi using the calibration constants.  The constants are
 * looked-up in NVRAM by oldpr_init(), if they are not found, compiled in
 * values are used.
 *
 * @param  index  device index, 0 or 1.
 * @param  mv  pressure sensor sample in millivolts.
 * @return psi
 */
double
oldpr_mv_to_psi(double mv)
{
    return mv*psi_per_mv[0] + psi_offset[0];
}
