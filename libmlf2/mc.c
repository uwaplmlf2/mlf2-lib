/**@file
** SPI interface to PIC-based motor controller
**
**
** SPI interface to the PIC microcontroller which manages the motor which adjusts
** the ballast-control piston.
**
**  - CS lines are active high.
**  - CS lines must be low between transfers.
**  - SCLK is active low.
**  - data is changed on the leading edge and captured on the following edge.
**
**
*/
#include <stdio.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8lib.h>
#include <ctype.h>
#include "nvram.h"
#include "power.h"
#include "serial.h"
#include "log.h"
#include "lpsleep.h"
#include "mc.h"

#define MIN(a, b)	((a) < (b) ? (a) : (b))

#define MC_TIMEOUT	4000L
#define SERIAL_EOL	'\n'
#define MC_SERIAL_BAUD	9600L

#define MC_BAUD	100000L
#define MC_CLKDELAY	30L
#define MC_CMDINTERVAL	500L

static
ushort basic_cmd[] = { (M_CONT  & CLR)    /* CS driven by QPDR after transfer */
| (M_BITSE & CLR)	/* always transfer 8 bits */
| (M_DT    & SET)	/* use SPCR1 DTL delay after xfer */
| (M_DSCK  & SET)	/* use SPCR1 DSCKL delay, SCK to PCS */
| (M_CS3   & SET)  	/* PCS3, Max186 A/D, SET == disabled */
| (M_CS2   & CLR) 	/* PCS2, Picadc0, CLR == disabled */
| (M_CS1   & CLR) 	/* PCS1, UNUSED, CLR == disabled */
| (M_CS0   & SET), 	/* PCS0, Motor Controller, SET == enabled */
};


static int serial_dev = -1;

/*
 * Initialize the QSPI interface.
 *
 * @param  baud  serial clock baud rate
 * @param  clkdelay  delay between chip-select valid and clock transition (usecs)
 * @param  cmdinterval  interval between commands (usecs)
 *
 *
 */
static void
qspi_setup(long baud, long clkdelay, long cmdinterval)
{
    int			dsckl, dtl;
    unsigned		rate_div;
    ulong		fclk = SimGetFSys();
    unsigned		cs;
    
    cs = M_PCS0;
    
    /* Convert timing parameters from microseconds */
    dsckl = (ulong)clkdelay*fclk/1000000L;
    dtl = (ulong)cmdinterval*fclk/(32L*1000000L);

    /* 
    ** Output values of GP-I/O pins AND the state of the chip-select lines
    ** between QSPI transfers.  Chip select must remain low between transfers,
    ** except for PCS3 which is used by the MAX186.
    */
    *QPDR = (M_SCK | M_MISO | M_PCS3);
    
    /* QSPI needs MOSI, MISO, and chip-select */
    *QPAR = (M_MOSI | M_MISO | cs);
    /* All chip-selects, MOSI and SCK are outputs */
    *QDDR = M_PCS3 | M_PCS2 | M_PCS1 | M_PCS0 | M_SCK | M_MOSI;


    /* 
    ** Specify baud rate and set Master Mode. Clock is active low, data is
    ** changed on the leading edge and captured on the following edge.
    */
    rate_div = SimGetFSys()/(2*baud);
    *SPCR0 = rate_div | (M_MSTR & SET) | (M_CPOL & SET) | (M_CPHA & SET);

    /* Disable Loop Mode, HALTA interrupts, and Halt */
    *SPCR3 = 0;

    /* Set delays */
    *SPCR1 = ((dsckl & 0x7f) << 8) | (dtl & 0xff);
}

static unsigned long
simple_cmd(int cmdcode, int arg_len, unsigned long arg, int response_len)
{
    int			endqp, i, q_index, response_start;
    unsigned long	retval, mask;
    
    endqp = 0;
    
    SPIXMT[0] = cmdcode;
    SPICMD[0] = basic_cmd[0];
    q_index = 1;
    mask = (unsigned)0xff << ((arg_len-1)*8);
    
    for(i = arg_len-1;i >= 0;i--)
    {
	/* Argument is sent MSB first */
	SPIXMT[q_index] = (arg & mask) >> (i*8);
	SPICMD[q_index] = basic_cmd[0];
	mask >>= 8;
	q_index++;
    }

    response_start = q_index;
    
    for(i = 0;i < response_len;i++)
    {
	SPIXMT[q_index] = 0;
	SPICMD[q_index] = basic_cmd[0];
	q_index++;
    }
    
    endqp = q_index - 1;
    
    *SPCR2 = (M_WREN & CLR) |
      (M_WRTO & CLR) |
      (endqp << 8) | 0x0;

    *SPSR &= ~M_SPIF;	/* clear flag */
    *SPCR1 |= M_SPE;	/* enable QSPI transfer */
    /* wait for pointer to reach ENDQP */
    while(!(*SPSR & M_SPIF))
	;
    /* Response is sent MSB first */
    retval = 0;
    for(i = 0;i < response_len;i++)
	retval = (retval << 8) | (unsigned)SPIRCV[response_start+i];
	
    return retval;
}

static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char	c;

    while(serial_read(desc, (char*)&c, 1L) != 1L)
    {
	if((MilliSecs() - start) >= timeout)
	    return -1;
    }
    return ((int) c)&0xff;
}

static unsigned long
serial_command(int cmdcode, int arg_len, unsigned long arg, int need_response)
{
    long		t0;
    int			c;
    unsigned long	retval = 0;
    char		buf[16];
    
    if(arg_len)
	sprintf(buf, "%c%ld\r", cmdcode, arg);
    else
	sprintf(buf, "%c", cmdcode);
    
    serial_write_zstr(serial_dev, buf);
    if(need_response)
    {
	t0 = MilliSecs();
	while((c = sgetc_timed(serial_dev, t0, MC_TIMEOUT)) != -1 && c != SERIAL_EOL)
	    if(isdigit(c))
		retval = 10*retval + (c - '0');
	if(c == -1)
	    log_error("mc", "Timeout waiting for response\n");
    }
    
    return retval;
}

static void
mc_serial_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
}

int
mc_init(int serial_port)
{
    int  status = 1;

    if(serial_port > 0 && serial_port < 11)
	status = mc_serial_init(serial_port);
    else
	mc_spi_init();
    
    return status;
}

int
mc_serial_init(int port)
{
    if((serial_dev = serial_open(port, 0, MC_SERIAL_BAUD)) < 0)
    {
	log_error("mc", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    DelayMilliSecs(1500L);
    serial_inflush(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    return 1;
}

/**
 * Initialize the SPI interface to the device.
 *
 */
void
mc_spi_init(void)
{
    qspi_setup(MC_BAUD, MC_CLKDELAY, MC_CMDINTERVAL);
}

void
mc_shutdown(void)
{
    if(serial_dev > 0)
    {
	serial_close(serial_dev);
	serial_dev = -1;
    }
}

unsigned long 
mc_send_command(mc_command_t code, unsigned long arg)
{
    int		arg_len = 0, response_len = 0;
    
    switch(code)
    {
	case MC_GET_COUNTER:
	    response_len = 4;
	    break;
	case MC_SET_COUNTER:
	case MC_GOTO:
	    arg_len = 4;
	    break;
	default:
	    arg_len = 0;
	    response_len = 0;
	    break;
    }
    
    if(serial_dev > 0)
	return serial_command(code, arg_len, arg, response_len);
    else
	return simple_cmd(code, arg_len, arg, response_len);
}

unsigned long
mc_get_counter(void)
{
    return serial_dev < 0 ? 0L : mc_send_command(MC_GET_COUNTER, 0);
}
