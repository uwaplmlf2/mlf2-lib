/**@file
** Driver for Transponder/Range-Meter board. 
**
** arch-tag: 4c0d1157-f5f4-4b6a-9192-0ad6b89f5398 SERIAL
**
*/
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "lpsleep.h"
#include "xponder.h"

#ifndef __GNUC__
#define __inline__
#endif

/* Macro to "wake" the device from its quiescent state */
#define WAKEUP()	do {\
	                    sputs(serial_dev, "\r\n");\
                            DelayMilliSecs(50L);\
                            serial_inflush(serial_dev);\
                        } while(0)

#define PURGE_INPUT(d) while(sgetc_timed(d, MilliSecs(), 200L) != -1)

static int 		ref = 0;
static int 		serial_dev;
static unsigned long	nr_reads, nr_errors;
static unsigned long	t_sent = 0L, reply_timeout = 0L;


static __inline__ int sgetc_timed(int desc, ulong start, long timeout)
{
    unsigned char	c;

    while(serial_read(desc, &c, 1L) != 1L)
    {
	if((MilliSecs() - start) >= timeout)
	    return -1;
    }
    return ((int) c)&0xff;
}

static __inline__ void sputc(int desc, int out)
{
    char	c = out;
    
    serial_write(desc, &c, 1);
}

static void sputs(int desc, char *str)
{
    while(*str)
    {
	serial_write(desc, str, 1);
	DelayMilliSecs(10L);
	str++;
    }
}

static int
xp_set_response_pw(long usecs)
{
    char	pwstr[16];
    
    if(usecs < 1536)
	usecs = 1536;
    else if(usecs > 9984)
	usecs = 9984;
    
    sprintf(pwstr, "#PW32%04lx\r\n", usecs);
    
    sputs(serial_dev, "#U\r\n");

    if(!serial_chat(serial_dev, pwstr, "OK\r\n", 2L))
    {
	log_error("xp", "No reply to #PW32 command\n");
	return 0;
    }
    
    return 1;
}

/**
 * Initialize the device interface.
 *
 */
int
xp_init(void)
{
    if(ref > 0)
	goto initdone;
    
    if((serial_dev = serial_open(XP_DEVICE, 0, 
        XP_BAUD)) < 0)
    {
	log_error("xp", "Cannot open serial device (code = %d)\n",
		serial_dev);
	return 0;
    }

    /*
    ** Add a hook function to reinitialize the serial interface after
    ** leaving LP-sleep mode.
    */
#if 0
    add_after_hook("XP-reopen", xp_reopen);
#endif
    /* Give it a little time to startup */
    DelayMilliSecs(250L);
    
    WAKEUP();
    
    if(!serial_chat(serial_dev, "$C1\r\n", "OK\r\n", 2L))
    {
	log_error("xp", "No response from device\n");
	xp_shutdown();
	return 0;
    }

    xp_set_response_pw(1536L);
    
initdone:
    ref++;

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
    
    return 1;
}

void
xp_reopen(void)
{
    serial_reopen(serial_dev);
    serial_inmode(serial_dev, SERIAL_NONBLOCKING);
    serial_inflush(serial_dev);
}

/**
 * Shutdown the device interface.
 * Shutdown the device interface.  The device is powered off.
 */
void
xp_shutdown(void)
{
    if(ref == 0)
	return;
    ref = 0;
#if 0
    remove_after_hook("XP-reopen");
#endif
    serial_close(serial_dev);
}

/**
 * Check if device is ready.
 *
 * @return 1 if device is ready to accept a command, otherwise 0.
 */
int
xp_dev_ready(void)
{
    return ref;
}


/**
 * Fetch read error statistics.
 *
 * @param  errs  returned error count
 * @param  reads  returned read count.
 */
void
xp_get_stats(unsigned long *errs, unsigned long *reads)
{
    *errs = nr_errors;
    *reads = nr_reads;
}

/**
 * Transmit a ranging ping and listen for a reply.
 * 
 * @param  f_xmit  transmit frequency in khz (8.0 - 16.0).
 * @param  f_recv  receive frequency in khz (8.0 - 16.0).
 * @param  pw  pulse width in milliseconds (1 - 20).
 * @param  timeo  reply timeout in milliseconds.
 * @return 1 if successful, 0 on error.
 */
int
xp_ping(float f_xmit, float f_recv, int pw, long timeo)
{
    char	cmd[20];

    WAKEUP();
    
    sprintf(cmd, "$M%05ld\r\n", timeo);
    if(!serial_chat(serial_dev, cmd, "OK\r\n", 2L))
    {
	log_error("xp", "Error sending command: %s", cmd);
	return 0;
    }

    sprintf(cmd, "$P%02d\r\n", pw);
    if(!serial_chat(serial_dev, cmd, "OK\r\n", 2L))
    {
	log_error("xp", "Error sending command: %s", cmd);
	return 0;
    }
    
    sprintf(cmd, "$X%04.1f,%04.1f,00\r\n", f_xmit, f_recv);
    sputs(serial_dev, cmd);
    
    t_sent = MilliSecs();
    reply_timeout = timeo;
    
    return 1;
}


/**
 * Check the status of a response to the most recent ping.
 *
 * @return 1 if reply is ready, otherwise 0.
 */
int
xp_response_ready(void)
{
    return ((MilliSecs() - t_sent) > reply_timeout);
}

/**
 * Return the range from the most recent ping.
 *
 * @return travel time in 0.1 millisecond ticks or -1 for timeout.
 */
long
xp_range(void)
{
    long		rtt;
    unsigned long	t0;
    int			c;

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    WAKEUP();
    PURGE_INPUT(serial_dev);
    
    sputs(serial_dev, "$X\r\n");
    rtt = 0;
    t0 = MilliSecs();
    while((c = sgetc_timed(serial_dev, t0, 3000L)) != -1)
    {
	if(c == '-')
	{
	    serial_inflush(serial_dev);
	    return -1;
	}
	
	if(c == '\n')
	    break;

	if(isdigit(c))
	    rtt = rtt*10 + (c - '0');
	    
    }
    
    return rtt;
}

/**
 * Listen for ranging pings and send a response.
 *
 * @param  f_xmit  response frequency in khz (8.0 - 16.0)
 * @param  f_recv  frequency to listen for in khz (8.0 - 16.0)
 * @param  pw  response pulse width in milliseconds (1.5 - 10)
 * @return 1 if successful, 0 on error.
 */
int
xp_listen(float f_xmit, float f_recv, float pw)
{
    char	cmd[20];

    WAKEUP();
    xp_set_response_pw((long)(pw*1000));
    
    sprintf(cmd, "$T%04.1f,%04.1f\r\n", f_recv, f_xmit);
    if(!serial_chat(serial_dev, cmd, "OK\r\n", 2L))
    {
	log_error("xp", "Error sending command: %s", cmd);
	return 0;
    }
    
    return 1;
}

/**
 * Return a count of the number of replies sent.
 */
long
xp_replies(void)
{
    long		count;
    unsigned long	t0;
    int			c;

    serial_inmode(serial_dev, SERIAL_NONBLOCKING);

    WAKEUP();
    sputs(serial_dev, "$Q\r\n");
    count = 0;
    t0 = MilliSecs();
    while((c = sgetc_timed(serial_dev, t0, 1500L)) != -1)
    {
#ifdef DEBUGME
	if(isprint(c))
	    printf("c = %c\n", c);
	else
	    printf("c = 0x%02x\n", c);
#endif	
	if(c == '\r' || c == '\n')
	    break;
	
	if(isdigit(c))
	    count = count*10 + (c - '0');
	    
    }
    
    return count;
}


/**
 * Pass through mode.
 *
 * Allows user to interact directly with the device by passing all 
 * console input to the device and all device output to the console.
 */
void
xp_test(void)
{
    printf("Pass-through mode: CTRL-c to exit\n");
    serial_passthru(serial_dev, 0x03, 0x02);
}
