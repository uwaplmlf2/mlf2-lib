/*
** $Id: alt.h,v a8282da34b0f 2007/04/17 20:11:37 mikek $
*/
#ifndef _ALT_H_
#define _ALT_H_

#define ALT_DEVICE	3
#define ALT_BAUD	9600L
#define ALT_TIMEOUT	2L

int alt_init(void);
void alt_shutdown(void);
int alt_read_data(void);
int alt_dev_ready(void);
void alt_get_stats(unsigned long *errs, unsigned long *reads);
#endif
