/*
** arch-tag: power switch codes
** Time-stamp: <2004-11-22 14:38:35 mike>
*/
#ifndef _POWER_H_
#define _POWER_H_

#define V7_POWER	ANALOG_POWER

enum {SERDEV_POWER1=0,
      SERDEV_POWER2,
      SERDEV_POWER3,
      SERDEV_POWER4,
      SERDEV_POWER5,
      SERDEV_POWER6,
      SERDEV_POWER7,
      SERDEV_POWER8,
      SERDEV_POWER9,
      SERDEV_POWER10,
      GPS_POWER,
      COMM_POWER,
      POS_POWER,
      QSPIDEV_POWER1,
      QSPIDEV_POWER2,
      QSPIDEV_POWER3,
      ANALOG_POWER,
      ALGDEV_POWER1,
      ALGDEV_POWER2,
      ALGDEV_POWER3,
      HIGHV_POWER,
      LED_POWER,
      USER_POWER,
      ARGOS_POWER,
      NSWITCHES};

void power_on(int n);
void power_off(int n);
unsigned long power_on_time(int n);

#endif
