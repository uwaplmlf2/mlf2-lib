/*
** $Id: serial.h,v e2c9c047a753 2007/11/07 19:36:02 mikek $
*/
#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <stddef.h>

#define SERIAL_EBADNUM  -1
#define SERIAL_EMINOR   -2
#define SERIAL_EBUSY    -3
#define SERIAL_EOPEN    -4
#define SERIAL_EINIT    -5

#define SERIAL_BLOCKING     1
#define SERIAL_NONBLOCKING  2

#define SERIAL_NDEVS    12

int serial_open(int major, int minor, long baud);
void serial_close(int desc);
int serial_read(int desc, char *buf, size_t n);
int serial_write(int desc, const char *buf, size_t n);
int serial_write_zstr(int desc, const char *buf);
int serial_inmode(int desc, int mode);
long serial_speed(int desc, long baud);
void serial_passthru(int desc, int stopchar, int breakchar);
void serial_break(int desc);
int serial_reopen(int desc);
void serial_inflush(int desc);
int serial_chat(int desc, const char *send, const char *expect, long timeout);
int serial_chat_bin(int desc, const char *send, int ns, const char *expect,
            int nr, long timeout);
int serial_chat_bin_multi(int desc, const char *send, int ns,
              const char **expect,  int *nr, long timeout);
void serial_chat_response(char *sbuf, size_t n);
void serial_chat_debug(int state);
void serial_reset_speed(void);

#endif
