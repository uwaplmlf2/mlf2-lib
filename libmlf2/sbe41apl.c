/**@file
 *
 * Driver for the old APL version of the SBE-41. This driver works with the sbe driver
 * and will replace the newctd driver.
 */
#include <stdio.h>
#include <time.h>
#include <string.h>
#define __C_MACROS__
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include "serial.h"
#include "log.h"
#include "sbe.h"
#include "sbe41apl.h"

#ifndef __GNUC__
#define __inline__
#endif

#include "iofuncs.h"

#define MAX_DEVS    2
#define SAMPLE_TIME_MS          3000L
#define SLEEP_DELAY_MS          500L

// Maintain the state of each device
typedef struct devstate {
    int         sd; /**< serial device descriptor */
    int         ref; /**< reference count */
    int         profile_active; /**< in profile mode */
    long        t_sample; /**< time of last sample */
    long        t_sleep; /**< time of the last sleep */
} devstate_t;
static devstate_t dstab[MAX_DEVS];

typedef struct {
    float   p;
    float   t;
    float   s;
} PTS_t;

/*
 * Various response parsing functions used as call-backs
 * to sbe_command
 */
static CBstatus_t
parse_ts(void *obj, char *linebuf)
{
    PTS_t *psample = (PTS_t*)obj;
    if(sscanf(linebuf, "%f, %f", &psample->t, &psample->s) != 2)
    {
        log_error("sbe41apl", "Error parsing TS data: (%s)\n", linebuf);
        return CB_ERROR;
    }

    return CB_OK;
}

/**
 * sbe41apl_init - initialize the interface.
 */
int
sbe41apl_init(int which)
{
    devstate_t  *dsp;

    dsp = &dstab[which];
    if(dsp->ref > 0)
        return 1;

    if((dsp->sd = sbe_init(SBE41APL_DEVICE(which), SBE41APL_BAUD)) < 0)
        return 0;

    DelayMilliSecs(2000L);
    if(sbe_command(dsp->sd, "tswait=2\r", 3000L, NULL, NULL) == 0)
        log_error("sbe41apl", "No response to \"tswait\" command\n");

    dsp->ref = 1;
    dsp->t_sleep = MilliSecs();

    return 1;
}

/**
 * sbe41apl_shutdown - shutdown the device interface.
 *
 * Shutdown the device interface.  The device is powered off.
 */
void
sbe41apl_shutdown(int which)
{
    if(dstab[which].ref == 0)
        return;
    dstab[which].ref = 0;
    sbe_shutdown(dstab[which].sd);
}

/**
 * sbe_dev_ready - check if the device is ready for commands.
 *
 * @return 1 (yes) or 0 (no)
 */
int
sbe41apl_dev_ready(int which)
{
    if(dstab[which].ref)
        return sbe_dev_ready(dstab[which].sd);
    else
        return 0;
}

/**
 * sbe41apl_test - pass through mode.
 *
 * Allows user to interact directly with the device by passing all
 * console input to the device and all device output to the console.
 *
 */
void
sbe41apl_test(int which)
{
    if(dstab[which].ref)
        sbe_test(dstab[which].sd);
}

static void
msdelay(long ms)
{
    if(ms <= 0L)
        return;
    DelayMilliSecs(ms);
}

/**
 * sbe41apl_sample - read a single CTD sample
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @param p         pressure in dbars
 * @param *ctd      if non-NULL will contain returned temperature and salinity
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41apl_sample(int which, long timeout, float p, NewCTDdata *ctd)
{
    PTS_t       pts;
    int         rval;
    devstate_t  *dsp = &dstab[which];
    char        cmd[12];

    rval = 0;

    if(ctd != NULL)
    {
        sprintf(cmd, "%05dtt\r", (int)(p * 10));
        memset(&pts, 0, sizeof(pts));
        // We need to delay a bit before sending another command
        // if the CTD is sleeping
        msdelay(SLEEP_DELAY_MS - (MilliSecs() - dsp->t_sleep));
        rval = sbe_command(dsp->sd, cmd, timeout, parse_ts, &pts);
        if(rval)
        {
            ctd->t = pts.t;
            ctd->s = pts.s;
        }
        dsp->t_sleep = MilliSecs();
    }

    return rval;
}

/**
 * sbe41apl_sample_hold - start acquisition of a single CTD sample
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @param p         pressure in dbars
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41apl_sample_hold(int which, long timeout, float p)
{
    devstate_t  *dsp = &dstab[which];
    char        cmd[12];

    sprintf(cmd, "%05dts\r", (int)(p * 10));
    // We need to delay a bit before sending another command
    // if the CTD is sleeping
    msdelay(SLEEP_DELAY_MS - (MilliSecs() - dsp->t_sleep));
    if(sbe_command(dsp->sd, cmd, timeout, NULL, NULL) == 0)
        return 0;
    dsp->t_sample = MilliSecs();
    return 1;
}

/**
 * sbe41apl_data_ready - test whether sampling process is done.
 *
 * @param  which  device index, 0 or 1.
 * @return true if sampling process is done.
 */
int
sbe41apl_data_ready(int which)
{
    devstate_t  *dsp = &dstab[which];
    return (MilliSecs() - dsp->t_sample) >= SAMPLE_TIME_MS;
}


/**
 * sbe41apl_read - read the most recent sample started by sbe41apl_sample_hold
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @param *ctd      if non-NULL will contain returned temperature and salinity
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41apl_read(int which, long timeout, NewCTDdata *ctd)
{
    PTS_t       pts;
    int         rval;
    devstate_t  *dsp = &dstab[which];

    rval = 0;

    if(ctd != NULL)
    {
        memset(&pts, 0, sizeof(pts));
        rval = sbe_command(dsp->sd, "ss\r", timeout, parse_ts, &pts);
        if(rval)
        {
            ctd->t = pts.t;
            ctd->s = pts.s;
        }
        dsp->t_sleep = MilliSecs();
    }

    return rval;
}

/**
 * sbe41apl_start_pump - run the pump.
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41apl_start_pump(int which, long timeout)
{
    devstate_t  *dsp = &dstab[which];

    return sbe_command(dsp->sd, "pumpon\r", timeout, NULL, NULL);
}

/**
 * sbe41apl_stop_pump - turn off the pump
 *
 * @param which     device index, 0 or 1
 * @param timeout   reply timeout in milliseconds
 * @return 1 if successful, 0 on error or timeout
 */
int
sbe41apl_stop_pump(int which, long timeout)
{
    devstate_t  *dsp = &dstab[which];

    return sbe_command(dsp->sd, "pumpoff\r", timeout, NULL, NULL);
}
