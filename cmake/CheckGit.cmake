#
# Gather git info at build time.
#
# See https://jonathanhamberg.com/post/cmake-embedding-git-hash/
#
include_guard()

set(CURRENT_LIST_DIR ${CMAKE_CURRENT_LIST_DIR})
if (NOT DEFINED pre_configure_dir)
    set(pre_configure_dir ${CMAKE_CURRENT_LIST_DIR})
endif ()

if (NOT DEFINED post_configure_dir)
    set(post_configure_dir ${CMAKE_BINARY_DIR}/generated)
endif ()

set(pre_configure_file ${pre_configure_dir}/libversion.h.in)
set(post_configure_file ${post_configure_dir}/libversion.h)

function(CheckGitWrite val)
  string(REPLACE ";" "\n" val "${val}")
  file(WRITE ${CMAKE_BINARY_DIR}/git-state.txt ${val})
endfunction()

function(CheckGitRead val)
    if (EXISTS ${CMAKE_BINARY_DIR}/git-state.txt)
        file(STRINGS ${CMAKE_BINARY_DIR}/git-state.txt CONTENT)
        set(${val} ${CONTENT} PARENT_SCOPE)
    endif ()
endfunction()

function(CheckGitVersion)
    # Get the latest commit hash of the working branch
    execute_process(
        COMMAND git log -1 --format=%H
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
        OUTPUT_VARIABLE GIT_HASH
        OUTPUT_STRIP_TRAILING_WHITESPACE)

    # Check if the working tree has uncommitted changes
    execute_process(
        COMMAND git diff --quiet
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
        OUTPUT_QUIET
        ERROR_QUIET
        RESULT_VARIABLE GIT_IS_DIRTY)

    set(GIT_STATE "${GIT_HASH};${GIT_IS_DIRTY}")

    CheckGitRead(GIT_STATE_CACHE)
    if (NOT EXISTS ${post_configure_dir})
        file(MAKE_DIRECTORY ${post_configure_dir})
    endif ()

    if (NOT DEFINED GIT_STATE_CACHE)
        set(GIT_STATE_CACHE "INVALID")
    endif ()

    # Only update the header file if the hash has changed. This will
    # prevent us from rebuilding the project more than we need to.
    if (NOT "${GIT_STATE}" STREQUAL "${GIT_STATE_CACHE}" OR NOT EXISTS ${post_configure_file})
        # Set the GIT_STATE_CACHE variable the next build won't have
        # to regenerate the source file.
        CheckGitWrite("${GIT_STATE}")
        list(GET GIT_STATE 0 GIT_HASH)
        list(GET GIT_STATE 1 GIT_IS_DIRTY)
        string(SUBSTRING ${GIT_HASH} 0 8 GIT_SHORT_COMMIT)
        configure_file(${pre_configure_file} ${post_configure_file} @ONLY)
    endif ()

endfunction()

function(CheckGitSetup tgt)

    add_custom_target(AlwaysCheckGit COMMAND ${CMAKE_COMMAND}
        -DRUN_CHECK_GIT_VERSION=1
        -Dpre_configure_dir=${pre_configure_dir}
        -Dpost_configure_file=${post_configure_dir}
        -DGIT_STATE_CACHE="${GIT_STATE_CACHE}"
        -P ${CURRENT_LIST_DIR}/CheckGit.cmake
        BYPRODUCTS ${post_configure_file}
        )

    target_include_directories(${tgt} PUBLIC ${CMAKE_BINARY_DIR}/generated)
    add_dependencies(${tgt} AlwaysCheckGit)

    CheckGitVersion()
endfunction()

# This is used to run this function from an external cmake process.
if (RUN_CHECK_GIT_VERSION)
    CheckGitVersion()
endif ()
