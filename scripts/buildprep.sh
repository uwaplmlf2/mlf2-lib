#!/usr/bin/env bash
#
# Prepare the build directory and run the configuration phase
#

set -e
type cmake >&2

mkdir -p build/prebuilt/mlf2

# Download tt8 library
curl -sS -L -X GET "https://bitbucket.org/uwaplmlf2/tt8-lib/downloads/tt8-lib_latest.tar.gz" |\
    tar -x -z -f - -C build/prebuilt/mlf2
cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=cmake/mlf2gcc.cmake

echo "Run 'cmake --build build' to build firmware"
