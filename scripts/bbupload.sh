#!/usr/bin/env bash

if [[ -z "$1" ]]; then
    echo "Missing filename" 1>&2
    exit 1
fi

if [[ -z $BITBUCKET_USERNAME ]] || [[ -z $BITBUCKET_APP_PASSWORD ]]; then
    echo "Missing upload credentials" 1>&2
    exit 1
fi

set -e

BB_AUTH_STRING="${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}"

if [[ -z $BITBUCKET_REPO_SLUG ]]; then
    remote=$(git remote get-url origin)
    path=$(cut -f2 -d: <<< "$remote")
    BITBUCKET_WORKSPACE="${path%/*}"
    slug="${path##*/}"
    BITBUCKET_REPO_SLUG="${slug%.*}"
fi

url="https://api.bitbucket.org/2.0/repositories/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/downloads"

for arg; do
    curl -s -S -X POST --user "${BB_AUTH_STRING}" "$url" --form files=@"$arg"
done
